A list of all good Matlab Repositories made by others

Add Recursive Paths: 
addpath(genpath('c:\toolbox')); savepath;

To Install All:
addpath(genpath(pwd)); savepath;


Contents
========

DistinguishableColours
    DEPENDENCIES: none
    COMPILE:      none
    PATHS:        distinguishable_colors
    DESCRIPTION:  call distinguishable_colors(n) for [nx3] matrix of maximally distinguishable colors

drtoolbox
    DESCRIPTION: Dimensionality Reduction Toolbox. Various ways to reduce dimensions.    

KSvd
    DEPENDENCIES: OrthogonalMatchingPursuit
    COMPILE:      /private/ --> >>make
    PATHS:        Parent Folder
    DESCRIPTION:  Algorithm for K-Singular Value Decomposition

LibLinear_1.94
    DEPENDENCIES: none
    COMPILE:      /matlab/make.m
    PATHS:        Parent Folder
    DESCRIPTION:  Linear Support Vector Machine

LibSvm_3.18
    DEPENDENCIES: none
    COMPILE:      /matlab/make.m
    PATHS:        Parent Folder
    DESCRIPTION:  Support Vector Machine

OrthogonalMatchingPursuit
    DEPENDENCIES: none
    COMPILE:      /private/ --> >>make
    PATHS:        Parent Folder
    DESCRIPTION:  Algorithm for Orthogonal Matching Pursuit

PiotrToolbox
    DEPENDENCIES: none
    COMPILE:      >>external/toolboxCompile.m
    PATHS:        Parent and Subfolders
    DESCRIPTION:  Includes various computer vision algorithms

RandomForestStochasticBosque
    DEPENDENCIES: none
    COMPILE:      >>cartree/mx_files/mx_compile_cartree.m
    PATHS:        Parent and Subfolders
    DESCRIPTION:  Random Forest Classifier

ViewDetectionLeonid
    DEPENDENCIES: VlFeat, Armadillo, Datasets: EPFL Cars, 3D Objects, Pointing ’04
    COMPILE:      see read me
    PATHS:        Parent and Subfolders
    DESCRIPTION:  Multi-view Pose Paramaterization. Leonid Segal’s code.

vl_feat
    DEPENDENCIES: None
    COMPILE:      /toolbox/vl_setup
    PATHS:        Parent and Subfolders
    DESCRIPTION:  Lots of algorithms.

