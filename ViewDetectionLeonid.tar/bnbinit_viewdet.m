function [ybar, viobar, mbar, phibar, phi, ygt] = bnbinit_viewdet(samples, params, varargin)
global ids xtrain ytrain
% note samples are used to index into ids.train
% i.e. each "sample" is the index for a training example
% NOT the absolute index in whole dataset!
nsample = length(samples);

%% load integral histograms and feature info
tic; disp('loading integral histograms + feature grids');
for i = 1:length(ids.train)
	idx = ids.train(i);
	if isempty(xtrain.IHfn{i})
		fprintf('H');
		xtrain.IHfn{i} = sprintf('%s%s_%s.mat', params.localdir, ids.name{idx}, params.IHstr);
	end
end
toc;

%% compute ground truth features
step = uint32(params.gridstep);
nlv  = uint32(params.pyralvs);
vps  = (params.viewpoints-1) * 2*pi/length(params.viewpoints);

disp('computing ground truth features');
for i = samples
	fprintf('\n[#%4d: %s] ', i, ids.name{ids.train(i)});

	if isempty(xtrain.gtft{i})
		ld = load(xtrain.IHfn{i});
		ihist = double(ld.inthist);
		xx = uint32(ld.xgrid);
		yy = uint32(ld.ygrid);
		wh = uint32(ld.imwh);

		[~, ~, ~, fv, ~] = ess_posedet(...
			params.Kx, ...    % kernel types
			ihist, nlv, ...   % integral histogram & SP levels
			wh, xx, yy, ...   % image dimensions, feature grid
			[], vps, ...      % bb sizes + vps to search for
			ytrain(:, i), ... % ground truths
			[], []);          % classifiers

		xtrain.gtft{i} = fv;
	end
end

%% construct first cutting plane (just sample some)
viobar = zeros(nsample, 1);
mbar   = zeros(nsample, 1);
ybar   = zeros(5, nsample);
ygt    = zeros(5, nsample);
phibar = zeros(params.ftdim, nsample);
phi    = zeros(params.ftdim, nsample);
tic; 
disp('doing sample_a_box');
for i = 1:nsample
	idx = samples(i);
	fprintf('\n[#%4d: %s] ', i, ids.name{ids.train(idx)});

	ld = load(xtrain.IHfn{idx});
	ihist = double(ld.inthist);
	xx = uint32(ld.xgrid);
	yy = uint32(ld.ygrid);
	wh = uint32(ld.imwh);
	bbsize = uint32(randi([50 100],2,1));

	[y, v, m, f, ~] = ess_posedet(...
		params.Kx, ...      % kernel types
		ihist, nlv, ...     % integral histogram & SP levels
		wh, xx, yy, ...     % image dimensions, feature grid
		bbsize, vps, ...    % bb sizes + vps to search for
		ytrain(:, idx), ... % ground truths
		[], []);            % classifiers

	viobar(i)    = v;
	ybar(:, i)   = y;
	mbar(i)      = m;
	phibar(:, i) = f; 
	phi(:, i)    = xtrain.gtft{idx};
	ygt(:, i)    = ytrain(:, idx);
end
toc;

%% clear temp vars
clear hists fts

