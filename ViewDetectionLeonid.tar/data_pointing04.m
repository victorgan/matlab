function [ndata, inst, name, imfn, bbvp] = data_pointing04(params)

%% gather training data
ndata = 0; inst = []; name = []; imfn = []; bbvp = [];
for person = 1:15 
	disp(['person ' num2str(person)]);
	for seq = 1:2
		for i = 0:92
			pitch = params.viewpoints(i+1, 1);
			yaw = params.viewpoints(i+1, 2);
			ndata = ndata + 1;
			inst(ndata) = person;
			name{ndata} = sprintf('person%02d%d%02d%+d%+d', person, seq, i, pitch, yaw);
			imfn{ndata} = sprintf('%sPerson%02d/%s.jpg', params.imdir, person, name{ndata});
			gtfn = sprintf('%sPerson%02d/%s.txt', params.imdir, person, name{ndata});
			bbvp(ndata, :) = [readgt_pointing04(gtfn), pitch, yaw];
		end
	end
end


function Y = readgt_pointing04(gtfn)
fd = fopen(gtfn, 'r');
ss = fscanf(fd, '%s\n%s\n', [1 2]);
bb = fscanf(fd, '%d', [1 4]);
fclose(fd);
w2 = round(bb(3)/2);
h2 = round(bb(4)/2);
Y  = [bb(1)-w2, bb(2)-h2, bb(1)+w2, bb(2)+h2];

