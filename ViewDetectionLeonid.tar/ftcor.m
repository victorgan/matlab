function ftcor(cls, ftype, nvw, nlv, Kx, params) 
if nargin < 6
	dofitting = 0;
else 
	dofitting = 1;
end
global ids xtrain ytrain
sig   = 1;
alph  = 1;
split = 1;
nstd  = 5;
if strcmpi(Kx, 'nlin'), Kx = 'nlin2'; end
if ~strcmpi('epfl', cls) && ~strcmpi('epflcars', cls)
	params = init_viewdet('3dobj', cls, ...
		ftype, nvw, nlv, Kx, sig, alph, 5, 8, split);
else
	cls = 'epflcars';
	params = init_viewdet(cls, cls, ...
		ftype, nvw, nlv, Kx, sig, alph, 3, 16, split);
end

% load data
okfn = sprintf('%s/%s_std%d_%s.ok', params.modeldir, cls, params.nstdbb, params.IHstr);
if ~exist(okfn, 'file')
	data_viewdet(params);
	system(sprintf('touch %s\n', okfn), '-echo');
end
disp('loading data');
[ids, xtrain, ytrain, xtest, ytest] = loaddata_viewdet(cls, params, 0);
ntrain = length(ids.train)

% compute ground truth features
disp('computing ground truth features'); tic;
gtft = zeros(params.ftdim, ntrain);
step = uint32(params.gridstep);
nlv  = uint32(params.pyralvs);
vps  = (params.viewpoints-1)*2*pi/length(params.viewpoints);
parfor i = 1:ntrain
	idx = ids.train(i);
	fprintf('\n[No.%d: %s] ', i, ids.name{idx});
	IHfn = sprintf('%s%s_%s.mat', params.localdir, ids.name{idx}, params.IHstr);
	ld = load(IHfn);
	ihist = double(ld.inthist);
	xx = uint32(ld.xgrid);
	yy = uint32(ld.ygrid);
	wh = uint32(ld.imwh);
	[~, ~, ~, F, ~] = ess_viewdet(...
		params.Kx, ...  % kernel type & spatial pyamid levels   
		ihist, nlv, ...      % integral histogram & feature grid step
		wh, xx, yy, ...            % image dimensions, feature grid
		[], vps, ...               % bb sizes + vps to search for
		ytrain(:, i), ...          % ground truths
		[], []);                   % misc parameters
	gtft(:, i) = F;
end
toc;

% gram matrix
switch (params.Kx) 
	case {'linear', 'lin', 'nlin', 'nlin1', 'nlin2'}
		% linear
		K = gtft' * gtft; 

	case {'hell', 'nhell'}
		% hellinger kenel
		K = sqrt(gtft)' * sqrt(gtft);
end

% correspondence
vgt = ytrain(5, :);
dvs = abs( repmat(vgt', [1 ntrain]) - repmat(vgt, [ntrain 1]) );
dvs = min(dvs, 2*pi-dvs);
dvs = dvs(itriu(ntrain));
cor = K(itriu(ntrain));

% bin them, and display
nbins = length(params.viewpoints) + 1;
[n, cen] = hist(dvs, nbins);
dis = repmat(dvs, [1 nbins]) - repmat(cen, size(dvs));
[val, idx] = min(abs(dis), [], 2);
c = zeros(1, nbins);
for i = 1:nbins
	c(i) = mean(cor(idx == i));
	fprintf('bin%2d center: %.3f, mean cor %.3f\n', i, cen(i), c(i));
end
pi_confusion = (c(1)-c(end))/c(1)

%clf; %subplot(211); 
bar(cen*180/pi, c); grid on; hold all;
title(sprintf('%s %s--%d%s: PI confusion = %.3f\n', cls, ...
	strrep(params.IHstr, '_', '-'), nlv, Kx, pi_confusion));
if ~dofitting
	hold off;
	return;
end

% fit gaussian or polynomial

%Ker = params.Ky;
Ker = 'gauss';
if strcmpi(Ker, 'gauss') 
	inum = find(~isnan(c));
	c = c(inum);
	cen = cen(inum);
	for i = 2:nbins
		if c(i) > c(i-1), break; end
	end
	hold off; bar(cen*180/pi, c); grid on; hold all;
	fprintf('fitting Gaussian using dvp < %.3f\n', cen(i));

	if strcmpi(cls, 'epflcars')
		gx = cen(1:i-1)'-cen(1);
		gy = c(1:i-1)';
		gb = 0;
		gs = c(1);
		gy = (gy-gb)/(gs-gb);
		% If we have curve fitting toolbox
		% F = fit([gx; -gx], [gy; gy], 'gauss1')
		[sigma, mu, A] = mygaussfit([gx;-gx], [gy;gy])
	else
		mu = 0;
		sigma = sqrt(-(pi/4)^2/2/log(c(2)/c(1)));
		gb = 0;
		gs = c(1);
	end

	keyboard

	xx = 0:0.01:pi;
	plot(xx*180/pi, gb+(gs-gb)*exp(-(xx-mu).^2/(2*sigma^2)), 'r', 'linewidth', 2);
	hold off;

	keyboard

	data = sigma;
	save(params.kerfn, 'data');

elseif strcmpi(Ker, 'poly')
	data = polyfit(dvs, cor, 4)
	xx = 0:0.01:pi;
	plot(xx, polyval(data, xx), 'r', 'linewidth', 2);
	hold off;
	% make sure 0 evaluates to 1
	data = data/polyval(data, 0);
	save(params.kerfn, 'data');

else
	error(['unkonwn Ky ' Ker]);
end

