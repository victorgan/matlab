
function model = sampledcuts_npass(ntrain, nsample, para, MVC_init, MVC_1slack, ...
	Verb, topk, npass, modelpath)
%
% Training structural SVM with sampled cuts, n-slack version
% In each iteration, compute the cutting plane on a sampled subset
% Default: scan the training set for 2 passes
%
if nargin < 6, Verb = 1; end
if nargin < 7 || isempty(topk), topk = 3; end
if nargin < 8 || isempty(npass), npass = 2; end
if nargin < 9, modelpath = []; end
if Verb, fprintf('-------- sampledcuts_2pass --------\n'); end
assert(ntrain > 2*nsample);

%% paramters
maxidle = 7;
nms_th  = 0.36;
TolRel  = 0.001;
TolVio  = 0.01;
C       = para.svm_C / ntrain;

%% init
t0     = tic;
idinit = get_bootstrap(0, 'uniform', ntrain, nsample); 
[Ycp, V, Mcp, PSIcp, PSIgt, Ygt] = MVC_init(idinit, para);
wset   = cell(1, ntrain);  % working set for each example
revind = [];  % reverse index for constraints
ncp    = 0;
for i = idinit
	ncp = ncp + 1;
	wset{i} = [wset{i} ncp];
	revind  = [revind i];
end
H = expand_gram_matrix([], ncp, PSIgt, Ygt, PSIcp, Ycp, para);

% it's OK to use Matlab for first QP
% todo: use libqp for this
opts = optimset('algorithm', 'interior-point-convex', 'display', 'off');
[alph, negdual, flg] = quadprog(...
	H, -Mcp, ...  % problem data
	[], [], ...   % constraint Ax<=b
	[], [], ...   % constraint Ax==b
	zeros(ncp,1), ...   % LB
	C*ones(ncp,1), ...  % UB
	zeros(ncp, 1), ...  % initial solution
	opts);
idle  = zeros(ncp, 1);
xi    = zeros(1, ntrain);
v1    = ones(1, ntrain);
model = struct('type', 'samN', 'nid', nsample)
[~, model.machine] = system('hostname');
model.matlabpoolsize = matlabpool('size');

%% go!
iter = 0;  vios = [];  xis = [];
tpass = zeros(1, npass);
for pass = 1:npass
	fprintf('=====================================\n');
	fprintf('              pass %d\n', pass);
	fprintf('=====================================\n');
	tp = tic;
	ID = randperm(ntrain); % important

	for bat = 1:ceil(ntrain/nsample)

		if ~isempty(modelpath) && ~mod(iter, 10) % save intermediate models
			model.traintime = toc(tp);
			iterfn = sprintf('%s-iter%d.mat', modelpath, iter)
			save(iterfn, 'model', '-v7.3');
		end

		iter = iter + 1;
		if Verb
			fprintf('\n-------- Iter %d --------\n', iter); 
			fprintf('%s(N=%d), k=%d, C=%g, C/n=%.4f, alpha=%.2f, IHstr=%s\n', ...
				para.cls, ntrain, nsample, para.svm_C, C, para.alpha, para.IHstr);
		end
		%% update model
		%TODO: for budget mantaineance (SV removal), see Struck's approach
		if mod(bat, 10)
			svidx = find(alph > 0);
		else
			[V, I] = sort(alph, 'descend');
			svidx = I( 1 : find(cumsum(V)>0.99*sum(alph), 1) );
		end
		fprintf('==> Model: %d support vectors\n', length(svidx)); 
		model.alph  = alph(svidx);
		model.psicp = PSIcp(:, svidx);  model.ycp = Ycp(:, svidx);
		model.psigt = PSIgt(:, svidx);  model.ygt = Ygt(:, svidx);

		%% remove SVs that are inactive for some time
		idle(svidx) = 0;
		inact       = find(~ismember(1:ncp, svidx));
		idle(inact) = idle(inact) + 1;
		active      = find(idle(1:ncp) < maxidle);
		if mod(iter, 10) == 0 && length(active) < ncp
			fprintf('\n==> remove inactives: %d -> %d\n', ncp, length(active));
			ncp = length(active);
			for i = 1:ncp
				if i == active(i), continue; end
				PSIcp(:, i) = PSIcp(:, active(i));
				PSIgt(:, i) = PSIgt(:, active(i));
				Ygt(:, i)   = Ygt(:, active(i));
				Ycp(:, i)   = Ycp(:, active(i));
				Mcp(i)      = Mcp(active(i));
				alph(i)     = alph(active(i));
				idle(i)     = idle(active(i));
				revind(i)   = revind(active(i));
			end
			for i = 1:ntrain
				wset{i} = find(revind(1:ncp) == i);
			end
			H = H(active, active);
		end

		%% go to separation oracle to find cutting plane
		idmvc = ID(get_bootstrap(iter, 'disjoint', ntrain, nsample));
		fprintf('==> Sampled %d\n', length(idmvc)); 
		vv = mean(v1); xx = mean(xi);
		th = max(0.15, nms_th*min(1, (vv-xx)/(vv+xx)))
		ts = tic;
		[Yc, V, M, Fc, Fg, Yg] = MVC_1slack(idmvc, model, para, topk, th);
		te = toc(ts);  
		%inftime = [inftime te/length(idmvc)];
		fprintf('\n==> Running inference: %.2f sec\n', te); 

		%% add MVCs & expand gram matrix
		%todo: vectorize?
		new_mvc = 0;  gaps = [];  ts = tic;
		for i = 1:length(idmvc), 
			idx = idmvc(i);
			% add violated constraints
			for j = 1:topk,  if V(i,j) > xi(idx) + TolVio
				new_mvc = new_mvc + 1;
				gaps = [gaps, V(i,j)-xi(idx)];
				v1(idx) = max(v1(idx), V(i, j));
				% add cutting plane
				ncp   = ncp + 1;
				PSIcp = [PSIcp, Fc(:, i, j)];  Ycp = [Ycp, Yc(:, i, j)];
				PSIgt = [PSIgt, Fg(:, i, j)];  Ygt = [Ygt, Yg(:, i, j)];
				Mcp   = [Mcp; M(i, j)];
				% add dual variable
				alph = [alph; 0];
				idle = [idle; 0];
				% expand working set & reverse index
				wset{idx}   = [wset{idx}, ncp];
				revind(ncp) = idx;
			end, end
		end
		if ~new_mvc
			fprintf('\nNo violated constraint.\n\n');
			continue;
		end
		% expand H
		H = expand_gram_matrix(H, ncp, PSIgt, Ygt, PSIcp, Ycp, para);
		fprintf('\n==> Add MVCs: %.2f sec\n', toc(ts)); 
		fprintf('\n==> Added %d cutting planes, gap_max=%.3f, gap_avg=%.3f\n', ...
			new_mvc, max(gaps), mean(gaps));

		%% solve QP
		dH = double(H);
		df = double(Mcp(1:ncp));
		%[R, p] = chol(dH);
		%if p, disp('    Warning: non-PSD gram matrix'); end
		id = find( ~cellfun(@isempty, wset) );
		nid = length(id);
		S = ones(1, nid, 'uint8');
		b = ones(1, nid) * C;
		I = zeros(1, ncp, 'uint32');
		for i = 1:nid
			I(wset{id(i)}) = i;
		end
		tic; fprintf('\n==> QP solver: ');
		% use specialized QP solver
		[alph, ~] = qp(dH, -df, b, I, S, alph, 1e5, 0, TolRel, -inf, 1);
		toc;  fprintf('    QP solved: %d support vectors.\n', sum(alph>0));

		%% recover current slack variables
		for i = 1:ntrain
			if isempty(wset{i}), continue; end
			HH = H(wset{i}, 1:ncp);
			df = Mcp(1:ncp);
			ff = df(wset{i});
			xi(i) = max(0, max(ff - HH * alph(1:ncp)));
		end
		fprintf('\n==> Slack variables: max=%.3f, avg=%.3f\n\n', max(xi), mean(xi));

		% display
		vios = [vios; mean(V, 1)];
		xis  = [xis; mean(xi)];
		figure(26); clf; 
		subplot(3,1,1:2); plot(vios); hold all; plot(xis); hold off; grid on; 
		subplot(3,1,3); grid off; bar(xi);
		drawnow; pause(.1);

	end % end iteration

	passtime(pass) = toc(tp);
	if ~isempty(modelpath) % save intermediate models
		model.traintime = toc(tp);
		passfn = sprintf('%s-pass%d.mat', modelpath, pass)
		save(passfn, 'model', '-v7.3');
	end

end % end pass
t1 = toc(t0);

%% final model update and summary
if 1
	svidx = find(alph > 1e-6);
else
	[V, I] = sort(alph, 'descend');
	svidx = I( 1 : find(cumsum(V)>0.99*sum(alph), 1) );
end
model.alph  = alph(svidx);
model.psicp = PSIcp(:, svidx);  model.ycp = Ycp(:, svidx);
model.psigt = PSIgt(:, svidx);  model.ygt = Ygt(:, svidx);
model.traintime = passtime;
model.npass = npass;
fprintf('\n============ [sampledcuts_2pass] ============\n');
fprintf(' %d support vectors in %.2f secs\n', length(svidx), t1);
fprintf(' %d examples, avg slack: %g\n', ntrain, mean(xi));
fprintf(  '=============================================\n\n');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
function id = get_bootstrap(t, method, ntrain, nuse)
if strcmpi(method, 'all')
	id = 1:ntrain;
elseif strcmpi(method, 'uniform')
	if (nuse >= ntrain)
		id = 1:ntrain;
	else
		perm = randperm(ntrain);
		id = perm(1:nuse);
	end
elseif strcmpi(method, 'disjoint')
	id = (t-1)*nuse + (1:nuse);
	id = mod(id, ntrain)+1;
else
	error(['unknown sampling method: ' method]);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
function H = expand_gram_matrix(H, Np, PSIgt, Ygt, PSIcp, Ycp, para)
n = size(H, 1);
if n == 0
	H = sv_innerprod(para, ...
		PSIgt(:, 1:Np), Ygt(:, 1:Np), PSIcp(:, 1:Np), Ycp(:, 1:Np), ...
		PSIgt(:, 1:Np), Ygt(:, 1:Np), PSIcp(:, 1:Np), Ycp(:, 1:Np));
else
	h = sv_innerprod(para, ...
		PSIgt(:, 1:n), Ygt(:, 1:n), PSIcp(:, 1:n), Ycp(:, 1:n), ...
		PSIgt(:, n+1:Np), Ygt(:, n+1:Np), PSIcp(:, n+1:Np), Ycp(:, n+1:Np));
	hh = sv_innerprod(para, ...
		PSIgt(:, n+1:Np), Ygt(:, n+1:Np), PSIcp(:, n+1:Np), Ycp(:, n+1:Np), ...
		PSIgt(:, n+1:Np), Ygt(:, n+1:Np), PSIcp(:, n+1:Np), Ycp(:, n+1:Np));
	H = [H h; h' hh];
	H = (H+H')/2;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
function Kv = sv_innerprod(para, ...
	PSI1, Y1, psi1, y1, ...
	PSI2, Y2, psi2, y2)
% this returns a vector/matrix!
kernel = para.kfunc;
K1 = kernel(PSI1, PSI2, Y1, Y2, para);
K2 = kernel(PSI1, psi2, Y1, y2, para);
K3 = kernel(psi1, PSI2, y1, Y2, para);
K4 = kernel(psi1, psi2, y1, y2, para);
Kv = K1 - K2 - K3 + K4;
%K = sum(K(:));

