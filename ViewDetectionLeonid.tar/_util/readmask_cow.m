function bb = readmask_cow(imfn, params)
% read object mask from a .png file in cows dataset
% return the resulting bounding box
[p, n, e] = fileparts(imfn);
maskfn = sprintf('%smaps/%s-map%s', params.dsdir, n, e);
im = rgb2gray(imread(maskfn));
im = (im > 200);
im = bwmorph(im, 'open');
[I, J] = find(im);
%bb = [min(I) max(I) min(J) max(J)];
% use PASCAL VOC format: l t r b
bb = [min(J), min(I), max(J), max(I)];

