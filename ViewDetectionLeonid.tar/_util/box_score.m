function o = box_score(box1,box2)
y1 = max(box1(1),box2(1));
x1 = max(box1(2),box2(2));
y2 = min(box1(3),box2(3));
x2 = min(box1(4),box2(4));

w = x2-x1+1;
h = y2-y1+1;
if w<=0 || h<=0,
    o = 0;
else
    inter = w*h;
    aarea = (box1(3)-box1(1)+1)*(box1(4)-box1(2)+1);
    barea = (box2(3)-box2(1)+1)*(box2(4)-box2(2)+1);
    % intersection over union overlap
    o = inter / (aarea+barea-inter);
end