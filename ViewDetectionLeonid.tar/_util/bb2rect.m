function rect = bb2rect(y)
rect = [y(:,1) y(:,2) y(:,3)-y(:,1) y(:,4)-y(:,2)];
