function bb = readmask_3dobj(maskfn)
% read mask from a .mask file in 3Dobject dataset
% return the resulting bounding box
fp = fopen(maskfn);
w = fscanf(fp, '%d', 1);
h = fscanf(fp, '%d', 1);
A = fscanf(fp, '%d', [w, h]);
fclose(fp);

[I, J] = find(A > 0);
%bb = [min(I), max(I), min(J), max(J)];
% use PASCAL VOC format: l t r b
bb = [min(J), min(I), max(J), max(I)];
