function bb = clip_bb(bb, imhw)
% bb: [y1 y2 x1 x2]
% imhw: [h w]
h = imhw(1); 
w = imhw(2);
bb = max(1, bb);
bb = min([h h w w], bb);
