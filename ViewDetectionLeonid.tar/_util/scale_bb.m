function bb = scale_bb(bb, scaling)
% input
% bb: [y1 y2 x1 x2]
% scaling: [yscaling xscaling]
% 
s = [scaling(1) scaling(1) scaling(2) scaling(2)];
bb = bb .* s;
