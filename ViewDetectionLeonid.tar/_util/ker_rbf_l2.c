/****************************************************************************
  modified from Lazebnik's spatial pyramid implementation, 02/19/2013
  Kun He

  input:   x1 d*m, x2 d*n, each column is a feature vector
  output:  K m*n, K(i,j) = kernel(x1_i, x2_j)
 ****************************************************************************/

#include <math.h>
#include "mex.h"
#define min(a,b) (((a)<(b))?(a):(b))
#define PI 3.1415927

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  double *x1, *x2, *K;
  double *p1, *p2, *sigma, *sig1;
  double s, cutoff, diff1, diff2;
  int i, j, f1, f2;
  int m, n, d, d2;

  /* check number of input and output arguments */
  if (nrhs != 4 && nrhs != 3)
    mexErrMsgTxt("Wrong number of input arguments.");
  else if (nlhs > 1)
    mexErrMsgTxt("Too many output arguments.");

  /* get input arguments */
  if (!mxIsDouble(prhs[0]) || mxIsComplex(prhs[0]))
    mexErrMsgTxt("x1 must be a double matrix.");
  if (!mxIsDouble(prhs[1]) || mxIsComplex(prhs[1]))
    mexErrMsgTxt("x2 must be a double matrix.");
  d  = mxGetM(prhs[0]);
  if (d != mxGetM(prhs[1])) /* feature vectors must be of same length */
    mexErrMsgTxt("x1 & x2 must have same number of rows!");
  n  = mxGetN(prhs[0]);
  m  = mxGetN(prhs[1]);
  x1 = mxGetPr(prhs[0]);
  x2 = mxGetPr(prhs[1]);

  /* inverse of squared covariance matrix */
  if (mxGetM(prhs[2]) != d || mxGetN(prhs[2]) != d)
    mexErrMsgTxt("sigma must be d*d");
  sigma = (double*) mxGetPr(prhs[2]);

  /* distance cutoff */ 
  if (nrhs == 4)
    cutoff = (double)*mxGetPr(prhs[3]);
  else 
    cutoff = -1;

  /* allocate and initialize (to zero) output matrix */
  plhs[0] = mxCreateDoubleMatrix(n, m, mxREAL);
  K = mxGetPr(plhs[0]);

  /* COMPUTE KERNEL MATRIX
   * note: parallelize wrt f since usually 
   * feature dimension >> number of feature vectors
   * */
  for (j = 0; j < m; j++)
    {
      p2 = x2 + j * d;
      for (i = 0; i < n; i++)
	{
	  p1 = x1 + i * d;
	  s = 0;
	  for (f1 = 0; f1 < d; f1++) 
	    {
	      diff1 = p1[f1] - p2[f1];
	      if (cutoff > 0)
		diff1 = min(abs(diff1), cutoff-abs(diff1));
	      sig1 = sigma + f1*d;
	      for (f2 = 0; f2 < d; f2++)
		{
		  diff2 = p1[f2] - p2[f2];
		  if (cutoff > 0)
		    diff2 = min(abs(diff2), cutoff-abs(diff2));
		  s += diff1 * sig1[f2] * diff2;
		}
	    }
	  K[j*n+i] = exp(-0.5 * s);
	} /* end for i */
    } /* end for j */
}


