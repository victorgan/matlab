ppwd = pwd;
%dsdir = '/research/humanpose/Data/3dobj/';
dsdir = '~/humanpose/Data/3dobj/';
classes = {'bicycle' 'car' 'cellphone' 'head' 'iron' ...
	'monitor' 'mouse' 'shoe' 'stapler' 'toaster'};
viewpoints = 1:8;

for cl = 1:length(classes)
	cls = classes{cl};
	disp(cls);
	listing = dir([dsdir cls '/' cls '*']);
	for inst = 1:length(listing)
		dirname = [dsdir cls '/' listing(inst).name];
		cd(dirname);
		pwd
		cmd = 'LD_LIBRARY_PATH=~/lib mmv -v ''*A*'' ''A#2''';
		disp(cmd);
		unix(cmd);
		maskdir = [dirname '/mask'];
		cd(maskdir);
		pwd
		disp(cmd);
		system(cmd);
	end
end
cd(ppwd);
