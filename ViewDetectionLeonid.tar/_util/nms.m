function [bb pick] = nms(boxes, th)
% note: boxes should be sorted in descending order of score
if nargin < 2, th = .5; end
nbox = size(boxes, 1);
pick = logical(ones(nbox, 1));
for i = 1:nbox-1
	if ~pick(i)
		continue;
	end
	for j = i+1:nbox
		if ~pick(j)
			continue;
		end
		if overlap(boxes(i, :), boxes(j, :)) > th
			pick(j) = false;
		end
	end
end
bb = boxes(pick, :);

function ov = overlap(b1, b2)
% left top right bottom
a1 = (b1(3) - b1(1)) * (b1(4) - b1(2));
a2 = (b2(3) - b2(1)) * (b2(4) - b2(2));
bl = max(b1(1), b2(1));
bt = max(b1(2), b2(2));
br = min(b1(3), b2(3));
bb = min(b1(4), b2(4));
aov = max(0, (br - bl) * (bb - bt));
ov = aov / (a1 + a2 - aov);

