function disp_det_loc(ydet, ytest, params, smap)
if nargin < 4
	smap = [];
end
ntest = size(ytest, 1);
for i = 1:ntest
	inst = params.inst_test(i);
	imfn = params.listing(inst).name;
	im = imread(sprintf('%s%s', params.dsdir, imfn));

	if isempty(smap)
		imagesc(im);
		% ground truth in yellow
		rectangle('Position', bb2rect(ytest(i, :)), ...
			'Edgecolor', 'y', 'Linewidth', 3);
		% detection in green (good), red (bad)
		rectangle('Position', bb2rect(ydet(i, :)), ...
			'Edgecolor', 'g', 'Linewidth', 3);
	else
		%img = ind2rgb(rgb2gray(im), gray(255));
		map = smap{i};
		map = map - min(map(:));
		map = map / max(map(:)) * 255;
		map = ind2rgb(round(map), jet(255));
		ima = im2double(im) + map;
		ima = ima / max(ima(:));
		imagesc(ima);
	end

	pause();
end
