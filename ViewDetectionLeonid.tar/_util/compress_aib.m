function [training_im_aib,testing_im_aib] =  compress_aib(training_im,testing_im,parents,N,kSize)

% training_im, testing_im are the matrices containing training and test
% points you want to reduce n x d where d is dim and n is # of points
% parents is from vl_aib
% N is the new size you want to reduce 
% kSize is the original codebook size
% e.g. kSize = d, the dimensionality of the histogram


[~,map,~] = vl_aibcut(parents,N);

for i = 1:N
   compress(i).ind = find(map(1:kSize) == i);   
end
training_im_aib = zeros(size(training_im,1),N);
testing_im_aib = zeros(size(testing_im,1),N);


for i=1:size(training_im,1)
    for j = 1:N
        training_im_aib(i,j) = sum(training_im(i,compress(j).ind));
    end
end


for i=1:size(testing_im,1)
    for j = 1:N
        testing_im_aib(i,j) = sum(testing_im(i,compress(j).ind));
    end
end



