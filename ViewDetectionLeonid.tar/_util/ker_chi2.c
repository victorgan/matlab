/****************************************************************************
  modified from Lazebnik's spatial pyramid implementation, 02/19/2013
  Kun He

  input:   x1 n*d, x2 m*d, each column is a feature vector
  output:  K n*m, K(i,j) = kernel(x1_i, x2_j)
 ****************************************************************************/

#include <math.h>
#include <values.h>
#include "mex.h"
#define min(a,b) (((a)<(b))?(a):(b))

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  double *x1, *x2, *K;
  double *p1, *p2;
  double s;
  int i, j, f;
  int m, n, d, d2;

  /* check number of input and output arguments */
  if (nrhs != 2)
    mexErrMsgTxt("Wrong number of input arguments.");
  else if (nlhs > 1)
    mexErrMsgTxt("Too many output arguments.");

  /* get input arguments */
  if (!mxIsDouble(prhs[0]) || mxIsComplex(prhs[0]))
    mexErrMsgTxt("x1 must be a double matrix.");
  if (!mxIsDouble(prhs[1]) || mxIsComplex(prhs[1]))
    mexErrMsgTxt("x2 must be a double matrix.");
  n  = mxGetN(prhs[0]);
  m  = mxGetN(prhs[1]);
  x1 = mxGetPr(prhs[0]);
  x2 = mxGetPr(prhs[1]);

  /* feature vectors must be of same length */
  d  = mxGetM(prhs[1]);
  d2 = mxGetM(prhs[1]);
  if (d != d2)
    mexErrMsgTxt("x1 & x2 must have same number of rows!");

  /* allocate and initialize (to zero) output matrix */
  plhs[0] = mxCreateDoubleMatrix(n, m, mxREAL);
  K = mxGetPr(plhs[0]);

  /* COMPUTE KERNEL MATRIX
   * note: parallelize wrt f since usually 
   * feature dimension >> number of feature vectors
   * */
  for (j = 0; j < m; j++)
    {
      p2 = x2 + j * d;
      for (i = 0; i < n; i++)
	{
	  p1 = x1 + i * d;
	  s = 0;
#pragma omp parallel for private(f) reduction(+:s)
	  for (f = 0; f < d; f++) 
	    {
	      s += p1[f]*p2[f] / (p1[f]+p2[f]+FLT_MIN);
	    }
	  K[j*n+i] = s;
	}
    }
}


