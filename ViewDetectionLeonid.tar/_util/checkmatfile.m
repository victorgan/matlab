
datadir = '_cache/voc/';
listing = dir(datadir);
bad = [];
for i = 1:length(listing)
	if ~matcat([datadir listing(i).name])
		bad = [bad i];
	end
end
for i = 1:length(bad)
	system(['rm -f ' datadir listing(bad(i)).name], '-echo');
end
