function pairs = allpairs(a, b)
% a, b: 2 vectors
% really slick way of generating all pairs (a_i, b_j)
[p q] = meshgrid(a, b);
pairs = [p(:) q(:)];
