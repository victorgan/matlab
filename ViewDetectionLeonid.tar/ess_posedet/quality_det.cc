/*
 * ==========================================================================
 *    Description:  MEX'd efficient subwindow search
 *                  for joint detection and viewpoint estimation
 *        Created:  03/31/2013 04:08:10 PM EST
 *         Author:  Kun He
 *   Organization:  Boston University
 * ==========================================================================
 * various quality bounds for spatial pyramid features
 * and much more
 *
 * note: we don't explicitly distinguish between quality_wloss & quality
 *       but rather implicitly handle that depending on the input
 *
 * note: assume: integral histogram has been computed and passed as input
 *
 * WARNING: UGLY IMPLEMENTATION
 */
#include <omp.h>
#include <string.h>
#include <stdint.h>

#include <cmath>
#include <vector>
#include <string>
#include <valarray>
#include <algorithm>  // for binary search

#include "mex.h"
#include "matrix.h"
#include "newdelete.h" // let Matlab manage new/delete
#include "quality_det.hh"

typedef uint32_t int32;
using namespace std;

extern vector<int> xpos;
extern vector<int> ypos;

extern double Tsetup;
extern double Tbound;

//double get_runtime(void)
//{
//  /* returns the current processor time in hundredth of a second */
//  clock_t start;
//  start = clock();
//  return((double)start / ((double)(CLOCKS_PER_SEC) / 100.0));
//}
///////////////////////////////////////////////////////////////////////////////
// class SV
///////////////////////////////////////////////////////////////////////////////

SV::SV(const int dsp)
{
  hist.resize(dsp, 0);
}

SV::SV(const int L, const int dhist)
{
  int dsp = dhist * (exp2(2*L) - 1) / 3;
  hist.resize(dsp, 0);
}

SV::SV(const double a, double * ptr, const int L, 
       const int dhist, const int dsp)
{
  coef = a;
  mxAssert(dsp == dhist*(exp2(2*L)-1)/3, "dsp doesn't match dhist");
  hist.resize(dsp, 0);
  hist = valarray<double> (ptr, dsp);
}

///////////////////////////////////////////////////////////////////////////////
// class bnbQuality::init
///////////////////////////////////////////////////////////////////////////////

spQuality* new_spq(string K, int W, int H, int D, int L, int Dsp, double g);

void bnbQuality::init(string K, const mxArray * P, 
		      int S, int W, int H, int D, int L, int Dsv,
		      vector<Box> & boxes_in, 
		      int nsv0, double * sv0coef, double * sv0ptr, 
		      int nsv1, double * sv1coef, double * sv1ptr, 
		      double g_in)
{
  // 1) get parameters, precompute cell weights, etc
  kernel  = K;
  inthist = P;
  step    = S;
  spq     = new_spq(K, W, H, D, L, Dsv, g_in);
  // 2) copy svs and coefficients
  sv0.clear();
  sv1.clear();
  if (nsv0 > 0 && sv0coef != NULL && sv0ptr != NULL)
    {
      for (int i = 0; i < nsv0; i++)
	{
	  const double a = sv0coef[i];
	  mxAssert(a != 0., "no inactive support vectors!");
	  sv0.push_back(SV(a, sv0ptr+i*Dsv, L, D, Dsv));
	}
    }
  if (nsv1 > 0 && sv1coef != NULL && sv1ptr != NULL)
    {
      for (int i = 0; i < nsv1; i++)
	{
	  const double a = sv1coef[i];
	  mxAssert(a != 0., "no inactive support vectors!");
	  sv1.push_back(SV(a, sv1ptr+i*Dsv, L, D, Dsv));
	}
    }
  // 3) copy support vectors, or precompute the linear weights
  linear = (kernel.find("lin") != string::npos);
  if (linear)  // linear kernel, construct weight vector
    {
      w0.resize(spq->dsp, 0.);
      w1.resize(spq->dsp, 0.);
      for (int i = 0; i < sv0.size(); i++)
	{
	  w0 += sv0[i].coef * sv0[i].hist;
	}
      for (int i = 0; i < sv1.size(); i++)
	{
	  w1 += sv1[i].coef * sv1[i].hist;
	}
    }
  // 4) store ygt (and score it if necessary)
  gtbox = boxes_in;
  gtscore0.resize(gtbox.size(), 0.0);
  gtscore1.resize(gtbox.size(), 0.0);
  vector<double> s0 = gtscore0;
  vector<double> s1 = gtscore1;
  for (int i = 0; i < gtbox.size(); i++)
    {
      sstate s(gtbox[i]);
      spq->setup(&s, inthist, step);
      compute_upper_bound(&s);
      ivpair iv = loss_ub(&s);
      s0[i] = s.rhoUB - iv.val;
      s1[i] = s.upper - s.rhoUB;
      gtbox[i].score = s0[i] + s1[i];
    }
  gtscore0 = s0;
  gtscore1 = s1;
}

///////////////////////////////////////////////////////////////////////////////
// class bnbQuality::bounds
///////////////////////////////////////////////////////////////////////////////

inline double overlap_ub(const sstate* s, const Box& gt)
{
  // note: boxes in s are of fixed size (h*w)!
  int sh = s->h, sw = s->w;
  int x1 = s->l[0], x2 = s->l[1], y1 = s->t[0], y2 = s->t[1];
  int maxw = 0, maxh = 0;
  // w
  if (x2+sw > gt.l && x2+sw <= gt.r)
    maxw = min(sw, x2+sw-gt.l);
  else if (x1 >= gt.l && x1 < gt.r)
    maxw = min(sw, gt.r-x1);
  else if (x1 < gt.l && x2+sw > gt.r)
    maxw = min(sw, gt.r-gt.l);
  // h
  if (y2+sh > gt.t && y2+sh <= gt.b)
    maxh = min(sh, y2+sh-gt.t);
  else if (y1 >= gt.t && y1 < gt.b)
    maxh = min(sh, gt.b-y1);
  else if (y1 < gt.t && y2+sh > gt.b)
    maxh = min(sh, gt.b-gt.t);
  mxAssert(maxw >= 0, "negative maxw in overlap_ub()");
  mxAssert(maxh >= 0, "negative maxh in overlap_ub()");
  return maxw * maxh;
}

inline double overlap_lb(const sstate* s, const Box& gt)
{
  // note: boxes in s are of fixed size (h*w)!
  int sh = s->h, sw = s->w;
  int x1 = s->l[0], x2 = s->l[1], y1 = s->t[0], y2 = s->t[1];
  //
  // if min overlap is 0
  if (x2+sw <= gt.l || y2+sh <= gt.t || x1 >= gt.r || y1 >= gt.b)
    return 0; 
  //
  // min overlap != 0, find it: look at the 4 corners
  // TODO: is there a more efficient way?
  double a0 = area_ov(Box(x1, y1, x1+sw, y1+sh), gt);
  double a1 = area_ov(Box(x1, y2, x1+sw, y2+sh), gt);
  double a2 = area_ov(Box(x2, y1, x2+sw, y1+sh), gt);
  double a3 = area_ov(Box(x2, y2, x2+sw, y2+sh), gt);
  return min(a0, min(a1, min(a2, a3)));
}


//double bnbQuality::loss_ub(const sstate* s) 
ivpair bnbQuality::loss_ub(const sstate* s) 
{
  if (gtbox.empty())
    return ivpair(-1, 0);

  double ast = s->w * s->h;
  double maxc = -1e10;
  int maxi = -1;
  for (int i = 0; i < gtbox.size(); i++)
    {
      double agt = (gtbox[i].r - gtbox[i].l) * (gtbox[i].b - gtbox[i].t);
      double aint = overlap_lb(s, gtbox[i]);
      //maxc = max(maxc, 1 - aint/(ast + agt - aint));
      double t = 1 - aint/(ast + agt - aint) - gtscore0[i] - gtscore1[i];
      if (t > maxc)
	{
	  maxc = t;
	  maxi = i;
	}
    }
  return ivpair(maxi, maxc);
}

//double bnbQuality::loss_lb(const sstate* s) 
ivpair bnbQuality::loss_lb(const sstate* s) 
{
  if (gtbox.empty())
    return ivpair(-1, 0);

  double ast = s->w * s->h;
  double minc = 1e10;
  int mini = -1;
  for (int i = 0; i < gtbox.size(); i++)
    {
      double agt = (gtbox[i].r - gtbox[i].l) * (gtbox[i].b - gtbox[i].t);
      double aint = overlap_ub(s, gtbox[i]);
      //minc = min(minc, 1 - aint/(ast + agt - aint));
      double t = 1 - aint/(ast + agt - aint) - gtscore0[i] - gtscore1[i];
      if (t < minc)
	{
	  minc = t;
	  mini = i;
	}
    }
  return ivpair(mini, minc);
}

void bnbQuality::compute_bounds(sstate * s, bool getLB)
{
  //double t0 = get_runtime();
  spq->setup(s, inthist, step);
  //double t1 = get_runtime();
  compute_upper_bound(s);
  if (getLB)
    compute_lower_bound(s);
  //double t2 = get_runtime();
  //Tsetup += t1 - t0;
  //Tbound += t2 - t1;
}

void bnbQuality::compute_upper_bound(sstate * state)
{
  double ub = 0;
  double rho = 0;
  ivpair iv = loss_ub(state);
  state->maxgtid = iv.idx;
  double s1 = (iv.idx >= 0) ? gtscore1[iv.idx] : 0;

  // now score using SV model
  if (linear)
    {
      rho = max(0.0, iv.val + spq->UB(w0) + s1);
      ub = rho + spq->UB(w1) - s1;
    }
  else  
    {
      // loop through every SV
      double UBw0 = 0;
      for (int p = 0; p < sv0.size(); p++)
	{
	  double a = sv0[p].coef;
	  if (a > 0)
	    UBw0 += a * spq->UB(sv0[p].hist);
	  else
	    UBw0 += a * spq->LB(sv0[p].hist);
	}
      // loop through every SV
      rho = max(0.0, iv.val + UBw0 + s1);
      double UBw1 = 0;
      for (int p = 0; p < sv1.size(); p++)
	{
	  double a = sv1[p].coef;
	  if (a > 0)
	    UBw1 += a * spq->UB(sv1[p].hist);
	  else
	    UBw1 += a * spq->LB(sv1[p].hist);
	}
      ub = rho + UBw1 - s1;
    }
  state->rhoUB = rho;
  state->upper = ub;
}

void bnbQuality::compute_lower_bound(sstate * state)
{
  ivpair iv = loss_lb(state);
  double lb = iv.val;

  // now score using SV model
  if (linear)
    {
      lb += spq->LB(w0) + spq->LB(w1);
    }
  else  
    {
      // loop through every SV
      for (int p = 0; p < sv0.size(); p++)
	{
	  double a = sv0[p].coef;
	  if (a < 0)
	    lb += a * spq->UB(sv0[p].hist);
	  else
	    lb += a * spq->LB(sv0[p].hist);
	}
      // loop through every SV
      for (int p = 0; p < sv1.size(); p++)
	{
	  double a = sv1[p].coef;
	  if (a < 0)
	    lb += a * spq->UB(sv1[p].hist);
	  else
	    lb += a * spq->LB(sv1[p].hist);
	}
    }
  state->lower = lb;
}

///////////////////////////////////////////////////////////////////////////////
// class spQuality
///////////////////////////////////////////////////////////////////////////////

// init is independent on input state
void spQuality::init(int W, int H, int D, int L, int Dsp, int nor=0, double gam=2) 
{ 
  imw = W;  imh = H;  dhist = D;  nlevels = L;  dsp = Dsp; 
  normalize = nor; gamma = gam; norm_out = norm_inn = 0;
  inout.clear();
  for (int m = 0; m < 2; m++)
    {
      inout.push_back(valarray<double>(0., dsp));
    }
  ndiv.clear();
  int divs = exp2(L);
  for (int l = 0; l < nlevels; l++, divs/=2)
    {
      ndiv.push_back(divs/2);
    }
}

spQuality* new_spq(string K, int W, int H, int D, int L, int Dsp, double g)
{
  // set kernel type. accomodate various abbreviations
  //
  // IMPORTANT: q should never be de-referenced
  // because base class cellQuality is abstract (has pure virtual func)
  spQuality * q;
  if (K == "linear" || K == "lin") 
    {
      q = new linQuality();
      q->init(W, H, D, L, Dsp);
    }
  else if (K == "nlinear1" || K == "nlin1")
    {
      q = new nlin1Quality();
      q->init(W, H, D, L, Dsp, 1);
    }
  else if (K == "nlinear2" || K == "nlin2")
    {
      q = new nlin2Quality();
      q->init(W, H, D, L, Dsp, 2);
    }
  else if (K == "rbf_chi2" || K == "rbfchi2")
    {
      q = new rbfchi2Quality();
      q->init(W, H, D, L, Dsp, 1, g);
    }
  else if (K == "rbf_hi" || K == "rbfhi")
    {
      q = new rbfhiQuality();
      q->init(W, H, D, L, Dsp, 1, g);
    }
  else if (K == "chi2")
    {
      q = new chi2Quality();
      q->init(W, H, D, L, Dsp);
    }
  else if (K == "nchi2")
    {
      q = new nchi2Quality();
      q->init(W, H, D, L, Dsp, 1);
    }
  else if (K == "hi" || K == "hik")
    {
      q = new hiQuality();
      q->init(W, H, D, L, Dsp);
    }
  else if (K == "nhi" || "nhik")
    {
      q = new nhiQuality();
      q->init(W, H, D, L, Dsp, 1);
    }
  else	// default to normalized linear
    {
      q = new nlin1Quality();
      q->init(W, H, D, L, Dsp, 2);
    }
  return q;
}

// setup is dependent on input state
void spQuality::setup(const sstate * s, const mxArray* inthist, const int step)
{
  // 1) build two spatial pyramids
  int sh = s->h, sw = s->w;
  int x1 = s->l[0], x2 = s->l[1], y1 = s->t[0], y2 = s->t[1];
  build_spatial_pyramids(sw, sh, x1, x2, y1, y2, inthist, step, nlevels);

  // 2) normalization
  switch (normalize)
    {
    case 1:  // 2a) get L1 norm, weighted
	{
	  norm_inn = inout[0].sum();
	  norm_out = inout[1].sum();
	}
      break;

    case 2:  // 2b) get L2 norm, weighted
	{
	  valarray<double> inn2 = inout[0] * inout[0];
	  valarray<double> out2 = inout[1] * inout[1];
	  norm_inn = inn2.sum();
	  norm_out = out2.sum();
	}
      break;

    default: // 2c) no normalization
      break;
    }
}

///////////////////////////////////////////////////////////////////////////////
inline int binsearch(int pt, vector<int> & ppos)
{
  pair<vector<int>::iterator, 
    vector<int>::iterator> p = equal_range(ppos.begin(), ppos.end(), pt);

  if (p.second == ppos.end())
    {
      return ppos.size();
    }
  else if (p.second == ppos.begin())
    {
      return 1;
    }
  else if (p.first != p.second)
    {
      vector<int>::iterator t = (pt-*p.first < *p.second-pt) ? p.first:p.second;
      return t - ppos.begin() + 1;
    }
  else
    {
      return p.first - ppos.begin() + 1;
    }
}

inline void sample_inthist(double * histPt, double * intHistPt, 
			   int width, int height, int numLabels, 
			   int * boxesPt, int numBoxes)
{
  //mexPrintf("W%d H%d K%d nbb%d\n", width, height, numLabels, numBoxes);
  for (int bi = 0 ; bi < numBoxes ; ++ bi) 
    {
      int x1 = *boxesPt++ ;
      int y1 = *boxesPt++ ;
      int x2 = *boxesPt++ ;
      int y2 = *boxesPt++ ;
      //mexPrintf("box %d: [%d-%d, %d-%d]\n", bi+1, x1, x2, y1, y2);
      int stride = width ;
      int labelStride = width * height ;
      double * iter = NULL;

      /* empty box case */
      if (x1 > x2 || y1 > y2) 
	{
	  for (int k = 0; k < numLabels; ++k)
	    histPt [k] = 0 ;
	  histPt += numLabels ;
	  continue ;
	}

      if (x1 < 1  || y1 < 1 || x2 > width || y2 > height)
	{
	  char msg[256];
	  sprintf(msg, "Box %d OOB: [%d-%d, %d-%d] w=%d, h=%d\n", 
		  bi+1, x1, x2, y1, y2, width, height);
	  mexErrMsgTxt(msg);
	}
      -- x1 ;
      -- x2 ;
      -- y1 ;
      -- y2 ;

      iter = intHistPt + x2 + y2 * stride ;
      for (int k = 0 ; k < numLabels ; ++k) 
	{
	  histPt [k] = *iter ;
	  iter += labelStride ;
	}

      if (x1 > 0) 
	{
	  iter = intHistPt + (x1 - 1) + y2 * stride ;
	  for (int k = 0 ; k < numLabels ; ++k) 
	    {
	      histPt [k] -= *iter ;
	      iter += labelStride ;
	    }
	}

      if (x1 > 0 && y1 > 0) 
	{
	  iter = intHistPt + (x1 - 1) + (y1 - 1) * stride ;
	  for (int k = 0 ; k < numLabels ; ++k) 
	    {
	      histPt [k] += *iter ;
	      iter += labelStride ;
	    }
	}

      if (y1 > 0) 
	{
	  iter = intHistPt + x2 + (y1 - 1) * stride ;
	  for (int k = 0 ; k < numLabels ; ++k) 
	    {
	      histPt [k] -= *iter ;
	      iter += labelStride ;
	    }
	}
      histPt += numLabels ;
    }
}
///////////////////////////////////////////////////////////////////////////////

void spQuality::
build_spatial_pyramids(int w, int h, int x1, int x2, int y1, int y2, 
		       const mxArray* inthist, const int step, const int L)
{
  // 0) init
  //  mwSize sz_mxbbs[] = {4, ndiv[0] * ndiv[0]};
  //  mxArray *mxbbs[2], *vl_lhs[1], *vl_rhs[2];
  //  int32 *bbs[2];
  //  mxbbs [0] = mxCreateNumericArray(2, sz_mxbbs, mxUINT32_CLASS, mxREAL);
  //  mxbbs [1] = mxCreateNumericArray(2, sz_mxbbs, mxUINT32_CLASS, mxREAL);
  //  bbs   [0] = (int32*)mxGetData(mxbbs[0]);
  //  bbs   [1] = (int32*)mxGetData(mxbbs[1]);
  //  vl_rhs[0] = const_cast<mxArray*>(inthist);

  const int div = ndiv[0];
  double wfine = w / double(div);  // note: NO PADDING
  double hfine = h / double(div);  // note: NO PADDING

  double * p_inthist = const_cast<double*>(mxGetPr(inthist));
  int * sz_inthist = (int*)mxGetDimensions(inthist);
  int H = sz_inthist[0];
  int W = sz_inthist[2];
  int K = sz_inthist[4];
  int dim = dhist * div * div;
  int * bb = new int[4*div*div];
  double * hist = new double[dim];

  for (int io = 0; io < 2; io++)
    {
      inout[io].resize(dsp, 0);

      // 1) get histograms for finest level
      //int * p_bb = bb;
      double * p_hist = hist;

      if (io == 0) // intersection
	for (int x = 0; x < div; x++)
	  for (int y = 0; y < div; y++)
	    {
	      int x_lo = max(1, (int)floor(x2+w - (div-x)*wfine));
	      int y_lo = max(1, (int)floor(y2+h - (div-y)*hfine));
	      int x_hi = min(imw, (int)ceil(x1 + (x+1)*wfine));
	      int y_hi = min(imh, (int)ceil(y1 + (y+1)*hfine));

	      int cnt = x * div + y;
	      bb[cnt*4+0] = binsearch(y_lo, ypos);
	      bb[cnt*4+1] = binsearch(x_lo, xpos);
	      bb[cnt*4+2] = binsearch(y_hi, ypos);
	      bb[cnt*4+3] = binsearch(x_hi, xpos);
	    }
      else // union
	for (int x = 0; x < div; x++)
	  for (int y = 0; y < div; y++)
	    {
	      int x_lo = min(imw, (int)floor(x1 + x*wfine));
	      int y_lo = min(imh, (int)floor(y1 + y*hfine));
	      int x_hi = max(1, (int)ceil(x2+w - (div-x-1)*wfine));
	      int y_hi = max(1, (int)ceil(y2+h - (div-y-1)*hfine));
	      mxAssert(x_lo <= x_hi, "union box EMPTY (x)");
	      mxAssert(y_lo <= y_hi, "union box EMPTY (y)");

	      int cnt = x * div + y;
	      bb[cnt*4+0] = binsearch(y_lo, ypos);
	      bb[cnt*4+1] = binsearch(x_lo, xpos);
	      bb[cnt*4+2] = binsearch(y_hi, ypos);
	      bb[cnt*4+3] = binsearch(x_hi, xpos);
	    }
      sample_inthist(p_hist, p_inthist, H, W, K, bb, div*div);

      // 2) area normalization? (essentially L1 normalization!)
      //    NO
      inout[io][slice(0, dim, 1)] = valarray<double>(hist, dim) * 0.01;

      // 3) construct upper levels, bottom up
      int low = 0;
      int cur = ndiv[0] * ndiv[0];
      for (int l = 1; l < L; l++)
	{
	  int n;
	  for (n = 0; n < ndiv[l]*ndiv[l]; n++)
	    {
	      int i = n / ndiv[l];	// integer division
	      int j = n - i * ndiv[l]; 
	      valarray<double> temp(0., dhist);
	      for (int x = 0; x < 2; x++)
		for (int y = 0; y < 2; y++)
		  {
		    int idx = (2*i+x) * ndiv[l-1] + (2*j+y);
		    temp += inout[io][slice((low+idx)*dhist, dhist, 1)];
		  }
	      //inout[io][slice(cur+n*dhist, dhist, 1)] = temp * w;
	      inout[io][slice((cur+n)*dhist, dhist, 1)] = temp / 4.0;
	    }
	  low = cur;
	  cur += n;
	}
    }
  //
  delete [] bb;
  delete [] hist;
}

void spQuality::get_sp(valarray<double>& hist, Box& b, const mxArray* inthist, 
		       const int step)
{
  // recompute based on output state
  sstate * st = new sstate(b);
  setup(st, inthist, step);
  hist = inout[1];
  switch (normalize)
    {
    case 1: 
	{
	  mexPrintf("L1-normalize by %g", norm_out); 
	  hist /= norm_out; 
	  break;
	}
    case 2:
	{
	  mexPrintf("L2-normalize by %g", sqrt(norm_out)); 
	  hist /= sqrt(norm_out); 
	  break;
	}
    default:
      break;
    }
  delete st;
}

//============================================================================
//==================  quality bounds for various kernels  ====================
//============================================================================
//
// un-normalized linear kernel
double linQuality::UB(const valarray<double>& hist)
{
  double ub = 0;
  for (int i = 0; i < dsp; i++)
    {
      double x = inout[0][i]; 
      double X = inout[1][i]; 
      double y = hist[i];
      if (y > 0.)
	ub += X * y;
      else if (y < 0.)
	ub += x * y;
    }
  return ub;
}
double linQuality::LB(const valarray<double>& hist)
{
  double lb = 0;
  for (int i = 0; i < dsp; i++)
    {
      double x = inout[0][i]; 
      double X = inout[1][i]; 
      double y = hist[i];
      if (y > 0.)
	lb += x * y;
      else if (y < 0.)
	lb += X * y;
    }
  return lb;
}
//
// L1 normalized linear kernel
double nlin1Quality::UB(const valarray<double>& hist)
{
  double ub = 0;
  for (int i = 0; i < dsp; i++)
    {
      double x = inout[0][i]; 
      double X = inout[1][i]; 
      double y = hist[i];
      if (y > 0. && X > 0.)
	ub += y * X/(norm_inn + X - x);
      else if (y < 0. && x > 0.)
	ub += y * x/(norm_out + x - X);
    }
  return ub;
}
double nlin1Quality::LB(const valarray<double>& hist)
{
  double lb = 0;
  for (int i = 0; i < dsp; i++)
    {
      double x = inout[0][i]; 
      double X = inout[1][i]; 
      double y = hist[i];
      if (y > 0. && x > 0.)
	lb += y * x/(norm_out + x - X);
      else if (y < 0. && X > 0.)
	lb += y * X/(norm_inn + X - x);
    }
  return lb;
}
//
// L2 normalized linear kernel
double nlin2Quality::UB(const valarray<double>& hist)
{
  double ub = 0;
  //#pragma omp parallel for reduction(+:ub)
  for (int i = 0; i < dsp; i++)
    {
      double x = inout[0][i]; 
      double X = inout[1][i]; 
      double y = hist[i];
      if (y > 0. && X > 0.)
	ub += y * X/sqrt(norm_inn + X*X - x*x);
      else if (y < 0. && x > 0.)
	ub += y * x/sqrt(norm_out + x*x - X*X);
    }
  return ub;
}
double nlin2Quality::LB(const valarray<double>& hist)//SV& sv)
{
  double lb = 0;
  //#pragma omp parallel for reduction(+:lb)
  for (int i = 0; i < dsp; i++)
    {
      double x = inout[0][i]; 
      double X = inout[1][i]; 
      double y = hist[i];
      if (y > 0. && x > 0.)
	lb += y * x/sqrt(norm_out + x*x - X*X);
      else if (y < 0. && X > 0.)
	lb += y * X/sqrt(norm_inn + X*X - x*x);
    }
  return lb;
}
//
// un-normalized Histogram Intersection
double hiQuality::UB(const valarray<double>& hist)//SV& sv)
{
  double ub = 0;
  //#pragma omp parallel for reduction(+:ub)
  for (int i = 0; i < dsp; i++)
    ub += min(inout[1][i], hist[i]); 
  return ub; 
}
double hiQuality::LB(const valarray<double>& hist)
{
  double lb = 0;
  //#pragma omp parallel for reduction(+:lb)
  for (int i = 0; i < dsp; i++)
    lb += min(inout[0][i], hist[i]);
  return lb;
}
//
// normalized Histogram Intersection
// important assumption: input svhist is already L1-normalized
double nhiQuality::UB(const valarray<double>& hist)
{
  double ub = 0;
  for (int i = 0; i < dsp; i++)
    {
      double x = inout[0][i]; 
      double X = inout[1][i]; 
      double y = hist[i];
      if (X > 0. && y > 0.)
	ub += min( y, X/(norm_inn + X - x) );
    }
  return ub;
}
double nhiQuality::LB(const valarray<double>& hist)
{
  double lb = 0;
  for (int i = 0; i < dsp; i++)
    {
      double x = inout[0][i]; 
      double X = inout[1][i]; 
      double y = hist[i];
      if (x > 0. && y > 0.)
	lb += min( y, x/(norm_out + x - X) );
    }
  return lb;
}
//
// RBF w histogram intersection distance
// important assumption: input svhist is already L1-normalized
double rbfhiQuality::UB(const valarray<double>& hist)
{
  double hiub = 0;
  //#pragma omp parallel for reduction(+:hiub)
  for (int i = 0; i < dsp; i++)
    {
      double x = inout[0][i]; 
      double X = inout[1][i]; 
      double y = hist[i];
      if (X > 0. && y > 0.)
	hiub += min( y, X/(norm_inn + X - x) );
    }
  return exp(-gamma * (1-hiub) * (1-hiub));
}
double rbfhiQuality::LB(const valarray<double>& hist)
{
  double hilb = 0;
  //#pragma omp parallel for reduction(+:hilb)
  for (int i = 0; i < dsp; i++)
    {
      double x = inout[0][i]; 
      double X = inout[1][i]; 
      double y = hist[i];
      if (x > 0. && y > 0.)
	hilb += min( y, x/(norm_out + x - X) );
    }
  return exp(-gamma * (1-hilb) * (1-hilb));
}
//
// un-normalized Chi-square
double chi2Quality::UB(const valarray<double>& hist)
{
  double ub = 0;
  //#pragma omp parallel for reduction(+:ub)
  for (int i = 0; i < dsp; i++)
    {
      double X = inout[1][i]; 
      double y = hist[i];
      if (X > 0. && y > 0.)
	ub += 2 * y / (1 + y/X);
    }
  return ub;
}
double chi2Quality::LB(const valarray<double>& hist)
{
  double lb = 0;
  //#pragma omp parallel for reduction(+:lb)
  for (int i = 0; i < dsp; i++)
    {
      double x = inout[0][i]; 
      double y = hist[i];
      if (x > 0. && y > 0.)
	lb += 2 * y / (1 + y/x);
    }
  return lb;
}
//
// normalized Chi-square
// important assumption: input svhist is already L1-normalized
double nchi2Quality::UB(const valarray<double>& hist)
{
  double ub = 0;
  //#pragma omp parallel for reduction(+:ub)
  for (int i = 0; i < dsp; i++)
    {
      double x = inout[0][i]; 
      double X = inout[1][i]; 
      double y = hist[i];
      if (X > 0. && y > 0.)
	{
	  double xx = (norm_inn - x + X) / X;
	  ub += 2 * y / (1 + xx * y);
	}
    }
  return ub;
}
double nchi2Quality::LB(const valarray<double>& hist)
{
  double lb = 0;
  //#pragma omp parallel for reduction(+:lb)
  for (int i = 0; i < dsp; i++)
    {
      double x = inout[0][i]; 
      double X = inout[1][i]; 
      double y = hist[i];
      if (x > 0. && y > 0.)
	{
	  double xx = (norm_out - X + x) / x;
	  lb += 2 * y / (1 + xx * y);
	}
    }
  return lb;
}
//
// RBF w chi2 distance
// important assumption: input svhist is already L1-normalized
//#include <values.h>	// for DBL_MIN
#include <limits.h>	// for DBL_MIN
double rbfchi2Quality::UB(const valarray<double>& hist)
{
  double chi2distlb = 0;
  //#pragma omp parallel for reduction(+:chi2distlb)
  for (int i = 0; i < dsp; i++)
    {
      double x = inout[0][i]; 
      double X = inout[1][i]; 
      double y = hist[i];
      double x1 = x/(norm_out - X + x);
      double x2 = X/(norm_inn - x + X);
      if (y < x1)
	chi2distlb += (x1-y)*(x1-y)/(x1+y+DBL_MIN);
      else if (y > x2)
	chi2distlb += (x2-y)*(x2-y)/(x2+y+DBL_MIN);
    }
  return exp(-gamma * chi2distlb * chi2distlb);
}
double rbfchi2Quality::LB(const valarray<double>& hist)
{
  double chi2distub = 0;
  //#pragma omp parallel for reduction(+:chi2distub)
  for (int i = 0; i < dsp; i++)
    {
      double x = inout[0][i]; 
      double X = inout[1][i]; 
      double y = hist[i];
      double x1 = x/(norm_out - X + x);
      double x2 = X/(norm_inn - x + X);
      chi2distub += max((x1-y)*(x1-y)/(x1+y+DBL_MIN), 
			(x2-y)*(x2-y)/(x2+y+DBL_MIN));
    }
  return exp(-gamma * chi2distub * chi2distub);
}

