/*
 * ==========================================================================
 *    Description:  MEX'd efficient subwindow search
 *                  for joint detection and viewpoint estimation
 *        Created:  03/31/2013 04:08:10 PM EST
 *         Author:  Kun He
 *   Organization:  Boston University
 * ==========================================================================
 * various quality bounds for spatial pyramid features
 * and much more
 *
 * note: we don't explicitly distinguish between quality_wloss & quality
 *       but rather implicitly handle that depending on the input
 *
 * note: assume: integral histogram has been computed and passed as input
 *
 * WARNING: UGLY IMPLEMENTATION
 */
#include <omp.h>
#include <string.h>
#include <stdint.h>
#include <algorithm>  // for binary search
#include "mex.h"
#include "matrix.h"
#include "newdelete.h" // let Matlab manage new/delete
#include "quality_posedet.hh"

typedef uint32_t int32;
extern std::vector<int> xpos;
extern std::vector<int> ypos;
extern std::vector<double> vps;

///////////////////////////////////////////////////////////////////////////////
// class SV - nontrivial constructor
///////////////////////////////////////////////////////////////////////////////

SV::SV(const double a, double * ptr, const int L, 
       const int dhist, const int dsp, const Box y)
{
  coef  = a;
  label = y;
  mxAssert(dsp == dhist*(exp2(2*L)-1)/3, "dsp doesn't match dhist");
  hist = Vec(ptr, dsp);
}

SV::SV(const Vec & inH)
{
  hist = inH;
#ifdef _USE_ARMADILLO_
  ipos = arma::find(hist > 0);
  ineg = arma::find(hist < 0);
#endif
  coef = 0;
  label = Box(0.0);
}

///////////////////////////////////////////////////////////////////////////////
// class bnbQuality::init
///////////////////////////////////////////////////////////////////////////////

spQuality* new_spq(std::string K, int W, int H, int D, int L, int Dsp);

void bnbQuality::init(std::string kx, const mxArray * P, // Kx & inthist
		      int D, int L, int Dsv,          // nvw, nlv, dsp
		      int W, int H,                   // imw, imh
		      std::vector<Box> & boxes_in,    // gt labels
		      std::vector<double*> & wptr,    // linear classifiers
		      std::vector<double> & bias,     // bias terms
		      double a_in, double l_in)       // weighting losses
{
  // 1) get parameters
  inthist = P;
  spq     = new_spq(kx, W, H, D, L, Dsv);
  alpha   = a_in;
  lambda  = l_in;

  // 2) store Ws
  w_linsvm.clear();
  for (int i = 0; i < wptr.size(); i++)
    {
      w_linsvm.push_back(SV(Vec(wptr[i], Dsv)));
      b_linsvm.push_back(bias[i]);
    }

  // 3) store ygt (and score it if there are SVs)
  gtbox = boxes_in;
  if (!w_linsvm.empty())
    {
      for (int i = 0; i < gtbox.size(); i++)
	{
	  // find closest vp id
	  int minid = -1;
	  double mindv = 2*PI;
	  for (int v = 0; v < vps.size(); v++)
	    {
	      double dv = std::abs(gtbox[i].vp - vps[v]);
	      dv = std::min(dv, 2*PI-dv);
	      if (dv < mindv)
		{
		  mindv = dv;
		  minid = v;
		}
	    }
	  gtbox[i].score = 0;
	  // construct sstate according to gt
	  sstate s(gtbox[i]);
	  s.vpid = minid;
	  spq->setup(&s, inthist); // get histograms
	  compute_upper_bound(&s);
	  gtbox[i].score = s.upper - s.Delta;
	}
    }
}

///////////////////////////////////////////////////////////////////////////////
// class bnbQuality::bounds
///////////////////////////////////////////////////////////////////////////////

double overlap_ub(const sstate* s, const Box& gt)
{
  // note: boxes in s are of fixed size (h*w)!
  int maxw = 0, maxh = 0;
  int sh = s->h, sw = s->w;
  int x1 = s->l[0], x2 = s->l[1];
  int y1 = s->t[0], y2 = s->t[1];
  // w
  if (x2+sw > gt.l && x2+sw <= gt.r)
    maxw = std::min(sw, x2+sw-gt.l);
  else if (x1 >= gt.l && x1 < gt.r)
    maxw = std::min(sw, gt.r-x1);
  else if (x1 < gt.l && x2+sw > gt.r)
    maxw = std::min(sw, gt.r-gt.l);
  // h
  if (y2+sh > gt.t && y2+sh <= gt.b)
    maxh = std::min(sh, y2+sh-gt.t);
  else if (y1 >= gt.t && y1 < gt.b)
    maxh = std::min(sh, gt.b-y1);
  else if (y1 < gt.t && y2+sh > gt.b)
    maxh = std::min(sh, gt.b-gt.t);
  mxAssert(maxw >= 0, "negative maxw in overlap_ub()");
  mxAssert(maxh >= 0, "negative maxh in overlap_ub()");
  return maxw * maxh;
}

double overlap_lb(const sstate* s, const Box& gt)
{
  // note: boxes in s are of fixed size (h*w)!
  int sh = s->h,    sw = s->w;
  int x1 = s->l[0], x2 = s->l[1];
  int y1 = s->t[0], y2 = s->t[1];
  //
  // if min overlap is 0
  if (x2+sw <= gt.l || y2+sh <= gt.t || x1 >= gt.r || y1 >= gt.b)
    return 0; 
  //
  // min overlap > 0, find it: look at the 4 corners
  // is there a more efficient way?
  double a0 = area_ov(Box(x1, y1, x1+sw, y1+sh), gt);
  double a1 = area_ov(Box(x1, y2, x1+sw, y2+sh), gt);
  double a2 = area_ov(Box(x2, y1, x2+sw, y1+sh), gt);
  double a3 = area_ov(Box(x2, y2, x2+sw, y2+sh), gt);
  return std::min(a0, std::min(a1, std::min(a2, a3)));
}

double bnbQuality::loss_vp(const sstate* s, const Box& gt)
{
  // cost based on viewpoint label difference
  double dvp = gt.vp - vps[s->vpid];
  dvp = std::abs(dvp);
  dvp = std::min(dvp, 2*PI-dvp);
  //return dvp/PI;
  return std::min(lambda*dvp/PI, 1.0);
}

ivpair bnbQuality::loss_ub(const sstate* s) 
{
  if (gtbox.empty())
    return ivpair(-1, 0);

  double ast  = s->w * s->h;
  double maxc = -1e10;
  int maxi    = -1;
  for (int i = 0; i < gtbox.size(); i++)
    {
      // assume: each state only holds a single vp
      double agt  = (gtbox[i].r - gtbox[i].l) * (gtbox[i].b - gtbox[i].t);
      double aint = overlap_lb(s, gtbox[i]);
      double t    = alpha * (1 - aint / (ast + agt - aint));
      t += (1-alpha) * loss_vp(s, gtbox[i]);
      if (t > maxc)
	{
	  maxc = t;
	  maxi = i;
	}
    }
  return ivpair(maxi, maxc);
}

ivpair bnbQuality::loss_lb(const sstate* s) 
{
  if (gtbox.empty())
    return ivpair(-1, 0);

  double ast = s->w * s->h;
  double minc = 1e10;
  int mini = -1;
  for (int i = 0; i < gtbox.size(); i++)
    {
      // assume: each state only holds a single vp
      double agt  = (gtbox[i].r - gtbox[i].l) * (gtbox[i].b - gtbox[i].t);
      double aint = overlap_ub(s, gtbox[i]);
      double t    = alpha * (1 - aint / (ast + agt - aint));
      t += (1 - alpha) * loss_vp(s, gtbox[i]);
      if (t < minc)
	{
	  minc = t;
	  mini = i;
	}
    }
  return ivpair(mini, minc);
}

///////////////////////////////////////////////////////////////////////////////
// bounds
///////////////////////////////////////////////////////////////////////////////

void bnbQuality::compute_bounds(sstate * s, bool getLB)
{
  spq->setup(s, inthist);
  compute_upper_bound(s);
  if (getLB)
    compute_lower_bound(s);
}

void bnbQuality::compute_upper_bound(sstate * state)
{
  ivpair iv      = loss_ub(state);
  state->Delta   = iv.val;
  state->maxgtid = iv.idx;
  double ub      = iv.val;
  if (iv.idx >= 0)
    ub -= gtbox[iv.idx].score;

  ub += spq->UB(w_linsvm[state->vpid]);
  ub += b_linsvm[state->vpid];
  state->upper = ub;
}

void bnbQuality::compute_lower_bound(sstate * state)
{
  ivpair iv = loss_lb(state);
  double lb = iv.val;
  if (iv.idx >= 0)
    lb -= gtbox[iv.idx].score;

  lb += spq->LB(w_linsvm[state->vpid]);
  lb += b_linsvm[state->vpid];
  state->lower = lb;
}

///////////////////////////////////////////////////////////////////////////////
// class spQuality
///////////////////////////////////////////////////////////////////////////////

// init is independent on input state
void spQuality::init(int W, int H, int D, int L, int Dsp, int nor=0)
{
  imw = W;  imh = H;  dhist = D;  nlevels = L;  dsp = Dsp;
  normalize = nor;
  norm_out = norm_inn = 0;
  inout.clear();
  for (int m = 0; m < 2; m++)
    {
#ifdef _USE_ARMADILLO_
      inout.push_back(arma::zeros<Vec>(dsp));
#else
      inout.push_back(Vec(0., dsp));
#endif
    }
  ndiv.clear();
  int divs = exp2(L);
  for (int l = 0; l < nlevels; l++, divs/=2)
    {
      ndiv.push_back(divs/2);
    }
}

spQuality* new_spq(std::string K, int W, int H, int D, int L, int Dsp)
{
  // set kernel type. accomodate various abbreviations
  // note: this only controls Kx, Ky is fixed at Gaussian
  //
  // IMPORTANT: q should never be de-referenced
  // because base class cellQuality is abstract (has pure virtual func)
  spQuality * q;
  if (K == "linear" || K == "lin") 
    {
      q = new linQuality();
      q->init(W, H, D, L, Dsp);
    }
  else if (K == "nlinear1" || K == "nlin1")
    {
      q = new nlin1Quality();
      q->init(W, H, D, L, Dsp, 1);
    }
  else if (K == "nlinear" || K == "nlin" || K == "nlin2")
    {
      q = new nlinQuality();
      q->init(W, H, D, L, Dsp, 2);
    }
  else if (K == "hell" || K == "hellin")
    {
      q = new hellQuality();
      q->init(W, H, D, L, Dsp);
    }
  else if (K == "nhell" || K == "nhellin")
    {
      q = new nhellQuality();
      q->init(W, H, D, L, Dsp, 1);
    }
  else	// default to linear
    {
      q = new linQuality();
      q->init(W, H, D, L, Dsp);
    }
  return q;
}

// setup is dependent on input state
void spQuality::setup(const sstate * s, const mxArray* inthist//, const int step
)
{
  // 1) build two spatial pyramids
  int sh = s->h, sw = s->w;
  int x1 = s->l[0], x2 = s->l[1], y1 = s->t[0], y2 = s->t[1];
  build_spatial_pyramids(sw, sh, x1, x2, y1, y2, inthist, nlevels);

  // 2) normalization
  switch (normalize)
    {
    case 1:  // 2a) get L1 norm, weighted
	{
#ifdef _USE_ARMADILLO_
	  norm_inn = norm(inout[0], 1);
	  norm_out = norm(inout[1], 1);
#else
	  norm_inn = inout[0].sum();
	  norm_out = inout[1].sum();
#endif
	}
      break;

    case 2:  // 2b) get L2 norm, weighted
	{
#ifdef _USE_ARMADILLO_
	  norm_inn = norm(inout[0], 2);
	  norm_out = norm(inout[1], 2);
#else
	  Vec inn2 = inout[0] * inout[0];
	  Vec out2 = inout[1] * inout[1];
	  norm_inn = inn2.sum();
	  norm_out = out2.sum();
	  norm_inn = sqrt(inn2.sum());
	  norm_out = sqrt(out2.sum());
#endif
	}
      break;

    default: // 2c) no normalization
      break;
    }
}

///////////////////////////////////////////////////////////////////////////////
inline int binsearch(int pt, std::vector<int> & ppos)
{
  std::pair<std::vector<int>::iterator, 
    std::vector<int>::iterator> p = equal_range(ppos.begin(), ppos.end(), pt);

  if (p.second == ppos.end())
    {
      return ppos.size();
    }
  else if (p.second == ppos.begin())
    {
      return 1;
    }
  else if (p.first != p.second)
    {
      std::vector<int>::iterator t = (pt-*p.first < *p.second-pt) ? p.first:p.second;
      return t - ppos.begin() + 1;
    }
  else
    {
      return p.first - ppos.begin() + 1;
    }
}

// sample_inithist taken from VLFeat
// Thanks Andrea!
inline void sample_inthist(double * histPt, double * intHistPt, 
			   int width, int height, int numLabels, 
			   int * boxesPt, int numBoxes)
{
  for (int bi = 0 ; bi < numBoxes ; ++ bi) 
    {
      int x1 = *boxesPt++ ;
      int y1 = *boxesPt++ ;
      int x2 = *boxesPt++ ;
      int y2 = *boxesPt++ ;
      int stride = width ;
      int labelStride = width * height ;
      double * iter = NULL;

      // empty box case
      if (x1 > x2 || y1 > y2) 
	{
	  for (int k = 0; k < numLabels; ++k)
	    histPt [k] = 0 ;
	  histPt += numLabels ;
	  continue ;
	}
      if (x1 < 1  || y1 < 1 || x2 > width || y2 > height)
	{
	  char msg[256];
	  sprintf(msg, "Box %d OOB: [%d-%d, %d-%d] w=%d, h=%d\n", 
		  bi+1, x1, x2, y1, y2, width, height);
	  mexErrMsgTxt(msg);
	}
      -- x1 ;
      -- x2 ;
      -- y1 ;
      -- y2 ;

      iter = intHistPt + x2 + y2 * stride ;
      for (int k = 0 ; k < numLabels ; ++k) 
	{
	  histPt [k] = *iter ;
	  iter += labelStride ;
	}
      if (x1 > 0) 
	{
	  iter = intHistPt + (x1 - 1) + y2 * stride ;
	  for (int k = 0 ; k < numLabels ; ++k) 
	    {
	      histPt [k] -= *iter ;
	      iter += labelStride ;
	    }
	}
      if (x1 > 0 && y1 > 0) 
	{
	  iter = intHistPt + (x1 - 1) + (y1 - 1) * stride ;
	  for (int k = 0 ; k < numLabels ; ++k) 
	    {
	      histPt [k] += *iter ;
	      iter += labelStride ;
	    }
	}
      if (y1 > 0) 
	{
	  iter = intHistPt + x2 + (y1 - 1) * stride ;
	  for (int k = 0 ; k < numLabels ; ++k) 
	    {
	      histPt [k] -= *iter ;
	      iter += labelStride ;
	    }
	}
      histPt += numLabels ;
    }
}
///////////////////////////////////////////////////////////////////////////////

void spQuality::
build_spatial_pyramids(int w, int h, int x1, int x2, int y1, int y2, 
		       const mxArray* inthist, const int L)
{
  const int div      = ndiv[0];
  double wfine       = w / double(div);  // note: NO PADDING
  double hfine       = h / double(div);  // note: NO PADDING
  double * p_inthist = const_cast<double*>(mxGetPr(inthist));
  int * sz_inthist   = (int*)mxGetDimensions(inthist);
  int H              = sz_inthist[0];
  int W              = sz_inthist[2];
  int K              = sz_inthist[4];
  int spdim          = dhist * div * div;
  int * bb           = new int[4*div*div];
  double * hist      = new double[spdim];

  for (int io = 0; io < 2; io++)
    {
#ifdef _USE_ARMADILLO_
      inout[io].zeros(dsp);
#else
      inout[io].resize(dsp, 0);
#endif
      // 1) get histograms for finest level
      double * p_hist = hist;

      if (io == 0) // intersection
	for (int x = 0; x < div; x++)
	  for (int y = 0; y < div; y++)
	    {
	      int x_lo    = std::max(1, (int)floor(x2+w - (div-x)*wfine));
	      int y_lo    = std::max(1, (int)floor(y2+h - (div-y)*hfine));
	      int x_hi    = std::min(imw, (int)ceil(x1 + (x+1)*wfine));
	      int y_hi    = std::min(imh, (int)ceil(y1 + (y+1)*hfine));
	      int cnt     = x * div + y;
	      bb[cnt*4+0] = binsearch(y_lo, ypos);
	      bb[cnt*4+1] = binsearch(x_lo, xpos);
	      bb[cnt*4+2] = binsearch(y_hi, ypos);
	      bb[cnt*4+3] = binsearch(x_hi, xpos);
	    }
      else // union
	for (int x = 0; x < div; x++)
	  for (int y = 0; y < div; y++)
	    {
	      int x_lo = std::min(imw, (int)floor(x1 + x*wfine));
	      int y_lo = std::min(imh, (int)floor(y1 + y*hfine));
	      int x_hi = std::max(1, (int)ceil(x2+w - (div-x-1)*wfine));
	      int y_hi = std::max(1, (int)ceil(y2+h - (div-y-1)*hfine));
	      mxAssert(x_lo <= x_hi, "union box EMPTY (x)");
	      mxAssert(y_lo <= y_hi, "union box EMPTY (y)");

	      int cnt     = x * div + y;
	      bb[cnt*4+0] = binsearch(y_lo, ypos);
	      bb[cnt*4+1] = binsearch(x_lo, xpos);
	      bb[cnt*4+2] = binsearch(y_hi, ypos);
	      bb[cnt*4+3] = binsearch(x_hi, xpos);
	    }
      sample_inthist(p_hist, p_inthist, H, W, K, bb, div*div);

      // 2) assignment
#ifdef _USE_ARMADILLO_
      Vec temp = arma::vec(hist, spdim) * 0.1;
      arma::uvec neg = arma::find(temp < 0);
      //mexPrintf("#neg = %d\n", arma::sum(neg));
      temp.elem(neg) = arma::zeros(neg.n_elem);
      inout[io](arma::span(0, spdim-1)) = temp;
#else
      inout[io][std::slice(0, spdim, 1)] = Vec(hist, spdim) * 0.1;
      inout[io] = abs(inout[io]);
#endif

      // 3) construct upper levels, bottom up
      int low = 0;
      int cur = ndiv[0] * ndiv[0];
      for (int l = 1; l < L; l++)
	{
	  for (int n = 0; n < ndiv[l]*ndiv[l]; n++)
	    {
	      int i = n / ndiv[l];	// integer division
	      int j = n - i * ndiv[l]; 
#ifdef _USE_ARMADILLO_
	      Vec tmp = arma::zeros<Vec>(dhist);
#else
	      Vec tmp(0., dhist);
#endif
	      for (int x = 0; x < 2; x++)
		for (int y = 0; y < 2; y++)
		  {
		    int idx = (2*i+x) * ndiv[l-1] + (2*j+y);
#ifdef _USE_ARMADILLO_
		    Vec tt = inout[io](arma::span((low+idx)*dhist, (low+idx+1)*dhist-1));
		    tmp += tt/16.0;
		    //tmp = arma::max(tmp, tt/2);
#else
		    Vec tt= inout[io][std::slice((low+idx)*dhist, dhist, 1)];
		    tmp += tt/16.0;
		    //tmp = max(tmp, tt/2);
#endif
		  }
#ifdef _USE_ARMADILLO_
	      inout[io](arma::span((cur+n)*dhist, (cur+n+1)*dhist-1)) = tmp;
#else
	      inout[io][std::slice((cur+n)*dhist, dhist, 1)] = tmp;
#endif
	    }
	  low = cur;
	  cur += ndiv[l]*ndiv[l];
	}
    }
  //
  delete [] bb;
  delete [] hist;
}


void spQuality::get_sp(Vec& hist, Box& b, const mxArray* inthist)
{
  // recompute based on output state
  sstate * st = new sstate(b);
  setup(st, inthist);
  hist = inout[1];
  switch (normalize)
    {
    case 1: 
	{
	  //mexPrintf("L1-normalize by %g\n", norm_out); 
	  hist /= norm_out; 
	  break;
	}
    case 2:
	{
	  //mexPrintf("L2-normalize by %g\n", norm_out); 
	  hist /= norm_out; 
	  break;
	}
    default:
      break;
    }
  delete st;
}

//============================================================================
//==================  quality bounds for various kernels  ====================
//============================================================================
//
// un-normalized linear kernel
double linQuality::UB(const SV& sv)
{
  double ub = 0;
#ifdef _USE_ARMADILLO_
  ub = arma::dot(sv.hist.elem(sv.ipos), inout[1].elem(sv.ipos))
    + arma::dot(sv.hist.elem(sv.ineg), inout[0].elem(sv.ineg));
#else
  for (int i = 0; i < dsp; i++)
    {
      double x = inout[0][i]; 
      double X = inout[1][i]; 
      double y = sv.hist[i];
      if (y > 0.)
	ub += X * y;
      else if (y < 0.)
	ub += x * y;
    }
#endif
  return ub;
}
double linQuality::LB(const SV& sv)
{
  double lb = 0;
#ifdef _USE_ARMADILLO_
  lb = arma::dot(sv.hist.elem(sv.ipos), inout[0].elem(sv.ipos))
    + arma::dot(sv.hist.elem(sv.ineg), inout[1].elem(sv.ineg));
#else
  for (int i = 0; i < dsp; i++)
    {
      double x = inout[0][i]; 
      double X = inout[1][i]; 
      double y = sv.hist[i];
      if (y > 0.)
	lb += x * y;
      else if (y < 0.)
	lb += X * y;
    }
#endif
  return lb;
}
//
// L1 normalized linear kernel
double nlin1Quality::UB(const SV& sv)
{
  double ub = 0;
  for (int i = 0; i < dsp; i++)
    {
      double x = inout[0][i]; 
      double X = inout[1][i]; 
      double y = sv.hist[i];
      if (y > 0. && X > 0.)
	ub += y * X/(norm_inn + X - x);
      else if (y < 0. && x > 0.)
	ub += y * x/(norm_out + x - X);
    }
  return ub;
}
double nlin1Quality::LB(const SV& sv)
{
  double lb = 0;
  for (int i = 0; i < dsp; i++)
    {
      double x = inout[0][i]; 
      double X = inout[1][i]; 
      double y = sv.hist[i];
      if (y > 0. && x > 0.)
	lb += y * x/(norm_out + x - X);
      else if (y < 0. && X > 0.)
	lb += y * X/(norm_inn + X - x);
    }
  return lb;
}
//
// L2 normalized linear kernel
double nlinQuality::UB(const SV& sv)
{
  double ub = 0;
#ifdef _USE_ARMADILLO_
  if (norm_inn > 0)
    ub = arma::dot(sv.hist.elem(sv.ipos), inout[1].elem(sv.ipos))/norm_inn
      + arma::dot(sv.hist.elem(sv.ineg), inout[0].elem(sv.ineg))/norm_out;
  else
    ub = arma::dot(sv.hist.elem(sv.ipos), inout[1].elem(sv.ipos) > 0);
#else
  for (int i = 0; i < dsp; i++)
    {
      double x = inout[0][i]; 
      double X = inout[1][i]; 
      double y = sv.hist[i];
      if (y > 0. && X > 0.)
	ub += y * X/sqrt(norm_inn*norm_inn + X*X - x*x);
      else if (y < 0. && x > 0.)
	ub += y * x/sqrt(norm_out*norm_out + x*x - X*X);
      //
      // note: refrain from using the tighter bound to reduce computation
      //
      //      if (y > 0. && X > 0.)
      //	ub += y * X/norm_inn;
      //      else if (y < 0. && x > 0.)
      //	ub += y * x/norm_out;
    }
#endif
  return ub;
}
double nlinQuality::LB(const SV& sv)
{
  double lb = 0;
#ifdef _USE_ARMADILLO_
  if (norm_inn > 0)
    lb = arma::dot(sv.hist.elem(sv.ipos), inout[0].elem(sv.ipos))/norm_out
      + arma::dot(sv.hist.elem(sv.ineg), inout[1].elem(sv.ineg))/norm_inn;
  else
    lb = arma::dot(sv.hist.elem(sv.ineg), inout[1].elem(sv.ineg) > 0);
#else
  for (int i = 0; i < dsp; i++)
    {
      double x = inout[0][i]; 
      double X = inout[1][i]; 
      double y = sv.hist[i];
      if (y > 0. && x > 0.)
	lb += y * x/sqrt(norm_out*norm_out + x*x - X*X);
      else if (y < 0. && X > 0.)
	lb += y * X/sqrt(norm_inn*norm_inn + X*X - x*x);
      //
      // note: refrain from using the tighter bound to reduce computation
      //
      //      if (y > 0. && x > 0.)
      //	lb += y * x/norm_out;
      //      else if (y < 0. && X > 0.)
      //	lb += y * X/norm_inn;
    }
#endif
  return lb;
}
//
// Hellinger kernel (unnormalized)
double hellQuality::UB(const SV& sv)
{
  double ub = 0;
#ifdef _USE_ARMADILLO_
  if (norm_inn > 0)
    ub = arma::dot(sv.hist.elem(sv.ipos), arma::sqrt(inout[1].elem(sv.ipos)))
      + arma::dot(sv.hist.elem(sv.ineg), arma::sqrt(inout[0].elem(sv.ineg)));
  else
    ub = arma::dot(sv.hist.elem(sv.ipos), inout[1].elem(sv.ipos) > 0);
#else
  for (int i = 0; i < dsp; i++)
    {
      double x = inout[0][i]; 
      double X = inout[1][i]; 
      double y = sv.hist[i];
      if (y > 0. && X > 0.)
	ub += y * sqrt(X);
      else if (y < 0. && x > 0.)
	ub += y * sqrt(x);
    }
#endif
  return ub;
}
double hellQuality::LB(const SV& sv)
{
  double lb = 0;
#ifdef _USE_ARMADILLO_
  if (norm_inn > 0)
    lb = arma::dot(sv.hist.elem(sv.ipos), arma::sqrt(inout[0].elem(sv.ipos)))
      + arma::dot(sv.hist.elem(sv.ineg), arma::sqrt(inout[1].elem(sv.ineg)));
  else
    lb = arma::dot(sv.hist.elem(sv.ineg), inout[1].elem(sv.ineg) > 0);
#else
  for (int i = 0; i < dsp; i++)
    {
      double x = inout[0][i]; 
      double X = inout[1][i]; 
      double y = sv.hist[i];
      if (y > 0. && x > 0.)
	lb += y * sqrt(x);
      else if (y < 0. && X > 0.)
	lb += y * sqrt(X);
    }
#endif
  return lb;
}
//
// Hellinger kernel (with L1 normalization)
double nhellQuality::UB(const SV& sv)
{
  double ub = 0;
#ifdef _USE_ARMADILLO_
  if (norm_inn > 0)
    ub = arma::dot(sv.hist.elem(sv.ipos), arma::sqrt(inout[1].elem(sv.ipos)/norm_inn))
      + arma::dot(sv.hist.elem(sv.ineg), arma::sqrt(inout[0].elem(sv.ineg)/norm_out));
  else
    ub = arma::dot(sv.hist.elem(sv.ipos), inout[1].elem(sv.ipos) > 0);
#else
  for (int i = 0; i < dsp; i++)
    {
      double x = inout[0][i]; 
      double X = inout[1][i]; 
      double y = sv.hist[i];
      if (y > 0. && X > 0.)
	ub += y * sqrt(X / (norm_inn + X - x));
      else if (y < 0. && x > 0.)
	ub += y * sqrt(x / (norm_out + x - X));
    }
#endif
  return ub;
}
double nhellQuality::LB(const SV& sv)
{
  double lb = 0;
#ifdef _USE_ARMADILLO_
  if (norm_inn > 0)
    lb = arma::dot(sv.hist.elem(sv.ipos), arma::sqrt(inout[0].elem(sv.ipos)/norm_out))
      + arma::dot(sv.hist.elem(sv.ineg), arma::sqrt(inout[1].elem(sv.ineg)/norm_inn));
  else
    lb = arma::dot(sv.hist.elem(sv.ineg), inout[1].elem(sv.ineg) > 0);
#else
  for (int i = 0; i < dsp; i++)
    {
      double x = inout[0][i]; 
      double X = inout[1][i]; 
      double y = sv.hist[i];
      if (y > 0. && x > 0.)
	lb += y * sqrt(x / (norm_out + x - X));
      else if (y < 0. && X > 0.)
	lb += y * sqrt(X / (norm_inn + X - x));
    }
#endif
  return lb;
}

