/*
 * ==========================================================================
 *    Description:  MEX'd efficient subwindow search
 *                  for object localization
 *        Created:  05/27/2013 10:13:52 PM EST
 *         Author:  Kun He
 *   Organization:  Boston University
 * ==========================================================================
 * returns top K results
 * ==========================================================================
 * NOTE: this is only used for test-time inference
 *       histograms will not be returned
 * ==========================================================================
 * INPUT
 * kernel  - string, kernel type
 * nlv     - uint32, # levels in spatial pyramid
 * inthist - H * W * K double. K: # of visual words
 * step    - uint32, separation between feature points
 * imwh    - uint32, 2-vector image width and height
 * xgrid   - uint32 vec, x coordinates of feature points
 * ygrid   - uint32 vec, y coordinates of feature points
 * bbsizes - 2*nsize uint32, all the boxes sizes to search for
 * svcoef  - double, support vector coefficients
 * svhist  - double, support vectors (histogram)
 * topk    - # of boxes to return, default 1
 * nms     - overlap threshold for NMS
 * gamma   - double, bandwidth parameter for RBF kernels
 *
 * OUTPUT
 * ybar   - results, K*[l,t,r,b]
 * viobar - scores, K*1
 */
#include <stdint.h>
#include <cmath>
#include <string>
#include <vector>
#include <iostream>
#include <algorithm>

#include "mex.h"
#include "matrix.h"
#include "newdelete.h"  // let Matlab manage new/delete
#include "ess_hingeboost.hh"
#include "quality_det.hh"

using namespace std;
typedef uint32_t int32;

static int VERBOSE = 10000;
static int ITERMAX = 10000000;
double TH_NMS = 1;
int TOPK = 1;
int rejected = 0;

vector<int> xpos;
vector<int> ypos;
static sstate_heap H;

double Tsetup = 0.0;
double Tbound = 0.0;

///////////////////////////////////////////////////////////////////////////
// NMS
inline double overlap_ratio(const Box& b1, const Box& b2)
{
  double a1 = (b1.r - b1.l) * (b1.b - b1.t);
  double a2 = (b2.r - b2.l) * (b2.b - b2.t);
  double aint = area_ov(b1, b2);
  return aint / (a1 + a2 - aint);
}
inline int do_NMS(vector<Box>& boxes, Box& bb, const double th = TH_NMS)
{
  for (int i = 0; i < boxes.size(); i++)
    {
      double ov = overlap_ratio(boxes[i], bb);
      if ( ov >= th )
	{
	  //mexPrintf("overlap w/ #%d = %.3f >= %.3f\n", i, ov, th);
	  //++ rejected;
	  return 1;
	}
    }
  boxes.push_back(bb);
  return 0;
}
///////////////////////////////////////////////////////////////////////////

// central routine during branch-and-bound search:
inline int extract_split_insert(bnbQuality& qual, bool getLB = true)
{
  // 1) find most promising candidate region, check stop criterion
  // 2) split and create two new states
  const sstate * cur = H.top();
  sstate * newstate[2];
  //vector<sstate*> newstate(0);
  if (cur->px[1] - cur->px[0] > 0 && 
      cur->px[1] - cur->px[0] >= cur->py[1] - cur->py[0])
    {
      // split x
      H.pop();
      for (int x = 0; x < 2; x++)
	{
	  sstate * s = new sstate(*cur);
	  s->px[x] = (cur->px[0] + cur->px[1] + 2-2*x) >> 1;
	  s->l[x] = xpos[s->px[x]];
	  //newstate.push_back(s);
	  newstate[x] = s;
	}
    }
  else if (cur->py[1] - cur->py[0] > 0)
    {
      // split y
      H.pop();
      for (int y = 0; y < 2; y++)
	{
	  sstate * s = new sstate(*cur);
	  s->py[y] = (cur->py[0] + cur->py[1] + 2-2*y) >> 1;
	  s->t[y] = ypos[s->py[y]];
	  //newstate.push_back(s);
	  newstate[y] = s;
	}
    }
  else 
    return -1;
  // 3) calculate bounds for new states and reinsert them
  //    with some pruning
  for (int i = 0; i < 2; i++)
    {
      qual.compute_bounds(newstate[i], getLB);
      if (!getLB || H.empty() || newstate[i]->upper > H.top()->lower)
	H.push(newstate[i]);
    }
  // 4) no error, but also no convergence, yet
  delete cur;
  cur = NULL;
  return 0;
}

void print_top(int count)
{
  const sstate * curmax = H.top();
  cout << "#" << count;
  cout << " |H|=" << H.size() << " ";
  cout << curmax->tostring();
}

/////////////////////////////////////////////////////////////////////////////
// pyaramid_search
/////////////////////////////////////////////////////////////////////////////

int pyramid_search(Params & para, vector<Box>& boxes, int K)
{
  // push initial states into heap
  for (int b = 0; b < para.bbsizes.size(); b++)
    {
      int ww = para.bbsizes[b].w;
      int hh = para.bbsizes[b].h;
      int wlim = para.imw - ww;
      int hlim = para.imh - hh;
      int pxlo = 0;
      int pylo = 0;
      int pxhi = pxlo;
      int pyhi = pylo;
      for (; pxhi+1 < xpos.size() && xpos[pxhi+1] <= wlim; pxhi++);
      for (; pyhi+1 < ypos.size() && ypos[pyhi+1] <= hlim; pyhi++);
      int xlo = xpos[pxlo];
      int xhi = xpos[pxhi];
      int ylo = ypos[pylo];
      int yhi = ypos[pyhi];
      sstate * subspace = 
	new sstate(ww, hh, pxlo, pxhi, pylo, pyhi, xlo, xhi, ylo, yhi);
      para.quality.compute_bounds(subspace);
      H.push(subspace);
    }

  // return top K boxes
  long globalcount = 0;
  while (!H.empty() && K--)
    {
      // do branch-and-bound search
      int count = 1;
      while (extract_split_insert(para.quality, globalcount>=0) >= 0 
	     && count < ITERMAX)
	{
	  if (VERBOSE && count % VERBOSE == 0)
	    print_top(count);
	  count++;
	}
      globalcount += count;
      if (boxes.size() % 250 == 0)
	{
	  print_top(globalcount);
	  //mexEvalString("drawnow");
	}

      // at convergence or error, return result or best guess
      const sstate * cur = H.top();
      Box outbox;
      outbox.l = (cur->l[0] + cur->l[1]) >> 1;
      outbox.t = (cur->t[0] + cur->t[1]) >> 1;
      outbox.r = outbox.l + cur->w; 
      outbox.b = outbox.t + cur->h;
      outbox.score = cur->upper;

      // pop
      delete cur;
      H.pop();

      // do NMS
      K += do_NMS(boxes, outbox);
    }

  // clean up and return
  while (!H.empty())
    {
      delete H.top();
      H.pop();
    }
  return globalcount;
}

/////////////////////////////////////////////////////////////////////////////
// mexFunction
/////////////////////////////////////////////////////////////////////////////

void err(const char * msg, bool usage = true)
{
  if (usage)
    {
      mexPrintf("Usage:\n[y vio mar psi] = ess_hingeboost(\n");
      mexPrintf("    kernel, nlv, inthist, step, imwh, xgrid, ygrid, bbsizes,\n");
      mexPrintf("    svcoef, svhist, topK, NMS, gamma\n");
    }
  mexErrMsgTxt(msg);
}
int check_args(int nlhs, int nrhs, const mxArray * prhs[]);
void get_args(int nrhs, const mxArray* prhs[], Params& para, const int mode);

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  // check and get input arguments
  mexPrintf("\n");
  int mode = check_args(nlhs, nrhs, prhs);
  if (mode!=0 && mode!=1 && mode!=2 && mode!=3)
    err("incorrect working mode");
  Params para;
  get_args(nrhs, prhs, para, mode);

  // create arrays for output
  plhs[0] = mxCreateDoubleMatrix(4, TOPK, mxREAL);
  plhs[1] = mxCreateDoubleMatrix(TOPK, 1, mxREAL);
  double * detlabel = mxGetPr(plhs[0]);	// y_bar
  double * detscore = mxGetPr(plhs[1]);	// violation (have y_gt) || score (otherwise)

  // go!
  vector<Box> outbox(0);
  int iter = pyramid_search(para, outbox, TOPK);
  //      for (int i = 0; i < outbox.size(); i++)
  //	{
  //	  Box out = outbox[i];
  //	  mexPrintf(" DET [%3d %3d %3d %3d] <score = %g>\n", 
  //		    out.l, out.t, out.r, out.b, out.score);
  //	}
  mexPrintf("--> %d dets, score:[%.4f, %.4f], %d iters\n", 
	    outbox.size(), outbox[0].score, outbox[outbox.size()-1].score, iter);
  //mexPrintf("--> setup: %.3f secs, bounds: %.3f secs\n", Tsetup/100, Tbound/100);

  for (int i = 0; i < TOPK && i < outbox.size(); i++)
    {
      detlabel[i*4+0] = outbox[i].l;
      detlabel[i*4+1] = outbox[i].t;
      detlabel[i*4+2] = outbox[i].r;
      detlabel[i*4+3] = outbox[i].b;
      detscore[i] = outbox[i].score;
    }
}

/////////////////////////////////////////////////////////////////////////////
// check_args
// the type constraints are pretty strict, but by no means a bad thing
/////////////////////////////////////////////////////////////////////////////

// check input arguments
int check_args(int nlhs, int nrhs, const mxArray * prhs[])
{
  if (nrhs != 13)	err("Wrong number of input arguments (must be 13).");
  if (nlhs != 2)	err("Wrong number of output arguments (must be 2).");

  // kernel type and SP levels
  const mxArray * _kernel 	= prhs[0];
  const mxArray * _nlv	 	= prhs[1];
  // feature related
  const mxArray * _inthist	= prhs[2];
  const mxArray * _step 	= prhs[3];
  const mxArray * _imwh 	= prhs[4];
  const mxArray * _xgrid 	= prhs[5];
  const mxArray * _ygrid 	= prhs[6];
  // search space related
  const mxArray * _bbsizes 	= prhs[7];
  // [support vector model]
  const mxArray * _svcoef 	= prhs[8];
  const mxArray * _svhist 	= prhs[9];
  // [parameters]
  const mxArray * _topk		= prhs[10];
  const mxArray * _nms		= prhs[11];
  const mxArray * _gamma	= prhs[12];

  // check arguments that should always be there
  // nonempty
  if (mxIsEmpty(_kernel))	err("kernel must be non-empty");
  if (mxIsEmpty(_nlv))		err("nlv must be non-empty");
  if (mxIsEmpty(_inthist))	err("inthist must be non-empty");
  if (mxIsEmpty(_step))		err("step must be non-empty");
  if (mxIsEmpty(_imwh))		err("imwh must be non-empty");
  if (mxIsEmpty(_xgrid))	err("xgrid must be non-empty");
  if (mxIsEmpty(_ygrid))	err("ygrid must be non-empty");
  if (mxIsEmpty(_bbsizes))     	err("bbsizes must be non-empty");
  // type
  if (!mxIsChar(_kernel))	err("kernel type must be string.");
  if (!mxIsUint32(_nlv))	err("nlv must be uint32.");
  if (!mxIsDouble(_inthist))	err("inthist must be double.");
  if (!mxIsUint32(_step))	err("step must be uint32");
  if (!mxIsUint32(_imwh))	err("imwh must be uint32.");
  if (!mxIsUint32(_xgrid))	err("xgrid must be uint32");
  if (!mxIsUint32(_ygrid))	err("ygrid must be uint32");
  if (!mxIsUint32(_bbsizes))	err("bbsizes must be uint32.");
  // size
  if (mxGetNumberOfDimensions(_inthist) != 3)	
    err("inthist must be 3-dimensional");
  if (mxGetM(_bbsizes) != 2)	
    err("bbsizes must be 2-by-n.");
  // topk
  if (!mxIsEmpty(_topk) && !mxIsUint32(_topk))
    err("topk must be uint32.");
  // NMS
  if (!mxIsEmpty(_nms) && !mxIsDouble(_nms))
    err("NMS must be double.");
  // gamma
  if (!mxIsEmpty(_gamma) && !mxIsDouble(_gamma))
    err("gamma must be double.");

  // different operating modes
  int mode = 1;
  if (mxIsEmpty(_svcoef) || mxIsEmpty(_svhist))
    {
      err("SV must be nonempty");
    }
  return mode;
}

/////////////////////////////////////////////////////////////////////////////
// get_args
/////////////////////////////////////////////////////////////////////////////

// get input arguments
void get_args(int nrhs, const mxArray* prhs[], Params& para, const int mode)
{
  const mxArray * _kernel 	= prhs[0];
  const mxArray * _nlv	 	= prhs[1];
  const mxArray * _inthist	= prhs[2];
  const mxArray * _step 	= prhs[3];
  const mxArray * _imwh		= prhs[4];
  const mxArray * _xgrid 	= prhs[5];
  const mxArray * _ygrid 	= prhs[6];
  const mxArray * _bbsizes 	= prhs[7];
  // [support vector model]
  const mxArray * _svcoef 	= prhs[8];
  const mxArray * _svhist 	= prhs[9];
  // [parameters]
  const mxArray * _topk		= prhs[10];
  const mxArray * _nms		= prhs[11];
  const mxArray * _gamma	= prhs[12];

  char kstr[64] = "test";
  int nlv 	= (int)mxGetScalar(_nlv);
  int* sz_inthist = (int*)mxGetDimensions(_inthist);
  //int imh 	= sz_inthist[0]; // kengdie!
  //int imw 	= sz_inthist[2]; // kengdie!
  int nvw 	= sz_inthist[4]; // kengdie!
  int step 	= (int)mxGetScalar(_step);
  int* imwh     = (int*)mxGetData(_imwh);
  int imw	= imwh[0];
  int imh	= imwh[1];
  int nxgrid  	= mxGetNumberOfElements(_xgrid);
  int nygrid  	= mxGetNumberOfElements(_ygrid);
  int* xgrid 	= (int*)mxGetData(_xgrid);
  int* ygrid 	= (int*)mxGetData(_ygrid);
  int nsize	= mxGetN(_bbsizes);
  int* bbsizes 	= (int*)mxGetData(_bbsizes);
  int dsp 	= nvw * (exp2(2*nlv) - 1) / 3;

  //////////////////////////////////////////////////////////
  // TOPK
  if (!mxIsEmpty(_topk))
    TOPK = (int)mxGetScalar(_topk);

  //////////////////////////////////////////////////////////
  // feature grid
  xpos.clear();
  ypos.clear();
  for (int x = 0; x < nxgrid; x++)
    {
      xpos.push_back(xgrid[x]);
    }
  for (int y = 0; y < nygrid; y++)
    {
      ypos.push_back(ygrid[y]);
    }
  sort(xpos.begin(), xpos.end());
  sort(ypos.begin(), ypos.end());

  //////////////////////////////////////////////////////////
  // update para
  //
  //===== kernel ===== 
  mxGetString(_kernel, kstr, 64);
  if (kstr[0] == 'n' || kstr[0] == 'r')
    para.normalize = true;
  else
    para.normalize = false;

  //=====  =====
  para.imh = (int)imh;
  para.imw = (int)imw;
  para.dsp = (int)dsp;

  //===== search space related =====
  para.bbsizes.clear();
  for (int b = 0; b < nsize; b++)
    {
      // NOTE: w comes before h
      int w = bbsizes[2*b+0];
      int h = bbsizes[2*b+1];
      if (h > imh || w > imw || h < 1 || w < 1)
	{
	  mexPrintf("omitted: BOX #%d OUT OF BOUNDS: wh[%dx%d], imwh[%dx%d]\n", 
		    b+1, w, h, imw, imh);
	  //err("problem with box size", false);
	}
      else
	para.bbsizes.push_back(bbsize(w, h)); 
    }

  //===== y_gt =====
  vector<Box> bb(0);
  //if (mode != 1)
  //    {
  //      double * ygt = (double*)mxGetPr(_ygt);
  //      int ngt = mxGetN(_ygt);
  //      for (int y = 0; y < ngt; y++)
  //	{
  //	  float l = ygt[y*4+0];	 
  //	  float t = ygt[y*4+1];
  //	  float r = ygt[y*4+2];	 
  //	  float b = ygt[y*4+3];
  //	  if (l >= r || t >= b || l < 1 || t < 1 || r > imw || b > imh)
  //	    {
  //	      mexPrintf("\nGT LABEL #%d ERROR: l=%g r=%g t=%g b=%g | imh=%d imw=%d\n", 
  //			y+1, l, r, t, b, imh, imw);
  //	      err("problem with gt label", false);
  //	    }
  //	  bb.push_back(Box(l, t, r, b));
  //	}
  //    }

  //===== SVs =====
  int nsv0 = 0;
  int nsv1 = 0;
  double * sv0coef = NULL;
  double * sv0hist = NULL;
  double * sv1coef = NULL;
  double * sv1hist = NULL;
  //  if (mode == 3)
  //    {
  //      if (mxGetM(_sv0hist) != dsp)
  //	err("SV0 dims don't match inthist+nlv");
  //      nsv0 	= mxGetN(_sv0hist);
  //      sv0coef 	= mxGetPr(_sv0coef);
  //      sv0hist 	= mxGetPr(_sv0hist);
  //    }
  if (mode > 0)
    {
      if (mxGetM(_svhist) != dsp)
	err("SV dims don't match inthist+nlv");
      nsv1 	= mxGetN(_svhist);
      sv1coef 	= mxGetPr(_svcoef);
      sv1hist 	= mxGetPr(_svhist);
    }

  //===== TH_NMS =====
  if (!mxIsEmpty(_nms))
    {
      double th = mxGetScalar(_nms);
      if (th <= 0)
	err("TH_NMS not positive");
      TH_NMS = th;
    }

  //===== gamma =====
  double gamma = 2;
  if (!mxIsEmpty(_gamma))
    {
      double g = mxGetScalar(_gamma);
      if (g <= 0)
	err("gamma not positive");
      gamma = g;
    }

  //////////////////////////////////////////////////////////
  // finally, set up quality bound class
  //
  //mexPrintf("going into bnbQuality::init()\n");
  para.quality.init(kstr, _inthist, step, imw, imh, nvw, nlv, dsp, bb,
		    nsv0, sv0coef, sv0hist, nsv1, sv1coef, sv1hist, 
		    gamma);
  //mexPrintf("bnbQuality::init() returned\n");
}


