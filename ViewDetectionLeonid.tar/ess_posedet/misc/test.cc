/*
 * =====================================================================================
 *
 *       Filename:  test.cc
 *
 *    Description:  test
 *
 *        Version:  1.0
 *        Created:  04/07/2013 11:31:36 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#include <iostream>
#include <vector>
using namespace std;

class A{
public:
  int dhist;
  A(){}
  A(int d) { dhist = d; }
  // these depend on specific kernel and normalization scheme
  virtual void disp(const vector<double>& sv)=0;
    //{cout << "A" << endl;}
};

class B: public A
{
public:
  B(){}
  B(int d) { dhist = d; }
  // these depend on specific kernel and normalization scheme
  void disp(const vector<double>& sv);
};

void B::disp(const vector<double>& sv)
{
  for (int i = 0; i < sv.size(); i++)
    cout << sv[i] << " ";
  cout << endl;
}

int main()
{
  //A aa(5233);
  A * obj;
  obj = new B(5233);
  //B * bb = static_cast<B*>(&aa);
  double v[] = {5, 2, 3, 3};
  obj->disp(vector<double>(4, 3.0));
  vector<A*> avec(0);
  for (int i = 0; i < 5; i++)
    avec.push_back(obj);
  for (int i = 0; i < 5; i++)
    avec[i]->disp(vector<double>(4, 3.0));
  delete obj;
  return 0;
}

