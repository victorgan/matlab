/*
 * =============================================================================
 *
 *       Filename:  ess_kun.cc
 *
 *    Description:  modification of original ESS 
 *                  to return top K results more efficiently
 *
 *        Version:  1.0
 *        Created:  03/20/2013 11:10:32 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Kun He
 *   Organization:  Boston University
 *
 * =============================================================================
 */
#include <cassert>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <string>
#include <cstdlib>

#include "ess_kun.hh"
#include "quality_pyramid_withloss.hh"

#define MAXDATAPOINTS 100000 // ad hoc limits
#define MAXWIDTH 8192
#define MAXHEIGHT 8192
#define MAXCLUSTERS 100000

// [kun]
static int batchsize = 1;
static int HeapPop = 1;
static int maxsplit = 0;
static double TH_NMS = .75;
//
static int verbose = 100;
static int maxresults = 1;
static int numlevels = 1;
// terminate after this number of iterations
// if this happens, ESSstruct might complain about non-optimality
static int maxiterations = 10000000;

// Here we chose the class to calculate quality bounds for us.
// It has to have at least the interface of the QualityFunction class.
//
// We use 'PyramidQualityFunction', because it's flexible.
// We use 'BoxQualityFunction' is a little easier to set up.
static PyramidQualityWithLossFunction quality_bound;

// parse an int value from env variable
static int igetenv(const char* name, int defaultvalue, int low, int high)
{
  int val = defaultvalue;
  if (getenv(name))
    return atoi(getenv(name));
  if (val < low)
    val = low;
  else if (val > high)
    val = high;
  return val;
}

// convenience function to control the behaviour through environment variables
static void set_parameters()
{
  maxresults = igetenv("maxresults", 1, 1, 10000);
  numlevels = igetenv("numlevels", 1, 1, 100);
  maxiterations = igetenv("iterations", 1, 100000000, 100000000);
  verbose = igetenv("verbose", 0, 0, 100000000);
  //
  batchsize = igetenv("batchsize", 1, 1, 100);
  HeapPop = igetenv("HeapPop", 1, 1, 100000);
  maxsplit = igetenv("maxsplit", 0, 0, 10);
  return;
}

// central routine during branch-and-bound search:
// 1) extract the most promising candidate region
// 2) split it, if necessary
// 3) calculate upper bounds for the parts
// 4) re-insert the parts
static int extract_split_and_insert(sstate_heap * pH)
{
  // step 1) find the most promising candidate region
  const sstate * curstate = pH->top();

  // step 2a) check if the stop criterion is reached
  const int splitindex = curstate->maxindex(maxsplit);
  if (splitindex < 0)
    return -1;    // no more splits => convergence

  // step 2b) otherwise, create two new states as copies of the old, 
  // except for splitindex
  pH->pop();
  sstate * newstate0 = new sstate(*curstate);
  newstate0->high[splitindex] = 
    (curstate->low[splitindex] + curstate->high[splitindex]) >> 1;

  sstate * newstate1 = new sstate(*curstate);
  newstate1->low[splitindex] = 
    (curstate->low[splitindex] + curstate->high[splitindex] + 1) >> 1;

  // the old state isn't needed anymore
  delete curstate;
  curstate = NULL;

  // step 3&4) calculate upper bounds for the parts and reinject them
  if ( newstate0->islegal() )
    {
      newstate0->upper = quality_bound.upper_bound(newstate0);
      pH->push(newstate0);
    }
  if ( newstate1->islegal() )
    {
      newstate1->upper = quality_bound.upper_bound(newstate1);
      pH->push(newstate1);
    }

  // no error, but also no convergence, yet
  return 0;
}

///////////////////////////////////////////////////////////////////////////
// NMS
inline double area_ov(const Box& b1, const Box& b2)
{
  int l = std::max(b1.left, b2.left), t = std::max(b1.top, b2.top);
  int r = std::min(b1.right, b2.right), b = std::min(b1.bottom, b2.bottom);
  if (l >= r || t >= b)
    return 0;
  else
    return (r - l) * (b - t);
}

inline double overlap_ratio(const Box& b1, const Box& b2)
{
  double a1 = (b1.right - b1.left) * (b1.bottom - b1.top);
  double a2 = (b2.right - b2.left) * (b2.bottom - b2.top);
  double aint = area_ov(b1, b2);
  return aint / (a1 + a2 - aint);
}

int do_NMS(std::vector<Box>& boxes, Box& bb, const double th = TH_NMS)
{
  for (int i = 0; i < boxes.size(); i++)
    {
      if (overlap_ratio(boxes[i], bb) > th)
	{
	  std::cout << "overlap with #" << i << " > " << th << std::endl;
	  return 1;
	}
    }
  boxes.push_back(bb);
  return 0;
}

///////////////////////////////////////////////////////////////////////////

// entry site for 'find_most_violated_constraint' in structured search
// it adds the loss-function in scoring the boxes
//
// INPUT: int argnumpoints    : number of data points
//        int width, height   : width and height of image
//        double* argxpos     : x-coordinate of every point
//        double* argypos     : y-coordinate of every point
//        double* argclst     : cluster ID of each point (starts at 0)
//        int argnumclusters  : number of clusterIDs
//        int argnumlevels    : number of levels in the pyramid
//        double* weightsdata : vector of cluster weights
//        int num_gt_boxes    : number of ground truth boxes to use in loss
//        double* gt_box      : coordinates of ground truth boxes in 1D vector
//
// OUTPUT: Box outputBox      : box in [left,top,right,bottom,score] format

void pyramid_search_structured(std::vector<Box>& boxes, int K, 
			       int argnumpoints, int argwidth, int argheight,
			       double* argxpos, double* argypos,
			       double* argclst,
			       int argnumclusters, int argnumlevels,
			       double* argweight,
			       int num_gt_boxes, double* gt_box)
{
  Box outputBox = { -1, -1, -1, -1, -1.};
  argwidth += 1; // make space for 1 pixel padding
  argheight += 1;

  // set up structure for pyramid grid parameters
  // TODO: find a nicer way to handle the variable number of parameters
  const int numcells = argnumlevels * (argnumlevels + 1) * (2 * argnumlevels + 1) / 6;

  PyramidWithLossParameters paramstruct;
  paramstruct.numlevels = argnumlevels;
  paramstruct.weightptr = new double*[numcells];
  for (unsigned int i = 0; i < numcells; i++)
    {
      paramstruct.weightptr[i] = &argweight[i * argnumclusters];
    }
  paramstruct.num_gt_boxes = num_gt_boxes;
  paramstruct.gt_boxes = gt_box;

  // set up everything needed to calculate qualities and bounds
  quality_bound.setup(argnumpoints, argwidth, argheight, argxpos, argypos,
		      argclst, &paramstruct);
  delete [] paramstruct.weightptr;

  // intialize the search space (start with full image)
  // push first box set into priority queue
  sstate * fullspace = new sstate(argwidth, argheight);
  sstate_heap H;
  H.push(fullspace);

  while (!H.empty() && K--)
    {
      // main loop. Iterate extract/split/evaluate/reinsert 
      // until convergence or forced exit
      long counter = 1;
      while ((extract_split_and_insert(&H) >= 0) && (counter < maxiterations))
	{
	  if (verbose)
	    {
	      if ((counter % verbose) == 0)
		{
		  const sstate *curmax = H.top();
		  std::cerr << "#counter " << std::setw(8) << counter;
		  std::cerr << " heapsize " << std::setw(8) << H.size();
		  std::cerr << " <" << std::setw(4) << curmax->upper << " > ";
		  std::cerr << curmax->tostring() << std::endl;
		}
	    }
	  counter++;
	}
      // at convergence or error, return result or best guess
      const sstate* curstate = H.top();
      // remove padding
      outputBox.left   = ((curstate->low[0] + curstate->high[0]) >> 1) - 1;
      outputBox.top    = ((curstate->low[1] + curstate->high[1]) >> 1) - 1;
      outputBox.right  = ((curstate->low[2] + curstate->high[2]) >> 1) - 1;
      outputBox.bottom = ((curstate->low[3] + curstate->high[3]) >> 1) - 1;
      outputBox.score  = curstate->upper;
      //	  // [kun] insert into result list
      //	  boxes.push_back(outputBox);
      //	  // [kun] remove top solutions and re-(warm)start
      //	  int reduce = (H.size() > 2*HeapPop) ? HeapPop : H.size()/4;
      //	  for (int test = 0; test < reduce; test++)
      //	    {
      //	      delete H.top();
      //	      H.pop();
      //	    }
      K += do_NMS(boxes, outputBox);
    }

  // Free memory of the states that are still in the queue
  while (!H.empty())
    {
      delete H.top();
      H.pop();
    }

  // generic function to free any internal resource
  quality_bound.cleanup();

  //return outputBox;
  return;
}


// main entry site for efficient subwindow search.
// performs preprocessing and then branch-and-bound
// We make it "extern C", so it's easier to call e.g. from Python
//
// INPUT: int argnumpoints,     : number of data points
//        int width, height     : width and height of image
//        double* argxpos,      : x-coordinate of every point
//        double* argypos,      : y-coordinate of every point
//        double* argclst       : cluster ID of each point (starts at 0)
//        int argnumclusters,   : number of clusterIDs
//        int argnumlevels,     : number of levels in the pyramid
//        double* weightsdata   : vector of cluster weights
// OUTPUT: Box outputBox        : box in [left,top,right,bottom,score] format
//
// [kun] ADDED INPUT: 
//  std::vector<Box>& boxes : vector of top boxes
//                    int K : number of top boxes to return

void pyramid_search(std::vector<Box>& boxes, int K, 
		    int argnumpoints, int argwidth, int argheight,
		    double* argxpos, double* argypos, double* argclst,
		    int argnumclusters, int argnumlevels, double* argweight)
{

  pyramid_search_structured(boxes, K, argnumpoints, argwidth, argheight,
			    argxpos, argypos, argclst, argnumclusters,
			    argnumlevels, argweight, 0, NULL);
  return;
}


#ifdef __MAIN__

static void usage(char *progname)
{
  std::cerr << "usage: " << progname << " width height weight-file data-file\n";
  exit(1);
}

// helper routine to read ASCII data files
// INPUT: filename: name of input file
// INPUT/OUTPUT: data: collection of vectors to be filled
static int readdata_NxM(const char* filename, std::vector< std::vector<double> > &data)
{

  const unsigned int numcolumns = data.size();

  // start with empty vectors
  for (unsigned int i = 0; i < numcolumns; i++)
    {
      data[i].clear();
    }

  std::ifstream infile(filename);
  if (!infile)
    return -1;

  while (! infile.eof() )
    {
      for (unsigned int i = 0; i < numcolumns; i++)
	{
	  double tmpval;
	  infile >> tmpval;
	  data[i].push_back(tmpval);
	}
    }
  infile.close();

  // usually, eof occurs too late: we have read one extra entry
  for (unsigned int i = 0; i < numcolumns; i++)
    data[i].pop_back();

  // all vector should have same length
  const int numpts = data[0].size();
  return numpts;
}


int main(int argc, char* argv[])
{
  if (argc < 5)
    usage(argv[0]);

  set_parameters();
  sstate_heap H;

  // first two arguments are width and height.
  const int width = atoi(argv[1]);
  const int height = atoi(argv[2]);
  if ((width < 2) || (width > MAXWIDTH) || (height < 2) || (height > MAXHEIGHT))
    usage(argv[0]);

  // read weight for clusters (1 column)
  std::vector< std::vector<double> > weightdata(1);
  int numweights = readdata_NxM(argv[3], weightdata);
  if (numweights <= 0)
    {
      std::cerr << "Error reading data from file " << argv[3] << std::endl;
      usage(argv[0]);
    }

  const int numcells = numlevels * (numlevels + 1) * (2 * numlevels + 1) / 6; // = 1+2^2+...+n^2
  const int numclusters = numweights / numcells;
  if (numclusters > MAXCLUSTERS)
    {
      std::cerr << "Can't handle that many clusters" << std::endl;
      usage(argv[0]);
    }

  // read 3-column data file in format x,y,clusterID
  std::vector< std::vector<double> > rawdata(3);
  int datapts = readdata_NxM(argv[4], rawdata);
  if (datapts <= 0)
    {
      std::cerr << "Error reading data from file " << argv[4] << std::endl;
      usage(argv[0]);
    }

  // convert from Nx3 matrix to individual vectors. Not very elegant...
  double* xpos = &rawdata[0][0];
  double* ypos = &rawdata[1][0];
  double* clst = &rawdata[2][0];
  double* weights = &weightdata[0][0];

  // do the search and return top batchsize results
  // without non-maximal suppression
  std::vector<Box> boxes(0);
  //int HeapShrink = 4;
  std::cout << std::endl;
  pyramid_search(boxes, maxresults, //HeapShrink, 
		 datapts, width, height, xpos, ypos, clst,
		 numclusters, numlevels, weights);
  for (int r = 0; r < boxes.size(); r++)
    {
      std::cout << std::setprecision(12) << boxes[r].score << " ";
      std::cout << boxes[r].left << " ";
      std::cout << boxes[r].top << " ";
      std::cout << boxes[r].right << " ";
      std::cout << boxes[r].bottom << std::endl;
    }
  /* 
     std::vector<Box> batch(0);
     int left = maxresults;
     while (left > batchsize)
     {
     batch.clear();
     pyramid_search(batch, batchsize, datapts, width, height, xpos, ypos, clst,
     numclusters, numlevels, weights);
     boxes.insert(boxes.end(), batch.begin(), batch.end());
     left -= batchsize;
  // before searching for next boxes, zero out the region of the box found.
  // because removing elements from an array is cumbersome, 
  // create a new one from only points outside the box
  std::vector< std::vector<double> > tmpdata(3);
  for (int i = 0; i < datapts; i++)
  {
  bool skip = false;
  for (int j = 0; j < batch.size(); j++)
  {
  Box box = batch[j];
  if  ((xpos[i] >= box.left) && (xpos[i] <= box.right)
  && (ypos[i] >= box.top) && (ypos[i] <= box.bottom))
  {
  skip = true;
  break;
  }
  }
  if (!skip)
  {
  tmpdata[0].push_back(xpos[i]);
  tmpdata[1].push_back(ypos[i]);
  tmpdata[2].push_back(clst[i]);
  }
  }
  datapts = tmpdata[0].size();
  for (int i = 0; i < 3; i++)
  {
  rawdata[i].resize(datapts);
  std::copy(tmpdata[i].begin(), tmpdata[i].end(), rawdata[i].begin());
  }
  for (int r = 0; r < batch.size(); r++)
  {
  std::cout << std::setprecision(12) << batch[r].score << " ";
  std::cout << batch[r].left << " ";
  std::cout << batch[r].top << " ";
  std::cout << batch[r].right << " ";
  std::cout << batch[r].bottom << std::endl;
  }
  std::cout.flush();
  }
  if (left > 0)
  {
  batch.clear();
  pyramid_search(batch, left, datapts, width, height, xpos, ypos, clst,
  numclusters, numlevels, weights);
  boxes.insert(boxes.end(), batch.begin(), batch.end());
  for (int r = 0; r < batch.size(); r++)
  {
  std::cout << std::setprecision(12) << batch[r].score << " ";
  std::cout << batch[r].left << " ";
  std::cout << batch[r].top << " ";
  std::cout << batch[r].right << " ";
  std::cout << batch[r].bottom << std::endl;
  }
  } */
  return 0;
}
#endif

