/*
 * ==========================================================================
 *    Description:  MEX'd efficient subwindow search
 *                  for joint detection and viewpoint estimation
 *        Created:  11/18/2013 11:08:10 AM EST
 *         Author:  Kun He
 *   Organization:  Boston University
 * ==========================================================================
 * search jointly for the best object bbox and viewpoint in a test image, 
 * perform either standard (testing) or loss-augmented (training) inference
 * 
 * sample-a-box is not supported 
 * ==========================================================================
 * INPUT
 * Kx      - string, x kernel type
 * inthist - H * W * K double
 *           H: image height, W: image width, K: # of visual words
 * nlv     - uint32, # levels in spatial pyramid
 * imwh    - size of original image (not available through inthist!)
 * xgrid   - uint32 vec, x coordinates of feature points
 * ygrid   - uint32 vec, y coordinates of feature points
 * bbsizes - 2*nsize uint32, all the boxes sizes to search for
 * vps     - 1*nvp or nvp*1 double, all the viewpoints to search for
 * ygt     - [empty OK] ground truth [l,t,r,b,vpid] 5-dim
 * W       - D*nvp double, support vector coefficients
 * topk    - uint32, # of labels to return, default 1
 * nms_th  - double, NMS threshold, default 0.9
 * alpha   - double, combining localization & vp costs, default 0.5
 * lambda  - double, vp_loss = min(1, lambda * vp_diff_0to1)
 *
 * OUTPUT
 * ybar   - [l,t,r,b,vpid]*K
 * viobar - 1*K, margin violation by ybar
 * mbar   - 1*K, margin associated with ybar
 * hist   - D*K, histogram (spatial pyramid) for ybar
 * gtid   - 1*K, gt ids associated with max violation
 */

// TODO:
// - [OK] SVs must be nonempty all the time
// - [OK] check (ygt==[]) to distinguish training/testing
// - [OK] for training, allocate space for ybar, viobar, mar & phi
// - [OK] make nms_th input (copy from ess_topk)
// - [OK] use loss_locvp in NMS
// - [OK] add input argument for scaling vp loss
//
#include <stdint.h>
#include <cmath>
#include <cstring>
#include <string>
#include <vector>
#include <algorithm>
#include "mex.h"
#include "matrix.h"
#include "newdelete.h"  // let Matlab manage new/delete
#include "ess_posedet.hh"
#include "quality_posedet.hh"

typedef uint32_t int32;
std::vector<int> xpos;
std::vector<int> ypos;
std::vector<int> xpos_aug;
std::vector<int> ypos_aug;
std::vector<double> vps;

int    MINSPLIT = 1;  // 0 for exact inference, >0 for approximate
int    VERBOSE  = 10000;
int    ITERMAX  = 1000000;
int    TOPK     = 1;
int    rejected = 0;
double alpha    = 0.5;
double lambda   = 1.0;
double TH_NMS   = 0.1;

static sstate_heap H;
double LB_global;

///////////////////////////////////////////////////////////////////////////
// NMS
double loss_overlap(const Box& b1, const Box& b2)
{
  double aint = area_ov(b1, b2);
  return 1 - aint / (area_ov(b1, b1) + area_ov(b2, b2) - aint);
}
double loss_viewpt(double v1, double v2)
{
  // cost based on viewpoint label difference
  double dvp = std::abs(v1 - v2);
  dvp = std::min(dvp, 2*PI-dvp);
  return std::min(lambda*dvp/PI, 1.0);
}
int do_NMS(std::vector<Box>& boxes, Box& bb)
{
  for (int i = 0; i < boxes.size(); i++)
    {
      // measure difference in terms of loss
      double diff = alpha * loss_overlap(boxes[i], bb) 
	+ (1-alpha) * loss_viewpt(boxes[i].vp, bb.vp); 
      if ( diff < TH_NMS )
	{
	  ++ rejected;
	  return 1;
	}
    }
  boxes.push_back(bb);
  return 0;
}
///////////////////////////////////////////////////////////////////////////
void print_top(int count)
{
  const sstate * curmax = H.top();
  mexPrintf("#%5d |H|=%5d %s", count, H.size(), curmax->tostring().c_str());
}

// central routine during branch-and-bound search:
int extract_split_insert(bnbQuality & qual, bool global)
{
  // 1) find most promising candidate region, check stop criterion
  // 2) split and create two new states
  const sstate * cur = H.top();
  sstate * newstate[2];
  if (cur->px[1] - cur->px[0] > MINSPLIT && 
      cur->px[1] - cur->px[0] >= cur->py[1] - cur->py[0])
    {
      // split x
      H.pop();
      for (int x = 0; x < 2; x++)
	{
	  sstate * s  = new sstate(*cur);
	  s->px[x]    = (cur->px[0] + cur->px[1] + 2-2*x) >> 1;
	  s->l[x]     = xpos_aug[s->px[x]];
	  newstate[x] = s;
	}
    }
  else if (cur->py[1] - cur->py[0] > MINSPLIT)
    {
      // split y
      H.pop();
      for (int y = 0; y < 2; y++)
	{
	  sstate * s  = new sstate(*cur);
	  s->py[y]    = (cur->py[0] + cur->py[1] + 2-2*y) >> 1;
	  s->t[y]     = ypos_aug[s->py[y]];
	  newstate[y] = s;
	}
    }
  else 
    return -1;

  // 3) calculate bounds for new states and reinsert them
  //    with some pruning
  for (int i = 0; i < 2; i++)
    {
      qual.compute_bounds(newstate[i]);

      // test: scale UB to prefer splitting large states
      //      int dy = newstate[i]->py[1] + newstate[i]->py[1] - MINSPLIT + 1;
      //      int dx = newstate[i]->px[1] + newstate[i]->px[1] - MINSPLIT + 1;
      //      newstate[i]->upper /= sqrt(sqrt(dy*dx));

      if (global) // use global LB for first result
	{
	  if (newstate[i]->upper >= LB_global)
	    H.push(newstate[i]);
	}
      else // next results: just check the top
	{
	  if (newstate[i]->upper >= H.top()->lower)
	    H.push(newstate[i]);
	}
      if (LB_global < newstate[i]->lower)
	LB_global = newstate[i]->lower;
    }

  // 4) no error, but also no convergence, yet
  delete cur;
  cur = NULL;
  return 0;
}

/////////////////////////////////////////////////////////////////////////////
// pyaramid_search
// implicitly handles with-or-without loss
/////////////////////////////////////////////////////////////////////////////

int pyramid_search(int K, Params & para, std::vector<Box>& boxes, 
		   std::vector<int>& gtids, std::vector<double>& deltas, 
		   std::vector<Vec>& hists, bool get_hists)
{
  // push initial states into heap
  for (int b = 0; b < para.bbsizes.size(); b++)
    {
      int ww   = para.bbsizes[b].w;
      int hh   = para.bbsizes[b].h;
      int pxlo = 0, pxhi = 0;
      int pylo = 0, pyhi = 0;
      for (; pxhi+1 < xpos_aug.size() && xpos_aug[pxhi+1] <= para.imw-ww; pxhi++);
      for (; pyhi+1 < ypos_aug.size() && ypos_aug[pyhi+1] <= para.imh-hh; pyhi++);
      int xlo  = xpos_aug[pxlo],  xhi  = xpos_aug[pxhi];
      int ylo  = ypos_aug[pylo],  yhi  = ypos_aug[pyhi];
      for (int v = 0; v < vps.size(); v++)
	{
	  sstate * subspace = 
	    new sstate(ww, hh, pxlo, pxhi, pylo, pyhi, xlo, xhi, ylo, yhi, v);
	  para.quality.compute_bounds(subspace);
	  //	  if (LB_global < subspace->lower)
	  LB_global = (v==0) ? subspace->lower : std::max(LB_global, double(subspace->lower));
	  H.push(subspace);
	}
    }
  // return top K boxes
  long globalcount = 0;
  while (!H.empty() && K--)
    {
      // do branch-and-bound search
      int count = 1;
      while (extract_split_insert(para.quality, boxes.empty()) >= 0 && count < ITERMAX)
	{
	  if (VERBOSE && count % VERBOSE == 0)
	    print_top(globalcount + count);
	  count++;
	}
      globalcount += count;

      // at convergence or error, return result or best guess
      const sstate * cur = H.top();
      Box outbox;
      outbox.l     = (cur->l[0] + cur->l[1]) >> 1;
      outbox.t     = (cur->t[0] + cur->t[1]) >> 1;
      outbox.r     = outbox.l + cur->w;
      outbox.b     = outbox.t + cur->h;
      outbox.score = cur->upper;
      outbox.vp    = vps[cur->vpid];

      // do NMS
      int ret = do_NMS(boxes, outbox);
      K += ret;
      if (ret == 0 && get_hists) // outbox accepted && it's during training
	{
	  print_top(globalcount);
	  Vec hist(para.dsp);
	  para.quality.get_output_hist(hist, outbox);
	  hists.push_back(hist);
	  gtids.push_back(cur->maxgtid + 1);
	  if (MINSPLIT)
	    {
	      sstate s(outbox);

	      s.vpid = cur->vpid; //NOTE: important since outbox doesn't have vpid

	      para.quality.compute_bounds(&s, false);
	      boxes[boxes.size()-1].score = s.upper;
	      deltas.push_back(s.Delta);
	    }
	  else
	    deltas.push_back(cur->Delta);
	}
      // pop
      delete cur;
      H.pop();
    }

  // clean up and return
  while (!H.empty())
    {
      delete H.top();
      H.pop();
    }
  return globalcount;
}

/////////////////////////////////////////////////////////////////////////////
// mexFunction
/////////////////////////////////////////////////////////////////////////////

void err(const char * msg, bool usage = true)
{
  if (usage)
    {
      mexPrintf("Usage:\n[y, vio, mar, psi, j] = ess_viewdet(\n");
      mexPrintf("    kernel, inthist, nlv, step, imwh, xgrid, ygrid, bbsizes, vps,\n");
      mexPrintf("    [y_gt], svcoef, svhist, svy,\n");
      mexPrintf("    [topk], [nms_th], [alpha], [lambda]\n)\n");
    }
  mexErrMsgTxt(msg);
}
int check_args(int nlhs, int nrhs, const mxArray * prhs[]);
void get_args(int nrhs, const mxArray* prhs[], Params& para, const int mode);

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  // check input arguments
  int mode = check_args(nlhs, nrhs, prhs);
  if (mode!=0 && mode!=1)
    err("incorrect working mode");

  // get input arguments
  Params para;
  get_args(nrhs, prhs, para, mode);

  // create arrays for output
  plhs[0] = mxCreateDoubleMatrix(5, TOPK, mxREAL);
  plhs[1] = mxCreateDoubleMatrix(1, TOPK, mxREAL);
  double * detlabel = mxGetPr(plhs[0]);	 // y_bar
  double * detscore = mxGetPr(plhs[1]);	 // violation (train) || score (test)
  double * detmargn, * detfeatv, * detgtid;
  if (mode)
    {
      plhs[2]  = mxCreateDoubleMatrix(1, TOPK, mxREAL);
      plhs[3]  = mxCreateDoubleMatrix(para.dsp, TOPK, mxREAL);
      plhs[4]  = mxCreateDoubleMatrix(1, TOPK, mxREAL);
      detmargn = mxGetPr(plhs[2]);  // \Delta(y_gt, y_bar) or -1
      detfeatv = mxGetPr(plhs[3]);  // phi_bar
      detgtid  = mxGetPr(plhs[4]);  // gtid
    }
  else
    {
      plhs[2] = mxCreateDoubleMatrix(0, 0, mxREAL);
      plhs[3] = mxCreateDoubleMatrix(0, 0, mxREAL);
      plhs[4] = mxCreateDoubleMatrix(0, 0, mxREAL);
    }

  // ps_ for "pyramid search"
  std::vector<Box>    ps_box; 
  std::vector<int>    ps_gtid;
  std::vector<double> ps_mar;
  std::vector<Vec>    ps_hist;
  int iter = pyramid_search(TOPK, para, ps_box, ps_gtid, ps_mar, ps_hist, mode>0);
  para.quality.print_gt();

  // write output and clean up
  // TODO: what if TOPK != outbox.size() ???
  // will create problem for SVM on the outside
  mxAssert(ps_box.size() >= TOPK, "ps_box.size() < TOPK");
  for (int i = 0; i < TOPK && i < ps_box.size(); i++)
    {
      Box boxi = ps_box[i];
      if (mode) // loss-augmented inference
	{
	  double mar = ps_mar [i];
	  int   gtid = ps_gtid[i];
	  Vec    h_i = ps_hist[i];
	  //	  if (boxi.score < -0.1) // fix for vio<0
	  //	    {
	  //	      boxi       = para.quality.gtruth(gtid - 1);
	  //	      boxi.score = 0;
	  //	      mar        = 0;
	  //	      para.quality.get_output_hist(h_i, boxi);
	  //	    }
	  mexPrintf(" DET [%3d %3d %3d %3d %.2f] <vio=%.3f mar=%.3f>\n", 
		    boxi.l, boxi.t, boxi.r, boxi.b, boxi.vp, boxi.score, mar);
	  detmargn[i] = mar;
	  detgtid [i] = gtid;
	  double* f_i = detfeatv + i * para.dsp;
	  for (int  j = 0; j < para.dsp; j++)
	    f_i[j] = h_i[j];
	}
      else // test-time inference
	{
	  mexPrintf(" DET [%3d %3d %3d %3d %.2f] <score=%.3f>\n", 
		    boxi.l, boxi.t, boxi.r, boxi.b, boxi.vp, boxi.score);
	}
      detlabel[i*5+0] = boxi.l;
      detlabel[i*5+1] = boxi.t;
      detlabel[i*5+2] = boxi.r;
      detlabel[i*5+3] = boxi.b;
      detlabel[i*5+4] = boxi.vp;
      detscore[i] = boxi.score;
    }
}

/////////////////////////////////////////////////////////////////////////////
// check_args
/////////////////////////////////////////////////////////////////////////////

// check input arguments
int check_args(int nlhs, int nrhs, const mxArray * prhs[])
{
  if (nrhs != 14)		err("Wrong number of input arguments.");
  if (nlhs != 5)		err("Wrong number of output arguments.");

  const mxArray * _Kx 		= prhs[0];
  const mxArray * _inthist	= prhs[1];
  const mxArray * _nlv	 	= prhs[2];
  const mxArray * _imwh		= prhs[3];
  const mxArray * _xgrid 	= prhs[4];
  const mxArray * _ygrid 	= prhs[5];
  // search space related
  const mxArray * _bbsizes 	= prhs[6];
  const mxArray * _vps	 	= prhs[7];
  // [ground truth]
  const mxArray * _ygt		= prhs[8];
  // [support vector model]
  const mxArray * _W 		= prhs[9];
  // [parameters]
  const mxArray * _topk		= prhs[10];
  const mxArray * _nms_th	= prhs[11];
  const mxArray * _alpha	= prhs[12];
  const mxArray * _lambda	= prhs[13];

  // check arguments that should always be there
  // nonempty
  if (mxIsEmpty(_Kx))		err("Kx must be non-empty");
  if (mxIsEmpty(_inthist))	err("inthist must be non-empty");
  if (mxIsEmpty(_nlv))		err("nlv must be non-empty");
  if (mxIsEmpty(_imwh))		err("imwh must be non-empty");
  if (mxIsEmpty(_xgrid))	err("xgrid must be non-empty");
  if (mxIsEmpty(_ygrid))	err("ygrid must be non-empty");
  if (mxIsEmpty(_bbsizes))     	err("bbsizes must be non-empty");
  if (mxIsEmpty(_vps))		err("vps must be non-empty");
  if (mxIsEmpty(_W))		err("W must be non-empty");
  if (mxIsEmpty(_topk))		err("topk must be non-empty");
  if (mxIsEmpty(_nms_th))	err("nms_th must be non-empty");
  if (mxIsEmpty(_alpha))	err("alpha must be non-empty");
  if (mxIsEmpty(_lambda))	err("lambda must be non-empty");
  // type
  if (!mxIsChar(_Kx))		err("Kx type must be string.");
  if (!mxIsDouble(_inthist))	err("inthist must be double.");
  if (!mxIsUint32(_nlv))	err("nlv must be uint32.");
  if (!mxIsUint32(_imwh))	err("imwh must be uint32.");
  if (!mxIsUint32(_xgrid))	err("xgrid must be uint32");
  if (!mxIsUint32(_ygrid))	err("ygrid must be uint32");
  if (!mxIsUint32(_bbsizes))	err("bbsizes must be uint32.");
  if (!mxIsDouble(_vps))	err("vps must be double.");
  if (!mxIsDouble(_W))		err("W must be double.");
  if (!mxIsUint32(_topk))	err("topk must be uint32.");
  if (!mxIsDouble(_nms_th))	err("nms_th must be double.");
  if (!mxIsDouble(_alpha))	err("alpha must be double.");
  if (!mxIsDouble(_lambda))	err("lambda must be double.");
  // size
  if (mxGetNumberOfDimensions(_inthist) != 3)	
    err("inthist must be 3-dimensional");
  if (mxGetM(_bbsizes) != 2)	
    err("bbsizes must be 2-by-n.");
  if (mxGetN(_W) != mxGetNumberOfElements(_vps))
    err("#cols(W) != #viewpoints");

  // different operating modes
  // mode 0:   no GT -> inference (test time)
  // mode 1: have GT -> inference (loss-augmented)
  int mode;
  if (mxIsEmpty(_ygt))
    {
      mode = 0;
      if (VERBOSE)
	mexPrintf(" Test-time Inference");
    }
  else
    {
      mode = 1;
      if (VERBOSE)
	mexPrintf(" Loss-augmented Inference");
      int Mgt = mxGetM(_ygt); 
      int Ngt = mxGetN(_ygt);
      if (Mgt*Ngt != 5 && Mgt != 5) 
	{
	  mexPrintf("\nmxGetM(_ygt)=%d, mxGetN(_ygt)=%d\n", Mgt, Ngt);
	  err("#row(ygt) must be 5 (bb4+vp1)");
	}
    }
  return mode;
}

/////////////////////////////////////////////////////////////////////////////
// get_args
/////////////////////////////////////////////////////////////////////////////

// get input arguments
void get_args(int nrhs, const mxArray* prhs[], Params& para, const int mode)
{
  const mxArray * _Kx 		= prhs[0];
  const mxArray * _inthist	= prhs[1];
  const mxArray * _nlv	 	= prhs[2];
  const mxArray * _imwh		= prhs[3];
  const mxArray * _xgrid 	= prhs[4];
  const mxArray * _ygrid 	= prhs[5];
  // search space related
  const mxArray * _bbsizes 	= prhs[6];
  const mxArray * _vps	 	= prhs[7];
  // [ground truth]
  const mxArray * _ygt		= prhs[8];
  // [support vector model]
  const mxArray * _W 		= prhs[9];
  // [parameters]
  const mxArray * _topk		= prhs[10];
  const mxArray * _nms_th	= prhs[11];
  const mxArray * _alpha	= prhs[12];
  const mxArray * _lambda	= prhs[13];

  int* sz_inthist = (int*)mxGetDimensions(_inthist);
  int nvw 	= sz_inthist[4]; // kengdie!
  int nlv 	= (int)mxGetScalar(_nlv);
  int* imwh     = (int*)mxGetData(_imwh);
  int imw	= imwh[0];
  int imh	= imwh[1];
  int nxgrid  	= mxGetNumberOfElements(_xgrid);
  int nygrid  	= mxGetNumberOfElements(_ygrid);
  int* xgrid 	= (int*)mxGetData(_xgrid);
  int* ygrid 	= (int*)mxGetData(_ygrid);
  int nsize	= mxGetN(_bbsizes);
  int* bbsizes 	= (int*)mxGetData(_bbsizes);
  int nvp 	= mxGetNumberOfElements(_vps);
  double* pvps 	= mxGetPr(_vps);
  int dsp 	= nvw * (exp2(2*nlv) - 1) / 3;

  //////////////////////////////////////////////////////////
  // update para
  para.imh = (int)imh;
  para.imw = (int)imw;
  para.dsp = (int)dsp;
  //
  //===== kernel ===== 
  char kx[16] = "test";
  mxGetString(_Kx, kx, 16);
  if (VERBOSE)
    mexPrintf(" (%s)\n", kx);
  if (kx[0] == 'n' || kx[0] == 'r')
    para.normalize = true;
  else
    para.normalize = false;
  //
  //===== search space related =====
  para.bbsizes.clear();
  for (int b = 0; b < nsize; b++)
    {
      // NOTE: w comes before h
      int w = bbsizes[2*b+0];
      int h = bbsizes[2*b+1];
      if (h > imh || w > imw || h < 1 || w < 1)
	{
	  mexPrintf("\nBOX #%d OOB, omitted: %dx%d, im: %dx%d\n", 
		    b+1, w, h, imw, imh);
	}
      para.bbsizes.push_back(bbsize(w, h)); 
    }
  vps.clear();
  for (int v = 0; v < nvp; v++)
    {
      double vp = pvps[v];
      if (vp < 0)  err("negative viewpoint value");
      vps.push_back(vp);
    }

  //===== y_gt =====
  std::vector<Box> gtlabel(0);
  if (mode)
    {
      double * ygt = (double*)mxGetPr(_ygt);
      if (ygt == NULL)
	{
	  mexPrintf("Get pointer failure: ygt@%ld\n", ygt);
	  err("problem with pointers", false);
	}
      int ngt      = (mxGetM(_ygt)*mxGetN(_ygt)==5)? 1:mxGetN(_ygt);
      for (int y = 0; y < ngt; y++)
	{
	  // NOTE: v should be actual vp value
	  float l = ygt[y*4+0];	 
	  float t = ygt[y*4+1];
	  float r = ygt[y*4+2];	 
	  float b = ygt[y*4+3];
	  float v = ygt[y*4+4];
	  if (l >= r || t >= b || l < 1 || t < 1 || r > imw || b > imh || v < 0)
	    {
	      mexPrintf("\nGT#%d ERROR: l=%g r=%g t=%g b=%g v=%g | im: %dx%d\n", 
			y+1, l, r, t, b, v, imw, imh);
	      err("problem with gt label", false);
	    }
	  gtlabel.push_back(Box(l, t, r, b, v));
	}
    }
  //
  //===== SVs =====
  if (mxGetM(_W) != dsp)
    err("SV dims don't match inthist+nlv");
  std::vector<double*> w_ptrs(0);
  std::vector<double>  bias(0);
  double * wptr = mxGetPr(_W);
  for (int s = 0; s < nvp; s++)
    {
      w_ptrs.push_back(wptr + s*dsp); 
      bias.push_back(0);
    }
  //
  //===== feature grid =====
  xpos.clear();
  ypos.clear();
  for (int x = 0; x < nxgrid; x++)
    {
      xpos.push_back(xgrid[x]);
    }
  for (int y = 0; y < nygrid; y++)
    {
      ypos.push_back(ygrid[y]);
    }
  sort(xpos.begin(), xpos.end());
  sort(ypos.begin(), ypos.end());
  //
  //===== augment using ground truths =====
  xpos_aug = xpos;
  ypos_aug = ypos;
  for (int gt = 0; gt < gtlabel.size(); gt++)
    {
      if (find(xpos_aug.begin(), xpos_aug.end(), gtlabel[gt].l) != xpos_aug.end())
	xpos_aug.push_back(gtlabel[gt].l);
      if (find(ypos_aug.begin(), ypos_aug.end(), gtlabel[gt].t) != ypos_aug.end())
	ypos_aug.push_back(gtlabel[gt].t);
    }
  sort(xpos_aug.begin(), xpos_aug.end());
  sort(ypos_aug.begin(), ypos_aug.end());
  //
  //===== get additional inputs =====
  TOPK = (int)mxGetScalar(_topk);
  if (TOPK < 1)
    err("topk < 1");

  TH_NMS  = mxGetScalar(_nms_th);
  if (TH_NMS < 0 || TH_NMS > 1)
    err("nms_th not in [0,1]");

  alpha = mxGetScalar(_alpha);
  if (alpha < 0 || alpha > 1)
    err("alpha not in [0,1]");

  lambda = mxGetScalar(_lambda);
  if (lambda < 0.1)
    err("lambda too small (< 0.1)");

  //////////////////////////////////////////////////////////
  // finally, set up quality bound class
  //
  //mexPrintf("going into bnbQuality::init()\n");
  para.quality.init(kx, _inthist, nvw, nlv, dsp, imw, imh, gtlabel, 
		    w_ptrs, bias, 
		    alpha, lambda);
  //mexPrintf("bnbQuality::init() returned\n");
}


