/*
 * ==========================================================================
 *    Description:  MEX'd efficient subwindow search
 *                  for joint detection and viewpoint estimation
 *        Created:  03/31/2013 04:08:10 PM EST
 *         Author:  Kun He
 *   Organization:  Boston University
 * ==========================================================================
 */
#ifndef _ESS_LOCVP_HH
#define _ESS_LOCVP_HH

#include <vector>
#include "matrix.h"
#include "quality_posedet.hh"

class bbsize
{
public:
  int w;
  int h;
  bbsize(int ww, int hh) { h = hh;  w = ww; };
};

///////////////////////////////////////////////////////////////////////////////
//class Box;
//class bnbQuality;

class Params
{
public:
  // directly read from inputs
  std::vector<bbsize> bbsizes;	// collection of box dimensions
  std::vector<double> vps;	// collection of viewpoints
  //  std::vector<double> svcoef;	// support vector coefficients
  //  std::vector<double*> svhist;	// support vectors (histogram features)
  //  std::vector<Box> svy;		// labels for support vectors

  // internal paramters derived from inputs
  bool normalize;
  //int step;		// feature density
  int dsp;		// dimension of spatial pyramid
  int imh, imw;		// image dimensions
  bnbQuality quality;	// quality bound function 

  Params(){}
  ~Params(){}
};

#endif
