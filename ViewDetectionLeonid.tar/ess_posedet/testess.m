function testess
global inthist
cd('..'); addpath(pwd); addpath([pwd '/_util']);
%{
params = init_params_locvp('3dobj', 'car');
if ~exist('inthist', 'var') || isempty(inthist)
	ld = load(sprintf('%s/car/car_1_A1_H1_S1_inthist_step6_HOG6_vw500.mat', ...
		params.cdir));
	inthist = double(ld.his);
end
%}
params = init_params_voc('car', {'SIFTg', 'SIFTc'}, [300 300], 1, 10);
id = '009959';
if ~exist('inthist', 'var') || isempty(inthist)
	ld = load(sprintf('%s%s_IH4_SIFTg300SIFTc300.mat', ...
		params.cdir, id));
	inthist = double(ld.his);
end
if ~exist('ft', 'var') || isempty(ft)
	ld = load(sprintf('%s%s_step4_SIFTg.mat', ...
		params.cdir, id));
	ft = ld.ft;
end

cd('ess_kun'); ess_compile; 
%keyboard

bbhw = uint32([110; 100]);
bbhw = uint32([60:20:140; 140:-20:60]);
vps = 1:4;
gt1 = uint32([120 120 220 220]); % l t r b
gt2 = uint32([21 120 121 220]); % l t r b
gt0 = uint32([40 40 140 140]); % l t r b
L = uint32(1);
step = uint32(4);
ker = 'nlin2';
xgrid = uint32(ft.xgrid);
ygrid = uint32(ft.ygrid);
%
[~, ~, ~, hist1, ~] = ess_hingeboost(ker, L, inthist, step, ...
	uint32(ft.imhw([2,1])), xgrid, ygrid, ...
	uint32([1;1]), double([gt1])', [], [], [], [], 1);
[~, ~, ~, hist2, ~] = ess_hingeboost(ker, L, inthist, step, ...
	uint32(ft.imhw([2,1])), xgrid, ygrid, ...
	uint32([1;1]), double([gt2])', [], [], [], [], 1);
%
fprintf('[hist1] L1 norm: %g, L2 norm: %g\n', norm(hist1, 1), norm(hist1, 2));
fprintf('[hist2] L1 norm: %g, L2 norm: %g\n', norm(hist2, 1), norm(hist2, 2));

% compare with vl_sampleinthist
%{
vlhist1 = vl_sampleinthist(inthist, to_grid(gt1, xgrid, ygrid));
vlhist2 = vl_sampleinthist(inthist, to_grid(gt2, xgrid, ygrid));
vlhist1 = .01* vlhist1;
vlhist2 = .01* vlhist2;
vlhist1 = vlhist1 / norm(vlhist1, 2);
vlhist2 = vlhist2 / norm(vlhist2, 2);
max(abs(hist1 - vlhist1))
max(abs(hist2 - vlhist2))
%keyboard
%[hist2 vlhist2]
%}
tic;
[ybar sc] = ess_topk(ker, L, inthist, step, ...
	uint32(ft.imhw([2,1])), xgrid, ygrid, bbhw, ...
	1, hist2, uint32(10), 1.0, []);
ybar
sc
toc;


function k = hi(x, y)
d = length(x);
assert(d == length(y));
k = 0;
for i=1:d
	k = k + min(x(i), y(i));
end


function gbb = to_grid(bb, xgrid, ygrid)
[ax, bx] = ismember(bb([1 3]), xgrid);
[ay, by] = ismember(bb([2 4]), ygrid);
gbb = uint32([by(1) bx(1) by(2) bx(2)])
gbb = gbb';
