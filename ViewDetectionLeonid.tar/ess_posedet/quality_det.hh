/*
 * ==========================================================================
 *    Description:  MEX'd efficient subwindow search
 *                  for joint detection and viewpoint estimation
 *        Created:  03/31/2013 04:08:10 PM
 *         Author:  Kun He
 *   Organization:  Boston University
 * ==========================================================================
 *
 * various quality bounds for spatial pyramid features
 *
 * TODO: use of "const" is kinda inconsistent
 */
#ifndef _QUALITY_HH
#define _QUALITY_HH
#include <cmath>
#include <queue>
#include <vector>
#include <string>
#include <valarray>
#include "matrix.h"

const double PI = 3.1415926536;

///////////////////////////////////////////////////////////////////////////////
class Box
{
public:
  int l, t, r, b;
  double score;
  Box(){}
  Box(float a) 
    { l = t = r = b = score = a; }
  Box(int ll, int tt, int rr, int bb, double s=0)
    { l = ll;  t = tt;  r = rr;  b = bb; score = s; }
  Box(const Box* bb) 
    { l = bb->l;  t = bb->t;  r = bb->r;  b = bb->b; }
};

inline double area_ov(const Box& b1, const Box& b2)
{
  int l = std::max(b1.l, b2.l), t = std::max(b1.t, b2.t);
  int r = std::min(b1.r, b2.r), b = std::min(b1.b, b2.b);
  if (l >= r || t >= b)
    return 0;
  else
    return (r - l) * (b - t);
}

class SV
{
public:
  double coef;
  std::valarray<double> hist;
  SV(){}
  SV(const int L, const int dhist);
  SV(const int dsp);
  SV(const double a, double * ptr, const int L, 
     const int dhist, const int dsp);
};

// used to represent search states
class sstate
{
public:
  short px[2];	// trade space for time
  short py[2];
  short l[2];	// limits on x1
  short t[2];	// limits on y1
  short w, h;	// bb size

  short maxgtid;// fix for multiple ground truths

  float upper;	// score upper bound, = score for single state
  float lower;	// score lower bound, = score for single state
  float rhoUB;

  sstate() {}
  sstate(int ww, int hh, int pxlo, int pxhi, int pylo, int pyhi, 
	 int xlo, int xhi, int ylo, int yhi)
    {
      w = ww;  h = hh;  
      upper = lower = 0;
      px[0] = pxlo;
      px[1] = pxhi;
      py[0] = pylo;
      py[1] = pyhi;
      l[0] = xlo;  
      l[1] = xhi;  
      t[0] = ylo;  
      t[1] = yhi;
    }
  sstate(int ww, int hh, int xlo, int xhi, int ylo, int yhi)
    {
      w = ww;  h = hh;  
      upper = lower = 0;
      l[0] = xlo;  t[0] = ylo;  l[1] = xhi;  t[1] = yhi;
    }
  sstate(const Box& b)
    {
      l[0] = l[1] = b.l;  t[0] = t[1] = b.t;
      w = b.r - b.l;  h = b.b - b.t;
      upper = lower = b.score;
    }

  // calculate argmax_i( high[i]-low[i] ), or -1 if high==low
  // the dimension along which to split
  //  int maxindex(int maxwidth=0) const 
  //    { 
  //      int splitindex = -1;
  //      for (unsigned int i = 0; i < 2; i++) 
  //	{ 
  //	  int interval_width = hi[i] - lo[i];
  //	  if (interval_width > maxwidth) 
  //	    {
  //	      splitindex = i;
  //	      maxwidth = interval_width;
  //	    }
  //	}
  //      return splitindex;
  //    }
  // "less" is needed to compare states in stl_priority queue
  //  bool less(const sstate* other) const 
  //    { 
  //      return upper < other->upper; 
  //    }

  std::string tostring() const 
    {
      char cstring[80];
      sprintf(cstring,"(%d) <%.3f %.3f> [%3dx%3d] {%2d-%2d, %2d-%2d}\n",
	      maxgtid+1, upper, lower, w, h, l[0], l[1], t[0], t[1]);
      return std::string(cstring);
    }
};

// helper routine for comparing elements in priority queue
class sstate_comparisson
{
public:
  bool operator() (const sstate* lhs, const sstate* rhs) const
    { return (lhs->upper < rhs->upper); }
};

// data structure for priority queue
typedef std::priority_queue 
<const sstate*, std::vector<const sstate*>, sstate_comparisson> 
sstate_heap;

///////////////////////////////////////////////////////////////////////////////
// generic quality function class for spatial pyramids
// abstract class - has pure virtual functions
class spQuality
{
public:
  int imh, imw;
  int dhist, nlevels;
  int dsp;
  int normalize;
  double norm_out;
  double norm_inn;
  double gamma;
  std::vector<int> ndiv;
  // first for intersection, second for union
  std::vector<std::valarray<double> > inout; 
  //double * Hinout[2];

  spQuality() {}
  ~spQuality(){}

  void init(int imw, int imh, int d, int splv, int dsp, int nor, double gam);
  void setup(const sstate * s, const mxArray* inthist, const int step);

  void build_spatial_pyramids(int w, int h, int x1, int x2, int y1, int y2, 
			      const mxArray* inthist, const int step, const int L);
  void get_sp(std::valarray<double>& h, Box& b, const mxArray* inthist, const int);

  // these depend on specific kernel and normalization scheme
  virtual double UB(const std::valarray<double>& sv) = 0;
  virtual double LB(const std::valarray<double>& sv) = 0;
};

///////////////////////////////////////////////////////////////////////////////
// index-value pair
// aux. simple fix for multiple ground truths in an image
class ivpair
{
public:
  int idx;
  double val;
  ivpair(){}
  ivpair(int i, double v) { idx = i; val = v; }
};

// interfacing class
class bnbQuality
{
private:
  spQuality * spq;

  std::vector<Box> gtbox;
  std::vector<double> gtscore0;
  std::vector<double> gtscore1;

  std::string kernel;
  bool linear;

  std::vector<SV> sv0; // first set of SVs
  std::vector<SV> sv1; // second set of SVs
  std::valarray<double> w0; // first w
  std::valarray<double> w1; // second w
  //std::vector<std::valarray<double> > svcoef_new;
  
  const mxArray * inthist;  // integral histogram
  int step; // spacing between feature points

public:
  bnbQuality() 
    { 
      spq = NULL;
    }
  ~bnbQuality() 
    {
      if (spq != NULL)
	delete spq;
    }
  void init(std::string K, const mxArray * P, 
	    int S, int W, int H, int D, int L, int Dsv,
	    std::vector<Box> & boxes_in, 
	    int nsv0, double * sv0coef, double * sv0ptr, 
	    int nsv1, double * sv1coef, double * sv1ptr, 
	    double g_in);
  double bias0(int i) 
    { 
      if (i < 0 || gtbox.empty())
	return 0;
      else
	return -gtscore0[i]; 
    }
  double bias1(int i)
    { 
      if (i < 0 || gtbox.empty())
	return 0;
      else
	return -gtscore1[i]; 
    }
  double bias(int i)
    { 
      if (i < 0 || gtbox.empty())
	return 0;
      else
	return -gtscore0[i]-gtscore1[i]; 
    }
  Box gtruth(const int i = 0) 
    { 
      if (gtbox.empty())
	return Box(i);
      else 
	return gtbox[i];
    }
  void print_gt()
    {
      mexPrintf("\n");
      for (int i = 0; i < gtbox.size(); i++)
	{
	  Box gt = gtbox[i];
	  mexPrintf("GT#%d [%3d %3d %3d %3d] <sc0=%.3f, sc1=%.3f>\n",
		    i+1, gt.l, gt.t, gt.r, gt.b, gtscore0[i], gtscore1[i]);
	}
    }
  void get_linear_weights(const sstate * s);
  void compute_bounds(sstate* state, bool getLB=true);
  void compute_upper_bound(sstate* state);
  void compute_lower_bound(sstate* state);
  ivpair loss_lb(const sstate* s);
  ivpair loss_ub(const sstate* s);
  void get_output_hist(std::valarray<double>& h, Box& b)
    { spq->get_sp(h, b, inthist, step); }
};

///////////////////////////////////////////////////////////////////////////////

// un-normalized linear kernel
class linQuality: public spQuality
{
public:
  linQuality(){}
  double UB(const std::valarray<double>& hist);//SV& sv) ;
  double LB(const std::valarray<double>& hist);//SV& sv) ;
};

// normalized linear kernel
class nlin1Quality: public spQuality
{
public:
  nlin1Quality(){}
  double UB(const std::valarray<double>& hist);//SV& sv) ;
  double LB(const std::valarray<double>& hist);//SV& sv) ;
};

// normalized linear kernel
class nlin2Quality: public spQuality
{
public:
  nlin2Quality(){}
  double UB(const std::valarray<double>& hist);//SV& sv) ;
  double LB(const std::valarray<double>& hist);//SV& sv) ;
};

// un-normalized Histogram Intersection
class hiQuality: public spQuality
{
public:
  hiQuality(){}
  double UB(const std::valarray<double>& hist);//SV& sv) ;
  double LB(const std::valarray<double>& hist);//SV& sv) ;
};

// normalized Histogram Intersection
class nhiQuality: public spQuality
{
public:
  nhiQuality(){}
  double UB(const std::valarray<double>& hist);//SV& sv) ;
  double LB(const std::valarray<double>& hist);//SV& sv) ;
};

// un-normalized Chi-square
class chi2Quality: public spQuality
{
public:
  chi2Quality(){}
  double UB(const std::valarray<double>& hist);//SV& sv) ;
  double LB(const std::valarray<double>& hist);//SV& sv) ;
};

// normalized Chi-square
class nchi2Quality: public spQuality
{
public:
  nchi2Quality(){}
  double UB(const std::valarray<double>& hist);//SV& sv) ;
  double LB(const std::valarray<double>& hist);//SV& sv) ;
};

// RBF w histogram intersection distance
class rbfhiQuality: public spQuality
{
public:
  rbfhiQuality(){}
  double UB(const std::valarray<double>& hist);//SV& sv) ;
  double LB(const std::valarray<double>& hist);//SV& sv) ;
};

// RBF w chi2 distance
class rbfchi2Quality: public spQuality
{
public:
  rbfchi2Quality(){}
  double UB(const std::valarray<double>& hist);//SV& sv) ;
  double LB(const std::valarray<double>& hist);//SV& sv) ;
};

#endif

