/*
 * ==========================================================================
 *    Description:  MEX'd efficient subwindow search
 *                  for object localization
 *        Created:  05/19/2013 03:32:10 PM EST
 *         Author:  Kun He
 *   Organization:  Boston University
 * ==========================================================================
 * reduces to inference in the standard ESS:
 * training: use mode 2
 *  testing: use mode 1
 *
 * mode 0: have ygt,    no sv0,    no sv1
 *         sample_a_box
 *
 * mode 1:   no ygt,    no sv0,  have sv1
 *         test-time inference
 *
 * mode 2: have ygt,    no sv0,  have sv1
 *         loss-augmented inference
 *
 * mode 3: have ygt,  have sv0,  have sv1
 *         hingeboost's loss-augmented inference
 * ==========================================================================
 * INPUT
 * kernel  - string, kernel type
 * nlv     - uint32, # levels in spatial pyramid
 * inthist - H * W * K double
 *           H: image height, W: image width, K: # of visual words
 * step    - uint32, separation between feature points
 * xgrid   - uint32 vec, x coordinates of feature points
 * ygrid   - uint32 vec, y coordinates of feature points
 * bbsizes - 2*nsize uint32, all the boxes sizes to search for
 * ygt     - [empty OK] ground truth [l,t,r,b] 4-dim
 * sv0coef - [empty OK] double, support vector coefficients
 * sv0hist - [empty OK] double, support vectors (histogram)
 * sv1coef - [empty OK] double, support vector coefficients
 * sv1hist - [empty OK] double, support vectors (histogram)
 * gamma   - [empty OK] double, bandwidth parameter for RBF kernels
 *
 * OUTPUT
 * ybar    - [l,t,r,b]
 * viobar  - margin violation by ybar
 * mbar    - margin associated with ybar
 * hist    - histogram (spatial pyramid) for ybar
 * maxgtid - index of ground truth box associated with ybar
 */
#include <stdint.h>
#include <cmath>
#include <string>
#include <vector>
#include <iostream>
#include <algorithm>

#include "mex.h"
#include "matrix.h"
#include "newdelete.h"  // let Matlab manage new/delete
#include "ess_hingeboost.hh"
#include "quality_det.hh"

using namespace std;
typedef uint32_t int32;

///////////////////////////////////////////////////////////////////////////
// parse an int value from env variable
static int igetenv(const char* name, int defaultvalue, int low, int high)
{
  int val = defaultvalue;
  if (getenv(name))
    return atoi(getenv(name));
  if (val < low)
    val = low;
  else if (val > high)
    val = high;
  return val;
}

static int MINSPLIT = 0;  // 0 for exact inference, >0 for approximate
static int VERBOSE = igetenv("VERBOSE", 0, 0, 10000000);
static int ITERMAX = 10000000;

vector<int> xpos;
vector<int> ypos;
vector<int> xpos_aug;
vector<int> ypos_aug;
static sstate_heap H;

double Tsetup = 0.0;
double Tbound = 0.0;

///////////////////////////////////////////////////////////////////////////

// central routine during branch-and-bound search:
inline int extract_split_insert(bnbQuality& qual)
{
  // 1) find most promising candidate region, check stop criterion
  // 2) split and create two new states
  const sstate * cur = H.top();
  sstate * newstate[2];
  if (cur->px[1] - cur->px[0] > MINSPLIT && 
      cur->px[1] - cur->px[0] >= cur->py[1] - cur->py[0])
    //  if (cur->l[1] - cur->l[0] > MINSPLIT && 
    //      cur->l[1] - cur->l[0] >= cur->t[1] - cur->t[0])
    {
      // split x
      H.pop();
      for (int x = 0; x < 2; x++)
	{
	  sstate * s = new sstate(*cur);
	  s->px[x] = (cur->px[0] + cur->px[1] + 2-2*x) >> 1;
	  s->l[x] = xpos_aug[s->px[x]];
	  //s->l[x] = (cur->l[0] + cur->l[1] + 1-x) >> 1;
	  newstate[x] = s;
	}
    }
  else if (cur->py[1] - cur->py[0] > MINSPLIT)
    //  else if (cur->t[1] - cur->t[0] > MINSPLIT)
    {
      // split y
      H.pop();
      for (int y = 0; y < 2; y++)
	{
	  sstate * s = new sstate(*cur);
	  s->py[y] = (cur->py[0] + cur->py[1] + 2-2*y) >> 1;
	  s->t[y] = ypos_aug[s->py[y]];
	  //s->t[y] = (cur->t[0] + cur->t[1] + 1-y) >> 1;
	  newstate[y] = s;
	}
    }
  else 
    return -1;
  // 3) calculate bounds for new states and reinsert them
  //    with some pruning
  for (int i = 0; i < 2; i++)
    {
      qual.compute_bounds(newstate[i]);
      if (newstate[i]->upper > .1*H.top()->upper + .9*H.top()->lower)
	H.push(newstate[i]);
    }
  // 4) no error, but also no convergence, yet
  delete cur;
  cur = NULL;
  return 0;
}

inline void print_top(int count)
{
  const sstate * curmax = H.top();
  cout << "#" << count;
  cout << " |H|=" << H.size() << "  ";
  cout << curmax->tostring();
}

/////////////////////////////////////////////////////////////////////////////
// pyaramid_search
/////////////////////////////////////////////////////////////////////////////

int pyramid_search(Params & para, Box & outbox, 
		   int * gtid, double * rho, valarray<double> & hist)
{
  // push initial states into heap
  for (int b = 0; b < para.bbsizes.size(); b++)
    {
      int ww = para.bbsizes[b].w;
      int hh = para.bbsizes[b].h;
      int wlim = para.imw - ww;
      int hlim = para.imh - hh;
      int pxlo = 0;
      int pylo = 0;
      int pxhi = pxlo;
      int pyhi = pylo;
      for (; pxhi+1 < xpos_aug.size() && xpos_aug[pxhi+1] <= wlim; pxhi++);
      for (; pyhi+1 < ypos_aug.size() && ypos_aug[pyhi+1] <= hlim; pyhi++);
      int xlo = xpos_aug[pxlo];
      int xhi = xpos_aug[pxhi];
      int ylo = ypos_aug[pylo];
      int yhi = ypos_aug[pyhi];
      //      int xlo = 1;
      //      int ylo = 1;
      //      int xhi = wlim;
      //      int yhi = hlim;
      sstate * subspace = 
	new sstate(ww, hh, pxlo, pxhi, pylo, pyhi, xlo, xhi, ylo, yhi);
      para.quality.compute_bounds(subspace);
      H.push(subspace);
    }
  // do branch-and-bound search
  long count = 1;
  while (extract_split_insert(para.quality) >= 0 && count < ITERMAX)
    {
      if (VERBOSE && count % VERBOSE == 0)
	{
	  print_top(count);
	  //mexEvalString("drawnow");
	}
      count++;
    }
  // at convergence or error, return result or best guess
  if (VERBOSE)
    print_top(count);
  const sstate * cur = H.top();
  outbox.l = (cur->l[0] + cur->l[1]) >> 1;
  outbox.t = (cur->t[0] + cur->t[1]) >> 1;
  outbox.r = outbox.l + cur->w;
  outbox.b = outbox.t + cur->h;
  outbox.score = cur->upper;
  *rho = cur->rhoUB;
  *gtid = cur->maxgtid + 1;

  // if approximate inference, recompute
  if (0)
    {
      //      sstate * st = new sstate(outbox);
      //      para.quality.compute_bounds(st);
      //      outbox.score = st->upper;
      //      *rho = st->rhoUB;
      //      delete st;
    }

  // get resulting histogram
  para.quality.get_output_hist(hist, outbox);

  // clean up and return
  while (!H.empty())
    {
      delete H.top();
      H.pop();
    }
  return count;
}

/////////////////////////////////////////////////////////////////////////////
// sample_a_box
/////////////////////////////////////////////////////////////////////////////

Box sample_a_box(Params & p, double * m, valarray<double> & hist)
{
  int w = p.bbsizes[0].w;
  int h = p.bbsizes[0].h;
  Box gt = p.quality.gtruth(0); // only care about first ground truth box
  int gtw = gt.r - gt.l;
  int gth = gt.b - gt.t;

  Box bb;
  bb.l = (gt.l > 50) ? max(1, gt.l-w/2) : gt.l+w/2;  
  bb.t = (gt.t > 50) ? max(1, gt.t-h/2) : gt.t+h/2;  
  bb.r = bb.l + gtw;
  bb.b = bb.t + gth;
  bb.score = 0;

  sstate * s = new sstate(bb);
  //*m = p.quality.loss_ub(s);
  *m = p.quality.loss_ub(s).val;
  p.quality.get_output_hist(hist, bb);
  delete s;
  return bb;
}

/////////////////////////////////////////////////////////////////////////////
// mexFunction
/////////////////////////////////////////////////////////////////////////////

void err(const char * msg, bool usage = true)
{
  if (usage)
    {
      mexPrintf("Usage:\n[y vio mar psi gtid] = ess_hingeboost(\n");
      mexPrintf("    kernel, nlv, inthist, step, imwh, xgrid, ygrid, bbsizes,\n");
      mexPrintf("    y_gt, sv0coef, sv0hist, sv1coef, sv1hist,\n");
      mexPrintf("    gamma\n)\nMust fill in all arguments\n");
    }
  mexErrMsgTxt(msg);
}
int check_args(int nlhs, int nrhs, const mxArray * prhs[]);
void get_args(int nrhs, const mxArray* prhs[], Params& para, const int mode);

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  // check and get input arguments
  int mode = check_args(nlhs, nrhs, prhs);
  if (mode!=0 && mode!=1 && mode!=2 && mode!=3)
    err("incorrect working mode");
  Params para;
  get_args(nrhs, prhs, para, mode);

  // create arrays for output
  plhs[0] = mxCreateDoubleMatrix(1, 4, mxREAL);
  plhs[1] = mxCreateDoubleMatrix(1, 1, mxREAL);
  plhs[2] = mxCreateDoubleMatrix(1, 1, mxREAL);
  plhs[3] = mxCreateDoubleMatrix(para.dsp, 1, mxREAL);
  mwSize dims[] = {1, 1};
  plhs[4] = mxCreateNumericArray(2, dims, mxINT32_CLASS, mxREAL);
  double * detlabel = mxGetPr(plhs[0]);	// y_bar
  double * detscore = mxGetPr(plhs[1]);	// violation (have y_gt) || score (otherwise)
  double * detmargn = mxGetPr(plhs[2]); // \rho(y_gt, y_bar) or -1
  double * detfeatv = mxGetPr(plhs[3]);	// phi_bar
  int * gtid = (int*)mxGetData(plhs[4]);// gtid

  // go!
  Box out(0.);
  double mar = 0;
  valarray<double> hist(0., para.dsp);
  if (mode > 0)
    {
      int iter = pyramid_search(para, out, gtid, &mar, hist);
      if (mode > 1 && out.score < 0)
	{
	  out = para.quality.gtruth(*gtid - 1);
	  out.score = 0;
	  mar = 0;
	  para.quality.get_output_hist(hist, out);
	}
      para.quality.print_gt();
      mexPrintf(" DET [%3d %3d %3d %3d] ", out.l, out.t, out.r, out.b);
      mexPrintf("<vio=%.3f, mar=%.3f>  %d iters\n", out.score, mar, iter);
      //mexPrintf("--> setup: %.3f secs, bounds: %.3f secs\n", Tsetup/100, Tbound/100);
    }
  else
    {
      *gtid = -1;
      out = sample_a_box(para, &mar, hist);
      if (1 || VERBOSE)
	{
	  para.quality.print_gt();
	  mexPrintf(" SAM [%3d %3d %3d %3d] ", out.l, out.t, out.r, out.b);
	  mexPrintf("L1 %.2f, L2 %.2f, Delta %.3f\n", hist.sum(), (hist*hist).sum(), mar);
	}
    }

  // write output and clean up
  *detscore   = out.score;
  *detmargn   = mar;
  detlabel[0] = out.l;
  detlabel[1] = out.t;
  detlabel[2] = out.r;
  detlabel[3] = out.b;
  for (int i = 0; i < para.dsp; i++)
    detfeatv[i] = hist[i];
  // sanity check
  if (mode > 1 && *detscore < 0)
    {
      mexPrintf("Warning: vio = %g < 0\n", *detscore);
      //err("impossible!");
    }
}

/////////////////////////////////////////////////////////////////////////////
// check_args
// the type constraints are pretty strict, but by no means a bad thing
/////////////////////////////////////////////////////////////////////////////

// check input arguments
int check_args(int nlhs, int nrhs, const mxArray * prhs[])
{
  if (nrhs != 14)	err("Wrong number of input arguments (must be 14).");
  if (nlhs != 5)	err("Wrong number of output arguments (must be 5).");

  // kernel type and SP levels
  const mxArray * _kernel 	= prhs[0];
  const mxArray * _nlv	 	= prhs[1];
  // feature related
  const mxArray * _inthist	= prhs[2];
  const mxArray * _step 	= prhs[3];
  const mxArray * _imwh		= prhs[4];
  const mxArray * _xgrid 	= prhs[5];
  const mxArray * _ygrid 	= prhs[6];
  // search space related
  const mxArray * _bbsizes 	= prhs[7];
  // [ground truth]
  const mxArray * _ygt		= prhs[8];
  // [support vector model]
  const mxArray * _sv0coef 	= prhs[9];
  const mxArray * _sv0hist 	= prhs[10];
  const mxArray * _sv1coef 	= prhs[11];
  const mxArray * _sv1hist 	= prhs[12];
  // [parameters]
  const mxArray * _gamma	= prhs[13];

  // check arguments that should always be there
  // nonempty
  if (mxIsEmpty(_kernel))	err("kernel must be non-empty");
  if (mxIsEmpty(_nlv))		err("nlv must be non-empty");
  if (mxIsEmpty(_inthist))	err("inthist must be non-empty");
  if (mxIsEmpty(_step))		err("step must be non-empty");
  if (mxIsEmpty(_imwh))		err("imwh must be non-empty");
  if (mxIsEmpty(_xgrid))	err("xgrid must be non-empty");
  if (mxIsEmpty(_ygrid))	err("ygrid must be non-empty");
  if (mxIsEmpty(_bbsizes))     	err("bbsizes must be non-empty");
  // type
  if (!mxIsChar(_kernel))	err("kernel type must be string.");
  if (!mxIsUint32(_nlv))	err("nlv must be uint32.");
  if (!mxIsDouble(_inthist))	err("inthist must be double.");
  if (!mxIsUint32(_step))	err("step must be uint32");
  if (!mxIsUint32(_imwh))	err("imwh must be uint32.");
  if (!mxIsUint32(_xgrid))	err("xgrid must be uint32");
  if (!mxIsUint32(_ygrid))	err("ygrid must be uint32");
  if (!mxIsUint32(_bbsizes))	err("bbsizes must be uint32.");
  // size
  if (mxGetNumberOfDimensions(_inthist) != 3)	
    err("inthist must be 3-dimensional");
  if (mxGetM(_bbsizes) != 2)	
    err("bbsizes must be 2-by-n.");
  // gamma
  if (!mxIsEmpty(_gamma) && !mxIsDouble(_gamma))
    err("gamma must be double.");

  // different operating modes
  int mode;
  if (mxIsEmpty(_sv1coef)) // no sv1, then no sv at all
    {
      // but must have ygt
      if (mxIsEmpty(_ygt))	
	err("ygt must be nonempty for sample_a_box");
      if (mxGetM(_ygt) != 4)	
	err("columns of ygt must be 4D (l,t,r,b)");
      //
      if (VERBOSE)
	mexPrintf("**** MODE: SAMPLE-A-BOX");
      mode = 0;
    }
  else // have sv1
    {
      // check sv1 first
      if (mxIsEmpty(_sv1hist))		err("sv1hist must be non-empty.");
      if (!mxIsDouble(_sv1coef))	err("sv1coef must be double.");
      if (!mxIsDouble(_sv1hist))	err("sv1hist must be double.");
      if (mxGetNumberOfElements(_sv1coef) != mxGetN(_sv1hist))	
	err("numel(sv1coef) != # of SV1");
      //
      if (mxIsEmpty(_sv0coef)) // no sv0
	{
	  if (mxIsEmpty(_ygt)) // no ygt either, test-inference
	    {
	      if (VERBOSE)
		mexPrintf("**** MODE: TEST-TIME INFERENCE");
	      mode = 1;
	    }
	  else // have sv1, no sv0, have ygt: delta-inference
	    {
	      // check ygt
	      if (mxIsEmpty(_ygt))	
		err("ygt must be nonempty for delta_inference");
	      if (mxGetM(_ygt) != 4)	
		err("columns of ygt must be 4D (l,t,r,b)");
	      //
	      if (VERBOSE) 
		mexPrintf("**** MODE: DELTA INFERENCE");
	      mode = 2;
	    }
	}
      else // have sv1, have sv0, then must have ygt: rho-inference
	{
	  // check sv0
	  if (mxIsEmpty(_sv0hist))	err("sv0hist must be non-empty");
	  if (!mxIsDouble(_sv0coef))	err("sv0coef must be double.");
	  if (!mxIsDouble(_sv0hist))	err("sv0hist must be double.");
	  if (mxGetNumberOfElements(_sv0coef) != mxGetN(_sv0hist))	
	    err("numel(sv0coef) != # of SV0");
	  // check ygt
	  if (mxIsEmpty(_ygt))	
	    err("ygt must be nonempty for rho_inference");
	  if (mxGetM(_ygt) != 4)	
	    err("columns of ygt must be 4D (l,t,r,b)");
	  //
	  if (VERBOSE)
	    mexPrintf("**** MODE: RHO INFERENCE");
	  mode = 3;
	}
    }
  return mode;
}

/////////////////////////////////////////////////////////////////////////////
// get_args
/////////////////////////////////////////////////////////////////////////////

// get input arguments
void get_args(int nrhs, const mxArray* prhs[], Params& para, const int mode)
{
  const mxArray * _kernel 	= prhs[0];
  const mxArray * _nlv	 	= prhs[1];
  const mxArray * _inthist	= prhs[2];
  const mxArray * _step 	= prhs[3];
  const mxArray * _imwh		= prhs[4];
  const mxArray * _xgrid 	= prhs[5];
  const mxArray * _ygrid 	= prhs[6];
  const mxArray * _bbsizes 	= prhs[7];
  // [ground truth]
  const mxArray * _ygt		= prhs[8];
  // [support vector model]
  const mxArray * _sv0coef 	= prhs[9];
  const mxArray * _sv0hist 	= prhs[10];
  const mxArray * _sv1coef 	= prhs[11];
  const mxArray * _sv1hist 	= prhs[12];
  // [parameters]
  const mxArray * _gamma	= prhs[13];

  char kstr[64] = "test";
  int nlv 	= (int)mxGetScalar(_nlv);
  int* sz_inthist = (int*)mxGetDimensions(_inthist);
  //int imh 	= sz_inthist[0]; // kengdie!
  //int imw 	= sz_inthist[2]; // kengdie!
  int nvw 	= sz_inthist[4]; // kengdie!
  int step 	= (int)mxGetScalar(_step);
  int* imwh     = (int*)mxGetData(_imwh);
  int imw	= imwh[0];
  int imh	= imwh[1];
  int nxgrid  	= mxGetNumberOfElements(_xgrid);
  int nygrid  	= mxGetNumberOfElements(_ygrid);
  int* xgrid 	= (int*)mxGetData(_xgrid);
  int* ygrid 	= (int*)mxGetData(_ygrid);
  int nsize	= mxGetN(_bbsizes);
  int* bbsizes 	= (int*)mxGetData(_bbsizes);
  int dsp 	= nvw * (exp2(2*nlv) - 1) / 3;


  //////////////////////////////////////////////////////////
  // update para
  //
  //===== kernel ===== 
  mxGetString(_kernel, kstr, 64);
  if (VERBOSE)
    mexPrintf(" (%s kernel) ****\n", kstr);
  if (kstr[0] == 'n' || kstr[0] == 'r')
    para.normalize = true;
  else
    para.normalize = false;

  //=====  =====
  para.imh = (int)imh;
  para.imw = (int)imw;
  para.dsp = (int)dsp;

  //===== search space related =====
  para.bbsizes.clear();
  for (int b = 0; b < nsize; b++)
    {
      // NOTE: w comes before h
      int w = bbsizes[2*b+0];
      int h = bbsizes[2*b+1];
      if (h > imh || w > imw || h < 1 || w < 1)
	{
	  //	  mexPrintf("BOX #%d OUT OF BOUNDS: h=%d, w=%d, imh=%d, imw=%d, omitted\n", 
	  //		    b+1, h, w, imh, imw);
	}
      else
	para.bbsizes.push_back(bbsize(w, h)); 
    }

  //===== y_gt =====
  //Box * bb = NULL;
  vector<Box> bb(0);
  if (mode != 1)
    {
      double * ygt = (double*)mxGetPr(_ygt);
      int ngt = mxGetN(_ygt);
      for (int y = 0; y < ngt; y++)
	{
	  float l = ygt[y*4+0];	 
	  float t = ygt[y*4+1];
	  float r = ygt[y*4+2];	 
	  float b = ygt[y*4+3];
	  if (l >= r || t >= b || l < 1 || t < 1 || r > imw || b > imh)
	    {
	      mexPrintf("\nGT LABEL #%d ERROR: l=%g r=%g t=%g b=%g | imh=%d imw=%d\n", 
			y+1, l, r, t, b, imh, imw);
	      err("problem with gt label", false);
	    }
	  bb.push_back(Box(l, t, r, b));
	}
    }

  //////////////////////////////////////////////////////////
  // feature grid
  xpos.clear();
  ypos.clear();
  for (int x = 0; x < nxgrid; x++)
    {
      xpos.push_back(xgrid[x]);
    }
  for (int y = 0; y < nygrid; y++)
    {
      ypos.push_back(ygrid[y]);
    }
  sort(xpos.begin(), xpos.end());
  sort(ypos.begin(), ypos.end());
  //
  // now augment using ground truths
  xpos_aug = xpos;
  ypos_aug = ypos;
  for (int gt = 0; gt < bb.size(); gt++)
    {
      if (find(xpos_aug.begin(), xpos_aug.end(), bb[gt].l) != xpos_aug.end())
	xpos_aug.push_back(bb[gt].l);
      if (find(ypos_aug.begin(), ypos_aug.end(), bb[gt].t) != ypos_aug.end())
	ypos_aug.push_back(bb[gt].t);
    }
  sort(xpos_aug.begin(), xpos_aug.end());
  sort(ypos_aug.begin(), ypos_aug.end());

  //===== SVs =====
  int nsv0 = 0;
  int nsv1 = 0;
  double * sv0coef = NULL;
  double * sv0hist = NULL;
  double * sv1coef = NULL;
  double * sv1hist = NULL;
  if (mode == 3)
    {
      if (mxGetM(_sv0hist) != dsp)
	err("SV0 dims don't match inthist+nlv");
      nsv0 	= mxGetN(_sv0hist);
      sv0coef 	= mxGetPr(_sv0coef);
      sv0hist 	= mxGetPr(_sv0hist);
    }
  if (mode > 0)
    {
      if (mxGetM(_sv1hist) != dsp)
	err("SV1 dims don't match inthist+nlv");
      nsv1 	= mxGetN(_sv1hist);
      sv1coef 	= mxGetPr(_sv1coef);
      sv1hist 	= mxGetPr(_sv1hist);
    }

  //===== gamma =====
  double gamma = 2;
  if (!mxIsEmpty(_gamma))
    {
      double g = mxGetScalar(_gamma);
      if (g <= 0)
	err("gamma not positive");
      gamma = g;
    }

  //////////////////////////////////////////////////////////
  // finally, set up quality bound class
  //
  //mexPrintf("going into bnbQuality::init()\n");
  para.quality.init(kstr, _inthist, step, imw, imh, nvw, nlv, dsp, bb,
		    nsv0, sv0coef, sv0hist, nsv1, sv1coef, sv1hist, 
		    gamma);
  //mexPrintf("bnbQuality::init() returned\n");
}


