/*
 * ==========================================================================
 *    Description:  MEX'd efficient subwindow search
 *                  for joint detection and viewpoint estimation
 *        Created:  03/31/2013 04:08:10 PM EST
 *         Author:  Kun He
 *   Organization:  Boston University
 * ==========================================================================
 * search jointly for the best object bbox and viewpoint in a test image, 
 * perform either standard or loss-augmented inference
 * ==========================================================================
 * INPUT
 * kernel  - string, kernel type
 * nlv     - uint32, # levels in spatial pyramid
 * inthist - H * W * K double
 *           H: image height, W: image width, K: # of visual words
 * imwh    - size of original image (not available through inthist!)
 * xgrid   - uint32 vec, x coordinates of feature points
 * ygrid   - uint32 vec, y coordinates of feature points
 * bbsizes - 2*nsize uint32, all the boxes sizes to search for
 * vps     - 1*nvp or nvp*1 double, all the viewpoints to search for
 * ygt     - [empty OK] ground truth [l,t,r,b,vpid] 5-dim
 * W       - [empty OK] ftdim*nvp double, set of classifiers
 * alpha   - double, weight for combining localization & vp costs
 *
 * OUTPUT
 * ybar   - [l,t,r,b,vpid]
 * viobar - margin violation by ybar
 * mbar   - margin associated with ybar
 * hist   - histogram (spatial pyramid) for ybar
 */
#include <stdint.h>
#include <cstring>
#include <string>
#include <vector>
#include <cmath>
#include <algorithm>
#include "mex.h"
#include "matrix.h"
#include "newdelete.h"  // let Matlab manage new/delete
#include "ess_posedet.hh"
#include "quality_posedet.hh"

typedef uint32_t int32;

static int MINSPLIT = 0;  // 0 for exact inference, >0 for approximate
static int VERBOSE  = 2000;
static int ITERMAX  = 1000000;

std::vector<int> xpos;
std::vector<int> ypos;
std::vector<int> xpos_aug;
std::vector<int> ypos_aug;
std::vector<double> vps;

static sstate_heap H;
double LB_global;

///////////////////////////////////////////////////////////////////////////
void print_top(int count)
{
  const sstate * curmax = H.top();
  mexPrintf("#%d |H|=%4d  %s", count, H.size(), curmax->tostring().c_str());
}

// central routine during branch-and-bound search:
int extract_split_insert(bnbQuality & qual)
{
  // 1) find most promising candidate region, check stop criterion
  // 2) split and create two new states
  //    split strategy: larger dimension or smaller dimension?
  const sstate * cur = H.top();
  sstate * newstate[2];
  if (cur->px[1] - cur->px[0] > MINSPLIT && 
      cur->px[1] - cur->px[0] >= cur->py[1] - cur->py[0]) // larger dim
      //(cur->px[1] - cur->px[0] <= cur->py[1] - cur->py[0] // smaller dim
       //|| cur->py[1] - cur->py[0] <= MINSPLIT))
    {
      // split x
      H.pop();
      for (int x = 0; x < 2; x++)
	{
	  sstate * s  = new sstate(*cur);
	  s->px[x]    = (cur->px[0] + cur->px[1] + 2-2*x) >> 1;
	  s->l[x]     = xpos_aug[s->px[x]];
	  newstate[x] = s;
	}
    }
  else if (cur->py[1] - cur->py[0] > MINSPLIT)
    {
      // split y
      H.pop();
      for (int y = 0; y < 2; y++)
	{
	  sstate * s  = new sstate(*cur);
	  s->py[y]    = (cur->py[0] + cur->py[1] + 2-2*y) >> 1;
	  s->t[y]     = ypos_aug[s->py[y]];
	  newstate[y] = s;
	}
    }
  else 
    return -1;
  // 3) calculate bounds for new states and reinsert them
  //    with some pruning
  for (int i = 0; i < 2; i++)
    {
      qual.compute_bounds(newstate[i]);

      if (newstate[i]->upper >= LB_global)
	H.push(newstate[i]);

      // update LB
      if (LB_global < newstate[i]->lower)
	LB_global = newstate[i]->lower;
    }
  // 4) no error, but also no convergence, yet
  delete cur;
  cur = NULL;
  return 0;
}

/////////////////////////////////////////////////////////////////////////////
// pyaramid_search
/////////////////////////////////////////////////////////////////////////////

int pyramid_search(Params & para, Box & outbox, 
		   int * gtid, double * delta, Vec & hist)
{
  // push initial states into heap
  for (int b = 0; b < para.bbsizes.size(); b++)
    {
      int ww   = para.bbsizes[b].w;
      int hh   = para.bbsizes[b].h;
      int pxlo = 0, pxhi = 0;
      int pylo = 0, pyhi = 0;
      for (; pxhi+1 < xpos_aug.size() && xpos_aug[pxhi+1] <= para.imw-ww; pxhi++);
      for (; pyhi+1 < ypos_aug.size() && ypos_aug[pyhi+1] <= para.imh-hh; pyhi++);
      int xlo  = xpos_aug[pxlo],  xhi  = xpos_aug[pxhi];
      int ylo  = ypos_aug[pylo],  yhi  = ypos_aug[pyhi];
      for (int v = 0; v < vps.size(); v++)
	{
	  sstate * subspace = 
	    new sstate(ww, hh, pxlo, pxhi, pylo, pyhi, xlo, xhi, ylo, yhi, v);
	  para.quality.compute_bounds(subspace);
	  //	  if (LB_global < subspace->lower)
	  LB_global = (v==0) ? subspace->lower : std::max(LB_global, double(subspace->lower));
	  H.push(subspace);
	}
    }
  // do branch-and-bound search
  long count = H.size();
  while (extract_split_insert(para.quality) >= 0 && count < ITERMAX)
    {
      if (VERBOSE && count % VERBOSE == 0)
	  print_top(count);
      count++;
    }
  // at convergence or error, return result or best guess
  if (VERBOSE)
    print_top(count);
  const sstate * cur = H.top();
  outbox.l     = (cur->l[0] + cur->l[1]) >> 1;
  outbox.t     = (cur->t[0] + cur->t[1]) >> 1;
  outbox.r     = outbox.l + cur->w;
  outbox.b     = outbox.t + cur->h;
  outbox.vp    = vps[cur->vpid];
  outbox.score = cur->upper;
  *delta       = cur->Delta;
  *gtid        = cur->maxgtid + 1;

  // get resulting histogram, clean up and return
  para.quality.get_output_hist(hist, outbox);
  while (!H.empty())
    {
      delete H.top();
      H.pop();
    }
  return count;
}

/////////////////////////////////////////////////////////////////////////////
// sample_a_box
/////////////////////////////////////////////////////////////////////////////

//Box sample_a_box(Params & p, double * delta, valarray<double> & hist)
Box sample_a_box(Params & p, double * delta, Vec & hist)
{
  Box gt = p.quality.gtruth(0); // only care about first ground truth box
  Box bb;
  int vid = 0;
  if (p.bbsizes.empty()) // get ground truth
    {
      bb = gt;
    }
  else // sample box with large loss
    {
      int w   = p.bbsizes[0].w;
      int h   = p.bbsizes[0].h;
      int gtw = gt.r - gt.l;
      int gth = gt.b - gt.t;
      bb.l    = (gt.l > 50) ? std::max(1, gt.l-w/2) : gt.l+w/2;
      bb.t    = (gt.t > 50) ? std::max(1, gt.t-h/2) : gt.t+h/2;
      bb.r    = std::min(bb.l + gtw, p.imw);
      bb.b    = std::min(bb.t + gth, p.imh);
      for (int i = 0; i < vps.size(); i++)
	{
	  double dv = std::abs(vps[i] - gt.vp);
	  dv = std::min(dv, 2*PI-dv);
	  if (dv > PI*0.75)
	    {
	      vid = i;
	      break;
	    }
	}
      bb.vp = vps[vid];
    }

  sstate s(bb);
  s.vpid = vid; //NOTE: important

  *delta = p.quality.loss_ub(&s).val;
  p.quality.get_output_hist(hist, bb);
  return bb;
}

/////////////////////////////////////////////////////////////////////////////
// mexFunction
/////////////////////////////////////////////////////////////////////////////

void err(const char * msg, bool usage = true)
{
  if (usage)
    {
      mexPrintf("Usage:\n[y vio mar psi j] = ess_viewdet(\n");
      mexPrintf("    Kx, inthist, nlv, imwh, xgrid, ygrid, bbsizes, vps,\n");
      mexPrintf("    [y_gt], [W], [b]\n");
    }
  mexErrMsgTxt(msg);
}
int check_args(int nlhs, int nrhs, const mxArray * prhs[]);
void get_args(int nrhs, const mxArray* prhs[], Params& para, const int mode);

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  // check input arguments
  int mode = check_args(nlhs, nrhs, prhs);
  if (mode!=0 && mode!=1 && mode!=2)
    err("incorrect working mode");

  // get input arguments
  Params para;
  get_args(nrhs, prhs, para, mode);

  // create arrays for output
  plhs[0] = mxCreateDoubleMatrix(1, 5, mxREAL);
  plhs[1] = mxCreateDoubleMatrix(1, 1, mxREAL);
  plhs[2] = mxCreateDoubleMatrix(1, 1, mxREAL);
  plhs[3] = mxCreateDoubleMatrix(para.dsp, 1, mxREAL);
  mwSize dims[] = {1, 1};
  plhs[4] = mxCreateNumericArray(2, dims, mxINT32_CLASS, mxREAL);
  double * detlabel = mxGetPr(plhs[0]);	 // y_bar
  double * detscore = mxGetPr(plhs[1]);	 // violation (have y_gt) || score (otherwise)
  double * detmargn = mxGetPr(plhs[2]);  // \Delta(y_gt, y_bar) or -1
  double * detfeatv = mxGetPr(plhs[3]);	 // phi_bar
  int * gtid = (int*)mxGetData(plhs[4]); // gtid

  // go!
  Box out(0.);
  double mar = 0;
  Vec hist(para.dsp);
  if (mode > 0)  // have SV, inference
    {
      int iter = pyramid_search(para, out, gtid, &mar, hist);
      if (mode > 1 && out.score < 0) // loss-augmented inference
	{
	  out       = para.quality.gtruth(*gtid - 1);
	  out.score = 0;
	  mar       = 0;
	  para.quality.get_output_hist(hist, out);
	}
      para.quality.print_gt();
      mexPrintf(" DET [%3d %3d %3d %3d %.2f] ", out.l, out.t, out.r, out.b, out.vp);
      mexPrintf("<vio=%.3f mar=%.3f>  %d iters\n", out.score, mar, iter);
    }
  else // mode=0, sample-a-box
    {
      *gtid = -1;
      out = sample_a_box(para, &mar, hist);
      if (1 || VERBOSE)
	{
	  para.quality.print_gt();
	  if (!para.bbsizes.empty())
	    {
	      mexPrintf(" SAM [%3d %3d %3d %3d %.2f] ", out.l, out.t, out.r, out.b, out.vp);
	      mexPrintf("Delta %.3f\n", mar);
	    }
	}
    }

  // write output and clean up
  *detscore   = out.score;
  *detmargn   = mar;
  detlabel[0] = out.l;
  detlabel[1] = out.t;
  detlabel[2] = out.r;
  detlabel[3] = out.b;
  detlabel[4] = out.vp;
  for (int i = 0; i < para.dsp; i++)
    detfeatv[i] = hist[i];
}

/////////////////////////////////////////////////////////////////////////////
// check_args
/////////////////////////////////////////////////////////////////////////////

// check input arguments
int check_args(int nlhs, int nrhs, const mxArray * prhs[])
{
  if (nrhs != 11)		err("Wrong number of input arguments.");
  if (nlhs != 5)		err("Wrong number of output arguments.");

  // kernel type and SP levels
  const mxArray * _Kx 		= prhs[0];
  // feature related
  const mxArray * _inthist	= prhs[1];
  const mxArray * _nlv	 	= prhs[2];
  const mxArray * _imwh		= prhs[3];
  const mxArray * _xgrid 	= prhs[4];
  const mxArray * _ygrid 	= prhs[5];
  // search space related
  const mxArray * _bbsizes 	= prhs[6];
  const mxArray * _vps	 	= prhs[7];
  // [ground truth]
  const mxArray * _ygt		= prhs[8];
  // [support vector model]
  const mxArray * _W 		= prhs[9];
  const mxArray * _b 		= prhs[10];

  // check arguments that should always be there
  // nonempty
  if (mxIsEmpty(_Kx))		err("Kx must be non-empty");
  if (mxIsEmpty(_inthist))	err("inthist must be non-empty");
  if (mxIsEmpty(_nlv))		err("nlv must be non-empty");
  if (mxIsEmpty(_imwh))		err("imwh must be non-empty");
  if (mxIsEmpty(_xgrid))	err("xgrid must be non-empty");
  if (mxIsEmpty(_ygrid))	err("ygrid must be non-empty");
  if (mxIsEmpty(_vps))		err("vps must be non-empty");
  // type
  if (!mxIsChar(_Kx))		err("Kx type must be string.");
  if (!mxIsDouble(_inthist))	err("inthist must be double.");
  if (!mxIsUint32(_nlv))	err("nlv must be uint32.");
  if (!mxIsUint32(_imwh))	err("imwh must be uint32.");
  if (!mxIsUint32(_xgrid))	err("xgrid must be uint32");
  if (!mxIsUint32(_ygrid))	err("ygrid must be uint32");
  if (!mxIsDouble(_vps))	err("vps must be double.");

  // size
  if (mxGetNumberOfDimensions(_inthist) != 3)	
    err("inthist must be 3-dimensional");
  if (!mxIsEmpty(_bbsizes))
    if (mxGetM(_bbsizes) != 2)	err("bbsizes must be 2-by-n.");

  // type
  if (!mxIsEmpty(_bbsizes))
    if (!mxIsUint32(_bbsizes))	err("bbsizes must be uint32.");

  // different operating modes
  // mode 0: have GT,   no SV -> sample-a-box
  // mode 1:   no GT, have SV -> inference (test time)
  // mode 2: have GT, have SV -> inference (loss-augmented)
  int mode;
  if (!mxIsEmpty(_ygt) && mxIsEmpty(_W))
    {
      // sample a box, compute delta and feature, and return
      if (VERBOSE)
	mexPrintf(" Sample-A-Box");
      mode = 0;
    }
  else if (!mxIsEmpty(_W))
    {
      // have SV, will continue check
      if (mxIsEmpty(_ygt))
	{
	  if (VERBOSE)
	    mexPrintf(" Test-time Inference");
	  mode = 1;
	}
      else
	{
	  if (VERBOSE)
	    mexPrintf(" Loss-augmented Inference");
	  mode = 2;
	}
      if (mxIsEmpty(_b))
	err("b must be nonempty when W nonempty.");
    }
  else  err("ygt & W both empty!");

  // types
  if (mode > 0) // don't care if mode is 0
    {
      if (!mxIsDouble(_W))	
	err("W must be double.");
      if (!mxIsDouble(_b))	
	err("b must be double.");
      if (mxGetN(_W) != mxGetNumberOfElements(_vps))
	err("#cols(W) != #viewpoints");
      if (mxGetNumberOfElements(_b) != mxGetNumberOfElements(_vps))
	err("#elem(b) != #viewpoints");
    }

  // check y_gt, if there is
  // it seems if ygt is 5*1, matlab will always?sometimes pass it as 1*5
  if (mode % 2 == 0 && mxGetM(_ygt)*mxGetN(_ygt)!=5 && mxGetM(_ygt) != 5) 
    {
      mexPrintf("\nmxGetM(_ygt)=%d, mxGetN(_ygt)=%d\n", mxGetM(_ygt), mxGetN(_ygt));
      err("#rows(ygt) must be 5 (bb4+vp1)");
    }

  return mode;
}

/////////////////////////////////////////////////////////////////////////////
// get_args
/////////////////////////////////////////////////////////////////////////////

// get input arguments
void get_args(int nrhs, const mxArray* prhs[], Params& para, const int mode)
{
  // kernel type and SP levels
  const mxArray * _Kx 		= prhs[0];
  // feature related
  const mxArray * _inthist	= prhs[1];
  const mxArray * _nlv	 	= prhs[2];
  const mxArray * _imwh		= prhs[3];
  const mxArray * _xgrid 	= prhs[4];
  const mxArray * _ygrid 	= prhs[5];
  // search space related
  const mxArray * _bbsizes 	= prhs[6];
  const mxArray * _vps	 	= prhs[7];
  // [ground truth]
  const mxArray * _ygt		= prhs[8];
  // [support vector model]
  const mxArray * _W 		= prhs[9];
  const mxArray * _b		= prhs[10];

  int* sz_inthist = (int*)mxGetDimensions(_inthist);
  int nvw 	= sz_inthist[4]; // kengdie!
  int nlv 	= (int)mxGetScalar(_nlv);
  int dsp 	= nvw * (exp2(2*nlv) - 1) / 3;

  int* imwh     = (int*)mxGetData(_imwh);
  int imw	= imwh[0];
  int imh	= imwh[1];
  int nxgrid  	= mxGetNumberOfElements(_xgrid);
  int nygrid  	= mxGetNumberOfElements(_ygrid);
  int* xgrid 	= (int*)mxGetData(_xgrid);
  int* ygrid 	= (int*)mxGetData(_ygrid);

  int nsize	= mxIsEmpty(_bbsizes) ? 0 : mxGetN(_bbsizes);
  int* bbsizes 	= mxIsEmpty(_bbsizes) ? NULL : (int*)mxGetData(_bbsizes);
  int nvp 	= mxGetNumberOfElements(_vps);
  double* pvps 	= mxGetPr(_vps);

  //////////////////////////////////////////////////////////
  // update para
  para.imh = (int)imh;
  para.imw = (int)imw;
  para.dsp = (int)dsp;
  //
  //===== kernel ===== 
  char kx[16] = "test";
  mxGetString(_Kx, kx, 16);
  if (VERBOSE)
    mexPrintf(" (%s)\n", kx);
  if (kx[0] == 'n' || kx[0] == 'r')
    para.normalize = true;
  else
    para.normalize = false;
  //
  //===== search space related =====
  para.bbsizes.clear();
  for (int b = 0; b < nsize; b++)
    {
      // NOTE: w comes before h
      int w = bbsizes[2*b+0];
      int h = bbsizes[2*b+1];
      if (h > imh || w > imw || h < 1 || w < 1)
	{
	  mexPrintf("BOX #%d OOB, omitted: %dx%d, im: %dx%d\n", 
		    b+1, w, h, imw, imh);
	}
      para.bbsizes.push_back(bbsize(w, h)); 
    }
  vps.clear();
  for (int v = 0; v < nvp; v++)
    {
      double vp = pvps[v];
      if (vp < 0)  err("negative viewpoint value");
      vps.push_back(vp);
    }

  //===== y_gt =====
  std::vector<Box> gtlabel(0);
  if (mode % 2 == 0)
    {
      double * ygt = (double*)mxGetPr(_ygt);
      int ngt      = (mxGetM(_ygt)*mxGetN(_ygt)==5)? 1:mxGetN(_ygt);
      for (int y = 0; y < ngt; y++)
	{
	  // NOTE: v should be actual vp value
	  float l = ygt[y*4+0];	 
	  float t = ygt[y*4+1];
	  float r = ygt[y*4+2];	 
	  float b = ygt[y*4+3];
	  float v = ygt[y*4+4];
	  if (l >= r || t >= b || l < 1 || t < 1 || r > imw || b > imh || v < 0)
	    {
	      mexPrintf("\nGT#%d ERROR: l=%g r=%g t=%g b=%g v=%g | im: %dx%d\n", 
			y+1, l, r, t, b, v, imw, imh);
	      err("problem with gt label", false);
	    }
	  gtlabel.push_back(Box(l, t, r, b, v));
	}
    }
  //
  //===== SV model =====
  std::vector<double*> w_ptrs(0);
  std::vector<double>  bias(0);
  if (mode > 0)
    {
      if (mxGetM(_W) != dsp)
	err("SV dims don't match inthist+nlv");
      double * wptr = mxGetPr(_W);
      double * bptr = mxGetPr(_b);
      for (int s = 0; s < nvp; s++)
	{
	  w_ptrs.push_back(wptr + s*dsp); 
	  bias.push_back(bptr[s]);
	}
    }
  //
  // feature grid
  xpos.clear();
  ypos.clear();
  for (int x = 0; x < nxgrid; x++)
    {
      xpos.push_back(xgrid[x]);
    }
  for (int y = 0; y < nygrid; y++)
    {
      ypos.push_back(ygrid[y]);
    }
  sort(xpos.begin(), xpos.end());
  sort(ypos.begin(), ypos.end());
  //
  // now augment using ground truths
  xpos_aug = xpos;
  ypos_aug = ypos;
  for (int gt = 0; gt < gtlabel.size(); gt++)
    {
      if (find(xpos_aug.begin(), xpos_aug.end(), gtlabel[gt].l) != xpos_aug.end())
	xpos_aug.push_back(gtlabel[gt].l);
      if (find(ypos_aug.begin(), ypos_aug.end(), gtlabel[gt].t) != ypos_aug.end())
	ypos_aug.push_back(gtlabel[gt].t);
    }
  sort(xpos_aug.begin(), xpos_aug.end());
  sort(ypos_aug.begin(), ypos_aug.end());
  //
  //===== additional parameters =====
  double alpha  = 0.5;
  double lambda = 1.0;

  //////////////////////////////////////////////////////////
  // finally, set up quality bound class
  //
  //mexPrintf("going into bnbQuality::init()\n");
  para.quality.init(kx, _inthist, nvw, nlv, dsp, imw, imh, gtlabel, 
		    w_ptrs, bias,  
		    alpha, lambda);
  //mexPrintf("bnbQuality::init() returned\n");
}

