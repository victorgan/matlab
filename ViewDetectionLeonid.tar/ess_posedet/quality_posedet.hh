/*
 * ==========================================================================
 *    Description:  MEX'd efficient subwindow search
 *                  for joint detection and viewpoint estimation
 *        Created:  03/31/2013 04:08:10 PM
 *         Author:  Kun He
 *   Organization:  Boston University
 * ==========================================================================
 *
 * various quality bounds for spatial pyramid features
 */
#ifndef _QUALITY_VIEWDET_HH
#define _QUALITY_VIEWDET_HH

#include <cmath>
#include <queue>
#include <vector>
#include <string>
#include <valarray>
#include <algorithm>
#include "matrix.h"

#ifdef _USE_ARMADILLO_
#include <armadillo>
typedef arma::vec Vec;
#else
#include <valarray>
typedef std::valarray<double> Vec;
#endif

const double PI = 3.1415926536;


///////////////////////////////////////////////////////////////////////////////
class Box
{
public:
  int l, t, r, b;
  double vp;
  double score;
  Box(){}
  Box(float a) 
    { l = t = r = b = vp = score = a; }
  Box(int ll, int tt, int rr, int bb, double vv = 0, double ss = 0)
    { l = ll; t = tt; r = rr; b = bb; vp = vv;  score = ss; }
  Box(const Box * bb) 
    { l = bb->l;  t = bb->t;  r = bb->r;  b = bb->b;  vp = bb->vp; }
};

inline double area_ov(const Box& b1, const Box& b2)
{
  int l = std::max(b1.l, b2.l), t = std::max(b1.t, b2.t);
  int r = std::min(b1.r, b2.r), b = std::min(b1.b, b2.b);
  if (l >= r || t >= b)
    return 0;
  else
    return (r - l) * (b - t);
}

class SV
{
public:
  double coef;
  Box label;
  Vec hist;
#ifdef _USE_ARMADILLO_
  arma::uvec ipos;
  arma::uvec ineg;
#endif

  // constructors
  SV(){}
  SV(const int dsp) 
    { 
#ifdef _USE_ARMADILLO_
      hist.zeros(dsp);
#else
      hist.resize(dsp, 0); 
#endif
    }
  SV(const int L, const int dhist)
    {
      int dsp = dhist * (exp2(2*L) - 1) / 3;
#ifdef _USE_ARMADILLO_
      hist.zeros(dsp);
#else
      hist.resize(dsp, 0);
#endif
    }
  SV(const Vec & inH);

  SV(const double a, double * ptr, const int L, 
     const int dhist, const int dsp, const Box y);
};

// used to represent search states
class sstate
{
public:
  short px[2], py[2];	// positions in xgrid & ygrid (trade space for time)
  short  l[2],  t[2];	// actual limits on x1, y1
  short vpid;		// vp index
  short maxgtid;	// fix for multiple ground truths

  short w, h;	// target bb size
  float upper;	// score upper bound, = score for single state
  float lower;	// score lower bound, = score for single state
  float Delta;

  sstate() {}
  sstate(int ww, int hh, int pxlo, int pxhi, int pylo, int pyhi, 
	 int xlo, int xhi, int ylo, int yhi, int v)
    {
      w     = ww;  
      h     = hh;
      vpid  = v;
      upper = lower = 0;
      px[0] = pxlo; px[1] = pxhi;
      py[0] = pylo; py[1] = pyhi;
      l[0]  = xlo;  l[1]  = xhi;
      t[0]  = ylo;  t[1]  = yhi;
    }
  sstate(int ww, int hh, int xlo, int xhi, int ylo, int yhi, int v)
    {
      w = ww;  h = hh;
      vpid  = v;
      upper = lower = 0;
      l[0]  = xlo;  t[0] = ylo;  l[1] = xhi;  t[1] = yhi;
    }
  sstate(const Box& b) //TODO: does anything depend on the vp assignment?
    {
      l[0] = l[1] = b.l;  t[0] = t[1] = b.t;
      w = b.r - b.l;  
      h = b.b - b.t;
      upper = lower = b.score;
    }

  std::string tostring() const 
    {
      char cstring[80];
      if (upper>0 && lower>0)
	sprintf(cstring,"<%.3f %.3f> [%3dx%3d]{%3d-%3d,%3d-%3d,%2d}\n",
		upper, lower, w, h, l[0], l[1], t[0], t[1], vpid+1);
      else if (upper<0 && lower>0)
	sprintf(cstring,"<%.2f %.3f> [%3dx%3d]{%3d-%3d,%3d-%3d,%2d}\n",
		upper, lower, w, h, l[0], l[1], t[0], t[1], vpid+1);
      else if (upper>0 && lower<0)
	sprintf(cstring,"<%.3f %.2f> [%3dx%3d]{%3d-%3d,%3d-%3d,%2d}\n",
		upper, lower, w, h, l[0], l[1], t[0], t[1], vpid+1);
      else
	sprintf(cstring,"<%.2f %.2f> [%3dx%3d]{%3d-%3d,%3d-%3d,%2d}\n",
		upper, lower, w, h, l[0], l[1], t[0], t[1], vpid+1);
      return std::string(cstring);
    }
};

// helper routine for comparing elements in priority queue
class sstate_comparisson
{
public:
  bool operator() (const sstate* lhs, const sstate* rhs) const
    { return (lhs->upper < rhs->upper); }
};

// data structure for priority queue
typedef std::priority_queue 
<const sstate*, std::vector<const sstate*>, sstate_comparisson> 
sstate_heap;


///////////////////////////////////////////////////////////////////////////////
// generic quality function class for spatial pyramids
// abstract class - has pure virtual functions
class spQuality
{
public:
  int imh, imw;
  int dhist, nlevels, dsp;
  int normalize;
  double norm_out, norm_inn;
  std::vector<int> ndiv;
  std::vector<Vec> inout; // 1st intersection, 2nd union

  spQuality() {}
  ~spQuality(){}
  void init(int imw, int imh, int d, int splv, int dsp, int nor);
  void setup(const sstate * s, const mxArray* inthist);
  void build_spatial_pyramids(int w, int h, int x1, int x2, int y1, int y2, 
			      const mxArray* inthist, const int L);
  void get_sp(Vec& h, Box& b, const mxArray* inthist);

  // these depend on specific kernel and normalization scheme
  // therefore pure virtual
  virtual double UB(const SV& sv) = 0;
  virtual double LB(const SV& sv) = 0;
};

///////////////////////////////////////////////////////////////////////////////
// index-value pair
// aux. simple fix for multiple ground truths in an image
class ivpair
{
public:
  int idx;
  double val;
  ivpair(){}
  ivpair(int i, double v) { idx = i; val = v; }
};

// interfacing class
class bnbQuality
{
private:
  spQuality * spq;
  const mxArray * inthist;  // integral histogram
  std::vector<Box> gtbox;   // GTBBs
  std::vector<SV>     w_linsvm;  // linear classifiers
  std::vector<double> b_linsvm;  // bias terms
  double alpha;  // weighting of two losses
  double lambda; // scaling of vp loss

public:
  bnbQuality() 
    { 
      spq = NULL; 
    }
  ~bnbQuality() 
    {
      if (spq != NULL)
	delete spq;
    }

  void init(std::string Kx, const mxArray * P, // Kx & inthist
	    int D, int L, int Dsv,          // nvw, nlv, dsp
	    int W, int H,                   // imw, imh
	    std::vector<Box> & boxes_in,    // gt labels
	    std::vector<double*> & wptr,    // (linear) classifiers
	    std::vector<double> & bias,     // bias terms
	    double a_in, double l_in);      // weighting losses

  Box gtruth(const double in = 0) 
    { 
      if (gtbox.empty())
	return Box(in);
      else 
	return gtbox[in];
    }
  void print_gt()
    {
      for (int i = 0; i < gtbox.size(); i++)
	{
	  Box gt = gtbox[i];
	  mexPrintf("GT#%d [%3d %3d %3d %3d %.2f] <sc=%.3f>\n",
		    i+1, gt.l, gt.t, gt.r, gt.b, gt.vp, gt.score);
	}
    }
  void compute_bounds(sstate* state, bool getLB = true);
  void compute_upper_bound(sstate* state);
  void compute_lower_bound(sstate* state);
  double loss_vp(const sstate* s, const Box& gt);
  ivpair loss_lb(const sstate* s);
  ivpair loss_ub(const sstate* s);
  void get_output_hist(Vec& h, Box& b) { spq->get_sp(h, b, inthist); }
};

///////////////////////////////////////////////////////////////////////////////

// un-normalized linear kernel
class linQuality: public spQuality
{
public:
  linQuality(){}
  double UB(const SV& sv);
  double LB(const SV& sv);
};

// normalized linear kernel
class nlin1Quality: public spQuality
{
public:
  nlin1Quality(){}
  double UB(const SV& sv);
  double LB(const SV& sv);
};

// normalized linear kernel
class nlinQuality: public spQuality
{
public:
  nlinQuality(){}
  double UB(const SV& sv);
  double LB(const SV& sv);
};

// Hellinger kernel
class hellQuality: public spQuality
{
public:
  hellQuality(){}
  double UB(const SV& sv);
  double LB(const SV& sv);
};

// L1-normalized Hellinger kernel
class nhellQuality: public spQuality
{
public:
  nhellQuality(){}
  double UB(const SV& sv);
  double LB(const SV& sv);
};


#endif

