#ifndef _ESS_H
#define _ESS_H

#include <stdio.h>
#include <limits>
#include <string>
#include <queue>

// structure holding a single box
typedef struct { 
    int left;
    int top;
    int right;
    int bottom;
    double score;
} Box;

// structure to hold a set of boxes ( = a state during search)
class sstate {
public:
  float upper; 
  short low[4];   // there can be millions of sstates, better save some memory
  short high[4];

  // construct empty
  sstate() { }

  // construct as full search space
  sstate(int argwidth, int argheight) {
      upper = std::numeric_limits<float>::max();
      low[0] = 1; // because of padding column/row 0 are never part of a rectangle
      low[1] = 1;
      low[2] = 1;
      low[3] = 1;
      high[0] = argwidth-1;
      high[1] = argheight-1;
      high[2] = argwidth-1;
      high[3] = argheight-1;
  }

  std::string tostring() const {
      char cstring[80];
      sprintf(cstring, "low < %d %d %d %d > high < %d %d %d %d >\n", 
	      low[0],low[1],low[2],low[3],high[0],high[1],high[2],high[3]);
      return std::string(cstring);
  }

  // calculate argmax_i( high[i]-low[i] ), or -1 if high==low
  int maxindex(int maxwidth=0) const { 
      int splitindex = -1;
      //int maxwidth = 0;
      for (unsigned int i = 0; i < 4; i++) { 
	  int interval_width = high[i] - low[i];
	  if (interval_width > maxwidth) {
	      splitindex = i;
	      maxwidth = interval_width;
	  }
      }
      return splitindex;
  }

  bool islegal() const {
      return ((low[0] <= high[2]) && (low[1] <= high[3]));
  }

  // "less" is needed to compare states in stl_priority queue
  bool less(const sstate* other) const {
      return upper < other->upper;
  }
};


// helper routine for comparing elements in priority queue
class sstate_comparisson {
public:
  bool operator() (const sstate* lhs, const sstate* rhs) const {
      return lhs->less(rhs);
  }
};

// data structure for priority queue
typedef std::priority_queue<const sstate*, 
	std::vector<const sstate*>, 
	sstate_comparisson> sstate_heap;


//extern "C" 
//{
void pyramid_search(std::vector<Box>& boxes, int K, 
		    int argnumpoints, int argwidth, int argheight,
		    double* argxpos, double* argypos, double* argclst,
		    int argnumclusters, int argnumlevels, double* argweight);
//}

#endif
