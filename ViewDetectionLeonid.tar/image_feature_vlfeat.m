function ft = compute_image_feature(im, step, ftype, sizes)
% compute dense features (choices: HOG, DSIFT, ...)
% note step is density of sampling grid (spacing between feature points)
% note step must be even
[h w ~] = size(im); 
ft = [];
if strcmpi(ftype, 'hog')
	hogft = vl_hog(single(im), step);
	% construct the sampling grid.
	%xgrid = (step/2: step: w)+step/2;
	%ygrid = (step/2: step: h)+step/2;
	xgrid = (step/2: step: w);
	ygrid = (step/2: step: h);
	[hogh hogw hogdim] = size(hogft); 
	% DEBUG
	assert(length(xgrid)>=hogw && length(ygrid)>=hogh);
	if length(ygrid) > hogh, ygrid = ygrid(1:hogh); fprintf('!y'); end
	if length(xgrid) > hogw, xgrid = xgrid(1:hogw); fprintf('!x'); end
	ft.xgrid = uint16(xgrid); %ft.wgrid = hogw; 
	ft.ygrid = uint16(ygrid); %ft.hgrid = hogh;
	yx = allpairs(ygrid, xgrid);
	ft.xy = uint16(yx(:, [2 1]));
	% each col is a hog descriptor. order is consistent with ft.y & ft.x
	ft.data = [];
	for y = 1:hogh
		t = squeeze(hogft(y, :, :));
		ft.data = [ft.data t'];
	end

	% post processing
	xy = ft.xy;
	xx = xy(:, 1);
	yy = xy(:, 2);
	id = find(xx<w & yy<h);
	ft.xy = [xx(id), yy(id)];
	ft.data = ft.data(:, id);
	ft.xgrid = unique(ft.xy(:, 1))';
	ft.ygrid = unique(ft.xy(:, 2))';

elseif strcmpi(ftype, 'SIFTg')
	if ndims(im) == 3
		im = rgb2gray(im);
	end
	[xy, ft.data] = vl_phow(single(im), 'Step', step, 'Sizes', sizes);
	ft.xy = uint16(xy(1:2, :))';
	ft.xgrid = unique(ft.xy(:, 1))';
	ft.ygrid = unique(ft.xy(:, 2))';

elseif strcmpi(ftype, 'SIFTc')
	[xy, ft.data] = vl_phow(single(im), 'Step', step, 'Color', 'opponent', 'Sizes', sizes);
	ft.xy = uint16(xy(1:2, :))';
	ft.xgrid = unique(ft.xy(:, 1))';
	ft.ygrid = unique(ft.xy(:, 2))';

elseif strcmpi(ftype, 'RSIFTg')
	if ndims(im) == 3
		im = rgb2gray(im);
	end
	[xy, ft.data] = vl_phow(single(im), 'Step', step, 'Sizes', sizes);
	ft.xy = uint16(xy(1:2, :))';
	ft.xgrid = unique(ft.xy(:, 1))';
	ft.ygrid = unique(ft.xy(:, 2))';
	% hellinger-kernelize
	D = zeros(size(ft.data));
	for i = 1:size(ft.data, 2)
		d = double(ft.data(:, i));
		d = sqrt(d/max(norm(d, 1), 1e-6));
		D(:, i) = d;
	end
	ft.data = D;

elseif strcmpi(ftype, 'RSIFTc')
	[xy, ft.data] = vl_phow(single(im), 'Step', step, 'Color', 'opponent', 'Sizes', sizes);
	ft.xy = uint16(xy(1:2, :))';
	ft.xgrid = unique(ft.xy(:, 1))';
	ft.ygrid = unique(ft.xy(:, 2))';
	% hellinger-kernelize
	D = zeros(size(ft.data));
	for i = 1:size(ft.data, 2)
		d = double(ft.data(:, i));
		d = sqrt(d/max(norm(d, 1), 1e-6));
		D(:, i) = d;
	end
	ft.data = D;

end

