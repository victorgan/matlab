function [ybar, viobar, mbar, phibar, phigt, ygt] = mvc_face(...
	samples, model, params, topk, nms_th, lossfn)
% do loss-augmented branch-and-bound for samples
%   topk: number of detections to return per image
% nms_th: non-maximum suppression threshold
%
global ids xtrain ytrain
if nargin < 4, topk = 1; end
if nargin < 5, nms_th = .3; end
if nargin < 6, lossfn = params.lossfn; end

nsample = length(samples);
ntrain = length(ids.train);

%% convert SVs to the format that ess takes
fprintf('constructing Ws: '); tic;
nvp = size(params.viewpoints, 1);
W = zeros(params.ftdim, nvp);
parfor v = 1:nvp
	vp = params.viewpoints(v, :);
	agt = model.alph .* Ky_face(vp', model.ygt, params.kyparm);
	W(:, v) = W(:, v) + model.psigt * agt; 
	acp = model.alph .* Ky_face(vp', model.ycp, params.kyparm);
	W(:, v) = W(:, v) - model.psicp * acp; 
end
toc;

%% construct cutting plane
disp('starting inference');
viobar  = zeros(nsample, topk);
mbar    = zeros(nsample, topk);
ybar    = zeros(2, nsample, topk);
ygt     = zeros(2, nsample, topk);
phigt   = zeros(params.ftdim, nsample, topk);
phibar  = zeros(params.ftdim, nsample, topk);
parfor i = 1:nsample
	ii = samples(i);  % training index
	fprintf('[No.%4d  %s]', ii, ids.name{ids.train(ii)});
	gt = ytrain(ii, :);

	%score = W * xtrain.gtft(:, ii); 
	score = W' * xtrain.gtft(:, ii); 
	loss  = lossfn(ytrain(ii, :), params.viewpoints);
	[val, j] = sort(score + loss, 'descend');
	[~, gid] = ismember(ytrain(ii, :), params.viewpoints, 'rows');

	fprintf(' %.3f\n', score(gid));
	for k = 1:topk
		fprintf('pred (%+02d, %+02d), s=%.3f, L=%.3f, v=%.3f\n', ...
			params.viewpoints(j(k),1), params.viewpoints(j(k),2), ...
			score(j(k)), loss(j(k)), val(k)-score(gid));
	end
	fprintf('\n');

	viobar(i, :)    = val(1:topk) - score(gid);
	mbar(i, :)      = loss(j(1:topk));
	ybar(:, i, :)   = params.viewpoints(j(1:topk), :)';
	ygt(:, i, :)    = repmat(gt', [1 topk]);
	phibar(:, i, :) = repmat(xtrain.gtft(:, ii), [1 topk]);
	phigt(:, i, :)  = repmat(xtrain.gtft(:, ii), [1 topk]);
end


