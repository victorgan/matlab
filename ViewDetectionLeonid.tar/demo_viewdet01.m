function [ap, mppe, mae] = demo_viewdet_01(...
	cls, ftype, nvw, nlv, ...  % mandatory
	Kx, C, split)              % optional

if nargin < 5, Kx = 'nlin2'; end;
if nargin < 6, C = 200; end;
if nargin < 7, split = 1; end

global ids xtrain xtest ytrain ytest

%% set parameters
if strcmpi(Kx, 'nlin'), Kx = 'nlin2'; end
if ~strcmpi('epfl', cls) && ~strcmpi('epflcars', cls)
	params = init_viewdet('3dobj', cls, ...
		ftype, nvw, nlv, Kx, pi/10, 0.5, 5, 8, split);
else
	cls = 'epflcars';
	params = init_viewdet(cls, cls, ...
		ftype, nvw, nlv, Kx, pi/10, 0.5, 3, 16, split);
end
params.altname = '-1vsall';
%params.altname = '-hardmine';

%% prepare data, load data
okfn = sprintf('%s/%s_std%d_%s.ok', params.modeldir, cls, params.nstdbb, params.IHstr);
if ~exist(okfn, 'file')
	data_viewdet(params);
	system(sprintf('touch %s\n', okfn), '-echo');
end
if isempty(ids) || isempty(xtrain) || isempty(ytrain)
	disp('loading data');
	[ids, xtrain, ytrain, xtest, ytest] = loaddata_viewdet(cls, params, 0);
end
ntrain = length(ids.train)

%% train model
modeldir = sprintf('%s/%s_split%d/%s%s/', params.modeldir, cls, params.split, ...
	params.IHstr, params.altname);
if ~exist(modeldir, 'dir'), mkdir(modeldir); end
modelstr = sprintf('std%d_%d%s_C%g', params.nstdbb, nlv, params.Kx, C)
modelfn = [modeldir modelstr '.mat'];
try 
	load(modelfn);
catch
	%model_sum = svm_1vsall_hardmine(C, params, modelfn);
	model_sum = svm_1vsall_viewdet(C, params);
	save(modelfn, 'model_sum', '-v7.3');
end

%% do testing
nvp = length(params.viewpoints);
try 
	resfn = sprintf('%s/res_%s.mat', modeldir, modelstr);
	load(resfn);
catch
	W = model_sum.w;
	b = model_sum.b;

	% prepare
	bbsize = uint32(xtrain.stdbb);
	step   = uint32(params.gridstep);
	nlv    = uint32(params.pyralvs);
	vps    = (params.viewpoints-1) * 2*pi/nvp;
	ntest  = length(ids.test);

	% do detection
	disp('start testing');
	ydet = zeros(5, ntest);
	sdet = zeros(ntest, 1);
	fdet = zeros(params.ftdim, ntest);
	parfor i = 1 : ntest
		idx = ids.test(i);
		fprintf('\n[%d/%d: %s] ', i, ntest, ids.name{idx});
		tic;
		IHfn = sprintf('%s%s_%s.mat', params.localdir, ids.name{idx}, params.IHstr);
		ld = load(IHfn);
		ihist = double(ld.inthist);
		xx = uint32(ld.xgrid);
		yy = uint32(ld.ygrid);
		wh = uint32(ld.imwh);
		[ydet(:, i), sdet(i), ~, fdet(:, i), ~] = ess_viewdet(...
			params.Kx, ...     % kernel type & spatial pyamid levels   
			ihist, nlv, ...    % integral histogram & feature grid step
			wh, xx, yy, ...    % image dimensions, feature grid
			bbsize, vps, ...   % bb sizes + vps to search for
			[], W, b);
		toc;
	end
	% save
	save(resfn, 'ydet', 'sdet', 'fdet');
end

% localization
ovth = 0:0.02:1;
[ap, rec, ratio] = report_det(sdet, ydet(1:4, :), ytest(1:4, :), ovth, 33);
title([params.cls '  AP=' num2str(ap)]);
fprintf('detection: AP=%g\n', ap);

% pose estimation
idx = find(ratio > 0.5);
fprintf('%d/%d correct detections\n', length(idx), length(ratio));
[mppe, mae, cfm, vdif] = report_pose(ydet(5,idx), ytest(5,idx), nvp);
title(['BnB  MPPE=' num2str(mppe) '  MAE=' num2str(mae)]);
fprintf('pose est: MPPE=%g, MAE=%g\n', mppe, mae);
figure(34); 
%cfm  = cfm ./ repmat(sum(cfm, 2), [1 nvp]);
imagesc(cfm);
set(gcf, 'position', [randi([200 600], 1, 2) 440 330]);

%% clean up
ids = [];
xtrain = []; xtest = [];
ytrain = []; ytest  = [];



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ap, rec, ratio] = report_det(sdet, bb_det, bb_gt, ovth, figno, color)
if nargin < 6, color = 'b'; end
A_int   = diag(rectint(bb2rect(bb_det'), bb2rect(bb_gt')));
A_det   = diag(rectint(bb2rect(bb_det'), bb2rect(bb_det')));
A_gt    = diag(rectint(bb2rect(bb_gt'), bb2rect(bb_gt')));
ratio   = A_int ./ (A_gt+A_det-A_int);
fprintf('min overlap: %g, %d/%d < 50%%\n', min(ratio), sum(ratio<.5), length(ratio));

[val, ind] = sort(-sdet);
fp = ratio < 0.5;
fp = fp(ind); 
tp = 1 - fp;
rec = cumsum(tp) / length(tp);
prec = cumsum(tp) ./ (cumsum(fp)+cumsum(tp));
ap = [];
for t = 0: 0.1: 1
	p = max(prec(rec >= t));
	if isempty(p), p = 0; end
	ap = [ap p];
end
ap = mean(ap);

% precision-recall
figure(figno-1);
plot(rec, prec, '-', 'linewidth', 2); grid on; xlim([0 1]); ylim([0 1.05]);
title(['AP=' num2str(ap)]);

% overlap-recall
rec = zeros(size(ovth));
for t = 1:length(ovth)
	rec(t) = mean(ratio > ovth(t));
end
figure(figno);
plot(ovth, rec, 'color', color, 'linewidth', 2);
%hold on; plot([0.5 0.5], [0 2], '--r', 'linewidth', 2); hold off;
grid on; ylim([0 1.05]); ylabel 'recall'; xlabel 'overlap';
set(gcf, 'position', [randi([200 600], 1, 2) 440 330]);


function [mppe, mae, cfm, vdf] = report_pose(vdet, vtest, nvp)
% viewpoint
dvp  = 2*pi/nvp;
vpr  = mod(round(vdet/dvp), nvp)+1;
vgt  = mod(round(vtest/dvp), nvp)+1;
mppe = 1 - mean((vpr-vgt) ~= 0);
vdf  = abs(vdet-vtest) * 180/pi;
vdf(vdf<0) = vdf(vdf<0) + 360;
vdf(vdf>360) = vdf(vdf>360) - 360;
vdf  = min(vdf, 360-vdf);
mae  = mean(vdf);
median(vdf)
cfm  = confusionmat(vgt, vpr, 'order', 1:nvp);

