function run_3dobj(sig, alph, C, nsample, classes)
if nargin < 4
	nsample = 36;
end
if nargin < 5
	%classes = {'bicycle' 'car' 'cellphone' 'iron' 'mouse' 'shoe' 'stapler' 'toaster'};
	classes = {'bicycle' 'car'};
end
global ids xtrain ytrain xtest ytest

curbest = 0; ap_best = 0; mppe_best = 0;
aps = []; mppes = []; maes = [];

for c = randperm(length(classes))
	for split = 1:10
		a = tic;
		[ap, mppe, mae] = demo_viewdet(classes{c}, ...
			{'SIFTg'}, [500], 3, 'nlin2', sig, alph, ...
			C, nsample, '-newloss', split);
		toc(a);
		aps  (split) = ap;
		mppes(split) = mppe;
		maes (split) = mae;
		if ap+mppe > curbest
			ap_best   = ap;
			mppe_best = mppe;
			curbest   = ap+mppe;
		end
		drawnow; pause(1)
	end
	fprintf('==========================================\n');
	fprintf('mAP %.3f+/-%.3f, mMPPE %.3f+/-%.3f, mMAE %.3f\n', ...
		mean(aps), std(aps), mean(mppes), std(mppes), mean(maes));
	fprintf('best AP %.3f, best MPPE %.3f\n', ap_best, mppe_best);
	fprintf('==========================================\n\n');
	ids = [];
	xtrain = [];  xtest = [];
	ytrain = [];  ytest = [];
end

