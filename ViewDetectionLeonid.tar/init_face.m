function params = init_face(ds, nlv, sig, Kx) 
addpath('_util');
params = struct('ds', ds);

% dirs
params.basedir  = [absolutepath('..', pwd) '/']; % humanpose
params.cachedir = [params.basedir '_cache/'];
params.localdir = [params.cachedir '/' ds '/'];
params.modeldir = [params.cachedir '/' ds 'model/'];
if ~exist(params.cachedir, 'dir'), mkdir(params.cachedir); end
if ~exist(params.localdir, 'dir'), mkdir(params.localdir); end
if ~exist(params.modeldir, 'dir'), mkdir(params.modeldir); end
params.cls   = 'pointing04';
params.dsdir = [params.basedir 'Data/pointing04'];
params.imdir = [params.dsdir '/'];

% weighting the 2 losses
params.alpha = 0.5;

% compatibility
params.gridstep = 6;
params.vwords = 100;
params.siftsizes = [4];
params.fttype = {'HOG'};

% feature
params.pyralvs = nlv;
params.ftdim = 2124;%4*(4^nlv-1)/3 * 31;
params.IHstr = sprintf('HOG_lv%d', nlv);

% kernel
if strcmpi(Kx, 'nlin'), Kx = 'nlin2'; end
if strcmpi(Kx, 'nchi2') || strcmpi(Kx, 'chi2')
	params.ftdim = params.ftdim * 7;
end
params.Kx = Kx;
params.Ky = 'gauss';
% note: sig should be in degrees
if isscalar(sig)
	sig = diag([sig sig]);
else
	assert(size(sig) == [2 2]);
	[R, p] = chol(sig);
	assert(p == 0);
end
%params.kyparm = sig;
params.kyparm = inv(sig^2); % note save computation in Ky
params.kfunc = @kernel_face;

% vlfeat
vlfeatdir = [params.basedir 'Libs/vlfeat/'];
run([vlfeatdir 'toolbox/vl_setup']); 

% for this dataset, do 5-fold cross validation
for i = 1:10
	rng(i*12345);
	params.iperm{i} = randperm(15);
end
rng('shuffle');
params.itrain = {4:15, [1:3 7:15], [1:6 10:15], [1:9 13:15], 1:12, ...
	7:15, [1:3 10:15], [1:6 13:15], [1:9], [4:10]};
params.itest  = {1:3, 4:6, 7:9, 10:12, 13:15, ...
	1:6, 4:9, 7:12, 10:15, [12:15 1:3]};

params.viewpoints = [-90 0; allpairs([-60 -30:15:30 60], -90:15:90); 90 0];

