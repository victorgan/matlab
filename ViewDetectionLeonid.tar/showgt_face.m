function show_gt_face(inds, saveimg)
if nargin < 2, saveimg = false; end
global ids
cls   = 'pointing04';
nlv   = 3;
Kx    = 'nlin2';
sig   = pi/10;
split = 1;
params = init_face(cls, nlv, sig, Kx);
ids = load(sprintf('%s/%s_ids.mat', params.modeldir, params.cls));
%[ndata, inst, name, imfn, bbvp] = data_pointing04(params);
inst_perm = params.iperm{1};
ids.test  = find(ismember([ids.inst], inst_perm(params.itest{split})));
ytest     = ids.bbvp(ids.test, :);

for i = inds
	display_face(ytest(i, 1:4), [], ytest(i, 5:6), i);
	if saveimg
		export_fig(sprintf('%s_gt%d.png', cls, i));
	else
		pause(.2);
	end
end

