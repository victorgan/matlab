function [ybar, viobar, mbar, phibar, phigt, ygt] = mvc_face(...
	samples, model, params, topk, nms_th, lossfn)
% do loss-augmented branch-and-bound for samples
%   topk: number of detections to return per image
% nms_th: non-maximum suppression threshold
%
global ids xtrain ytrain
if nargin < 4, topk = 1; end
if nargin < 5, nms_th = .3; end
if nargin < 6, lossfn = params.lossfn; end

nsample = length(samples);
ntrain = length(ids.train);

%% model compression
%{
fprintf('compressing the model: '); tt = tic;
nsv   = length(model.alph);
% gt
[psigt, ia, ic] = unique(model.psigt', 'rows');
ngt = length(ia);
ygt = model.ygt(:, ia);
agt = zeros(ngt, 1);
for c = 1:nsv
	agt(ic(c)) = agt(ic(c)) + model.alph(c);
end
% cp
[psicp, ia, ic] = unique([model.psicp' model.ycp'], 'rows');
psicp = psicp(:, 1:end-2);
ncp = length(ia);
ycp = model.ycp(:, ia);
acp = zeros(ncp, 1);
for c = 1:nsv
	acp(ic(c)) = acp(ic(c)) + model.alph(c);
end
ncp = nsv;
%}
agt = model.alph;
acp = model.alph;
ygt = model.ygt;
ycp = model.ycp;
psigt = model.psigt';
psicp = model.psicp';
A = [agt; -acp];
Y = [ygt, ycp];
F = [psigt; psicp];
%fprintf('%d*2 -> %d+%d SVs in %.2fsec\n', nsv, ngt,ncp, toc(tt));

% test
do_refine = ~mod(samples(1),5)


%% convert SVs to linear classifiers
fprintf('constructing Ws: '); tic;
invsig2 = params.kyparm;
nvp = size(params.viewpoints, 1);
W = zeros(params.ftdim, nvp);
parfor v = 1:nvp
	vp = params.viewpoints(v, :);
	%agt = model.alph .* Ky_face(vp', model.ygt, params.kyparm);
	%W(:, v) = W(:, v) + model.psigt * agt; 
	%acp = model.alph .* Ky_face(vp', model.ycp, params.kyparm);
	%W(:, v) = W(:, v) - model.psicp * acp; 
	coef = A .* Ky_face(vp', Y, params.kyparm);
	W(:, v) = F' * coef;
end
toc;


%% inference: with continuous refinement
disp('starting inference');
viobar  = zeros(nsample, topk);
mbar    = zeros(nsample, topk);
ybar    = zeros(2, nsample, topk);
ygt     = zeros(2, nsample, topk);
phigt   = zeros(params.ftdim, nsample, topk);
phibar  = zeros(params.ftdim, nsample, topk);
opt     = optimset('display', 'off', 'algorithm', 'active-set', 'tolx', 5e-3);
parfor i = 1:nsample
	ii = samples(i);  % training index
	fprintf('[No.%4d  %s]', ii, ids.name{ids.train(ii)});
	gt  = ytrain(ii, :);
	phi = xtrain.gtft(:, ii);

	% get top K
	score = W' * phi;
	loss  = lossfn(gt, params.viewpoints);
	[val, j] = sort(score + loss, 'descend');
	[~, gid] = ismember(gt, params.viewpoints, 'rows');
	sgt = score(gid);
	fprintf(' sgt=%.3f\n', sgt);

	ydet = params.viewpoints(j(1:topk), :)';
	sdet = score( j(1:topk) );
	vdet = val(1:topk) - sgt;
	ldet = loss(j(1:topk));

	% now, refine
	for k = 1:topk
		if do_refine
			vk = ydet(:, k);
			ky = Ky_face(vk, Y, invsig2);
			id = find(ky > 1e-3);
			if length(id) < 10
				fprintf('pred (%.1f, %.1f), s=%.3f, L=%.3f, v=%.3f\n', ...
					ydet(1, k), ydet(2, k), sdet(k), ldet(k), vdet(k));
				continue;
			end
			coeff = A(id) .* (F(id,:)*phi);
			theta = Y(:, id);
			[vk1, fval] = fmincon(@(x) -obj(x, coeff, theta, invsig2), vk, [], [], [], [], ...
				max(vk-[15;15]/3, -90), min(vk+[15;15]/3, 90), [], opt);
			ydet(:, k) = vk1;
			sdet(k) = -fval;
			ldet(k) = lossfn(gt, vk1');
			vdet(k) = sdet(k) + ldet(k) - sgt;
		end
		fprintf('pred (%.1f, %.1f), s=%.3f, L=%.3f, v=%.3f\n', ...
			ydet(1, k), ydet(2, k), sdet(k), ldet(k), vdet(k));
	end
	fprintf('\n');

	% save
	viobar(i, :)    = vdet;
	mbar(i, :)      = ldet;
	ybar(:, i, :)   = ydet;
	ygt(:, i, :)    = repmat(gt', [1 topk]);
	phibar(:, i, :) = repmat(phi, [1 topk]);
	phigt(:, i, :)  = repmat(phi, [1 topk]);
end

function f = obj(x, coeff, theta, invsig2)
k = Ky_face(x, theta, invsig2);
f = coeff' * k;

