function model = svm_1vsall_viewdet(C, params)
global ids ytrain
ntrain = length(ids.train);
nlv    = uint32(params.pyralvs);
nvp    = length(params.viewpoints);
dvp    = 2*pi/nvp;
gtftfn = sprintf('%s/GT%s_%s_%d%s.mat', ...
	params.modeldir, params.cls, params.IHstr, nlv, params.Kx);
try 
	load(gtftfn);
	fprintf('loaded: %s\n', gtftfn);
catch
	% load integral histograms and feature info
	tic; disp('loading integral histograms');
	IHfn = [];
	for i = 1:ntrain
		idx = ids.train(i);
		IHfn{i} = sprintf('%s%s_%s.mat', params.localdir, ids.name{idx}, params.IHstr);
	end
	toc;

	% compute ground truth features
	disp('computing ground truth features');
	gtft = zeros(params.ftdim, ntrain);
	vps  = (params.viewpoints-1) * dvp;
	parfor i = 1:ntrain
		fprintf('\n[#%4d: %s] ', i, ids.name{ids.train(i)});
		ld = load(IHfn{i});
		ihist = double(ld.inthist);
		xx = uint32(ld.xgrid);
		yy = uint32(ld.ygrid);
		wh = uint32(ld.imwh);

		[~, ~, ~, fgt, ~] = ess_viewdet(...
			params.Kx, ...    % kernel types
			ihist, nlv, ...   % integral histogram & SP levels
			wh, xx, yy, ...   % image dimensions, feature grid
			[], vps, ...      % bb sizes + vps to search for
			ytrain(:, i), ... % ground truths
			[], []);          % classifiers
		gtft(:, i) = fgt;

		bbsize = uint32(randi([50 100],2,1));
		[~, ~, ~, frand, ~] = ess_viewdet(...
			params.Kx, ...      % kernel types
			ihist, nlv, ...     % integral histogram & SP levels
			wh, xx, yy, ...     % image dimensions, feature grid
			bbsize, vps, ...    % bb sizes + vps to search for
			ytrain(:, i), ...   % ground truths
			[], []);            % classifiers
		randft(:, i) = frand;

	end
	save(gtftfn, 'gtft', 'randft');
	fprintf('saved to: %s\n', gtftfn);
end

W = zeros(params.ftdim, nvp);
b = zeros(nvp, 1);
nrand = size(randft, 2);
for v = 1:nvp
	yv = -ones(ntrain, 1);
	df = abs(ytrain(5, :) - (v-1)*dvp);
	df = min(df, 2*pi-df);
	iv = find(df <= dvp/2);
	yv(iv) = 1;
	fprintf('\nvp#%d, pos=%d, neg=%d\n', v, sum(yv==1), sum(yv==-1)+nrand);
	% add random BG samples
	md = libsvmtrain([yv; -ones(nrand,1)], [gtft randft]', sprintf('-c %g -t 0', C));
	W(:, v) = md.SVs' * md.sv_coef;
	b(v) = -md.rho;
end
model.w = W;
model.b = b;

