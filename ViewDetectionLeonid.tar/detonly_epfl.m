function ap = detonly_epfl(C)
if nargin < 1, C = 250.1, end;
global ids
% struct ids: 
%  inst - object instance numbers
%  name - image names
%  imfn - actual image file paths
% train - list of IDs in training set
%  test - list of IDs in testing set
global xtrain xtest
% struct xtrain:
% inthist - cell array of integral histograms for each ID (loaded on demand)
%    gtft - ground truth feature vectors
global ytrain ytest
% matrix ytrain:
%   ntrain*5 double, each row is a training label

%% set parameters
cls = 'epflcars';
nvp = 1;
sig = pi/15;
nlv = 3;
nbb = 3;
params = init_viewdet(cls, cls, {'SIFTg'}, 500, nlv, 'nlin2', sig, 1.0, nbb, nvp);
params.altname = '-detonly';

%% prepare data, load data
okfn = sprintf('%s/%s_std%d_%s.ok', params.modeldir, cls, params.nstdbb, params.IHstr);
if ~exist(okfn, 'file')
	data_viewdet(params);
	system(sprintf('touch %s\n', okfn), '-echo');
end
if isempty(ids) || isempty(xtrain) || isempty(ytrain)
	disp('loading data');
	[ids, xtrain, ytrain, xtest, ytest] = loaddata_viewdet(cls, params, 0);
	% important for detonly
	ytrain(5, :) = 0;
	ytest (5, :) = 0;
end
ntrain = length(ids.train)

%% train model
nsample = 48;
params.max_planes = 2*ceil(ntrain/nsample);
params.svm_C      = C;
params.sampling   = 'S';
do_sampling = (nsample < ntrain);

modeldir = sprintf('%s/%s_split%d/%s%s/', params.modeldir, cls, params.split, ...
	params.IHstr, params.altname);
if ~exist(modeldir, 'dir'), mkdir(modeldir); end
modelstr = sprintf('std%d_%d%s_C%g', nbb, nlv, params.Kx, C);
if do_sampling
	modelstr = [modelstr '_sam' num2str(nsample)]
end
modelfn = [modeldir modelstr '.mat'];

try 
	load(modelfn);
catch
	if do_sampling
		model_sum = sampledcuts_2pass(ntrain, nsample, params, @bnbinit_viewdet, @bnb_viewdet, 2);
	else
		[model_sum, ~] = structSVM_1slack_ker(1:ntrain, params, @bnbinit_viewdet, @bnb_viewdet, 2);
	end
	save(modelfn, 'model_sum', '-v7.3');
end

%% test model
ap = test_detonly(model_sum, modeldir, modelstr, params);

%% clean up
ids = [];
xtrain = [];
ytrain = [];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ap = test_detonly(model, modeldir, modelstr, params)
global xtrain xtest ytest ids

% load test data
ntest = length(ids.test)
nvp = length(params.viewpoints);

%% do testing
try 
	resfn = sprintf('%s/res_%s.mat', modeldir, modelstr);
	load(resfn);
catch
	% convert SVs to the format that ess takes
	fprintf('constructing Ws: '); tic;
	W = zeros(params.ftdim, nvp);
	parfor v = 1:nvp
		vp = (v-1) * 2*pi/nvp;

		vgt = model.ygt(5, :);
		dv  = abs(vp - vgt);
		dv  = min(dv, 2*pi-dv);
		agt = model.alph .* exp(-dv.^2/2/params.kyparm^2)';
		W(:, v) = W(:, v) + model.psigt * agt; 

		vcp = model.ycp(5, :);
		dv  = abs(vp - vcp);
		dv  = min(dv, 2*pi-dv);
		acp = model.alph .* exp(-dv.^2/2/params.kyparm^2)';
		W(:, v) = W(:, v) - model.psicp * acp; 
	end
	toc;

	% prepare
	bbsize = uint32(xtrain.stdbb);
	step   = uint32(params.gridstep);
	nlv    = uint32(params.pyralvs);
	vps    = (params.viewpoints-1) * 2*pi/nvp;

	% do detection
	disp('start testing');
	ydet = zeros(5, ntest);
	sdet = zeros(ntest, 1);
	fdet = zeros(params.ftdim, ntest);
	parfor i = 1 : ntest
		idx = ids.test(i);
		fprintf('\n[%d/%d: %s] ', i, ntest, ids.name{idx});
		tic;
		IHfn = sprintf('%s%s_%s.mat', params.localdir, ids.name{idx}, params.IHstr);
		ld = load(IHfn);
		ihist = double(ld.inthist);
		xx = uint32(ld.xgrid);
		yy = uint32(ld.ygrid);
		wh = uint32(ld.imwh);
		[ydet(:, i), sdet(i), ~, fdet(:, i), ~] = ess_viewdet(...
			params.Kx, ...     % kernel type & spatial pyamid levels   
			ihist, nlv, ...    % integral histogram & feature grid step
			wh, xx, yy, ...    % image dimensions, feature grid
			bbsize, vps, ...   % bb sizes + vps to search for
			[], W, []);
		toc;
	end
	% save
	save(resfn, 'ydet', 'sdet', 'fdet');
end
[val, ind] = sort(-sdet);

% localization
bb_det  = ydet(1:4, :);
bb_gt   = ytest(1:4, :);
A_int   = diag(rectint(bb2rect(bb_det'), bb2rect(bb_gt')));
A_det   = diag(rectint(bb2rect(bb_det'), bb2rect(bb_det')));
A_gt    = diag(rectint(bb2rect(bb_gt'), bb2rect(bb_gt')));
ratio   = A_int ./ (A_gt+A_det-A_int);
fprintf('min overlap: %g, %d/%d < 0.5\n', min(ratio), sum(ratio<.5), length(ratio));
fp = ratio < 0.5;
fp = fp(ind); 
tp = 1 - fp;
rec = cumsum(tp) / ntest;
prec = cumsum(tp) ./ (cumsum(fp)+cumsum(tp));
ap = 0;
for t = 0: 0.1: 1
	p = max(prec(rec >= t));
	if isempty(p), p = 0; end
	ap = ap + p/11;
end
figure(33); clf; %subplot(121); 
%{
plot(rec, prec, '-', 'linewidth', 2); grid on; xlim([0 1]); ylim([0 1.05]);
title([params.cls '  AP=' num2str(ap)]);
%}
ovth = 0:0.02:1;
rec  = zeros(size(ovth));
for t = 1:length(ovth)
	rec(t) = mean(ratio > ovth(t));
end
plot(ovth, rec, '-', 'linewidth', 2); hold on;
plot([0.5 0.5], [0 2], '--r', 'linewidth', 2); hold off;
grid on; ylim([0 1.05]); ylabel 'recall'; xlabel 'overlap';
title([params.cls '  AP=' num2str(ap)]);
