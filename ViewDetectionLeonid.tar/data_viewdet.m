function data_viewdet(params)
% prepare data: 
% - read annotations
% - cluster bounding boxes
% - generate visual dictionary
% - compute image features
% - compute integral histograms
%
cls   = params.cls;
step  = params.gridstep;
nvw   = params.vwords;
sizes = params.siftsizes;
ftype = params.fttype;
assert(iscell(ftype));
localdir = params.localdir;
if ~exist(localdir, 'dir'), mkdir(localdir); end

%% find training images, read annotation (dataset specific)
idsfn = sprintf('%s/%s_ids.mat', params.modeldir, params.ds);
if exist(idsfn, 'file')
	load(idsfn);
	ndata = length(name);
	disp(['loaded: ' idsfn]);
else
	if strcmpi(params.ds, 'epflcars')
		[ndata, inst, name, imfn, bbvp] = data_epflcars(params);
	elseif strcmpi(params.ds, '3dobj')
		[ndata, inst, name, imfn, bbvp] = data_3dobj(params);
	else
		error('unknown dataset');
	end
	save(idsfn, 'inst', 'name', 'imfn', 'bbvp');
end

% 3dobj fix
if strcmpi(params.ds, '3dobj')
	idsfn_cls = sprintf('%s/%s_ids.mat', params.modeldir, cls);
	if exist(idsfn_cls, 'file')
		load(idsfn_cls);
		ndata = length(name);
		disp(['loaded: ' idsfn_cls]);
	else
		[ndata, inst, name, imfn, bbvp] = data_3dobj(params, {cls});
		save(idsfn_cls, 'inst', 'name', 'imfn', 'bbvp');
	end
end

%% cluster bounding boxes
tic;
labelfn = sprintf('%s/%s_stdbb%d.mat', params.modeldir, cls, params.nstdbb);
if exist(labelfn, 'file')
	disp(['skipped: ' labelfn]);
else
	% do clustering
	B  = bbvp'; % [l t r b v] format
	wh = [B(3,:)-B(1,:); B(4,:)-B(2,:)];
	[centr assgn] = vl_kmeans(wh, params.nstdbb, 'algorithm', 'elkan', ...
		'numrepetitions', 42, 'verbose');
	stdbb = round(centr); % each col a canonical bb size

	% save
	save(labelfn, 'stdbb');
	disp(['saved to: ' labelfn]);
end
fprintf('BB clustering: '); toc;


%% load/generate visual dictionary
nft  = length(ftype);
dict = cell(nft, 1);
for i = 1:nft
	dictfn = sprintf('%s/dict_%s_vw%d.mat', params.modeldir, ftype{i}, nvw(i));
	try
		ld = load(dictfn);
		dict{i} = ld.dict;
		fprintf('dictionary loaded from %s\n', dictfn);
	catch
		if strcmpi(params.ds, '3dobj')
			load(idsfn);
			ndata = length(name);
			disp(['loaded: ' idsfn]);
		end
		% sample some images for visual dictionary
		idperm  = randperm(ndata);
		iduse   = idperm(1:ceil(ndata/3.3));
		dict{i} = gen_dict(dictfn, params, ftype{i}, nvw(i), {name{iduse}}, {imfn{iduse}});
	end
end

%% generate integral histograms (for this class)
if strcmpi(params.ds, '3dobj')
	load(idsfn_cls);
	ndata = length(name);
	disp(['loaded: ' idsfn_cls]);
end
tic;
%rng('shuffle');
if matlabpool('size')
	perm = randperm(ndata);
	parfor x = 1:ndata
		i = perm(x);
		if mod(x,100)==0, disp(i); end
		get_hist(dict, localdir, params.IHstr, name{i}, imfn{i}, ...
			step, ftype, sizes, nvw, params.encoding);
	end
else
	for i = randperm(ndata)
		if mod(i,100)==0, disp(i); end
		get_hist(dict, localdir, params.IHstr, name{i}, imfn{i}, ...
			step, ftype, sizes, nvw, params.encoding);
	end
end
fprintf('compute integral histograms: '); toc;


% -----------------------------------------------------------------------

function get_hist(dict, localdir, str, id, fname, step, f_type, sizes, nvw, encoding)
% first compute and save features if don't exist
% then for each feature channel, build integral histogram
% finally concatenate into global integral histogram
%
histfn = sprintf('%s/%s_%s.mat', localdir, id, str);
lockfn = [histfn '.lock'];
if ~exist(histfn, 'file') && ~exist(lockfn, 'file')
	system(['touch ' lockfn]);
	fprintf('inthist%d_%s: %s  [', step, encoding, id);

	% integral histograms for each channel, then concatenate
	imft    = get_ft(localdir, id, fname, step, f_type, sizes);
	inthist = cell(length(f_type), 1);
	for i = 1:length(f_type)
		fprintf(' %s-%d', f_type{i}, nvw(i));
		inthist{i} = build_inthist(imft{i}, dict{i}, nvw(i), encoding);
	end
	fprintf(' ]\n');
	inthist = cat(3, inthist{:});
	xgrid = imft{1}.xgrid;
	ygrid = imft{1}.ygrid;
	assert(length(xgrid) == size(inthist, 2));
	assert(length(ygrid) == size(inthist, 1));
	imwh = imft{1}.imhw([2 1]);

	% save
	save(histfn, 'inthist', 'xgrid', 'ygrid', 'imwh');
	system(['chmod o-w ' histfn]);
	system(['rm -f ' lockfn]);
end

% -----------------------------------------------------------------------

function fts = get_ft(localdir, id, fname, step, f_type, sizes)
% load image
I = imread(fname);
[h w ~] = size(I);

% compute and save features
fts = cell(1, length(f_type));
for i = 1:length(f_type)
	ftype = f_type{i};
	ftfn  = sprintf('%s/%s_step%d_%s.mat', localdir, id, step, ftype);
	try 
		ld     = load(ftfn);
		fts{i} = ld.ft;
	catch
		if 1 % VLfeat rules!
			ft = image_feature_vlfeat(I, step, ftype, sizes);
		else
			ft = image_feature_mexopencv(I, step, ftype);
		end
		ft.imhw = [h w];
		fts{i}  = ft;
		save(ftfn, 'ft');
		fprintf('step%d_%s: %s\n', step, ftype, id);
		system(['chmod o-w ' ftfn]);
	end
end

% -----------------------------------------------------------------------
% encoding + pooling

function vec = maxpool_D(I, D, vw)
% encoding: inverse-distance heuristic
% pooling: max
if isempty(I) || isempty(D)
	vec = zeros(1, vw); return;
end
num = size(I, 2);
vec = zeros(num, vw);
for i = 1:num
	vec(i, I(:, i)) = (1 - D/sum(D))/num;
end
vec = max(vec, [], 1);


function vec = maxpool_421111(I, vw)
% encoding: [4 2 1 1 1 1] heuristic
% pooling: max
if isempty(I)
	vec = zeros(1, vw); return;
end
vec = zeros(size(I, 2), vw);
for i = 1:size(I, 2)
	vec(i, I(:, i)) = [4 2 1 1 1 1]/10;
end
vec = max(vec, [], 1);


function vec = maxpool_42111(I, vw)
% encoding: [4 2 1 1 1] heuristic
% pooling: max
if isempty(I)
	vec = zeros(1, vw); return;
end
vec = zeros(size(I, 2), vw);
for i = 1:size(I, 2)
	vec(i, I(:, i)) = [4 2 1 1 1]/9;
end
vec = max(vec, [], 1);


function vec = maxpool_4211(I, vw)
% encoding: [4 2 1 1] heuristic
% pooling: max
if isempty(I)
	vec = zeros(1, vw); return;
end
vec = zeros(size(I, 2), vw);
for i = 1:size(I, 2)
	vec(i, I(:, i)) = [4 2 1 1]/8;
end
vec = max(vec, [], 1);


function vec = maxpool_421(I, vw)
% encoding: [4 2 1] heuristic
% pooling: max
if isempty(I)
	vec = zeros(1, vw); return;
end
vec = zeros(size(I, 2), vw);
for i = 1:size(I, 2)
	vec(i, I(:, i)) = [4 2 1]/7;
end
vec = max(vec, [], 1);


function vec = maxpool_llc(I, D, vw)
% encoding: LLC
% pooling: max
if isempty(I) || isempty(D)
	vec = zeros(1, vw); return;
end
vec = zeros(size(I, 2), vw);
for i = 1:size(I, 2)
	dd = D/sum(D);
	dd = exp(-dd.^2/2/0.1^2);
	vec(i, I(:, i)) = dd./sum(dd);
end
vec = max(vec, [], 1);

% -----------------------------------------------------------------------

function inthist = build_inthist(imft, dict, vwords, encoding)
% find (multiscale) descriptors at every grid point, gather into cells
xys    = allpairs(imft.xgrid, imft.ygrid);
Nx     = length(imft.xgrid);
Ny     = length(imft.ygrid);
[~, b] = ismember(imft.xy, xys, 'rows');
Gdesc  = cell(Nx*Ny, 1);
for i = 1 : Nx*Ny
	Gdesc{i} = find(b == i);
end
inthist = [];

if strcmpi(encoding, 'LLC3')
	D = vl_alldist2(dict, single(imft.data));
	[mindist, minidx] = sort(D);
	mindist(4:end, :) = [];
	minidx(4:end, :)  = [];
	enc = cellfun(@(x) maxpool_llc(minidx(:,x), mindist(:,x), vwords), Gdesc, ...
		'uniformoutput', false);
	map = cell2mat(enc);  % Npt-by-Nvw map
	for i = 1:vwords
		inthist(:, :, i) = vl_imintegral( reshape(map(:, i), Ny, Nx) );
	end

elseif strcmpi(encoding, 'LLC5')
	D = vl_alldist2(dict, single(imft.data));
	[mindist, minidx] = sort(D);
	mindist(6:end, :) = [];
	minidx(6:end, :)  = [];
	enc = cellfun(@(x) maxpool_llc(minidx(:,x), mindist(:,x), vwords), Gdesc, ...
		'uniformoutput', false);
	map = cell2mat(enc);  % Npt-by-Nvw map
	for i = 1:vwords
		inthist(:, :, i) = vl_imintegral( reshape(map(:, i), Ny, Nx) );
	end

elseif strcmpi(encoding, 'D')
	D = vl_alldist2(dict, single(imft.data));
	[mindist, minidx] = sort(D);
	mindist(6:end, :) = [];
	minidx(6:end, :)  = [];
	enc = cellfun(@(x) maxpool_D(minidx(:,x), mindist(:,x), vwords), Gdesc, ...
		'uniformoutput', false);
	map = cell2mat(enc);  % Npt-by-Nvw map
	for i = 1:vwords
		inthist(:, :, i) = vl_imintegral( reshape(map(:, i), Ny, Nx) );
	end

elseif strcmpi(encoding, '421111') % heuristic
	D = vl_alldist2(dict, single(imft.data));
	[mindist, minidx] = sort(D);
	mindist(7:end, :) = [];
	minidx(7:end, :)  = [];
	enc = cellfun(@(x) maxpool_421111(minidx(:,x), vwords), Gdesc, ...
		'uniformoutput', false);
	map = cell2mat(enc);  % Npt-by-Nvw map
	for i = 1:vwords
		inthist(:, :, i) = vl_imintegral( reshape(map(:, i), Ny, Nx) );
	end

elseif strcmpi(encoding, '42111') % heuristic
	D = vl_alldist2(dict, single(imft.data));
	[mindist, minidx] = sort(D);
	mindist(6:end, :) = [];
	minidx(6:end, :)  = [];
	enc = cellfun(@(x) maxpool_42111(minidx(:,x), vwords), Gdesc, ...
		'uniformoutput', false);
	map = cell2mat(enc);  % Npt-by-Nvw map
	for i = 1:vwords
		inthist(:, :, i) = vl_imintegral( reshape(map(:, i), Ny, Nx) );
	end

elseif strcmpi(encoding, '4211') % heuristic
	D = vl_alldist2(dict, single(imft.data));
	[mindist, minidx] = sort(D);
	mindist(5:end, :) = [];
	minidx(5:end, :)  = [];
	enc = cellfun(@(x) maxpool_4211(minidx(:,x), vwords), Gdesc, ...
		'uniformoutput', false);
	map = cell2mat(enc);  % Npt-by-Nvw map
	for i = 1:vwords
		inthist(:, :, i) = vl_imintegral( reshape(map(:, i), Ny, Nx) );
	end

elseif strcmpi(encoding, '421') % heuristic
	D = vl_alldist2(dict, single(imft.data));
	[mindist, minidx] = sort(D);
	mindist(4:end, :) = [];
	minidx(4:end, :)  = [];
	enc = cellfun(@(x) maxpool_421(minidx(:,x), vwords), Gdesc, ...
		'uniformoutput', false);
	map = cell2mat(enc);  % Npt-by-Nvw map
	for i = 1:vwords
		inthist(:, :, i) = vl_imintegral( reshape(map(:, i), Ny, Nx) );
	end

elseif strcmpi(encoding, 'BOW')
	D = vl_alldist2(dict, single(imft.data));
	[mindist, minidx] = sort(D);
	textonim = reshape(minidx, Ny, Nx); 
	inthist  = vl_inthist(uint32(textonim), 'numlabels', vwords);
	if max(inthist(:)) > 255, inthist = uint16(inthist);
	else, inthist = uint8(inthist); end

else
	error('unknown encoding method');
end

