function [model iter] = structSVM_1slack_ker(...
	idtrain, para, MVC_init, MVC_1slack, Verb)
%
% kernel structural SVM training with 1-slack cutting plane algorithm.
% Adapted from the implementation by Jianxiong Xiao (http://mit.edu/jxiao/)
% by Kun He, Boston University
%
% glboal xtrain - training patterns
% global ytrain - training labels
%
% idtrain - training instances involved
% para    - structure holding parameters
%           para.kfunc: function handle for computing kernels
%
% MVC_init   - function handle for init-ing and returning 1st cutting plane
% MVC_1slack - function handle for finding cutting planes
%   both take a set of instances (idtrain) and return:
%   Y : matrix, each ROW is ybar for a specific instance i in idtrain
%   V : vector, each is the violation amount (Delta+f(ybar)-f(yi)) for i
%   M : vector, each is the margin (loss) value Delta(ybar,yi) for i
%   Fc: matrix, each COLUMN is a violating feature vector \phi(xi, ybar_i)
%   Fg: matrix, each COLUMN is a ground truth feature vector \phi(xi, yi)
%   Yg: matrix, each ROW is a ground truth label yi
%
% PSIgt - ground truth feature vectors \psi(xi, yi)
% PSIcp - cutting plane feature vectors \psi(xi, yi_bar)
% Ygt   - ground truth labels yi
% Ycp   - cutting plane labels yi_bar
% Mcp   - RHS of cutting plane constraints
% H     - kernel matrix
% xi    - the slack variable in the 1-slack cutting plane algorithm
% alph  - dual variables

if nargin < 5, Verb = 1; end
if Verb, fprintf('-------- structSVM_1slack_ker --------\n'); end
global xtrain ytrain
idtrain = reshape(unique(idtrain), 1, []);  % make sure it's row vector
ntrain  = length(idtrain);
PSIcp   = cell(para.max_planes, 1);
PSIgt   = cell(para.max_planes, 1);
Ycp     = cell(para.max_planes, 1);
Ygt     = cell(para.max_planes, 1);
Mcp     = zeros(para.max_planes, 1, 'single');
idle    = zeros(para.max_planes, 1);
maxidle = 5;
TolRel  = 0.001; 
TolVio  = 0.025;

%% init
% MVC_init returns 1 MVC
[Y, V, M, Fc, Fg, Yg] = MVC_init(idtrain, para, 1);
PSIcp{1}  = Fc;
PSIgt{1}  = Fg;
Ycp{1}    = Y;
Ygt{1}    = Yg;
Mcp(1)    = sum(M);
ncp       = 1;
H         = expand_kernel_matrix([], ncp, PSIgt, Ygt, PSIcp, Ycp, para);
alph      = min(1, para.svm_C);
xi        = 0;
model     = [];
model.ids = idtrain;

%% structual SVM, go!
iter = 0;
vios = []; xis = [];
while iter < para.max_planes
	iter = iter + 1;
	if Verb
		fprintf('\n==== Iteration #%d ====\n', iter); 
		fprintf('%s(N=%d), k=%d, C=%g\n', ...
			para.cls, length(ytrain), length(idtrain), para.svm_C);
	end

	%% update model
	svidx       = find(abs(alph/ntrain) > 1e-6);
	model.alph  = alph(svidx)/ntrain; 
	model.psicp = {PSIcp{svidx}};
	model.psigt = {PSIgt{svidx}};
	model.ycp   = {Ycp{svidx}};
	model.ygt   = {Ygt{svidx}};

	%% [experimental] remove SVs that are inactive for some time
	idle(svidx) = 0;
	inact       = find(~ismember(1:ncp, svidx));
	idle(inact) = idle(inact) + 1;
	active      = find(idle(1:ncp) < maxidle);
	if length(active) < ncp
		fprintf('remove inactives: %d -> %d\n', ncp, length(active));
		ncp = length(active);
		for i = 1:ncp
			PSIcp{i} = PSIcp{active(i)};
			PSIgt{i} = PSIgt{active(i)};
			Ycp{i}   = Ycp{active(i)};
			Ygt{i}   = Ygt{active(i)};
			Mcp(i)   = Mcp(active(i));
			alph(i)  = alph(active(i));
			idle(i)  = idle(active(i));
		end
		H = H(active, active);
	end

	%% go to separation oracle to find MVC
	% MVC_1slack returns K MVCs
	%   topk: how many to return
	% nms_th: NMS threshold
	tic;
	[Y, V, M, Fc, Fg, Yg] = MVC_1slack(idtrain, model, para, topk, nms_th);
	vio = mean(V);
	fprintf('\n====> '); toc;

	if Verb > 1
		fprintf('\nnSV=%d, MVC vio=%g, xi=%g\n', length(svidx), vio, xi);
	elseif Verb > 0
		fprintf('.'); 
	end
	if vio < xi + TolVio
		if Verb
			fprintf('vio(%g) < xi(%g)+eps, @ %d iters\n', vio, xi, iter);
		end
		break;
	end

	%test 
	vios = [vios vio];
	xis  = [xis  xi];
	figure(33); plot(vios); hold all; plot(xis); 
	ylim([-.1, min(2, max(vios))]);
	hold off; drawnow; pause(.5);

	%% 1 slack: always add 1 new constraint
	ncp        = ncp + 1;
	PSIcp{ncp} = Fc;
	PSIgt{ncp} = Fg;
	Ycp{ncp}   = Y;
	Ygt{ncp}   = Yg;
	Mcp(ncp)   = sum(M);
	H    = expand_kernel_matrix(H, ncp, PSIgt, Ygt, PSIcp, Ycp, para);
	alph = [alph; max(0, para.svm_C - sum(alph))];

	%% solve QP, recover xi
	dH = double(H) / ntrain^2;
	df = double(Mcp(1:ncp)) / ntrain;
	%{
	% matlab's QP solver may be good enough?
	opts = optimset('algorithm', 'interior-point-convex', 'display', 'off');
	[alph, negdual, flg] = quadprog(dH, -df, ones(1,ncp), para.svm_C, ...
		[], [], zeros(ncp,1), [], alph, opts);
	%}
	%
	% use specialized QP solver
	I = ones(size(alph), 'uint32');
	S = ones(size(alph), 'uint8');
	[alph, ~] = qp(dH, -df, para.svm_C, I, S, alph, inf, 0, TolRel, -inf, 0);
	xi = max(df - dH * alph);

	% normalize
	if Verb > 1, disp(alph'); end
	if length(alph) > 2 && alph(end) < 1e-6, break; end

	% check for non-PSDness
	[R, p] = chol(dH);
	if p, disp('Warning: non-PSD gram matrix'); end
end

if Verb
	if iter >= para.max_planes
		fprintf('!! structSVM_1slack_ker did not converge\n');
	end
	fprintf('*********************************************\n');
else
	fprintf('>> structSVM_1slack_ker: %d iters\n', iter);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
function H = expand_kernel_matrix(H, Np, PSIgt, Ygt, PSIcp, Ycp, para)
n = size(H, 1);  assert(n+1 == Np);
% this part for the new padding col/row of H
h = zeros(n, 1);
for cp = 1:n
	h(cp) = sv_inprod(para, ...
		PSIgt{cp}, Ygt{cp}, ...
		PSIcp{cp}, Ycp{cp}, ...
		PSIcp{Np}, Ycp{Np});
end
% this part for the last entry of new H
hh = sv_inprod(para, ...
	PSIgt{Np}, Ygt{Np}, ...
	PSIcp{Np}, Ycp{Np}, ...
	PSIcp{Np}, Ycp{Np});
% final assembly
H = [H h; h' hh];
H = (H + H') / 2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
function I = sv_inprod(para, psi_, y_, psi1, y1, psi2, y2)
% psi_, y_ : ground truth features and labels
% psi1, y1 : features & labels from cutting plane, 1st set
% psi2, y2 : features & labels from cutting plane, 2nd set
%   kernel : returns a matrix of kernel values
kernel = para.kfunc;
K1 = kernel(psi_, psi_, y_, y_, para);
K2 = kernel(psi_, psi2, y_, y2, para);
K3 = kernel(psi1, psi_, y1, y_, para);
K4 = kernel(psi1, psi2, y1, y2, para);
K  = K1 - K2 - K3 + K4;
I  = sum(K(:));

