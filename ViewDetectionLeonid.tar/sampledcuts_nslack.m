function [model scache] = sampledcuts_nslack(...
	nsample, para, MVC_init, MVC_1slack, Verb)

% training structural SVM with sampled cuts, n-slack version
% in each iteration, compute the cutting plane on a sampled subset
%
if nargin < 5, Verb = 1; end
if Verb, fprintf('-------- sampledcuts_nslack --------\n'); end
global ids xtrain ytrain

ntrain  = length(ids.train);
PSIgt   = [];
PSIcp   = [];
Ygt     = [];
Ycp     = [];
Mcp     = [];

maxidle = 10;
Ngood   = 5;
topk    = 3;
nms_th  = 0.36;
TolRel  = 0.001;
TolVio  = 0.2;

xtrain.rho = ones(1, ntrain);
C = para.svm_C/ntrain;
ID = randperm(ntrain);

%% init
idinit = get_bootstrap(0, 'uniform', ntrain, nsample); 
[Ycp, V, Mcp, PSIcp, PSIgt, Ygt] = MVC_init(idinit, para, 1);
H = [];
for i = 1:nsample
	ncp = i;
	H = expand_kernel_matrix(H, ncp, PSIgt, Ygt, PSIcp, Ycp, para);
end
opts = optimset('algorithm', 'interior-point-convex', 'display', 'off');
[alph, negdual, flg] = quadprog(...
	H, -Mcp, ...  % problem data
	[], [], ...   % constraint Ax<=b
	[], [], ...   % constraint Ax==b
	zeros(ncp,1), ...   % LB
	C*ones(ncp,1), ...  % UB
	zeros(ncp, 1), ...  % initial solution
	opts);
idle  = zeros(ncp, 1);
xi    = zeros(1, ntrain);
v1    = ones(1, ntrain);
wset  = cell(1, ntrain);
model = struct('type', 'samN', 'nid', nsample)

%% go!
goodcnt = 0;
iter = 0;
vios = []; xis = [];
while iter < para.max_planes
	iter = iter + 1;
	if Verb
		fprintf('\n==== Iteration #%d ====\n', iter); 
		fprintf('%s(N=%d), k=%d, C=%g\n', ...
			para.cls, length(ytrain), nsample, para.svm_C);
	end

	%% update model
	svidx = find(abs(alph) > 1e-6);
	model.alph  = alph(svidx);
	model.psicp = PSIcp(:, svidx);
	model.psigt = PSIgt(:, svidx);
	model.ycp   = Ycp(:, svidx);
	model.ygt   = Ygt(:, svidx);

	%% [experimental] remove SVs that are inactive for some time
	idle(svidx) = 0;
	inact       = find(~ismember(1:ncp, svidx));
	idle(inact) = idle(inact) + 1;
	active      = find(idle(1:ncp) < maxidle);
	if mod(iter, 5)==0 && length(active) < ncp
		fprintf('remove inactives: %d -> %d\n', ncp, length(active));
		ncp = length(active);
		for i = 1:ncp
			PSIcp(:, i) = PSIcp(:, active(i));
			PSIgt(:, i) = PSIgt(:, active(i));
			Ygt(:, i)   = Ygt(:, active(i));
			Ycp(:, i)   = Ycp(:, active(i));
			Mcp(i)      = Mcp(active(i));
			alph(i)     = alph(active(i));
			idle(i)     = idle(active(i));
			revind(i)   = revind(active(i));
		end
		for i = 1:ntrain
			wset{i} = find(revind(1:ncp) == i);
		end
		H = H(active, active);
	end

	%% go to separation oracle to find cutting plane
	tic;
	idmvc = get_bootstrap(iter, 'disjoint', ntrain, nsample); 
	idmvc = ID(idmvc);
	fprintf('sampled %d\n', length(idmvc)); vv = mean(v1);
	xx = mean(xi);
	th = max(TolVio, nms_th*min(1, (vv-xx-TolVio)/(vv+xx+TolVio)));
	[Yc, V, M, Fc, Fg, Yg] = MVC_1slack(idmvc, model, para, topk, th);
	fprintf('\n====> '); toc;

	%% add MVCs
	new_mvc = 0;
	gaps = [];
	for i = 1:nsample, for j = 1:topk
		idx = idmvc(i);
		if V(i, j) >= xi(idx) + TolVio
			new_mvc = new_mvc + 1;
			gaps = [gaps, V(i,j)-xi(idx)];
			v1(idx) = V(i, j);
			% add cutting plane
			ncp = ncp + 1;
			PSIcp = [PSIcp, Fc(:, i, j)];
			PSIgt = [PSIgt, Fg(:, i, j)];
			Ycp   = [Ycp, Yc(:, i, j)];
			Ygt   = [Ygt, Yg(:, i, j)];
			Mcp   = [Mcp; M(i, j)];
			% expand H
			H = expand_kernel_matrix(H, ncp, PSIgt, Ygt, PSIcp, Ycp, para);
			% add dual variable
			alph = [alph; 0];
			idle = [idle; 0];
			% expand working set
			wset{idx} = [wset{idx}, ncp];
			% add reverse index for new dual variable
			revind(ncp) = idx;
		end
	end, end
	if new_mvc
		fprintf('\nAdded %d new cutting planes, avg gap=%g\n', new_mvc, mean(gaps));
		fprintf('\nnSV=%d, vio_max=%g, xi_avg=%g\n', length(svidx), max(V(:)), mean(xi));

		% solve QP
		dH = double(H);
		df = double(Mcp(1:ncp));
		[R, p] = chol(dH);
		if p, disp('Warning: non-PSD gram matrix'); end

		% matlab's QP solver may be good enough?
		id = find( ~cellfun(@isempty, wset) );
		A  = [];
		b  = [];
		for i = id
			A = [A; full( sparse(1, wset{i}, 1, 1, ncp) )];
			b = [b; C];
		end
		opts = optimset('algorithm', 'interior-point-convex', 'display', 'off');
		tic;
		[alph, negdual, flg] = quadprog(...
			dH, -df, ...        % problem data
			A, b, ...           % Ax<=b
			[], [], ...         % Ax==b
			zeros(ncp,1), ...   % LB
			[], ...             % UB
			zeros(ncp,1), ...   % initial
			opts);
		toc;

		% recover slack variables
		xtrain.rho(idmvc) = 0;
		xi_old = xi;
		for i = 1:ntrain
			if isempty(wset{i}), continue; end
			HH = dH(wset{i}, 1:ncp);
			ff = df(wset{i});
			xi(i) = max(0, max(ff - HH*alph));
			xtrain.rho(i) = xi(i);
		end
		%{
		if max(xtrain.rho)<0.95 && mean(abs(xi-xi_old))/mean(xi_old) < 1e-2
			goodcnt = goodcnt + 1;
			fprintf('no change in slack variables, goodcnt=%d\n\n', goodcnt);
		else
			goodcnt = 0;
		end
		%}
		goodcnt = 0;

	else
		goodcnt = goodcnt + 1;
		fprintf('no violated constraint, goodcnt=%d\n\n', goodcnt);
	end

	% display
	vios = [vios; mean(V, 1)];
	xis  = [xis; mean(xi)];
	figure(32); clf; 
	subplot(3,1,1:2); plot(vios); hold all; plot(xis); hold off; grid on; 
	subplot(3,1,3); grid off; bar(xtrain.rho);
	drawnow; pause(.2);

	% terminate?
	if goodcnt >= Ngood
		if Verb, fprintf('Stop @ %d iters\n', iter); end
		break;
	end

end

if Verb
	if iter >= para.max_planes
		fprintf('!! sampledcuts_nslack did not converge\n');
	end
	fprintf('*********************************************\n');
else
	fprintf('>> sampledcuts_nslack: %d iters\n', iter);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
function id = get_bootstrap(t, method, ntrain, nuse, rho)
if nargin < 5, rho = 1;
elseif (max(rho)-min(rho))/max(rho) < 1e-3
	method = 'uniform';
end
if strcmpi(method, 'all')
	id = 1:ntrain;

elseif strcmpi(method, 'D')
	D = rho + max(0.2, mean(rho));
	D = D./sum(D);
	while 1
		id = discretesample(D, nuse*2);
		id = unique(id);
		if length(id) >= nuse
			[val, idx] = sort(-D(id));
			id = id(idx(1:nuse))
			break;
		end
	end

elseif strcmpi(method, 'uniform')
	if (nuse >= ntrain)
		id = 1:ntrain;
	else
		perm = randperm(ntrain);
		id = perm(1:nuse);
	end

elseif strcmpi(method, 'disjoint')
	ii = 1:round(ntrain/nuse):ntrain;
	id = mod(ii+t-1, ntrain)+1;

else
	error(['unknown sampling method: ' method]);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
function H = expand_kernel_matrix(H, Np, PSIgt, Ygt, PSIcp, Ycp, para)
n = size(H, 1);
assert(n+1 == Np);
if n == 0
	H = sv_innerprod(para, ...
		PSIgt(:, 1), Ygt(:, 1), PSIcp(:, 1), Ycp(:, 1), ...
		PSIgt(:, 1), Ygt(:, 1), PSIcp(:, 1), Ycp(:, 1));
else
	%% this part for the new padding col/row of H
	h = zeros(n, 1);
	for cp = 1:n
		h(cp) = sv_innerprod(para, ...
			PSIgt(:, cp), Ygt(:, cp), PSIcp(:, cp), Ycp(:, cp), ...
			PSIgt(:, Np), Ygt(:, Np), PSIcp(:, Np), Ycp(:, Np));
	end

	%% this part for the last entry of new H
	hh = sv_innerprod(para, ...
		PSIgt(:, Np), Ygt(:, Np), PSIcp(:, Np), Ycp(:, Np), ...
		PSIgt(:, Np), Ygt(:, Np), PSIcp(:, Np), Ycp(:, Np));

	%% final assembly
	H = [H h; h' hh];
end
H = (H+H')/2;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
function K = sv_innerprod(para, ...
	PSI1, Y1, psi1, y1, ...
	PSI2, Y2, psi2, y2)
kernel = para.kfunc;
K1 = kernel(PSI1, PSI2, Y1, Y2, para);
K2 = kernel(PSI1, psi2, Y1, y2, para);
K3 = kernel(psi1, PSI2, y1, Y2, para);
K4 = kernel(psi1, psi2, y1, y2, para);
K = K1 - K2 - K3 + K4;
K = sum(K(:));


