function model = svm_1vsall_hardmine(C, params, modelfn)
global ids ytrain
ntrain = length(ids.train);
nlv    = uint32(params.pyralvs);
nvp    = length(params.viewpoints);
dvp    = 2*pi/nvp;
gtftfn = sprintf('%s/GT%s_%s_%d%s.mat', ...
	params.modeldir, params.cls, params.IHstr, nlv, params.Kx);
try 
	load(gtftfn);
	fprintf('loaded: %s\n', gtftfn);
catch
	% load integral histograms and feature info
	tic; disp('loading integral histograms');
	IHfn = [];
	for i = 1:ntrain
		idx = ids.train(i);
		IHfn{i} = sprintf('%s%s_%s.mat', params.localdir, ids.name{idx}, params.IHstr);
	end
	toc;

	% compute ground truth features
	disp('computing ground truth features');
	gtft = zeros(params.ftdim, ntrain);
	vps  = (params.viewpoints-1) * dvp;
	parfor i = 1:ntrain
		fprintf('\n[#%4d: %s] ', i, ids.name{ids.train(i)});
		ld = load(IHfn{i});
		ihist = double(ld.inthist);
		xx = uint32(ld.xgrid);
		yy = uint32(ld.ygrid);
		wh = uint32(ld.imwh);

		[~, ~, ~, fgt, ~] = ess_viewdet(...
			params.Kx, ...    % kernel types
			ihist, nlv, ...   % integral histogram & SP levels
			wh, xx, yy, ...   % image dimensions, feature grid
			[], vps, ...      % bb sizes + vps to search for
			ytrain(:, i), ... % ground truths
			[], []);          % classifiers
		gtft(:, i) = fgt;

		bbsize = uint32(randi([50 100],2,1));
		[~, ~, ~, frand, ~] = ess_viewdet(...
			params.Kx, ...      % kernel types
			ihist, nlv, ...     % integral histogram & SP levels
			wh, xx, yy, ...     % image dimensions, feature grid
			bbsize, vps, ...    % bb sizes + vps to search for
			ytrain(:, i), ...   % ground truths
			[], []);            % classifiers
		randft(:, i) = frand;

	end
	save(gtftfn, 'gtft', 'randft');
	fprintf('saved to: %s\n', gtftfn);
end

W = zeros(params.ftdim, nvp);
B = zeros(nvp, 1);
nrand = size(randft, 2);
Ttime = 0;
for v = 1:nvp
	vfn = [modelfn sprintf('-vp%d.mat', v)]
	%if ~exist(vfn, 'file')
	try 
		load(vfn);
	catch
		tt = tic;
		% init model
		w = zeros(params.ftdim, 1);
		b = 0;

		% determine pos/neg for this vp
		yv = -ones(ntrain, 1);
		df = abs(ytrain(5, :) - (v-1)*dvp);
		df = min(df, 2*pi-df);
		ip = find(df <= dvp/2);
		in = find(df >  dvp/2);
		np = length(ip);
		nn = length(in);
		posft = gtft(:, ip);
		negft = gtft(:, in);

		Y = [ones(np, 1); -ones(nn, 1)];
		X = [posft, negft];
		lastobj = 0;

		for t = 1:5
			if t > 1
				% do hard mining, add random BG samples
				[randft, score] = do_hardmine(w, b, in, params, sprintf('vp%d-%d', v, t));
				%randft(:, score < -1) = [];
				randft = randft(:, score>-1); % this is faster
				nrand = size(randft, 2)
			end
			nn = nn + nrand;
			fprintf('--------------------------------------\n');
			fprintf('vp#%d iter%d: pos=%d, neg=%d\n', v, t, np, nn);

			% train SVM
			Y = [Y; -ones(nrand,1)];
			X = [X, randft];
			ret = evalc('m = libsvmtrain(Y, X'', sprintf(''-c %g -t 0'', C));');
			disp(ret);
			w = m.SVs' * m.sv_coef;
			b = -m.rho;

			% dig out the obj value & report
			while 1
				[tok, ret] = strtok(ret);
				if strcmp(tok, 'obj')
					[tok, ret] = strtok(ret);      % get '='
					[tok, ret] = strtok(ret, ','); % get obj value 
					obj = -sscanf(tok, '%f')
					break;
				end
			end
			fprintf('vp#%d iter%d: obj=%.3f (%.2f%% last)\n', v, t, obj, obj/lastobj*100);
			fprintf('--------------------------------------\n\n');
			if (obj/lastobj < 1.02)
				break;
			end
			lastobj = obj;
		end

		% save this portion of the model in case of catestrophy
		traintime = toc(tt)
		save(vfn, 'w', 'b', 'traintime');
	end
	% end of learning for this vp
	W(:, v) = w; 
	B(v)    = b;
	Ttime = Ttime + traintime;
end
model.w = W;
model.b = B;
model.traintime = Ttime

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [fdet, sdet] = do_hardmine(w, b, in, params, str)
% do hard negative mining
global ids xtrain
nn = length(in);
ydet = zeros(5, nn);
sdet = zeros(nn, 1);
fdet = zeros(params.ftdim, nn);
bbsize = uint32(xtrain.stdbb);
nlv    = uint32(params.pyralvs);

disp('start hard mining');
parfor i = 1 : nn
	idx = ids.train(in(i));
	fprintf('\n%s [%d/%d %s]', str, i, nn, ids.name{idx});
	tic;
	IHfn = sprintf('%s%s_%s.mat', params.localdir, ids.name{idx}, params.IHstr);
	ld = load(IHfn);
	ihist = double(ld.inthist);
	xx = uint32(ld.xgrid);
	yy = uint32(ld.ygrid);
	wh = uint32(ld.imwh);
	[ydet(:, i), sdet(i), ~, fdet(:, i), ~] = ess_viewdet(...
		params.Kx, ...     % kernel type & spatial pyamid levels   
		ihist, nlv, ...    % integral histogram & feature grid step
		wh, xx, yy, ...    % image dimensions, feature grid
		bbsize, 0.0, ...   % bb sizes + vps to search for
		[], w, b);
	toc;
end

