function c = loss_pitchyaw(py1, py2)
% function c = loss_pitchyaw(py1, py2)
% py1, py2 must be degrees (0-360)
% py1 is 1*2 or 2*1
% py2 is n*2 
c = [];
for i = 1:size(py2, 1)
	dp = abs(py1(1) - py2(i, 1));
	dy = abs(py1(2) - py2(i, 2));
	dp = min(dp, 360-dp)/180;
	dy = min(dy, 360-dy)/180;
	% optional
	dp = min(2*dp, 1);
	dy = min(2*dy, 1);
	c = [c; 0.5*dp + 0.5*dy];
end

