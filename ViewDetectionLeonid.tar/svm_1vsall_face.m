function model = svm_1vsall_face(C, params)
global ids ytrain
ntrain = length(ids.train)
gtfts = zeros(params.ftdim, ntrain);
tic; fprintf('loading features: ');
parfor i = 1 : ntrain
	idx = ids.train(i);
	ld = load(sprintf('%s/%s_HOG_%dlv.mat', params.localdir, ids.name{idx}, params.pyralvs));
	switch params.Kx
		case 'lin'
			gtfts(:, i) = ld.ft;
		case 'nlin2'
			gtfts(:, i) = ld.ft/norm(ld.ft,2);
		case 'hell'
			gtfts(:, i) = sqrt(ld.ft);
		case 'nhell'
			gtfts(:, i) = sqrt(ld.ft/norm(ld.ft,1));
		case 'chi2'
			gtfts(:, i) = vl_homkermap(ld.ft, 3);
		case 'nchi2'
			gtfts(:, i) = vl_homkermap(ld.ft/norm(ld.ft,1), 3);
		otherwise
			error('unkown Kx');
	end
end
toc;

nvp = size(params.viewpoints, 1);
W   = zeros(params.ftdim, nvp);
b   = zeros(nvp, 1);
for v = 1:nvp
	yv = -ones(ntrain, 1);
	vp = params.viewpoints(v, :);
	df = ytrain - repmat(vp, [ntrain 1]);
	iv = find(~any(df, 2));
	yv(iv) = 1;
	fprintf('\nvp#%d, pos=%d, neg=%d\n', v, sum(yv==1), sum(yv==-1));
	md = libsvmtrain(yv, gtfts', sprintf('-c %g -t 0', C));
	W(:, v) = md.SVs' * md.sv_coef;
	b(v) = -md.rho;
end
model.w = W;
model.b = b;

