function [ybar, viobar, mbar, phibar, phi, ygt] = mvcinit_face(samples, params, lossfn)
global ids xtrain ytrain
% note samples are used to index into ids.train
% i.e. each "sample" is the index for a training example
% NOT the absolute index in whole dataset!
if nargin < 3, lossfn = params.lossfn,  end
nsample = length(samples);
ntrain = length(ids.train);

%% load features
gtfts = zeros(params.ftdim, ntrain);
tic; fprintf('loading features: ');
parfor i = 1 : ntrain
	idx = ids.train(i);
	ld = load(sprintf('%s/%s_HOG_%dlv.mat', params.localdir, ids.name{idx}, params.pyralvs));
	switch params.Kx
		case 'lin'
			gtfts(:, i) = ld.ft;
		case 'nlin2'
			gtfts(:, i) = ld.ft/norm(ld.ft,2);
		case 'hell'
			gtfts(:, i) = sqrt(ld.ft);
		case 'nhell'
			gtfts(:, i) = sqrt(ld.ft/norm(ld.ft,1));
		case 'chi2'
			gtfts(:, i) = vl_homkermap(ld.ft, 3);
		case 'nchi2'
			gtfts(:, i) = vl_homkermap(ld.ft/norm(ld.ft,1), 3);
		otherwise
			error('unkown Kx');
	end
end
xtrain.gtft = gtfts;
clear gtfts
toc;

%% construct first cutting plane (just sample some)
viobar = zeros(nsample, 1);
mbar   = zeros(nsample, 1);
ybar   = zeros(2, nsample);
ygt    = zeros(2, nsample);
phibar = zeros(params.ftdim, nsample);
phi    = zeros(params.ftdim, nsample);
tic; 
disp('getting initial MVC...');
for i = 1:nsample
	idx = samples(i);
	fprintf('[No.%d: %s]\n', i, ids.name{ids.train(idx)});

  gt = ytrain(idx, :);
	[~, pos] = ismember(gt, params.viewpoints, 'rows');
	y = params.viewpoints(mod(pos+47,93)+1, :);
	ybar(:, i)   = y;
	viobar(i)    = lossfn(gt, y);
	mbar(i)      = lossfn(gt, y);
	phibar(:, i) = xtrain.gtft(:, idx); 
	phi(:, i)    = xtrain.gtft(:, idx);
	ygt(:, i)    = gt;
end
toc;

