%figure 6a

%show confusion matrix

%load results

load('cm_results_fig6a.mat');

cnts = sum(CM_poses,2);
CM = CM_poses ./ repmat( cnts+eps, [1 size(CM_poses,2)] );
confMatrixShow(CM);