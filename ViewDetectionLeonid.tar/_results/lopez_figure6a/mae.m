function ae=mae(cfm)
adif = zeros(16,16); % angular differences
for i = 1:16
    for j = 1:16
        adif(i,j) = 360/16*abs(i-j);
    end
end
adif = min(adif,360-adif);

ae = []; % quantized angular errors
for i = 1:16
    for j = 1:16
        if cfm(i,j) > 0
            tt = sparse(1,1:cfm(i,j),adif(i,j));
            ae = [ae full(tt)];
        end
    end
end
sum(ae > 150)
mean(ae)
median(ae)
