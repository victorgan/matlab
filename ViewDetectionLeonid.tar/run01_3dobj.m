function run01_3dobj(cls, C, Kx)
if nargin < 3, Kx = 'nlin2'; end
%global ids xtrain ytrain xtest ytest

curbest = 0; ap_best = 0; mppe_best = 0;
aps = []; mppes = []; maes = [];

for split = 1:10
	a = tic;
	[ap, mppe, mae] = demo_viewdet01(cls, {'SIFTg'}, [500], 3, Kx, C, split);
	toc(a);
	aps  (split) = ap;
	mppes(split) = mppe;
	maes (split) = mae;
	if ap+mppe > curbest
		ap_best   = ap;
		mppe_best = mppe;
		curbest   = ap+mppe;
	end
	drawnow; pause(1)
end
fprintf('==========================================\n');
fprintf('mAP %.3f+/-%.3f, mMPPE %.3f+/-%.3f, mMAE %.3f\n', ...
	mean(aps), std(aps), mean(mppes), std(mppes), mean(maes));
fprintf('best AP %.3f, best MPPE %.3f\n', ap_best, mppe_best);
fprintf('==========================================\n\n');
%ids = [];
%xtrain = [];  xtest = [];
%ytrain = [];  ytest = [];

