function [ybar, viobar, mbar, phibar, phigt, ygt] = bnb_viewdet(...
	samples, model, params, topk, nms_th)
% do loss-augmented branch-and-bound for samples
%   topk: number of detections to return per image
% nms_th: non-maximum suppression threshold
%
global ids xtrain ytrain
if nargin < 4, topk = 1; end
if nargin < 5, nms_th = 0.2; end

nsample = length(samples);
nvp = length(params.viewpoints);

%% compute ground truth features
step = uint32(params.gridstep);
nlv  = uint32(params.pyralvs);
vps  = (params.viewpoints-1) * 2*pi/nvp;
for i = samples  % training index
	idx = ids.train(i);  % absolute index

	if isempty(xtrain.gtft{i})
		fprintf('\n[%4d: %s] ', i, ids.name{idx});
		ld = load(xtrain.IHfn{i});
		ihist = double(ld.inthist);
		xx = uint32(ld.xgrid);
		yy = uint32(ld.ygrid);
		wh = uint32(ld.imwh);

		[~, ~, ~, fv, ~] = ess_posedet(...
			params.Kx, ...    % kernel types
			ihist, nlv, ...   % integral histogram & SP levels
			wh, xx, yy, ...   % image dimensions, feature grid
			[], vps, ...      % bb sizes + vps to search for
			ytrain(:, i), ... % ground truths
			[], []);          % classifiers

		xtrain.gtft{i} = fv;
	end
end

%% convert SVs to the format that ess takes
fprintf('converting SVs: '); tic;
W = construct_Ws(model, params);
toc;

%% construct cutting plane
viobar  = zeros(nsample, topk);
mbar    = zeros(nsample, topk);
ybar    = zeros(5, nsample, topk);
ygt     = zeros(5, nsample, topk);
phigt   = zeros(params.ftdim, nsample, topk);
phibar  = zeros(params.ftdim, nsample, topk);
bbsizes = uint32(xtrain.stdbb);
hists   = {xtrain.IHfn{samples}};
gtfts   = {xtrain.gtft{samples}};
parfor i = 1:nsample
	ii  = samples(i);  % training index
	idx = ids.train(ii);  % absolute index
	fprintf('\n[%4d: %s] ', ii, ids.name{idx});

	ld = load(hists{i});
	ihist = double(ld.inthist);
	xx = uint32(ld.xgrid);
	yy = uint32(ld.ygrid);
	wh = uint32(ld.imwh);
	GT = ytrain(:, ii);
	tic;
	[y, v, m, f, j] = ess_posedet_topk(...
		params.Kx, ...            % kernel types
		ihist, nlv, ...           % inthist, grid step, #levels
		wh, xx, yy, ...           % image dimensions, feature grid
		bbsizes, vps, ...         % bb sizes + vps to search for
		GT, ...                   % ground truth label
		W, ...                    % classifiers
		uint32(topk), nms_th, ... % top K, NMS threshold
		params.alpha, ...         % weighting of losses
		params.lambda);           % weighting of losses
	toc;

	viobar(i, :)    = v;
	mbar(i, :)      = m;
	ybar(:, i, :)   = y;
	phibar(:, i, :) = f;
	ygt(:, i, :)    = repmat(GT, [1 topk]);
	phigt(:, i, :)  = repmat(gtfts{i}, [1 topk]);
end

