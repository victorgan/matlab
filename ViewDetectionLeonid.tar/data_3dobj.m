function [ndata, inst, name, imfn, bbvp] = data_3dobj(params, classes)
% [3Dobject] dataset-specific procedure
% get image names, bounding box + viewpoint annotation
if nargin < 2
	classes = params.classes;
end

%% find training images
% use all the instances in this class 
tic;

ndata = 0; inst = []; name = []; imfn = []; bbvp = [];

for cls = classes
	if iscell(cls), cls = cls{1}; end
	cls
	listing = dir(sprintf('%s/%s/%s*', params.dsdir, cls, cls));

	for i = 1:length(listing)
		instname = listing(i).name;
		dirname  = [params.dsdir cls '/' instname];
		[st re]  = strtok(instname, '_');
		inum     = str2num(strtok(re, '_'))

		for vp = params.viewpoints, for h = params.heights, for s = params.scales
			imgfn   = sprintf('%s/A%d_H%d_S%d.bmp', dirname, vp, h, s);
			mskfn   = sprintf('%s/mask/A%d_H%d_S%d.mask', dirname, vp, h, s);
			imgname = sprintf('%s_A%dH%dS%d', instname, vp, h, s);
			if ~exist(imgfn, 'file') || ~exist(mskfn, 'file')
				disp(['x ' imgname]); continue; 
			end
			% update
			ndata          = ndata + 1;
			inst(ndata)    = inum;
			name{ndata}    = imgname;
			imfn{ndata}    = imgfn;
			%bbvp(ndata, :) = [readmask_3dobj(mskfn) vp];
			bbvp(ndata, :) = [readmask_3dobj(mskfn) (vp-1)*pi/4];
		end, end, end
	end
end
