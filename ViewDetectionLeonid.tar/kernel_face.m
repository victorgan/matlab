function K = kernel_face(xm, xn, ym, yn, params, sigma)
%
% INPUT
% xm - D*m matrix, each column a feature vector
% xn - D*n matrix, each column a feature vector
% ym - 2*m matrix, each column a label: [pitch yaw]
% yn - 2*n matrix, each column a label: [pitch yaw]
%
% OUTPUT
% K - m*n matrix
%

%% structural kernel
switch (params.Kx) 
	case {'lin', 'nlin2', 'hell', 'nhell', 'chi2', 'nchi2', 'hi', 'nhi'}
		% linear (or treat as linear)
		Kx = xm' * xn; 

	case 'rbfhi' 
		% RBF w/ histogram intersection distance
		Kx = ker_rbf_hi(xm, xn, sigma);

	case 'rbfchi2' 
		% RBF w/ chi2 distance
		Kx = ker_rbf_chi2(xm, xn, sigma);

	case 'gaussian' 
		% Gaussian RBF
		Kx = ker_rbf_l2(xm, xn, sigma);

	otherwise
		error(sprintf('unsupported Kx type %s', params.Kx));
end

%% pose kernel
switch (params.Ky)
	case {'gaussian', 'gauss'}
		% Gaussian RBF
		m = size(ym, 2);
		n = size(yn, 2);
		dv = abs(repmat(reshape(ym', [m 1 2]), [1 n]) ...
			- repmat(reshape(yn', [1 n 2]), [m 1]));
		dv = min(dv, 360-dv);
		d1 = dv(:, :, 1);
		d2 = dv(:, :, 2);
		sg = inv(params.kyparm^2);
		sm = sg(1,1)*d1.^2 + (sg(1,2)+sg(1,2))*d1.*d2 + sg(2,2)*d2.^2;
		Ky = exp(-0.5 * sm);
		%Ky = ker_rbf_l2(ym, yn, inv(params.kyparm^2), 360);

	case {'poly', 'polynomial'}
		% Polynomial kernel
		vm = ym(5, :);
		vn = yn(5, :);
		m = length(vm);
		n = length(vn);
		dvmn = abs(repmat(vm', [1 n]) - repmat(vn, [m 1]));
		dvmn = min(dvmn, 2*pi-dvmn);
		Ky = polyval(params.kyparm, dvmn);

	otherwise
		error(sprintf('unsupported Ky type %s', params.Ky));

end

%% multiplicative joint kernel
K = Kx .* Ky;

