function [model iter] = structSVM_1slack_lin(...
	idtrain, para, MVC_init, MVC_1slack, Verb ...
	)
% linear structural SVM training with 1-slack cutting plane algorithm.
% Adapted from the implementation by Jianxiong Xiao (http://mit.edu/jxiao/)
% by Kun He, Boston University
%
% glboal xtrain - training patterns
% global ytrain - training labels
%
% idtrain - training instances involved
% para    - structure holding parameters
%           para.kfunc: function handle for computing kernels
%
% MVC_init   - function handle for initializing and returning first cutting plane
% MVC_1slack - function handle for finding cutting planes
% both take a set of instances (idtrain) and return:
% Y : matrix, each ROW is ybar for a specific instance i in idtrain
% V : vector, each is the violation amount (Delta+f(ybar)-f(yi)) for i
% M : vector, each is the margin (loss) value Delta(ybar,yi) for i
% Fc: matrix, each COLUMN is a violating feature vector \phi(xi, ybar_i)
% Fg: matrix, each COLUMN is a ground truth feature vector \phi(xi, yi)
% Yg: matrix, each ROW is a ground truth label yi
%
% PSI  - ground truth feature vectors \psi(xi, yi)
% RHO  - RHS of cutting plane constraints
% H    - kernel matrix
% xi   - the slack variable in the 1-slack cutting plane algorithm
% alph - dual variables

if nargin < 5, Verb = 1; end
if Verb, fprintf('-------- structSVM_1slack_ker --------\n'); end
global xtrain ytrain

idtrain = unique(idtrain); 
idtrain = reshape(idtrain, 1, []);  % make sure it's row vector
PSI     = zeros(para.ftdim, para.max_planes);
RHO     = zeros(para.max_planes, 1, 'single');
idle    = zeros(para.max_planes, 1);
maxidle = 16;
TolRel  = 0.001; 
TolVio  = 0.02;


%% init
[Y, V, M, Fc, Fg, Yg] = MVC_init(idtrain, para, 1);
PSI(:,1) = mean(Fg - Fc, 2);
RHO(1)   = mean(M);
ncp      = 1;
alph     = para.svm_C;
xi       = 0;

%% go!
model = [];  model.ids = idtrain;
iter = 0;

while iter < para.max_planes

	iter = iter + 1;
	if Verb 
		fprintf('\n==== Iteration #%d ====\n', iter); 
		fprintf('%s(N=%d), k=%d, C=%g\n', para.cls, length(ytrain), length(idtrain), para.svm_C);
	end

	%% update model
	% note there's implicit "divide by ntrain" here, since PSI is the mean, which
	%      involves the division operation
	svidx       = find(abs(alph) > 1e-6);
	model.alph  = alph(svidx); 
	model.psicp = PSI(:, svidx);

	%{
	%% [experimental] remove SVs that are inactive for some time
	idle(svidx) = 0;
	inac        = find(~ismember(1:ncp, svidx));
	idle(inac)  = idle(inac) + 1;
	active      = find(idle(1:ncp) < maxidle);
	if length(active) < ncp
		fprintf('remove inactives: %d -> %d\n', ncp, length(active));
		ncp = length(active);
		for i = 1 : ncp
			PSI(:,i) = PSI(:, active(i));
			RHO(i)   = RHO(active(i));
			alph(i)  = alph(active(i));
			idle(i)  = idle(active(i));
		end
	end
	%}
	
	%% go to separation oracle to find MVC
	tic;
	[Y, V, M, Fc, Fg, Yg] = MVC_1slack(idtrain, model, para);
	vio = mean(V);
	fprintf('\n====> '); toc;
	if Verb > 1
		fprintf('\nnsv=%d. MVC violation %g. xi=%g\n.', length(svidx), vio, xi);
	elseif Verb > 0
		fprintf('.'); 
	end
	if vio < xi+TolVio
		if Verb
			fprintf('vio(%g) < xi(%g)+eps, stop @ %d iters\n', vio, xi, iter);
		end
		break;
	end

	%% 1 slack: always add 1 new constraint
	ncp        = ncp + 1;
	PSI(:,ncp) = mean(Fg - Fc, 2);
	RHO(ncp)   = mean(M);
	H          = PSI(:, 1:ncp)' * PSI(:, 1:ncp);
	H          = (H + H') / 2;
	alph       = [alph; 0];

	%% solve QP, recover xi
	dH = double(H);
	df = double(RHO(1:ncp));
	%{
	% matlab's QP solver may be good enough?
	opts = optimset('algorithm', 'interior-point-convex', 'display', 'off');
	[alph, negdual, flg] = quadprog(dH, -df, ones(1,ncp), para.svm_C, ...
		[], [], zeros(ncp,1), [], alph, opts);
	%}
	%
	% use specialized QP solver
	I = ones(size(alph), 'uint32');
	S = ones(size(alph), 'uint8');
	[alph, negdual] = qp(dH, -df, para.svm_C, I, S, alph, inf, 0, TolRel, -inf, 0);
	%dualval = -negdual;
	xi = max(df - dH*alph);

	% normalize
	if Verb > 1, disp(alph'); end
	if length(alph) > 2 && alph(end) < 1e-6, break; end

	% check for non-PSDness
	[R, p] = chol(dH);
	if p, disp('Warning: non-PSD gram matrix'); end
end

if Verb
	if iter >= para.max_planes
		fprintf('!! structSVM_1slack_lin did not converge\n');
	end
	fprintf('*********************************************\n');
else
	fprintf('>> structSVM_1slack_lin: %d iters\n', iter);
end

