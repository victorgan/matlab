function [ndata, inst, name, imfn, bbvp] = data_epflcars(params)
% [EPFL cars] dataset-specific
% get image names, bounding box + viewpoint annotation
%
fd = fopen([params.imdir '/tripod-seq.txt'], 'r');
ninst     = fscanf(fd, '%d', [1 1]);
imwh      = fscanf(fd, '%d', [2 1]);
nimg      = fscanf(fd, '%d', [ninst 1]);
imgfnstr  = fscanf(fd, '%s', [1 1]);
boxfnstr  = fscanf(fd, '%s', [1 1]);
nimg_360  = fscanf(fd, '%d', [ninst 1]);
front_id  = fscanf(fd, '%d', [ninst 1]);
clockwise = fscanf(fd, '%d', [ninst 1]);
fclose(fd);

ndata = 0; inst = []; name = []; imfn = []; bbvp = [];
for inum = 1:ninst
	fn = sprintf([params.imdir boxfnstr], inum)
	fd = fopen(fn, 'r');
	bb = fscanf(fd, '%f', [4 nimg(inum)])';
	fclose(fd);
	for im = 1:nimg(inum) 
		ndata       = ndata + 1;
		inst(ndata) = inum;
		name{ndata} = sprintf('%02d_%03d', inum, im);
		imfn{ndata} = sprintf([params.imdir imgfnstr], inum, im);
		vp = (im-front_id(inum))*clockwise(inum)/nimg_360(inum);
		while vp < 0, vp = vp + 1; end
		while vp >= 1, vp = vp - 1; end
		B = bb(im,:);
		bbvp(ndata,:) = [round([B(1) B(2) B(1)+B(3) B(2)+B(4)]) vp*2*pi];
	end
end

%{
% ---- angle conventions:
% front_id - ground zero
% angle increases when frame id increases, if clockwise=1
% clockwise = -1: otherwise
% angle is in [0, 2*pi] hopefully
dvp = 2*pi / nvp;
params.binSid = zeros(10, nvp); % start of viewpoint bin
params.binEid = zeros(10, nvp); % end of viewpoint bin
params.binCid = zeros(10, nvp); % center of viewpoint bin
keyboard
params.binCid(:, 1) = params.front_id;
for j = 2:nvp
	temp = round(params.front_id + (j-1)*dvp ./ params.delta);
	temp = temp .* params.clockwise;
	params.binCid(:, j) = 1 + mod(temp - 1, params.nimgs);
end
% TODO: overlapping bins or non-overlapping bins?
for j = 1:nvp
	temp = round(params.binCid - dvp/2 ./ params.delta);
	temp = temp .* params.clockwise;
	params.binSid(:, j) = 1 + mod(temp - 1, params.nimgs);
	temp = round(params.binCid + dvp/2 ./ params.delta);
	temp = temp .* params.clockwise;
	params.binEid(:, j) = 1 + mod(temp - 1, params.nimgs);
end
%}

