function demo_face01(C, Kx)
if nargin < 2, Kx = 'lin'; end

global ids
% struct ids: 
%  inst - object instance numbers
%  name - image names
%  imfn - actual image file paths
% train - list of IDs in training set
%  test - list of IDs in testing set
global xtrain ytrain

% set parameters
nlv = 3;
params = init_face('pointing04', nlv, 10, Kx);

% prepare data, load data
okfn = sprintf('%s/HOG_%dlv.ok', params.modeldir, nlv);
if ~exist(okfn, 'file')
	% ids
	idsfn = sprintf('%s/%s_ids.mat', params.modeldir, params.ds);
	[ndata, inst, name, imfn, bbvp] = data_pointing04(params);
	save(idsfn, 'inst', 'name', 'imfn', 'bbvp');

	% features
	rng('shuffle');
	if matlabpool('size')
		perm = randperm(ndata);
		parfor x = 1:ndata
			i = perm(x);
			if mod(x,100)==0, disp(i); end
			crop_and_HOG(params.localdir, name{i}, imfn{i}, nlv, bbvp(i,1:4));
		end
	else
		for i = randperm(ndata)
			if mod(i,100)==0, disp(i); end
			crop_and_HOG(params.localdir, name{i}, imfn{i}, nlv, bbvp(i,1:4));
		end
	end

	% done
	system(sprintf('touch %s\n', okfn), '-echo');
end
ids = load(sprintf('%s/%s_ids.mat', params.modeldir, params.cls));

% multiple trials of 5-fold cross validation
nperm = length(params.iperm);
mae_pitch = zeros(nperm, 5);
mae_yaw = zeros(nperm, 5);
curbest = 100;
mp_best = 100;
my_best = 100;
	
modelstr = sprintf('HOG%dlv_%s_C%g_1vsall', nlv, params.Kx, C)

for perm = 1:10
	inst_perm = params.iperm{perm};
	modeldir = sprintf('%s/%s_perm%d/', params.modeldir, params.cls, perm);
	if ~exist(modeldir, 'dir')
		mkdir(modeldir); 
	end
	for split = 1:5
		ids.test  = [];
		ids.train = [];
		instest = inst_perm(params.itest{5+split});
		flg = 0;
		for ins = instest
			idx = find(ismember(ids.inst, ins));
			ids.test  = [ids.test idx(93*flg+(1:93))];
			ids.train = [ids.train idx(93*(1-flg)+(1:93))];
			flg = 1 - flg;
		end
		instrain  = inst_perm(params.itrain{5+split});
		ids.train = [ids.train find(ismember(ids.inst, instrain))];
		ntrain    = length(ids.train);
		ytrain    = ids.bbvp(ids.train, 5:6); % use rowvec
		ntest     = length(ids.test);
		ytest     = ids.bbvp(ids.test, 5:6); % use rowvec


		%% train model
		modelfn = sprintf('%s%s_split%d.mat', modeldir, modelstr, split);
		try 
			load(modelfn);
		catch
			model_sum = svm_1vsall_face(C, params);
			save(modelfn, 'model_sum', '-v7.3');
			disp('Model saved.');
		end

		%% test model
		try
			resfn = sprintf('%s/res_%s_split%d.mat', modeldir, modelstr, split);
			load(resfn);
		catch
			%{
			% construct 93 linear classifiers
			nvp = size(params.viewpoints, 1);
			W = zeros(params.ftdim, nvp);
			fprintf('constructing Ws: '); tic;
			parfor v = 1:nvp
				vp = params.viewpoints(v, :);
				agt = model_sum.alph .* Ky_face(vp', model_sum.ygt, params.kyparm);
				W(:, v) = W(:, v) + model_sum.psigt * agt; 
				acp = model_sum.alph .* Ky_face(vp', model_sum.ycp, params.kyparm);
				W(:, v) = W(:, v) - model_sum.psicp * acp; 
			end
			toc;
			%}
			W = model_sum.w;

			xtest = zeros(params.ftdim, ntest);
			tic; fprintf('loading features: ');
			parfor i = 1 : ntest
				idx = ids.test(i);
				ld = load(sprintf('%s/%s_HOG_%dlv.mat', params.localdir, ids.name{idx}, params.pyralvs));
				%xtest(:, i) = ld.ft;
				switch params.Kx
					case 'lin'
						xtest(:, i) = ld.ft;
					case 'nlin2'
						xtest(:, i) = ld.ft/norm(ld.ft,2);
					case 'hell'
						xtest(:, i) = sqrt(ld.ft);
					case 'nhell'
						xtest(:, i) = sqrt(ld.ft/norm(ld.ft,1));
					case 'chi2'
						xtest(:, i) = vl_homkermap(ld.ft, 3);
					case 'nchi2'
						xtest(:, i) = vl_homkermap(ld.ft/norm(ld.ft,1), 3);
					otherwise
						error('unkown Kx');
				end
			end
			toc;
			%sdet = W' * xtest;
			sdet = W' * xtest + repmat(model_sum.b, [1 ntest]);
			[sdet, id] = max(sdet, [], 1);
			ydet = params.viewpoints(id, :);

			% save
			save(resfn, 'ydet', 'sdet', 'xtest');
		end

		% compute MAE
		mae_p = mean(abs(ydet(:, 1)-ytest(:, 1)));
		mae_y = mean(abs(ydet(:, 2)-ytest(:, 2)));
		fprintf('perm%d split%d:  MAE_pitch %.3f, MAE_yaw %.3f\n', perm, split, mae_p, mae_y);
		mae_pitch(perm, split) = mae_p;
		mae_yaw(perm, split) = mae_y;

		% do refinement
		%{
		tt = tic;
		ydet = refine_vp(model_sum, ydet, xtest, params);
		mae_p = mean(abs(ydet(:, 1)-ytest(:, 1)));
		mae_y = mean(abs(ydet(:, 2)-ytest(:, 2)));
		fprintf('     refined:  MAE_pitch %.3f, MAE_yaw %.3f (%.2fsec)\n', mae_p, mae_y, toc(tt));
		mae_pitch_refined(perm, split) = mae_p;
		mae_yaw_refined(perm, split) = mae_y;
		keyboard
		%}
	end
	mp = mae_pitch(perm, :);
	my = mae_yaw(perm, :);
	fprintf('\nperm%d 5-CV:  MAE_pitch %.3f (+/-%.3f), MAE_yaw %.3f (+/-%.3f)\n\n', ...
		perm, mean(mp), std(mp), mean(my), std(my));
	if mean(mp+my) < curbest
		curbest = mean(mp + my);
		mp_best = mean(mp);
		my_best = mean(my);
	end
	pause(1)
end
ids = [];
mp_perm = mean(mae_pitch, 2);
my_perm = mean(mae_yaw, 2);
fprintf('====================================================\n');
fprintf('%d perms, MAE_pitch %.3f(+/-%.3f), MAE_yaw %.3f(+/-%.3f)\n', ...
	nperm, mean(mp_perm), std(mp_perm), mean(my_perm), std(my_perm));
fprintf('   best: MAE_pitch %.3f, MAE_yaw %.3f\n', mp_best, my_best);
fprintf('    avg: %.3f(+/-%.3f)\n', mean(mp_perm+my_perm)/2, std(mp_perm+my_perm)/2);
fprintf('====================================================\n');

keyboard

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function ydet = refine_vp(model, ydet, fdet, params)
% keep BB prediction intact, refine vp estimation
%
ntest = size(ydet, 1);
dv_th = 50;
sigma = params.kyparm;
nsv   = length(model.alph);

%fprintf('compressing the model: '); tt = tic;
% gt
[psigt, ia, ic] = unique(model.psigt', 'rows');
ngt = length(ia);
ygt = model.ygt(:, ia);
agt = zeros(ngt, 1);
for c = 1:nsv
	agt(ic(c)) = agt(ic(c)) + model.alph(c);
end
% cp
[psicp, ia, ic] = unique([model.psicp' model.ycp'], 'rows');
psicp = psicp(:, 1:end-2);
ncp = length(ia);
ycp = model.ycp(:, ia);
acp = zeros(ncp, 1);
for c = 1:nsv
	acp(ic(c)) = acp(ic(c)) + model.alph(c);
end
%fprintf('%d -> %d SVs in %.2fsec\n', 2*nsv, ngt+ncp, toc(tt));

parfor i = 1:ntest
	vi = ydet(i, :);
	coeff = [];
	theta = [];

	% find relevant SVs - positive ones
	dv = ygt' - repmat(vi, [ngt 1]); % n*2
	id = find(min(abs(dv), [], 2) < dv_th);
	coeff = agt(id) .* (psigt(id, :)*fdet(:,i));
	theta = dv(id, :)';

	% find relevant SVs - negative ones
	dv = ycp' - repmat(vi, [ncp 1]); % n*2
	id = find(min(abs(dv), [], 2) < dv_th);
	coeff = [coeff; -acp(id) .* (psicp(id, :)*fdet(:,i))];
	theta = [theta, dv(id, :)'];

	% constrained optimization on the increment
	opt = optimset('display', 'off', 'algorithm', 'active-set');
	inc = fmincon(@(x) -obj(x, coeff, theta, sigma), [0 0], [], [], [], [], ...
		-[45 45], [45 45], [], opt);
	ydet(i, :) = vi + inc;
end

function f = obj(x, coeff, theta, invsig2)
k = Ky_face(x, theta, invsig2);
f = coeff' * k;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function crop_and_HOG(localdir, id, fname, nlv, bb)
ftfn = sprintf('%s/%s_HOG_%dlv.mat', localdir, id, nlv);
lockfn = [ftfn '.lock'];
if ~exist(ftfn, 'file') && ~exist(lockfn, 'file')
	system(['touch ' lockfn]);
	% read, crop, resize to 64x64
	im = imread(fname);
	im = im(bb(2):bb(4), bb(1):bb(3), :);
	im = imresize(im, [64 64]);
	ft = [];
	step = 64;
	for l = 1:nlv
		hogft = HoG(im2double(im), [9 step/2 2 1 0.2]);
		%hogft = vl_hog(single(im), step/2);
		ft = [ft; hogft(:)];
		step = step/2;
	end
	save(ftfn, 'ft');
	fprintf('HOG_%dlv: %s\n', nlv, id);
	system(['chmod o-w ' ftfn]);
	system(['rm -f ' lockfn]);
end

