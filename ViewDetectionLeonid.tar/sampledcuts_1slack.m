function [model scache] = sampledcuts_1slack(...
	nsample, para, MVC_init, MVC_1slack, Verb ...
	)
if nargin < 5, Verb = 1; end
if Verb, fprintf('-------- sampledcuts_1slack --------\n'); end
global ids xtrain ytrain

ntrain  = length(ids.train);
PSIgt   = [];
Ygt     = [];
PSIcp   = [];
Ycp     = [];
Mcp     = [];
idle    = [0];
maxidle = 5;
Ngood   = 3;
topk    = 2;
nms_th  = 0.3;
TolRel  = 0.005;
TolVio  = 0.1;

%% init
idinit = get_bootstrap('uniform', ntrain, nsample); 
[Ycp{1}, V, M, PSIcp{1}, PSIgt{1}, Ygt{1}] = MVC_init(idinit, para, 1);
Mcp(1) = sum(M);
ncp    = 1;
H      = expand_kernel_matrix([], ncp, PSIgt, Ygt, PSIcp, Ycp, para);
alph   = min(1, para.svm_C);
v1     = 1;
xi     = 0;
model  = struct('nid', nsample)

%% go!
goodcnt = 0;
iter = 0;
vios = []; xis = [];
while iter < para.max_planes
	iter = iter + 1;
	if Verb
		fprintf('\n==== Iteration #%d ====\n', iter); 
		fprintf('%s(N=%d), k=%d, C=%g\n', ...
			para.cls, length(ytrain), nsample, para.svm_C);
	end

	%% update model
	svidx = find(abs(alph/nsample) > 1e-6);
	model.alph  = alph(svidx)/nsample;
	model.psicp = {PSIcp{svidx}};
	model.psigt = {PSIgt{svidx}};
	model.ycp   = {Ycp{svidx}};
	model.ygt   = {Ygt{svidx}};

	%% [experimental] remove SVs that are inactive for some time
	idle(svidx) = 0;
	inact       = find(~ismember(1:ncp, svidx));
	idle(inact) = idle(inact) + 1;
	active      = find(idle(1:ncp) < maxidle);
	if length(active) < ncp
		fprintf('remove inactives: %d -> %d\n', ncp, length(active));
		ncp = length(active);
		for i = 1:ncp
			PSIcp{i} = PSIcp{active(i)};
			PSIgt{i} = PSIgt{active(i)};
			Ygt{i}   = Ygt{active(i)};
			Ycp{i}   = Ycp{active(i)};
			Mcp(i)   = Mcp(active(i));
			alph(i)  = alph(active(i));
			idle(i)  = idle(active(i));
		end
		H = H(active, active);
	end

	%% go to separation oracle to find cutting plane
	tic;
	%idmvc = get_bootstrap('uniform', ntrain, nsample); 
	idmvc = get_bootstrap('D', ntrain, nsample, xtrain.rho); 
	fprintf('sampled %d\n', length(idmvc));

	th = max(TolVio, nms_th*min(1, (v1-xi-TolVio)/(v1+xi+TolVio)));
	[Y, V, M, F, Fg, Yg] = MVC_1slack(idmvc, model, para, topk, th);
	fprintf('\n====> '); toc;
	xtrain.rho(idmvc) = M(:, 1);

	%% check for stopping criterion
	vv = mean(V, 1);
	v1 = max(vv);
	fprintf('\nnSV=%d, MVC vio=%g, xi=%g\n', length(svidx), v1, xi);
	if v1 < xi + TolVio
		goodcnt = goodcnt + 1;
		if Verb, fprintf('vio<xi+eps, for %d iters\n\n', v1, xi, goodcnt); end
	else
		goodcnt = 0; 
	end

	%% add MVCs
	for k = find(vv >= xi)
		ncp        = ncp + 1;
		PSIcp{ncp} = squeeze(F(:, :, k));
		PSIgt{ncp} = squeeze(Fg(:, :, k));
		Ycp{ncp}   = squeeze(Y(:, :, k));
		Ygt{ncp}   = squeeze(Yg(:, :, k));
		Mcp(ncp)   = sum(M(:, k));
		H    = expand_kernel_matrix(H, ncp, PSIgt, Ygt, PSIcp, Ycp, para);
		alph = [alph; 0];
		idle = [idle; 0];
		fprintf('added cutting plane: vio=%g\n', vv(k));
	end

	%% solve QP, recover xi
	dH = double(H) / ntrain^2;
	df = double(Mcp(1:ncp)') / ntrain;
	if 0
		% matlab's QP solver may be good enough?
		opts = optimset('algorithm', 'interior-point-convex', 'display', 'off');
		[alph, negdual, flg] = quadprog(dH, -df, ones(1,ncp), para.svm_C, ...
			[], [], zeros(ncp,1), [], zeros(size(alph)), opts);
	else
		% use specialized QP solver
		I = ones(size(alph), 'uint32');
		S = ones(size(alph), 'uint8');
		[alph, ~] = qp(dH, -df, para.svm_C, I, S, zeros(size(alph)), 1e5, 0, TolRel, -inf, 1);
	end
	xi = max(df - dH * alph);
	if Verb > 1
		disp(alph'); sum(alph)
	end

	[R, p] = chol(H);
	if p, disp('Warning: non-PSD gram matrix'); end

	% terminate
	if goodcnt > Ngood
		if Verb
			fprintf('Stop @ %d iters\n', iter); end
		break;
	end
end

if Verb
	if iter >= para.max_planes
		fprintf('!! sampledcuts_1slack did not converge\n');
	end
	fprintf('*********************************************\n');
else
	fprintf('>> sampledcuts_1slack: %d iters\n', iter);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
function id = get_bootstrap(method, ntrain, nuse, rho)
if nargin < 4, rho = 1; end
if strcmpi(method, 'all')
	id = 1:ntrain;

elseif strcmpi(method, 'D')
	D  = rho + mean(rho)/2;
	id = discretesample(D./sum(D), nuse);

elseif strcmpi(method, 'uniform')
	if (nuse >= ntrain)
		id = 1:ntrain;
	else
		perm = randperm(ntrain);
		id = perm(1:nuse);
	end

elseif strcmpi(methoed, 'disjoint')
	id = t:T:ntrain;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
function H = expand_kernel_matrix(H, Np, PSIgt, Ygt, PSIcp, Ycp, para)
n = size(H, 1);
assert(n+1 == Np);
if n == 0
	H = sv_innerprod(para, ...
		PSIgt{1}, Ygt{1}, PSIcp{1}, Ycp{1}, ...
		PSIgt{1}, Ygt{1}, PSIcp{1}, Ycp{1});
else
	%% this part for the new padding col/row of H
	h = zeros(n, 1);
	for cp = 1:n
		h(cp) = sv_innerprod(para, ...
			PSIgt{cp}, Ygt{cp}, PSIcp{cp}, Ycp{cp}, ...
			PSIgt{Np}, Ygt{Np}, PSIcp{Np}, Ycp{Np});
	end

	%% this part for the last entry of new H
	hh = sv_innerprod(para, ...
		PSIgt{Np}, Ygt{Np}, PSIcp{Np}, Ycp{Np}, ...
		PSIgt{Np}, Ygt{Np}, PSIcp{Np}, Ycp{Np});

	%% final assembly
	H = [H h; h' hh];
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
function K = sv_innerprod(para, ...
	PSI1, Y1, psi1, y1, ...
	PSI2, Y2, psi2, y2)
kernel = para.kfunc;
K1 = kernel(PSI1, PSI2, Y1, Y2, para);
K2 = kernel(PSI1, psi2, Y1, y2, para);
K3 = kernel(psi1, PSI2, y1, Y2, para);
K4 = kernel(psi1, psi2, y1, y2, para);
K = K1 - K2 - K3 + K4;
K = sum(K(:));


