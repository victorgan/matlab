function display_viewdet(ydet, ytest, itest, flip, lw, gtcolor)
global ids
if nargin < 4 || isempty(flip), flip = 0; end
if nargin < 5, lw = 4; end
if nargin < 6, gtcolor = 'y'; end

% display image
im = imread(ids.imfn{ids.test(itest)});
[h w ~] = size(im);
figure(1);
image(im); axis off

% ground truth in yellow
rectangle('Position', bb2rect(ytest(1:4)'), 'Edgecolor', gtcolor, 'Linewidth', lw);
set(gca, 'position', [0 0 1 1]);
set(gcf, 'position', [0 0 w h]);
return;

if isempty(ydet)
	x0 = (ytest(2)+ytest(4))/2;
	y0 = (ytest(1)+ytest(3))/2;
	an = -ytest(5);
	if flip, an = flip-an; end
	arrow([y0 x0], [y0+60*sin(an) x0+60*cos(an)], 'edgecolor', gtcolor, 'linewidth', lw);
else
	% GT arrow
	x0 = (ytest(2)+ytest(4))/2;
	y0 = (ytest(1)+ytest(3))/2;
	an = -ytest(5);
	if flip, an = flip-an; end
	arrow([y0 x0], [y0+60*sin(an) x0+60*cos(an)], 'edgecolor', gtcolor, 'linewidth', lw);

	% detection in green (good), red (bad)
	bb_det = ydet(1:4)';
	bb_gt  = ytest(1:4)';
	agt  = rectint(bb2rect(bb_gt), bb2rect(bb_gt));
	adet = rectint(bb2rect(bb_det), bb2rect(bb_det));
	aint = rectint(bb2rect(bb_det), bb2rect(bb_gt));
	if aint/(agt+adet-aint) < .5
		rectangle('Position', bb2rect(ydet(1:4)'), 'Edgecolor', 'r', 'Linewidth', lw);
	else
		rectangle('Position', bb2rect(ydet(1:4)'), 'Edgecolor', 'g', 'Linewidth', lw);
	end

	x0 = (ydet(2)+ydet(4))/2;
	y0 = (ydet(1)+ydet(3))/2;
	dv = abs(ydet(5)-ytest(5));
	dv = min(dv, 2*pi-dv);
	if dv < pi/6
		color = 'g';
	else
		color = 'r';
	end
	an = -ydet(5);
	if flip, an = flip-an; end
	arrow([y0 x0], [y0+60*sin(an) x0+60*cos(an)], 'edgecolor', color, 'linewidth', lw);
end

