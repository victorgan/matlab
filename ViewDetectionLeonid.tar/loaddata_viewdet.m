function [ids xtrain ytrain xtest ytest] = loaddata_viewdet(...
	cls, params, reduce) 
if nargin < 3
	reduce = 1;
end
localdir = params.modeldir;

%% ids
ids       = load(sprintf('%s/%s_ids.mat', localdir, cls));
ids.train = find(ismember([ids.inst], params.inst_train));
ids.test  = find(ismember([ids.inst], params.inst_test));
ntrain    = length(ids.train);
ntest     = length(ids.test);

%% setting up ytrain & ytest
ytrain  = ids.bbvp(ids.train, :)';
ytest   = ids.bbvp(ids.test, :)';
ids     = rmfield(ids, 'bbvp');

%% setting up xtrain & xtest
xtrain      = load(sprintf('%s/%s_stdbb%d.mat', localdir, cls, params.nstdbb));
xtrain.gtft = cell(1, ntrain);
xtrain.IHfn = cell(1, ntrain);
xtrain.rho  = ones(1, ntrain);
xtest       = [];
xtest.IHfn  = [];

% [experimental] Training: only use a subset
if reduce
	idx         = find(ids.inst(ids.train) < 5);
	ids.train   = ids.train(idx);
	ytrain      = ytrain(:, idx);
	xtrain.gtft = {xtrain.gtft{idx}};
	xtrain.IHfn = {xtrain.IHfn{idx}};
	xtrain.rho  = xtrain.rho(idx);

	idx      = find(ids.inst(ids.test) > 16);
	ids.test = ids.test(idx);
	ytest    = ytest(:, idx);
end

