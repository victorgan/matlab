function c = loss_pitchyaw01(py1, py2)
% function c = loss_pitchyaw(py1, py2)
% py1, py2 must be degrees (0-360)
% py1 is 1*2 or 2*1
% py2 is n*2 
if size(py1, 1) == 2
	py1 = py1';
end
c = [];
for i = 1:size(py2, 1)
	c = [c; 1-isequal(py1, py2(i,:))];
end

