function K = kernel_viewdet(xm, xn, ym, yn, params, sigma)
% vectorized computation for K = Kx * Ky
%
% INPUT
% xm - D*m matrix, each column a feature vector
% xn - D*n matrix, each column a feature vector
% ym - 5*m matrix, each column a label: [bb vp]
% yn - 5*n matrix, each column a label: [bb vp]
%
% OUTPUT
% K - m*n matrix
%
if nargin < 6, sigma = params.sigma_x; end

%% structural kernel
switch (params.Kx) 
	case {'linear', 'lin', 'nlin', 'nlin1', 'nlin2'}
		% linear
		% note: assumes input features are already normalized accordingly
		Kx = xm' * xn; 

	case {'hell', 'hellin', 'nhell'}
		% hellinger kenel
		% note: NOT sqrt(xm'*xn) !
		Kx = sqrt(xm)' * sqrt(xn);

	case {'hi', 'nhi'}
		% histogram intersection
		Kx = ker_hi(xm, xn);

	case {'chi2', 'nchi2'} 
		% chi2 kernel
		Kx = ker_chi2(xm, xn);

	case 'rbfhi' 
		% RBF w/ histogram intersection distance
		Kx = ker_rbf_hi(xm, xn, sigma);

	case 'rbfchi2' 
		% RBF w/ chi2 distance
		Kx = ker_rbf_chi2(xm, xn, sigma);

	case 'gaussian' 
		% Gaussian RBF
		Kx = ker_rbf_l2(xm, xn, sigma);

	otherwise
		error(sprintf('unsupported Kx type %s', params.Kx));
end

%% pose kernel
switch (params.Ky)
	case {'gaussian', 'gauss'}
		% Gaussian RBF
		vm = ym(5, :);
		vn = yn(5, :);
		m = length(vm);
		n = length(vn);
		dvmn = abs(repmat(vm', [1 n]) - repmat(vn, [m 1]));
		dvmn = min(dvmn, 2*pi-dvmn);
		Ky = exp(-dvmn.*dvmn*0.5/params.kyparm^2);
		%Ky = ker_rbf_l2(vm', vn', params.kyparm);

	case {'poly', 'polynomial'}
		% Polynomial kernel
		vm = ym(5, :);
		vn = yn(5, :);
		m = length(vm);
		n = length(vn);
		dvmn = abs(repmat(vm', [1 n]) - repmat(vn, [m 1]));
		dvmn = min(dvmn, 2*pi-dvmn);
		Ky = polyval(params.kyparm, dvmn);

	otherwise
		error(sprintf('unsupported Ky type %s', params.Ky));

end

%% multiplicative joint kernel
K = Kx .* Ky;

