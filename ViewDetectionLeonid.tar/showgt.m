function show_gt(cls, inds, saveimg)
if nargin < 3, saveimg = false; end
global ids
ftype = {'SIFTg'};
nvw   = 500;
nlv   = 3;
Kx    = 'nlin2';
sig   = pi/10;
split = 1;
obj3d = 0;
if ~strcmpi('epfl', cls) && ~strcmpi('epflcars', cls)
	params = init_viewdet('3dobj', cls, ...
		ftype, nvw, nlv, Kx, sig, 0.5, 5, 8, split);
	obj3d = 1;
else
	cls = 'epflcars';
	params = init_viewdet(cls, cls, ...
		ftype, nvw, nlv, Kx, sig, 0.5, 3, 16, split);
end
	
[ids, ~, ~, xtest, ytest] = loaddata_viewdet(cls, params, 0);

for i = inds
	if strcmpi(cls, 'car')
		display_viewdet([], ytest(:, i), i, pi);
	elseif strcmpi(cls, 'bicycle')
		display_viewdet([], ytest(:, i), i, pi/2);
	else
		display_viewdet([], ytest(:, i), i);
	end

	if saveimg
		export_fig(sprintf('%s_gt%d.png', cls, i));
	else
		pause(.2);
	end
end

