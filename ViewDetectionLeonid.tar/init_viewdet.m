function params = init_viewdet(ds, cls, ...
	ftype, nvw, lvs, Kx, ...
	sigma, alpha, ...
	nbb, nvp, split) 
if nargin < 9,  nbb = 5, end
if nargin < 10, nvp = 16, end
if nargin < 11, split = 1, end

addpath('_util');
addpath('ess_posedet');
params = struct('ds', ds, 'cls', cls, 'vwords', nvw, 'pyralvs', lvs, 'Kx', Kx);
params.fttype = ftype;

%% dirs
params.basedir  = [absolutepath('..', pwd) '/']; % humanpose
params.cachedir = [params.basedir '_cache/'];
params.localdir = [params.cachedir ds '/'];
params.modeldir = [params.cachedir ds 'model/'];
if ~exist(params.cachedir, 'dir'), mkdir(params.cachedir); end
if ~exist(params.localdir, 'dir'), mkdir(params.localdir); end
if ~exist(params.modeldir, 'dir'), mkdir(params.modeldir); end

%% weighting the 2 losses, scaling of vp loss
params.alpha = alpha;
params.lambda = 1.0;

%% kernel
params.Ky = 'gauss';
params.kyparm = sigma;
params.sigma_x = 1;
params.kfunc = @kernel_viewdet;

%% feature
step = 6;
params.gridstep = step;
params.siftsizes = [4 6 8]; %round(step/2) * (2:5);
params.ftdim = sum(params.vwords)*(4^lvs-1)/3;
params.IHstr = ['IH' num2str(step) '_'];
for i = 1:length(ftype)
	params.IHstr = [params.IHstr ftype{i} num2str(nvw(i))];
end
% feature encoding method
% (various methods were tried, and LLC3 seems to work best)
%
%params.encoding = 'BOW';
%params.encoding = 'D';
%params.encoding = '421';
%params.encoding = '4211';
%params.encoding = '42111';
%params.encoding = '421111';
%params.encoding = 'LLC5';
params.encoding = 'LLC3';
params.IHstr = [params.IHstr '_' params.encoding];

%% various helpers
% vlfeat
% note: modify to point to your vlfeat installation
vlfeatdir = [params.basedir 'Libs/vlfeat/'];
run([vlfeatdir 'toolbox/vl_setup']); 
% voc-release5
%pffdir_bin = [params.basedir 'voc-release5/bin'];
%pffdir_vis = [params.basedir 'voc-release5/vis'];
%addpath(pffdir_bin);
%addpath(pffdir_vis);

if strcmpi(ds, '3dobj')
	% basic sanity check for class name
	params.classes = {'bicycle' 'car' 'cellphone' 'iron' ...
		'mouse' 'shoe' 'stapler' 'toaster'};
	if isempty(find(strcmp(cls, params.classes)))
		error('Invalid class name!');
	end
	params.cls     = cls;
	% note modify to point to your 3D objects
	params.dsdir   = [params.basedir 'Data/3dobj/'];
	params.listing = dir([params.dsdir cls '/' cls '*']);
	%
	% 3Dobject dataset info: split, etc.
	itrain = {1:7, 2:8, 3:9, 4:10, [5:10 1], [6:10 1:2], [7:10 1:3], [8:10 1:4], [9:10 1:5], [10 1:6]};  
	itest  = {8:10, [9 10 1], [10 1 2], 1:3, 2:4, 3:5, 4:6, 5:7, 6:8, 7:9};
	disp(['split: ' num2str(split)])
	params.split      = split;
	params.inst_train = itrain{split};
	params.inst_test  = itest{split};
	%
	params.viewpoints = 1:8;
	params.heights    = 1:3;
	params.scales     = 1:3;
	%
	% # of clusters for bbox size
	params.nstdbb = nbb;

elseif strcmpi(ds, 'epflcars')
	params.cls   = 'epflcars';
	% note modify to point to your epfl_cars
	params.dsdir = [params.basedir 'Data/epfl_cars'];
	params.imdir = [params.dsdir '/tripod-seq/'];

	itrain = {1:10, 1:19, [1 3 5]};
	itest  = {11:20, 20, 7:8};
	disp(['split: ' num2str(split)])
	params.split      = split;
	params.inst_train = itrain{split};
	params.inst_test  = itest{split};

	params.viewpoints = 1:nvp;
	params.nstdbb = nbb;

end

