function vdet = refine_pose(model, vdet, fdet, params)
% keep BB prediction intact, refine pose estimation
%
dv_th = pi*.75;  % ignore kernel responses beyond this
dvp   = 2*pi/length(params.viewpoints);
sigma = params.kyparm;

if strcmpi(model.type, '1slack')
	% trained by structSVM_1slack
	nid     = length(model.ids);
	svcoef  = sum(model.alph)*ones(nid, 1);
	svhist  = model.psigt;
	svlabel = model.ygt;
	for sv = 1:length(model.alph)
		a       = model.alph(sv);
		svcoef  = [svcoef; -a*ones(nid,1)];
		svhist  = [svhist, model.psicp{sv}];
		svlabel = [svlabel, model.ycp{sv}];
	end
elseif strcmpi(model.type, 'samN')
	% trained by sampledcuts_nslack
	svcoef  = [model.alph; -model.alph];
	svhist  = [model.psigt, model.psicp];
	svlabel = [model.ygt, model.ycp];
else
	error(['wrong model type (did you train with sampledcuts_1slack?)']);
end

tic;
for i = 1:length(vdet)
	if ~mod(i, 42), fprintf('*'); end
	vi = vdet(i);  coeff = [];  theta = [];

	% find relevant SVs
	dv = svlabel(5, :) - vi;
	dv(dv<-pi) = dv(dv<-pi) + 2*pi; % map to (-pi, pi)
	dv(dv> pi) = dv(dv> pi) - 2*pi;
	id = find(abs(dv) < dv_th);
	coeff = svcoef(id) .* (svhist(:,id)'*fdet(:,i));
	theta = dv(id)';

	% constrained optimization on the increment, details left to Matlab
	% restrict refinement in current bin + 2 neighboring bins
	opt = optimset('display', 'off', 'algorithm', 'interior-point');
	vdet(i) = vi + fmincon(@(x) -obj(x, coeff, theta, sigma), 0, [], [], [], [], ...
		-dvp*1, dvp*1, [], opt);

	% not using the unconstrained version, for now
	% vdet(i) = vi + fminunc(@(x) -obj(x, coeff, theta, sigma), 0, opt);
end
toc;

function f = obj(x, coeff, theta, sigma)
f = 0;
for i = 1:length(coeff)
	f = f + coeff(i) * exp(-(theta(i)-x)^2/2/sigma^2);
end

