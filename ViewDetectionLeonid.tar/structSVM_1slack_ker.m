function [model iter] = structSVM_1slack_ker(...
	idtrain, para, MVC_init, MVC_1slack, Verb, modelpath)
%
% kernel structural SVM training with 1-slack cutting plane algorithm.
% Adapted from the implementation by Jianxiong Xiao (http://mit.edu/jxiao/)
% by Kun He, Boston University
%
% xtrain  - training patterns
% ytrain  - training labels
% idtrain - training instances involved
% para    - structure holding parameters
%           para.kfunc: function handle for computing kernels
%
% MVC_init   - function handle for init-ing and returning 1st cutting plane
% MVC_1slack - function handle for finding cutting planes
%   both take a set of instances (idtrain) and return:
%   Y : matrix, each ROW is ybar for a specific instance i in idtrain
%   V : vector, each is the violation amount (Delta+f(ybar)-f(yi)) for i
%   M : vector, each is the margin (loss) value Delta(ybar,yi) for i
%   Fc: matrix, each COLUMN is a violating feature vector \phi(xi, ybar_i)
%   Fg: matrix, each COLUMN is a ground truth feature vector \phi(xi, yi)
%   Yg: matrix, each ROW is a ground truth label yi
%
% PSIgt - ground truth feature vectors \psi(xi, yi)
% Ygt   - ground truth labels yi
% PSIcp - cutting plane feature vectors \psi(xi, yi_bar)
% Ycp   - cutting plane labels yi_bar
% Mcp   - RHS of cutting plane constraints

if nargin < 5, Verb = 1; end
if Verb, fprintf('-------- structSVM_1slack_ker --------\n'); end
global xtrain ytrain
idtrain = reshape(unique(idtrain), 1, []);  % make sure it's row vector
ntrain  = length(idtrain);
PSIgt   = [];
Ygt     = [];
PSIcp   = [];
Ycp     = [];
Mcp     = [];
idle    = [0];
topk    = 3;
nms_th  = 0.3;
maxidle = 5;
TolRel  = 0.001;
TolVio  = 0.01;

t0 = tic;
iter = 0;

%% load existing intermediate model, reconstruct workspace
%{
if ~isempty(modelpath) % load/save intermediate models
	iterfn = sprintf('%s-iter%d.mat', modelpath, iter+1);
	while exist(iterfn, 'file')
		iter = iter + 1
		iterfn = sprintf('%s-iter%d.mat', modelpath, iter+1);
	end
end
%}

%% init
[Y, V, M, Fc, PSIgt, Ygt] = MVC_init(idtrain, para, 1);
if iter > 0
	iterfn = sprintf('%s-iter%d.mat', modelpath, iter)
	load(iterfn);
	alph  = model.alph;
	PSIcp = model.psicp;
	Ycp   = model.ycp;
	H     = [];
	for p = 1:length(model.alph)
		ncp = p;
		H = expand_kernel_matrix(H, ncp, PSIgt, Ygt, PSIcp, Ycp, para);
		figure(25); imagesc(H); drawnow; pause(.1);
	end
	xi = 0;
	v1 = 1;
	disp loaded
else
	PSIcp{1} = Fc;
	Ycp{1}   = Y;
	Mcp(1)   = sum(M);
	ncp      = 1;
	H        = expand_kernel_matrix([], ncp, PSIgt, Ygt, PSIcp, Ycp, para);
	alph     = min(1, para.svm_C);
	v1       = 1;
	xi       = 0;
	model    = struct('ids', idtrain, 'psigt', PSIgt, 'ygt', Ygt)
	[~, model.machine] = system('hostname');
	model.matlabpoolsize = matlabpool('size');
	model.type = '1slack';
end

%% structual SVM, go!
vios = []; xis = [];
terminate = 0;
while iter < para.max_planes
	iter = iter + 1;
	if Verb
		fprintf('\n==== Iteration #%d ====\n', iter); 
		fprintf('%s(N=%d), k=%d, C=%g\n', ...
			para.cls, length(ytrain), length(idtrain), para.svm_C);
	end

	% update model
	svidx       = find(abs(alph/ntrain) > 1e-6);
	model.alph  = alph(svidx)/ntrain; 
	model.psicp = {PSIcp{svidx}};
	model.ycp   = {Ycp{svidx}};
	if ~isempty(modelpath) % save intermediate model
		iterfn = sprintf('%s-iter%d.mat', modelpath, iter)
		model.traintime = toc(t0);
		save(iterfn, 'model', '-v7.3');
		disp saved
	end

	%% [experimental] remove SVs that are inactive for some time
	idle(svidx) = 0;
	inact       = find(~ismember(1:ncp, svidx));
	idle(inact) = idle(inact) + 1;
	active      = find(idle(1:ncp) < maxidle);
	if mod(iter, 3)==0 && length(active) < ncp
		fprintf('remove inactives: %d -> %d\n', ncp, length(active));
		ncp = length(active);
		for i = 1:ncp
			PSIcp{i} = PSIcp{active(i)};
			Ycp{i}   = Ycp{active(i)};
			Mcp(i)   = Mcp(active(i));
			alph(i)  = alph(active(i));
			idle(i)  = idle(active(i));
		end
		H = H(active, active);
	end

	%% go to separation oracle to find MVC
	% MVC_1slack returns K MVCs
	%   topk: how many to return
	% nms_th: NMS threshold
	tic;
	th = nms_th * max(TolVio, min(1, (v1-xi-TolVio)/(v1+xi+TolVio)));
	[Y, V, M, F, ~, ~] = MVC_1slack(idtrain, model, para, topk, th);
	vv = mean(V, 1);
	v1 = max(vv);
	fprintf('\n====> '); toc;

	%% test 
	vios = [vios; vv];
	xis  = [xis;  xi];
	figure(26); clf; plot(vios); hold all; plot(xis); 
	ylim([-.1, min(2, max(max(vios)))]);
	hold off; grid on; drawnow; pause(.2);

	%% 1 slack: add MVCs with vio>xi
	fprintf('\nnSV=%d, MVC vio=%g, xi=%g\n', length(svidx), v1, xi);
	if v1 < xi + TolVio
		terminate = 1;
	end
	for k = find(vv > xi)
		ncp        = ncp + 1;
		PSIcp{ncp} = squeeze(F(:, :, k));
		Ycp{ncp}   = squeeze(Y(:, :, k));
		Mcp(ncp)   = sum(M(:, k));
		H    = expand_kernel_matrix(H, ncp, PSIgt, Ygt, PSIcp, Ycp, para);
		alph = [alph; 0];
		idle = [idle; 0];
		fprintf('added cutting plane: vio=%g\n', vv(k));
	end

	%% solve QP, recover xi
	dH = double(H) / ntrain^2;
	df = double(Mcp(1:ncp)') / ntrain;
	if 0
		% matlab's QP solver may be good enough?
		opts = optimset('algorithm', 'interior-point-convex', 'display', 'off');
		[alph, negdual, flg] = quadprog(dH, -df, ones(1,ncp), para.svm_C, ...
			[], [], zeros(ncp,1), [], zeros(size(alph)), opts);
	else
		% use specialized QP solver
		I = ones(size(alph), 'uint32');
		S = ones(size(alph), 'uint8');
		[alph, ~] = qp(dH, -df, para.svm_C, I, S, zeros(size(alph)), 1e5, 0, TolRel, -inf, 1);
	end
	xi = max(df - dH * alph);
	if Verb > 1
		disp(alph'); sum(alph)
	end

	% check for non-PSDness
	[R, p] = chol(dH);
	if p, disp('Warning: non-PSD gram matrix'); end

	if terminate
		if Verb
			fprintf('vio(%g) < xi(%g)+eps, @ %d iters\n', v1, xi, iter);
		end
		break;
	end
end
t1 = toc(t0);
model.traintime = t1; 
model.niter = iter;
if Verb
	if iter >= para.max_planes
		fprintf('!! structSVM_1slack_ker did not converge\n');
	end
	fprintf('*********************************************\n');
else
	fprintf('>> structSVM_1slack_ker: %d iters in %.2f secs\n', iter, t1);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
function H = expand_kernel_matrix(H, Np, PSIgt, Ygt, PSIcp, Ycp, para)
n = size(H, 1);  assert(n+1 == Np);
% this part for the new padding col/row of H
h = zeros(n, 1);
for cp = 1:n
	h(cp) = sv_inprod(para, ...
		PSIgt, Ygt, ...
		PSIcp{cp}, Ycp{cp}, ...
		PSIcp{Np}, Ycp{Np});
end
% this part for the last entry of new H
hh = sv_inprod(para, ...
	PSIgt, Ygt, ...
	PSIcp{Np}, Ycp{Np}, ...
	PSIcp{Np}, Ycp{Np});
% final assembly
H = [H h; h' hh];
H = (H + H') / 2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
function I = sv_inprod(para, psi_, y_, psi1, y1, psi2, y2)
% psi_, y_ : ground truth features and labels
% psi1, y1 : features & labels from cutting plane, 1st set
% psi2, y2 : features & labels from cutting plane, 2nd set
%   kernel : returns a matrix of kernel values
kernel = para.kfunc;
K1 = kernel(psi_, psi_, y_, y_, para);
K2 = kernel(psi_, psi2, y_, y2, para);
K3 = kernel(psi1, psi_, y1, y_, para);
K4 = kernel(psi1, psi2, y1, y2, para);
K  = K1 - K2 - K3 + K4;
I  = sum(K(:));

