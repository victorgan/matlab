function dict = gen_dict(dictfn, params, ftype, nvw, names, imfns)
% generate visual dictionary for a single feature channel
% ftype: a single feature type
%   nvw: # visual words
% names: sampled training instance names 
% imfns: sampled training image paths
%
%% init
ndata = numel(names);
assert(ndata == numel(imfns));

localdir = params.localdir;
if ~exist(localdir, 'dir') mkdir(localdir); end
step  = params.gridstep;
sizes = params.siftsizes;
MAXFT = max(5e5, nvw*1024);
maxft = ceil(MAXFT / ndata);

%% compute and save features
tic;
datas = [];
parfor i = 1:ndata
	ft   = get_ft(localdir, names{i}, imfns{i}, step, ftype, sizes);
	data = ft.data;
	if size(data, 2) > maxft
		fprintf('Reduced from %d to %d descriptors\n', size(data, 2), maxft);
		idperm = randperm(size(data, 2));
		data   = data(:, idperm(1:maxft));
	end
	datas = [datas data];
end
fprintf('compute features: '); toc;

%% generate several dictionaries and pick best one
dict = dict_BOW(datas, nvw);
save(dictfn, 'dict');
fprintf('dictionary: %s_%d saved to %s\n', ftype, nvw, dictfn);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
function ft = get_ft(localdir, id, fname, step, ftype, sizes)
% load image
I = imread(fname);
[h w ~] = size(I);

% compute and save features
ftfn = sprintf('%s/%s_step%d_%s.mat', localdir, id, step, ftype);
try 
	ld = load(ftfn);
	ft = ld.ft;
catch
	if 1 % VLfeat rules!
		ft = image_feature_vlfeat(I, step, ftype, sizes);
	else
		ft = image_feature_mexopencv(I, step, ftype);
	end
	ft.imhw = [h w];
	save(ftfn, 'ft');
	fprintf('[step%d_%s] %s: ', step, ftype, id);
	system(['chmod o-w ' ftfn]);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
function dict = dict_BOW(datas, nvw)
T     = 6; %max(3, matlabpool('size'));
costs = zeros(T, 1);
dicts = cell(T, 1);
ndata = size(datas, 2);
parfor t = 1:T
%for t = 1:T
	fprintf('Running k-means\n');
	idperm = randperm(ndata);
	ids_t  = idperm(1:round(ndata*.75));
	[dicts{t}, ~, costs(t)] = annkmeans(single(datas(:,ids_t)), nvw, ...
		'MaxNumComparisons', ceil(nvw/4), 'verbose', true);
	%{
	[dicts{t}, ~, costs(t)] = vl_kmeans(single(datas(:,ids_t)), nvw, ...
		'verbose', 'algorithm', 'elkan');
	%}
	%{
	[dicts{t}, ~, costs(t)] = vl_kmeans(single(datas(:,ids_t)), nvw, ...
		'verbose', 'algorithm', 'ann', ...
		'MaxNumIterations', 150, 'MaxNumComparisons', ceil(nvw/4), 'verbose');
	%}
end
[minenergy idx] = min(costs)
dict = dicts{idx};


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
function save_ft(fn, ft)
save(fn, 'ft');
cmd = sprintf('chmod o-w %s', fn);
system(cmd);

