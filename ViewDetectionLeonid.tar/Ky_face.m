function K = Ky_face(y1, y2, invsig2)
% each column is a label
% y1 is 2*1
% y2 is 2*n
dv = abs(repmat(y1, [1 size(y2, 2)]) - y2);
dv = min(dv, 360-dv);
%K  = diag(exp(-0.5 * dv' * sigma.^2 * dv));
K  = exp(diag(-0.5*dv'*invsig2*dv)); % 1*n
