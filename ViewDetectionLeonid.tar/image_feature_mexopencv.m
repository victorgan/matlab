function ft = image_feature_mexopencv(im, step, ftype)
% compute dense features (choices: SIFT, ...)
% note step is fixed at 6
assert(step == 6);
[h w ~] = size(im); 
ft = [];

% fix name
if strcmpi(ftype, 'SIFTc')
	ftype = 'OpponentSIFT';
elseif strcmpi(ftype, 'SIFTg')
	ftype = 'SIFT';
elseif strcmpi(ftype, 'SURF')
	ftype = 'SURF';
else
	error(['unkown feature type ' ftype]);
end

% standard mexopencv procedure
det = cv.FeatureDetector('Dense');
kps = det.detect(im);
ext = cv.DescriptorExtractor(ftype);
des = ext.compute(im, kps);

% post processing
xy = cat(1, kps.pt);
assert(size(xy, 1) == size(des, 1));
xx = xy(:, 1);
yy = xy(:, 2);
id = find(xx & yy);

ft.xy = [xx(id), yy(id)];
ft.data = des(id, :)';
ft.xgrid = unique(ft.xy(:, 1))';
ft.ygrid = unique(ft.xy(:, 2))';


