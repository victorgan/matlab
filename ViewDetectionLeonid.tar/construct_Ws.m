function W = construct_Ws(model, params)
nvp = length(params.viewpoints);

if strcmpi(model.type, '1slack')
	% trained by structSVM_1slack
	nid     = length(model.ids);
	svcoef  = sum(model.alph)*ones(nid, 1);
	svhist  = model.psigt;
	svlabel = model.ygt;
	for sv = 1:length(model.alph)
		a       = model.alph(sv);
		svcoef  = [svcoef; -a*ones(nid,1)];
		svhist  = [svhist, model.psicp{sv}];
		svlabel = [svlabel, model.ycp{sv}];
	end
elseif strcmpi(model.type, 'samN')
	% trained by sampledcuts_nslack
	svcoef  = [model.alph; -model.alph];
	svhist  = [model.psigt, model.psicp];
	svlabel = [model.ygt, model.ycp];
else
	error(['wrong model type (did you train with sampledcuts_1slack?)']);
end
clear model

W = zeros(params.ftdim, nvp);
for v = 1:nvp  % todo is it possible to further vectorize?
	vp = (v-1) * 2*pi/nvp;

	%vgt = model.ygt(5, :);
	%dv  = abs(vp - vgt);
	%dv  = min(dv, 2*pi-dv);
	%agt = model.alph .* exp(-dv.^2/2/params.kyparm^2)';
	%W(:, v) = W(:, v) + model.psigt * agt; 
	
	dv  = abs(vp - svlabel(5, :));
	dv(dv>2*pi) = dv(dv>2*pi) - 2*pi;
	dv  = min(dv, 2*pi-dv);
	% reweight SV by pose kernel
	% todo: call Ky() function to enforce consistency
	newcoef = svcoef .* exp(-0.5*dv.^2/params.kyparm^2)';
	W(:, v) = svhist * newcoef; 

	%vcp = model.ycp(5, :);
	%dv  = abs(vp - vcp);
	%dv  = min(dv, 2*pi-dv);
	%acp = model.alph .* exp(-dv.^2/2/params.kyparm^2)';
	%W(:, v) = W(:, v) - model.psicp * acp; 
end
clear svcoef svhist svlabel

