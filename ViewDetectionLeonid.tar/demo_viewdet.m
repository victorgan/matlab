function [ap, mppe, mae] = demo_viewdet(...
	cls, ftype, nvw, nlv, ...                         % mandatory
	Kx, sig, C, vbin, beta, nsample, namestr, split)  % optional
% 
% function [ap, mppe, mae] = demo_viewdet(...
%   cls, ftype, nvw, nlv, ...                       
%   Kx, sig, C, vbin, beta, nsample, namestr, split)
%
% Performs detection + pose estimation for 'EPFL Cars' or '3D Objects'
%
%  INPUT - example/best-practice values in parentheses
%     cls: class name  ('epflcars' or 'car' or 'bicycle')
%   ftype: feature type, cell  ({'SIFTg'})
%     nvw: # of visual words in BOW  (500)
%     nlv: # of levels in spatial pyramid  (3)
%      Kx: choice of structural kernel  ('nlin2')
%     sig: sigma of pose kernel  (pi/15)
%       C: SVM's C parameter  (750)
%    vbin: # of viewpoint bins  (16)
%    beta: weighting of loc/pose losses  (0.5) 
% nsample: minibatch size in online learning  (48)
% namestr: affects definition of pose loss  ('-newloss')
%   split: train/set split ID  (1)
%
% OUTPUT - names are self-describing
% AP, MPPE, MAE
%
% Kun He, Boston University
% Jul 20, 2014

if nargin < 5, Kx = 'nlin2'; end;
if nargin < 6, sig = pi/15; end;
if nargin < 7, C = 750; end;
if nargin < 8, vbin = 16; end
if nargin < 9, beta = .5; end;
if nargin < 10, nsample = 48; end;
if nargin < 11, namestr = '-newloss'; end
if nargin < 12, split = 1; end

global ids
% struct ids: 
%  inst - object instance numbers
%  name - image names
%  imfn - actual image file paths
% train - list of IDs in training set
%  test - list of IDs in testing set
global xtrain xtest
% struct xtrain:
% inthist - cell array of integral histograms for each ID (loaded on demand)
%    gtft - ground truth feature vectors
global ytrain ytest
% matrix ytrain:
%   ntrain*5 double, each row is a training label

%% set parameters
if strcmpi(Kx, 'nlin'), Kx = 'nlin2'; end
if ~strcmpi('epfl', cls) && ~strcmpi('epflcars', cls)
	% if not EPFL, then it's a class name in 3Dobjects
	vbin = 8;
	params = init_viewdet('3dobj', cls, ...
		ftype, nvw, nlv, Kx, sig, beta, 5, vbin, split);
else
	% EPFL Cars
	cls = 'epflcars';
	params = init_viewdet(cls, cls, ...
		ftype, nvw, nlv, Kx, sig, beta, 3, vbin, split);
end
%params.altname = '';
params.altname = namestr
if strcmp(namestr, '-newloss')
	params.lambda = 2.0;
elseif strcmp(namestr, '-newloss2')
	params.lambda = 4.0;
else
	error('unknown params.altname');
end

%% prepare data, load data
okfn = sprintf('%s/%s_std%d_%s.ok', params.modeldir, cls, params.nstdbb, params.IHstr);
if ~exist(okfn, 'file')
	data_viewdet(params);
	system(sprintf('touch %s\n', okfn), '-echo');
end
if isempty(ids) || isempty(xtrain) || isempty(ytrain)
	disp('loading data');
	[ids, xtrain, ytrain, xtest, ytest] = loaddata_viewdet(cls, params, 0);
end
ntrain = length(ids.train)

%% train model
params.max_planes = 128;
%params.max_planes = 2*ceil(ntrain/nsample);
params.svm_C      = C;
params.sampling   = 'S';
do_sampling = (nsample < ntrain);

modeldir = sprintf('%s/%s_split%d/%s%s/%dbin/', params.modeldir, cls, ...
	params.split, params.IHstr, params.altname, vbin);
modelstr = sprintf('std%d_%d%s_sig%g_a%g_C%g', params.nstdbb, ...
	params.pyralvs, params.Kx, pi/sig, beta, params.svm_C);
if do_sampling, modelstr = [modelstr '_sam' num2str(nsample)]; end
modelfn = [modeldir modelstr '.mat']

if ~exist(modeldir, 'dir')
	mkdir(modeldir); 
end
if ~exist(modelfn, 'file')
	if do_sampling
		% n-slack cutting plane, online version
		model_sum = sampledcuts_npass(ntrain, nsample, params, ...
			@bnbinit_viewdet, @bnb_viewdet, 2, [], 2, modelfn);
	else
		% 1-slack cutting plane, batch version
		[model_sum, ~] = structSVM_1slack_ker(1:ntrain, params, ...
			@bnbinit_viewdet, @bnb_viewdet, 2, modelfn);
	end
	save(modelfn, 'model_sum', '-v7.3');
end

%% test model
[ap, mppe, mae] = test_viewdet(modeldir, modelstr, params);

if 0  % for drawing learning curves
	for iter = 10:10:40  % per 10 iters
		istr = sprintf('%s.mat-iter%d', modelstr, iter)
		[ap(iter), mppe(iter), mae(iter)] = test_viewdet(modeldir, istr, params);
	end
	plot(mae)
	for p = 1:2  % per pass
		pstr = sprintf('%s.mat-pass%d', modelstr, p)
		load(sprintf('%s-pass%d.mat', modelfn, p))
		[ap(p), mppe(p), mae(p)] = test_viewdet(modeldir, pstr, params);
	end
end

%% clean up
ids = [];
xtrain = []; xtest = [];
ytrain = []; ytest = [];


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ap, vp_mppe, vp_mae] = test_viewdet(modeldir, modelstr, params)
global xtrain xtest ytest ids

% load test data
ntest = length(ids.test)
nvp = length(params.viewpoints);

%% do testing
try 
	resfn = sprintf('%s/res_%s.mat', modeldir, modelstr);
	load(resfn);
catch
	% load model
	modelfn = sprintf('%s/%s.mat', modeldir, modelstr);
	ld = load(modelfn);
	%model = ld.model;
	model = ld.model_sum;

	% convert SVs to the format that ess takes
	fprintf('constructing Ws: '); tic;
	W = construct_Ws(model, params); toc;

	% prepare
	bbsize = uint32(xtrain.stdbb);
	step   = uint32(params.gridstep);
	nlv    = uint32(params.pyralvs);
	vps    = (params.viewpoints-1) * 2*pi/nvp;

	% do detection
	disp('doing test-time inference');
	ydet = zeros(5, ntest);
	sdet = zeros(ntest, 1);
	fdet = zeros(params.ftdim, ntest);
	tdet = zeros(1, ntest);
	parfor i = 1 : ntest
		idx = ids.test(i);
		fprintf('\n[%d/%d: %s] ', i, ntest, ids.name{idx});
		ti = tic;
		IHfn = sprintf('%s%s_%s.mat', params.localdir, ids.name{idx}, params.IHstr);
		ld = load(IHfn);
		ihist = double(ld.inthist);
		xx = uint32(ld.xgrid);
		yy = uint32(ld.ygrid);
		wh = uint32(ld.imwh);
		[ydet(:, i), sdet(i), ~, fdet(:, i), ~] = ess_posedet( ...
			params.Kx, ...     % x-kernel type
			ihist, nlv, ...    % integral histogram & num of SP levels
			wh, xx, yy, ...    % image dimensions, feature grid
			bbsize, vps, ...   % bb sizes + vps to search for
			[], W, zeros(nvp, 1));
		tdet(i) = toc(ti);
	end
	fprintf('avg test-time inferece time %f sec\n', mean(tdet));

	% [optional] pose refinement
	vref = [];
	if strcmpi(params.cls, 'epflcars')
		disp('continuously refining pose estimates');
		vref = refine_pose(model, ydet(5, :), fdet, params);
	end

	% save
	save(resfn, 'ydet', 'sdet', 'fdet', 'vref');
end

fprintf('run dbcont to show results\n\n');
%pause
keyboard

% localization
ovth = 0:0.02:1;
[ap, rec, ratio] = report_det(sdet, ydet(1:4, :), ytest(1:4, :), ovth, 33);
idx = find(ratio >= 0.5);
fprintf('%d/%d correct detections\n', length(idx), length(ratio));
title([params.cls '  AP=' num2str(ap*100) '%']);
fprintf('   detection: AP=%g\n', 100*ap);

% pose estimation
% (note: only on correct detections!)
[vp_mppe, vp_mae, vp_medae, vp_cfm, vdif] = report_pose(ydet(5,idx), ytest(5,idx), nvp);
title(['BnB  MPPE=' num2str(vp_mppe) '  MAE=' num2str(vp_mae)]);
fprintf('BnB pose est: MPPE=%g, MAE=%g, medAE=%g, flip=%d\n', ...
	vp_mppe, vp_mae, vp_medae, sum(vdif>150));
figure(34), imagesc(vp_cfm);
set(gcf, 'position', [randi([200 600], 1, 2) 440 330]);

if strcmpi(params.cls, 'epflcars')
	%[vp_mppe, vp_mae, vp_medae, vp_cfm, vdif] = report_pose(vref, ytest(5, :), nvp);
	[vp_mppe, vp_mae, vp_medae, vp_cfm, vdif] = report_pose(vref(idx), ytest(5,idx), nvp);
	title([params.cls '  MAE=' num2str(vp_mae)]);
	fprintf('REF pose est: MPPE=%g, MAE=%g, medAE=%g, flip=%d\n', ...
		vp_mppe, vp_mae, vp_medae, sum(vdif>150));
	figure(35), imagesc(vp_cfm);
	set(gcf, 'position', [randi([200 600], 1, 2) 440 330]);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ap, rec, ratio] = report_det(sdet, bb_det, bb_gt, ovth, figno, color)
% detection evaluation
if nargin < 6, color = 'b'; end
A_int   = diag(rectint(bb2rect(bb_det'), bb2rect(bb_gt')));
A_det   = diag(rectint(bb2rect(bb_det'), bb2rect(bb_det')));
A_gt    = diag(rectint(bb2rect(bb_gt'), bb2rect(bb_gt')));
ratio   = A_int ./ (A_gt+A_det-A_int);
fprintf('min overlap: %g, %d/%d < 0.5\n', min(ratio), sum(ratio<.5), length(ratio));

[val, ind] = sort(-sdet);
fp = ratio < 0.5;
fp = fp(ind); 
tp = 1 - fp;
rec = cumsum(tp) / length(tp);
prec = cumsum(tp) ./ (cumsum(fp)+cumsum(tp));
ap = [];
for t = 0: 0.01: 1
	p = max(prec(rec >= t));
	if isempty(p), p = 0; end
	ap = [ap p];
end
ap = mean(ap);
% precision-recall
figure(figno-1);
plot(rec, prec, '-', 'linewidth', 2); grid on; xlim([0 1]); ylim([0 1.05]);
title(['AP=' num2str(ap)]);

% overlap-recall
rec = zeros(size(ovth));
for t = 1:length(ovth)
	rec(t) = mean(ratio > ovth(t));
end
figure(figno);
plot(ovth, rec, 'color', color, 'linewidth', 2);
%hold on; plot([0.5 0.5], [0 2], '--r', 'linewidth', 2); hold off;
grid on; ylim([0 1.05]); ylabel 'recall'; xlabel 'overlap';
set(gcf, 'position', [randi([200 600], 1, 2) 440 330]);

% -------------------------------------------------------------------

function [mppe, mae, medae, cfm, vdf] = report_pose(vdet, vtest, nvp)
% pose estimation evaluation
dvp  = 2*pi/nvp;
vpr  = mod(round(vdet/dvp), nvp)+1;
vgt  = mod(round(vtest/dvp), nvp)+1;
mppe = (1 - mean((vpr-vgt) ~= 0))*100;
vdf  = abs(vdet-vtest) * 180/pi;
vdf(vdf<0) = vdf(vdf<0) + 360;
vdf(vdf>360) = vdf(vdf>360) - 360;
vdf  = min(vdf, 360-vdf);
mae  = mean(vdf);
medae = median(vdf);
cfm  = confusionmat(vgt, vpr, 'order', 1:nvp);

