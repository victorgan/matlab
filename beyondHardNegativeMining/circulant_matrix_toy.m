
% A small toy example that shows the diagonalization of a block-circulant
% Gram matrix.
%
% Joao F. Henriques, 2013


rng(0);  %random seed
n = 3;  %number of base samples
s = 3;  %number of spatial cells / shifts (1D)

%generate a random set of base samples
base_X = rand(s,n);


%create a data matrix containing all shifts of the base samples
X = [];
for u = 0:s-1,  %iterate shifts
	for i = 1:n,  %iterate base samples
		%add a new shifted sample
		X = [X, circshift(base_X(:,i), [u, 0])];
	end
end
% %visualize X
% figure, imagesc(X), axis equal; axis off;


%compute the corresponding Gram matrix, which should be block-circulant
G = X' * X;

%visualize G
figure, subplot(1,2,1), imagesc(G)
axis square; axis off; title('Block-circulant Gram matrix')


%create the unitary matrix U, which will apply the Discrete Fourier
%Transform to each sample (using a DFT matrix, instantiated with "dftmtx")
U = kron(dftmtx(s), eye(n));

%use U to block-diagonalize G
Gd = U' * G * U;

%visualize Gd
subplot(1,2,2), imagesc(abs(Gd))
axis square; axis off; title('Block-diagonalized Gram matrix')

