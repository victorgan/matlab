
% This script generates plots as in the paper. Note that results are a bit
% improved in this release version. Make sure the evaluation results are
% stored beforehand, by setting 'save_plots = true' in 'set_defaults.m',
% and running 'run_circulant_ethz.m' and 'run_mining_ethz.m'.
%
% Joao F. Henriques, 2013


dataset_name = 'ETHZ';

colors = 0.8 * [1 0 0; 0 0 0];  %color sequence to use
line_width = 2;
rounds = 3;  %plot mining results for this number of rounds


%make sure the needed functions are in the path
addpath('./detection', './evaluation', './training', './utilities', './libraries')

set_defaults;  %set data paths and other parameters
dataset = dataset_init(dataset_name, paths);  %initialize dataset index
figure('Number','off','Name',['Precision vs. Recall for ' dataset_name])

n = numel(dataset.classes);

circulant_aps = zeros(n,1);  %to calculate mean AP over all classes
mining_aps = zeros(n,1);

for i = 1:n,  %iterate object classes
	class = dataset.classes{i};
	
	%create axes in appropriate position of the grid
	axes('OuterPosition', [(i-1)/(n+1), 0, 1/(n+1), 1]);  %#ok<LAXES>
	hold on; title(class)
	
	%load data for circulant and plot it
	load([paths.cache 'plots/circulant_' dataset_name '_' class])
	h = plot(recall, precision, '-', 'Color',colors(1,:), 'LineWidth',line_width);
	circulant_aps(i) = ap;
	
	%same for mining
	load([paths.cache 'plots/mining_' dataset_name '_' class '_' int2str(rounds)])
	plot(recall, precision, '--', 'Color',colors(2,:), 'LineWidth',line_width);
	mining_aps(i) = ap;
	
	xlabel('Precision'); ylabel('Recall');
	grid on; box on;
	
	uistack(h, 'top');  %put the first plot on top
end

%create a single legend in the corner
axes('Position', [0, 0, 1, 1]); hold on;
plot(1:2, 1:2,'-', 'Color',colors(1,:), 'LineWidth', line_width, 'Visible','off');  %dummy plots to set legend appearance
plot(1:2, 1:2,'--', 'Color',colors(2,:), 'LineWidth', line_width, 'Visible','off');
legend({['Circulant (' num2str(mean(circulant_aps), 3) ')'], ...
	['Mining,' char(10) int2str(rounds) ' rounds (' num2str(mean(mining_aps), 3) ')']}, 'Location','SouthEast');
axis off;


% %save figure
% set(gcf, 'Position', [50, 50, 990, 130])
% export_fig([paths.cache 'plots/' dataset_name '.pdf'], '-transparent');

