
% This script downloads and extracts all datasets to the paths specified
% in "set_defaults.m". The downloads may take a while, especially Caltech
% Pedestrians.
%
% Joao F. Henriques


%get user-defined paths
set_defaults;


%Inria Pedestrians
disp('Downloading and extracting Inria Pedestrians...')
untar('http://pascal.inrialpes.fr/data/human/INRIAPerson.tar', paths.inria)
movefile([paths.inria 'INRIAPerson/*'], paths.inria)  %move to specified dir
unused1 = rmdir([paths.inria 'INRIAPerson/']);  %remove original extracted dir only if it's empty


%ETHZ Shapes
disp('Downloading and extracting ETHZ Shapes...')
untar('http://www.vision.ee.ethz.ch/datasets/downloads/ethz_shape_classes_v12.tgz', paths.ethz_shapes)
movefile([paths.ethz_shapes 'ETHZShapeClasses-V1.2/*'], paths.ethz_shapes)  %move to specified dir
unused2 = rmdir([paths.ethz_shapes 'ETHZShapeClasses-V1.2/']);  %remove original extracted dir only if it's empty


%Caltech Pedestrians
base_url = 'http://www.vision.caltech.edu/Image_Datasets/CaltechPedestrians/datasets/USA/';

disp('Downloading and extracting Caltech Pedestrians annotations...')
unzip([base_url 'annotations.zip'], [paths.caltech 'data-USA/'])

for i = 0:10,  %iterate video sets for data-USA
	disp(['Downloading and extracting Caltech Pedestrians set ' int2str(i) '...'])
	untar(sprintf('%sset%02i.tar', base_url, i), [paths.caltech 'data-USA/videos/'])
end

disp('Downloading and extracting Caltech Pedestrians code...')
unzip('http://www.vision.caltech.edu/Image_Datasets/CaltechPedestrians/code/code3.2.0.zip', paths.caltech)


%check if Piotr's Toolbox is installed
if isempty(which('seqIo')),
	disp('Piotr''s Toolbox not found. If you want to run experiments on the Caltech Pedestrians')
	disp('dataset, please install it from: http://vision.ucsd.edu/~pdollar/toolbox/doc/index.html')
end
