
% Main script for the Circulant Decomposition experiment on Caltech
% Pedestrians.
%
% Joao F. Henriques, 2013


%set data paths and other parameters
set_defaults;


%select dataset and object class
dataset_name = 'Caltech';
class = 'person';

circulant = true;  %use circulant decomposition

%cell size for HOG or any other dense feature
cell_size = 4;

%size of object in pixels, as vector [height, width]. objects on this
%dataset have a standardized aspect ratio of 0.41
object_size = 16 * cell_size * [1, 0.41];


%pad samples with extra cells around the object, to minimize wrap-around effects
padding_cells = [3, 2];

%crop these cells from the final template. this may be needed because
%large templates (too much padding) can't detect near image borders.
cropping_cells = 0;  %should be an even number


%negative sampling parameters
sampling.neg_stride = 2/3;  %stride of grid sampling
sampling.neg_image_size = [240, 320];  %max. size of sampled images [rows, columns]


%standard deviation and magnitude of regression targets
target_sigma = 0.8;
target_magnitude = 1;


%train a complex SVR (liblinear) with no bias term
training.type = 'svr';
training.regularization = 1e-3;  %SVR-C, obtained by cross-validation on a log. scale
training.epsilon = 5e-3;
training.complex = true;
training.bias_term = 0;


%make sure the needed functions are in the path
addpath('./detection', './evaluation', './training', './utilities', './libraries')

%main script for training and evaluation
run_circulant;


