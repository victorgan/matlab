function x = get_features(im, features, cell_size)
%GET_FEATURES
%   Extracts dense features from image.
%
%   X = GET_FEATURES(IM, FEATURES, CELL_SIZE)
%   Extracts features specified in struct FEATURES, from image IM. The
%   features should be densely sampled, in cells or intervals of CELL_SIZE.
%   The output has size [height in cells, width in cells, features].
%
%   To specify HOG features, set field 'hog' to true, and
%   'hog_orientations' to the number of bins.
%
%   To experiment with other features simply add them to this function
%   and include any needed parameters in the FEATURES struct.
%
%   Joao F. Henriques, 2013

	%right now, only HOG features (from VLFeat)
	if features.hog,
		x = vl_hog(single(im), cell_size, ...
			'NumOrientations', features.hog_orientations, 'BilinearOrientations');
	end
end

