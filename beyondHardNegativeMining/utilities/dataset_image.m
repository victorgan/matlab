function [boxes, im] = dataset_image(dataset, class, image)
%DATASET_IMAGE
%   Returns an image and bounding boxes for objects in that image.
%
%   [BOXES, IM] = DATASET_IMAGE(DATASET, CLASS, IMAGE)
%   Returns bounding box information, and optionally the image as well,
%   for objects of CLASS in the specified IMAGE (a cell element from the
%   cell array returned by DATASET_LIST).
%
%   Joao F. Henriques, 2013

	switch dataset.format
	case 'ETHZ',
		%load bounding box information from text file
		f = fopen([dataset.data_path image(1:end-4) '_' lower(class) '.groundtruth']);
		
		%if the file can't be opened, the image has no boxes of this class
		if f == -1,
			boxes = [];
		else
			boxes = textscan(f, '%f %f %f %f');
			fclose(f);
			
			boxes = cat(2, boxes{:});  %cell array to matrix
			
			%convert from [x1, y1, x2, y2] format to [x, y, w, h].
			boxes(:, 3:4) = boxes(:, 3:4) - boxes(:, 1:2);
		end
		
		%return image
		if nargout > 1,
			image_path = [dataset.data_path image];
		end
		
	case 'Inria',
		if ~isempty(strfind(image, '/neg/')),
			%easy case, a negative image. no object bounding boxes.
			boxes = [];
		else
			%make necessary changes to the image file name to get to the
			%ground truth text file (remember this is only the partial path)
			filename = strrep(image, '/pos/', '/annotations/');
			filename = strrep(filename, '.png', '.txt');
			filename = strrep(filename, '.jpg', '.txt');

			boxes = [];
			f = fopen([dataset.data_path filename], 'r');

			%find lines specifying bounding boxes
			line = fgets(f);
			while ~isequal(line, -1),
				limits = textscan(line, 'Bounding box for object %*d "PASperson" (Xmin, Ymin) - (Xmax, Ymax) : (%d, %d) - (%d, %d)');

				if ~isempty(limits{1}),
					limits = double([limits{:}]);  %convert to 1x4 vector

					%convert from [x1, y1, x2, y2] format to [x, y, w, h],
					%and from base-0 coordinates to base-1
					boxes = [boxes; limits(1:2) + 1, limits(3:4) - limits(1:2)];  %#ok
				end

				line = fgets(f);
			end

			fclose(f);
		end
		
		%return image
		if nargout > 1,
			image_path = [dataset.data_path image];
		end

	case 'Caltech',
		%"image" is a vector containing indexes: subset (1-train or
		%2-test), set/video/frame (all 1-based), then set/video indexes in
		%the dataset's internal indexing (total Nx6) (for figuring out the
		%file name).

		if image(1) == 1,  %retrive struct from "train" subset
			video_gt = dataset.train_gt{image(2)}{image(3)};
		else  %"test" subset
			video_gt = dataset.test_gt{image(2)}{image(3)};
		end
		
		%the Caltech dataset requires some filtering, others do not
		if any(strcmp(dataset.name, {'Caltech', 'CaltechSubset'})),
			%use Caltech toolbox's criteria to filter "reasonable" boxes.
			test_func = @testGt_reasonable;
		else
			test_func = @testGt_null;
		end

		%return bounding boxes for this frame
		boxes = vbb('frameAnn', video_gt, image(4), dataset.classes, test_func);

		boxes(boxes(:,5) ~= 0, :) = [];  %delete boxes to ignore
		boxes(:,5) = [];  %last column is not used after this

		%return image
		if nargout > 1,
			image_path = sprintf('%s/videos/set%02i/V%03i/I%05i.%s', ...
				dataset.data_path, image(5), image(6), image(4) - 1, dataset.extension);
		end
		
	otherwise
		error(['Unknown dataset: ' dataset.name '.'])
	end
	
	%return image
	if nargout > 1,
		im = imread(image_path);
		if size(im,3) == 1,
			im = repmat(im, [1, 1, 3]);  %from grayscale to RGB
		end
	end

end


%imported and modified from Caltech evaluation toolbox (dbEval.m).
function p = testGt_reasonable(lbl, bb, bbv)
	%used to be:  function p = testGt( lbl, bb, bbv, hs, vs, ar )

	%set parameters for "reasonable" subset:
	hs = [50 inf];
	vs = [.65 inf];
	ar = 0;
	
	%note: "bnds" are ignored.

	p=strcmp(lbl,'person'); % only 'person' objects are considered
	h=bb(4); p=p & (h>=hs(1) & h<hs(2)); % height must be in range
	if(all(bbv==0)), v=inf; else v=bbv(3).*bbv(4)./(bb(3)*bb(4)); end
	p=p & v>=vs(1) & v<=vs(2); % fraction visible area in range
	if(ar~=0), p=p & sign(ar)*abs(bb(3)./bb(4)-.41)<ar; end % ar test
% 	p = p & bb(1)>=bnds(1) & (bb(1)+bb(3)<=bnds(3));
% 	p = p & bb(2)>=bnds(2) & (bb(2)+bb(4)<=bnds(4));
end

%similar to the above, but always returns true. used for datasets other
%than Caltech (in the same format), where we shouldn't filter any boxes.
function p = testGt_null(lbl, bb, bbv)  %#ok
	p = true(size(bb,1), 1);
end

