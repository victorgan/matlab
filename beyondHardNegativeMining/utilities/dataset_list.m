function images = dataset_list(dataset, subset, class, positives)
%DATASET_LIST
%   Lists images from a dataset.
%
%   IMAGES = DATASET_LIST(DATASET, SUBSET)
%   Returns a cell array where each element represents an image of DATASET
%   (loaded with DATASET_INIT). Each one can be passed to DATASET_IMAGE to
%   get the image and ground truth information. The SUBSET can be either
%   'train' or 'test'.
%
%   IMAGES = DATASET_LIST(DATASET, SUBSET, CLASS)
%   Restricts images to those that contain objects of CLASS.
%
%   IMAGES = DATASET_LIST(DATASET, SUBSET, CLASS, POSITIVES)
%   With POSITIVES = false, restricts images to those that do not contain
%   objects of CLASS.
%
%   Joao F. Henriques

	if nargin < 3,  %default is to load all images ("class" is empty)
		class = [];
	end
	if nargin < 4,  %default is images of specified class, not negatives
		positives = true;
	end
	
	assert(isempty(class) || any(strcmp(class, dataset.classes)), ['Unknown class:' class '.'])
	assert(any(strcmp(subset, {'train', 'test'})), ['Unknown subset:' subset '.'])

	switch dataset.format
	case 'ETHZ',
		if ~positives && ~isempty(class),
			%return images from all classes except the one specified.
			%recursively call self for each individual class
			n = numel(dataset.classes);
			images = cell(1,n);
			for c = find(~strcmp(class, dataset.classes(:)')),
				images{c} = dataset_list(dataset, subset, dataset.classes{c});
			end
			images = [images{:}];  %concatenate into single cell array
		
		elseif isempty(class),
			%return images from all classes
			n = numel(dataset.classes);
			images = cell(1,n);
			for c = 1:n,
				images{c} = dataset_list(dataset, subset, dataset.classes{c});
			end
			images = [images{:}];  %concatenate into single cell array
			
		else  %a single class
			
			%list all image files
			images = dir([dataset.data_path class '/*.jpg']);
			images = {images.name};
			images = sort(images);  %make sure we always use the same order

			%split the dataset in half, determined by the train/test proportion
			last_train = floor(numel(images) * dataset.train_images_proportion);

			%return the chosen subset
			switch subset
			case 'train',
				images = images(1:last_train);
			case 'test',
				images = images(last_train+1:end);
			end
			
			%append folder (class name) to each image
			images = cellfun(@(s) [class '/' s], images, 'Uniform',false);
		end
		
	case 'Inria',
		if strcmp(subset, 'test'),  %select subset
			folder = 'Test';
		else
			folder = 'Train';
		end
		images = {};
		
		if isempty(class) || positives,  %add positives to list
			f = fopen([dataset.data_path folder '/pos.lst']);
			lines = textscan(f, '%s', 'Delimiter','\n');
			fclose(f);
			images = [images; lines{1}];
		end
		
		if isempty(class) || ~positives,  %add negatives to list
			f = fopen([dataset.data_path folder '/neg.lst']);
			lines = textscan(f, '%s', 'Delimiter','\n');
			fclose(f);
			images = [images; lines{1}];
		end
		
	case 'Caltech',
		
		skip = dataset.skip;  %only use one of every M frames
		
		%use specified subset of GT
		switch subset
		case 'train',
			gt = dataset.train_gt;
			set_ids = dataset.train_set_ids;
			vid_ids = dataset.train_vid_ids;
			subset_idx = 1;
		case 'test',
			gt = dataset.test_gt;
			set_ids = dataset.test_set_ids;
			vid_ids = dataset.test_vid_ids;
			subset_idx = 2;
		end
		
		sets = cell(numel(gt), 1);
		for i = 1:numel(gt),  %iterate sets

			videos = cell(numel(gt{i}), 1);

			for j = 1:numel(gt{i}),  %iterate videos
				%frame indexes, 1-based and taking skip into account
				frames = skip : skip : gt{i}{j}.nFrame;
				
				%if class is empty, we want every image.
				if ~isempty(class),
					%otherwhise, return either only negative images, or
					%only non-negative images
					if ~positives,
						frames(~gt{i}{j}.is_negative) = [];
					else
						frames(gt{i}{j}.is_negative) = [];
					end
				end
				
				%one line per frame, containing indexes: subset (1-train
				%or 2-test), set/video/frame (all 1-based), then set/video
				%indexes in the dataset's internal indexing (total Nx6)
				idx = repmat([subset_idx, i, j, 0, set_ids(i), vid_ids{i}(j)], numel(frames), 1);
				if ~isempty(idx),
					idx(:,4) = frames;
				end

				%store Nx1 cell array of 1x6 vectors (one cell per frame)
				videos{j} = num2cell(idx, 2);
			end

			sets{i} = videos;  %contents of set
		end

		%flatten the structure to get a single cell array of frames
		images = cat(1, sets{:});
		images = cat(1, images{:});
		
	otherwise
		error(['Unknown dataset: ' dataset_name '.'])
	end
end

