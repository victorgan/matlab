function dataset = dataset_init(dataset_name, paths)
%DATASET_INIT
%   Initializes DATASET struct with information from specified dataset.
%
%   DATASET = DATASET_INIT(DATASET_NAME, PATHS)
%   Loads information from specified DATASET_NAME, which currently can be
%   'ETHZ', 'Inria' or 'Caltech'. They will be stored in the DATASET
%   struct, which can be used with the DATASET_LIST and DATASET_IMAGE
%   functions.
%
%   Dataset paths must be set in the PATHS struct, in fields 'ethz_shapes',
%   'inria' and 'caltech'. The folder where cache files are stored must be
%   set in the 'cache' field.
%
%   Joao F. Henriques, 2013

	persistent last_dataset  %to speed-up loading on some datasets (e.g. Caltech)

	switch dataset_name
	case 'ETHZ',  %info for the ETHZ-Shapes dataset
		dataset = [];
		dataset.format = 'ETHZ';
		dataset.data_path = paths.ethz_shapes;
		dataset.classes = {'Swans', 'Applelogos', 'Bottles', 'Giraffes', 'Mugs'};
		dataset.train_images_proportion = 0.5;  %split train and test evenly

	case 'Inria',  %Inria Pedestrians
		dataset = [];
		dataset.format = 'Inria';
		dataset.data_path = paths.inria;
		dataset.classes = {'person'};  %only one class

	case {'Caltech', 'CaltechSubset'},
		%Caltech Pedestrians, and other datasets in the same format

		%if it's the same dataset as before, re-use it
		if ~isempty(last_dataset) && isequal(last_dataset.name, dataset_name),
			dataset = last_dataset;
		else
			%load from scratch
			dataset = [];
			dataset.format = 'Caltech';
			dataset.classes = {'person'};  %only one class

			addpath(paths.caltech);  %where the annotation functions ("vbb") are located
			
			%names of training and test subsets, and file extensions
			switch dataset_name
			case 'Caltech',
				subset_names = {'usatrain', 'usatest'};
				dataset.extension = 'jpg';
			case 'CaltechSubset',  %only 1 train set and 1 test set
				subset_names = {'usa01', 'usa07'};
				dataset.extension = 'jpg';
			end

			%load annotation data for all videos of both train and test sets

			%needed to load ground-truth
			test_func = @(lbl, bb, bbv) strcmp('person', lbl);

			for subset = 1:2,  %first the training set, then the test set
				[data_path, setIds, vidIds, skip, minHt] = dbInfo(subset_names{subset});

				gt = cell(numel(vidIds), 1);  %bounding box data for each video

				for i = 1:numel(setIds),  %iterate sets
					gt{i} = cell(numel(vidIds{i}), 1);

					for j = 1:numel(vidIds{i}),  %iterate videos
						%extract images from SEQ file if needed
						filename = sprintf('%s/videos/set%02i/V%03i', data_path, setIds(i), vidIds{i}(j));
						if isempty(dir([filename '/I*'])),  %no images found, convert
							disp(['Extracting "' filename '.seq"... (done only once)'])
							seqIo([filename '.seq'], 'toImgs', filename, skip, skip - 1);
						end

						%load ground truth data
						filename = sprintf('%s/annotations/set%02i/V%03i', data_path, setIds(i), vidIds{i}(j));
						video_gt = vbb('vbbLoad', filename);

						%find out which frames don't contain any people; this
						%is the negative set
						frames = skip : skip : video_gt.nFrame;  %1-based indexes
						is_negative = false(numel(frames),1);
						for f = 1:numel(frames),
							%bounding boxes for this frame
							boxes = vbb('frameAnn', video_gt, frames(f), dataset.classes, test_func);

							%it's a negative image if there are no objects, or
							%all have the "ignore" flag
							is_negative(f) = all(boxes(:,5) ~= 0);
						end

						video_gt.is_negative = is_negative;
						gt{i}{j} = video_gt;  %store it
					end
				end

				if subset == 1,  %save train and test info separately
					dataset.train_gt = gt;  %annotation data
					dataset.train_set_ids = setIds;  %set indexes
					dataset.train_vid_ids = vidIds;  %video indexes
				else
					dataset.test_gt = gt;
					dataset.test_set_ids = setIds;
					dataset.test_vid_ids = vidIds;
				end
			end

			%remember data path and frame skip (note: should be the same for
			%both training and test)
			dataset.data_path = data_path;
			dataset.skip = skip;
		end

	otherwise
		error(['Unknown dataset: ' dataset_name '.'])
	end

	%to make the name more easily accessible
	dataset.name = dataset_name;

	%this is just to ensure the path ends with the directory separator
	%(note that '/' works on all systems, including Windows).
	if isfield(dataset, 'data_path') && all(dataset.data_path(end) ~= '/\'),
		dataset.data_path(end+1) = '/';
	end
	
	%cache the struct in a persistent variable. note that due to Matlab's
	%copy-on-write semantics, the data is not duplicated in memory.
	if any(strcmp(dataset_name, {'Caltech', 'CaltechSubset'})),
		last_dataset = dataset;
	end
end
