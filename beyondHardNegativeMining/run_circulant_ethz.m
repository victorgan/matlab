
% Main script for the Circulant Decomposition experiment on ETHZ Shapes.
%
% Joao F. Henriques, 2013


%set data paths and other parameters
set_defaults;


%select dataset
dataset_name = 'ETHZ';

circulant = true;  %use circulant decomposition

%cell size for HOG or any other dense feature
cell_size = 8;

%size of object in pixels (width or height, whichever is largest)
object_size = 16 * cell_size;


%pad samples with extra cells around the object, to minimize wrap-around effects
padding_cells = 3;

%crop these cells from the final template. this may be needed because
%large templates (too much padding) can't detect near image borders.
cropping_cells = 0;  %should be an even number


%negative sampling parameters
sampling.neg_stride = 2/3;  %stride of grid sampling
sampling.neg_image_size = [480, 640];  %max. size of sampled images [rows, columns]
sampling.flip_positives = false;  %ETHZ-Shapes objects are not symmetric


%standard deviation and magnitude of regression targets
target_sigma = 0.5;
target_magnitude = 1;


%train a complex SVR (liblinear) with no bias term
training.type = 'svr';
training.regularization = 1e-2;  %SVR-C, obtained by cross-validation on a log. scale
training.epsilon = 1e-2;
training.complex = true;
training.bias_term = 0;


%make sure the needed functions are in the path
addpath('./detection', './evaluation', './training', './utilities', './libraries')

%run main script (training and evaluation) for each class
dataset = dataset_init(dataset_name, paths);  %to get list of classes
for class_idx = 1:numel(dataset.classes),
	class = dataset.classes{class_idx};
	run_circulant;
end

