
% Main script for the Hard Negative Mining experiment on Caltech
% Pedestrians.
%
% Joao F. Henriques, 2013


%set data paths and other parameters
set_defaults;


%evaluate detector after each round of mining
eval_all_rounds = true;


%select dataset and object class
dataset_name = 'Caltech';
class = 'person';

circulant = false;  %use hard negative mining

%cell size for HOG or any other dense feature
cell_size = 4;

%size of object in pixels, as vector [height, width]. objects on this
%dataset have a standardized aspect ratio of 0.41
object_size = 16 * cell_size * [1, 0.41];

%pad samples with few extra cells around the object, provides some context
padding_cells = [3, 2];


%number of initial random samples to extract in each negative image
sampling.neg_samples_per_image = 10;

%number of hard negative mining rounds, and threshold
hard_neg_rounds = 3;
mining_threshold = 0.5;


%train a standard SVM (liblinear) with bias term
training.type = 'svm';
training.regularization = 1e-2;  %SVM-C, obtained by cross-validation of initial detector on a log. scale
training.complex = false;
training.bias_term = 1;


%make sure the needed functions are in the path
addpath('./detection', './evaluation', './training', './utilities', './libraries')

%main script for training and evaluation
run_mining;

