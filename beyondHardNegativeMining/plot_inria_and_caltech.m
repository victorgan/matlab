
% This script generates plots as in the paper. Note that results are a bit
% improved in this release version. Make sure the evaluation results are
% stored beforehand, by setting 'save_plots = true' in 'set_defaults.m',
% and running 'run_circulant_*.m' and 'run_mining_*.m' for the selected
% dataset (which you can choose below).
%
% Joao F. Henriques, 2013


%choose dataset to plot
% dataset_name = 'Inria';
dataset_name = 'Caltech';

class = 'person';

colors = 0.8 * [1 0 0; 0 0 1; 0 1 1; 0 1 0; 1 0 1];  %color sequence to use
line_width = 2;


%make sure the needed functions are in the path
addpath('./detection', './evaluation', './training', './utilities', './libraries')

set_defaults;  %set data paths and other parameters
figure('Number','off','Name',['Precision vs. Recall for ' dataset_name]), hold on
strs = {};

%load data, plot it and store legend string (including AP)
load([paths.cache 'plots/circulant_' dataset_name '_' class])
h = plot(recall, precision, '-', 'Color',colors(1,:), 'LineWidth',line_width);
strs{1} = ['Circulant (' num2str(ap, 3) ')'];

%same for different rounds of mining
for i = 0:3,
	load([paths.cache 'plots/mining_' dataset_name '_' class '_' int2str(i)])
	plot(recall, precision, '--', 'Color',colors(i+2,:), 'LineWidth',line_width);
	strs{i+2} = ['Mining, ' int2str(i) ' rounds (' num2str(ap, 3) ')'];  %#ok<SAGROW>
end

%finish the plot
xlabel('Precision'); ylabel('Recall');
grid on; box on;
if strcmp(dataset_name, 'Inria'),  %more convenient legend location for each plot
	legend(strs, 'Location','SouthWest');
else
	legend(strs, 'Location','NorthEast');
end

uistack(h, 'top');  %put the first plot on top



% %save figure
% set(gcf, 'Position', [50, 50, 490, 340])
% export_fig([paths.cache 'plots/' dataset_name '.pdf'], '-transparent');


