
% Main script for the Hard Negative Mining experiment on ETHZ Shapes.
%
% Joao F. Henriques, 2013


%set data paths and other parameters
set_defaults;


%evaluate detector after each round of mining
eval_all_rounds = false;


%select dataset
dataset_name = 'ETHZ';

circulant = false;  %use hard negative mining

%cell size for HOG or any other dense feature
cell_size = 8;

%size of object in pixels (width or height, whichever is largest)
object_size = 16 * cell_size;

%pad samples with few extra cells around the object, provides some context
padding_cells = 3;


%number of initial random samples to extract in each negative image
sampling.neg_samples_per_image = 10;
sampling.flip_positives = false;  %ETHZ-Shapes objects are not symmetric


%note that the ETHZ-Shapes dataset is small, and so the PR curves are very
%jagged. for this reason, the AP of some classes may decrease temporarily
%during mining (though after some rounds, the AP increases on average).

%number of hard negative mining rounds, and threshold
hard_neg_rounds = 3;
mining_threshold = -0.5;


%train a standard SVM (liblinear) with bias term
training.type = 'svm';
training.regularization = 1e-1;  %SVM-C, obtained by cross-validation of initial detector on a log. scale
training.complex = false;
training.bias_term = 1;


%make sure the needed functions are in the path
addpath('./detection', './evaluation', './training', './utilities', './libraries')

%run main script (training and evaluation) for each class
dataset = dataset_init(dataset_name, paths);  %to get list of classes
for class_idx = 1:numel(dataset.classes),
	class = dataset.classes{class_idx};
	run_mining;
end

