
                         Beyond Hard Negative Mining:
            Efficient Detector Learning via Block-Circulant Decomposition

               J. F. Henriques   J. Carreira   R. Caseiro   J. Batista
                                  ICCV 2013

_______________
Project webpage: http://www.isr.uc.pt/~henriques/beyond

This code implements fast single-template detector learning with the Circulant
Decomposition, as well as the Hard Negative Mining baseline. It allows reproduction
of the experiments in the paper, and visualization of the detection results.

__________
Quickstart

1. Extract code.

2. Edit the paths in 'set_defaults.m' to your liking:
   a. 'paths.cache': Where the sample cache will be stored.
   b. 'paths.ethz_shapes|inria|caltech': Where each dataset will be located.

3. If you don't have the datasets already, run 'download_datasets.m' (may take
   some time).

4. Run any of the 'run_(method)_(dataset).m' scripts to train and test a detector
   on each dataset using one of the methods (circulant or mining).


Note. For Caltech Pedestrians you need Piotr's Toolbox, which you can get from:
http://vision.ucsd.edu/~pdollar/toolbox/doc/index.html

If you don't have the MATLAB Parallel Computing Toolbox, you can switch to a
single-threaded implementation by setting 'parallel = false' in 'set_defaults.m'.

_______
Results

In the release version, results are improved slightly from the paper. AP's are
summarized in the following table:

         Mining (3 rounds)  Circulant
 Inria         80.3            82.2
Caltech        37.4            38.2

For the Circulant Decomposition, AP improvement is mostly due to additional
padding, which alleviates wrap-around effects. Smaller differences due to cleaning
up the code for release were not deemed statistically significant.

_______________
Technical notes

The crux of the Circulant Decomposition algorithm is in 'training/run_circulant.m'.
It follows closely the code in the paper (Alg. 1), but also includes a parallelized
version, which is a bit more complex.


Features extracted from samples are cached in MAT files, but will be refreshed
automatically if any relevant parameters change.


An interface to visualize the detections is created in each run (can be turned off
in 'set_defaults.m'). The scrollbar at the bottom allows you to navigate images.
True detections are green, false detections are red, with opacity indicating the
detection score. Ground truth boxes are black.

Keyboard shortcuts:
   Right/left arrow keys - advance/go back one image.
   Page down/page up - advance/go back 30 images.
   Home/end - go to first/last image.
   Enter (Return) - play/pause video.
   Backspace - play/pause video 5 times slower.


To generate plots like the ones in the paper, do the following:
1. Create results files for a dataset by running 'run_circulant_(dataset).m' and
   'run_mining_(dataset).m'.
2. Run 'plot_ethz.m' for ETHZ Shapes, or 'plot_inria_and_caltech.m' for the others
   (you can change 'dataset_name' to either 'Inria' or 'Caltech').


Generic functions, which may be useful elsewhere:
'linear_training.m' implements Ridge Regression, SVM and SVR (using liblinear) for
both real and complex data.
'dataset_init|list|image.m' abstracts away the details of dealing with different
detection datasets using a simple interface.
'progress.m' is a text progress bar that can be used inside a 'parfor'.
'videofig.m' creates an interactive figure that has video-like controls.


Naming conventions, mostly borrowed from The Mathworks:
1. SZ always denotes an array's dimensions, as returned by Matlab's size() function.
2. IDX is always an index or list of indexes.
3. Common abbreviations: POS for positive (e.g. samples), NEG for negative.

_____________________________________
Copyright (c) 2013, Joao F. Henriques

Permission to use, copy, modify, and distribute this software for research
purposes with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


